# Notices for Eclipse opGUI

This content is produced and maintained by the Eclipse openpass project.

 * Project home: https://projects.eclipse.org/projects/automotive.openpass

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/openpass/opGUI

## Third-party Content

emotion/react (11.10.5)
 * License: MIT
 * Source: https://github.com/emotion-js/emotion/tree/main/packages/react

emotion/styled (11.10.5)
 * License: MIT 
 * Source: https://github.com/emotion-js/emotion/tree/main/packages/styled 

mui/icons-material (5.11.0)
 * License: MIT  
 * Source: https://github.com/mui/material-ui/

mui/lab (5.0.0-alpha.153)
 * License: MIT
 * Source: https://github.com/mui/material-ui/ 

mui/material (5.11.5)
 * License: MIT 
 * Source: https://github.com/mui/material-ui/

react (18.2.0)
 * License: MIT 
 * Source: https://github.com/facebook/react/  

react-dom (18.2.0)
 * License: MIT 
 * Source: https://github.com/facebook/react/ 

react-folder-tree (5.1.1)
 * License: MIT 
 * Source: https://github.com/shunjizhan/react-folder-tree/ 

react-router-dom (6.11.2)
 * License: MIT
 * Source: https://github.com/remix-run/react-router/ 

react-xarrows (2.0.2)
 * License: MIT 
 * Source: https://github.com/Eliav2/react-xarrows  
 
react-scripts/create-react-app (5.0.1)
 * License: MIT
 * Source: https://github.com/facebook/create-react-app/

babel/plugin-transform-react-jsx (7.22.5)
 * License: MIT
 * Source: https://github.com/babel/babel/

babel/preset-react (7.18.6)
 * License: MIT
 * Source: https://github.com/babel/babel/

babel/plugin-transform-runtime (7.21.4)
 * License: MIT
 * Source: https://github.com/babel/babel/

babel/preset-env (7.21.5)
 * License: MIT
 * Source: https://github.com/babel/babel/

babel/preset-typescript (7.21.5)
 * License: MIT
 * Source: https://github.com/babel/babel/

testing-library/jest-dom (5.14.1)
 * License: MIT
 * Source: https://github.com/testing-library/jest-dom/

testing-library/react (14.0.0)
 * License: MIT
 * Source: https://github.com/testing-library/react-testing-library/

testing-library/user-event (13.2.1)
 * License: MIT
 * Source: https://github.com/testing-library/user-event/

types/react (18.0.37)
 * License: MIT 
 * Source: https://github.com/DefinitelyTyped/DefinitelyTyped/

types/react-dom (18.0.11)
 * License: MIT
 * Source: https://github.com/DefinitelyTyped/DefinitelyTyped/

typescript-eslint/eslint-plugin (5.59.0)
 * License: MIT
 * Source: https://github.com/typescript-eslint/typescript-eslint/

typescript-eslint/parser (5.59.0)
 * License: MIT
 * Source: https://github.com/eslint/typescript-eslint-parser

vitejs/plugin-react (4.0.0)
 * License: MIT
 * Source: https://github.com/vitejs/vite-plugin-react/

eslint (8.38.0)
 * License: MIT
 * Source: https://github.com/eslint/eslint/

eslint-plugin-react-hooks (4.6.0)
 * License: MIT
 * Source: https://github.com/facebook/react/

eslint-plugin-react-refresh (0.3.4)
 * License: MIT
 * Source: https://github.com/ArnaudBarre/eslint-plugin-react-refresh/

jest (29.5.0)
 * License: MIT
 * Source: https://github.com/jestjs/jest/

jest-environment-jsdom (29.5.0)
 * License: MIT
 * Source: https://github.com/jestjs/jest/

jest-fetch-mock (3.0.3)
 * License: MIT
 * Source: https://github.com/jefflau/jest-fetch-mock/

vite (4.3.9)
 * License: MIT
 * Source: https://github.com/vitejs/vite/

vitejs/plugin-react (4.0.0)
 * License: MIT
 * Source: https://github.com/vitejs/vite-plugin-react/

qhttp-engine (1.0.1)
 * License: MIT
 * Source: https://github.com/nitroshare/qhttpengine/

sqlite (3.39.2)
 * License: SQLite Blessing
 * Source: https://github.com/sqlite/sqlite/

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
