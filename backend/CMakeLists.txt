cmake_minimum_required(VERSION 3.5)
project(OPGUICore CXX)

include(CheckIncludeFileCXX)

check_include_file_cxx(any HAS_ANY)
check_include_file_cxx(string_view HAS_STRING_VIEW)
check_include_file_cxx(coroutine HAS_COROUTINE)

set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(APPLE)
    set(CMAKE_PREFIX_PATH /opt/homebrew/Cellar/qt@5/5.15.13_1)
endif()

if(WITH_TESTS)
  enable_testing()
  add_subdirectory(test)
endif()


find_package(Qt5 COMPONENTS Core REQUIRED)
find_package(Qt5 COMPONENTS Concurrent REQUIRED)
find_package(Qt5 COMPONENTS Xml REQUIRED)
find_package(Qt5 COMPONENTS Network REQUIRED)
find_package(Qt5 COMPONENTS Sql REQUIRED)

set(CMAKE_AUTOMOC ON)

set(CMAKE_VERBOSE_MAKEFILE ON)

add_executable(${PROJECT_NAME} main.cc)

target_link_libraries(
      ${PROJECT_NAME} 
      PRIVATE 
        Qt5::Concurrent
        Qt5::Core
        Qt5::Xml
        Qt5::Network
        Qt5::Sql
)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/qhttpengine_export.h.in 
              "${CMAKE_CURRENT_BINARY_DIR}/qhttpengine_export.h"
)

file(GLOB_RECURSE ALL_FILES
        # OPGUI CORE FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/core/common/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/pcmSimulation/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/systemEditor/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/fileSystemManager/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/logger/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/globalSetupEditor/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/configEditor/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/configEditor/simulationConfig/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/simulationExecution/*.cpp
        
        # OPEN API GENERATED FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src/models/*.cpp 
        ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src/requests/*.cpp 
        ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src/handlers/*.cpp 

        # OPENAPI ROUTER FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/router/*.cpp

        # QHTTP ENGINE FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/src/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/src/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/include/qhttpengine/*.h

        # PCM SIMULATION
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/PCM_Manager/*.cpp 
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/PCM_Manager/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/DataStructuresXml/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/DataStructuresXosc/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/PCM_Data/*.cpp
	      
)

target_include_directories(
        ${PROJECT_NAME}
        PUBLIC
          # CORE
          ${CMAKE_CURRENT_SOURCE_DIR}/core/common
          ${CMAKE_CURRENT_SOURCE_DIR}/core/pcmSimulation
          ${CMAKE_CURRENT_SOURCE_DIR}/core/systemEditor
          ${CMAKE_CURRENT_SOURCE_DIR}/core/fileSystemManager
          ${CMAKE_CURRENT_SOURCE_DIR}/core/logger
          ${CMAKE_CURRENT_SOURCE_DIR}/core/globalSetupEditor
          ${CMAKE_CURRENT_SOURCE_DIR}/core/configEditor
          ${CMAKE_CURRENT_SOURCE_DIR}/core/configEditor/simulationConfig
          ${CMAKE_CURRENT_SOURCE_DIR}/core/simulationExecution

          # ROUTER
          ${CMAKE_CURRENT_SOURCE_DIR}/router
          
          # QHTTPENGINE
          ${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/include
          ${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/include/qhttpengine
          ${CMAKE_CURRENT_SOURCE_DIR}/thirdParty/qhttpengine/src/src
          
          # OPEN API GENERATED
          ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src
          ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src/models
          ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src/requests
          ${CMAKE_CURRENT_SOURCE_DIR}/OAIgenerated/src/handlers
          
          # CMAKE GENERATED
          ${CMAKE_CURRENT_BINARY_DIR}

          # LEGACY PCM SIMULATION
          ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/PCM_Manager
          ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/DataStructuresXml
          ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/DataStructuresXosc
          ${CMAKE_CURRENT_SOURCE_DIR}/plugins/pcmSimulation/PCM_Data
          
          # QT INCLUDES
          ${Qt5Xml_INCLUDE_DIRS}
          ${Qt5Sql_INCLUDE_DIRS}
          ${Qt5Network_INCLUDE_DIRS}
          ${Qt5Core_INCLUDE_DIRS}
)


target_sources(
    ${PROJECT_NAME}
    PRIVATE
      ${ALL_FILES}
)

if(NOT WITH_DOC)
  set(WITH_DOC OFF)
endif()


if (WITH_DOC)
  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/doc)
endif()

               



