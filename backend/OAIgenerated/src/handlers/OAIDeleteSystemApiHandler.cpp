/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIDeleteSystemApiHandler.h"
#include "OAIDeleteSystemApiRequest.h"

namespace OpenAPI {

OAIDeleteSystemApiHandler::OAIDeleteSystemApiHandler(){

}

OAIDeleteSystemApiHandler::~OAIDeleteSystemApiHandler(){

}

void OAIDeleteSystemApiHandler::apiDeleteSystemDelete(QString path) {
    Q_UNUSED(path);
    auto reqObj = qobject_cast<OAIDeleteSystemApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAIDefault200Response res;
        reqObj->apiDeleteSystemDeleteResponse(res);
    }
}


}
