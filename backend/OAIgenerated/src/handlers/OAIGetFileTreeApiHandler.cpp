/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIGetFileTreeApiHandler.h"
#include "OAIGetFileTreeApiRequest.h"

namespace OpenAPI {

OAIGetFileTreeApiHandler::OAIGetFileTreeApiHandler(){

}

OAIGetFileTreeApiHandler::~OAIGetFileTreeApiHandler(){

}

void OAIGetFileTreeApiHandler::apiGetFileTreePost(OAIFileTreeRequest oai_file_tree_request) {
    Q_UNUSED(oai_file_tree_request);
    auto reqObj = qobject_cast<OAIGetFileTreeApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAIFileTreeExtended res;
        reqObj->apiGetFileTreePostResponse(res);
    }
}


}
