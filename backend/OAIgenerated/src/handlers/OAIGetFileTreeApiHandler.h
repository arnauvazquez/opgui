/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetFileTreeApiHandler_H
#define OAI_OAIGetFileTreeApiHandler_H

#include <QObject>

#include "OAIError400.h"
#include "OAIError500.h"
#include "OAIFileTreeExtended.h"
#include "OAIFileTreeRequest.h"
#include <QString>

namespace OpenAPI {

class OAIGetFileTreeApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIGetFileTreeApiHandler();
    virtual ~OAIGetFileTreeApiHandler();


public Q_SLOTS:
    virtual void apiGetFileTreePost(OAIFileTreeRequest oai_file_tree_request);
    

};

}

#endif // OAI_OAIGetFileTreeApiHandler_H
