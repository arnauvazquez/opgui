/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetSystemApiHandler_H
#define OAI_OAIGetSystemApiHandler_H

#include <QObject>

#include "OAIError400.h"
#include "OAIError500.h"
#include "OAISystemUI.h"
#include <QString>

namespace OpenAPI {

class OAIGetSystemApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIGetSystemApiHandler();
    virtual ~OAIGetSystemApiHandler();


public Q_SLOTS:
    virtual void apiGetSystemGet(QString path);
    

};

}

#endif // OAI_OAIGetSystemApiHandler_H
