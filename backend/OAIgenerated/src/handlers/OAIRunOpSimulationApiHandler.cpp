/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIRunOpSimulationApiHandler.h"
#include "OAIRunOpSimulationApiRequest.h"

namespace OpenAPI {

OAIRunOpSimulationApiHandler::OAIRunOpSimulationApiHandler(){

}

OAIRunOpSimulationApiHandler::~OAIRunOpSimulationApiHandler(){

}

void OAIRunOpSimulationApiHandler::apiRunOpSimulationGet(QString config_path) {
    Q_UNUSED(config_path);
    auto reqObj = qobject_cast<OAIRunOpSimulationApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAIDefault200Response res;
        reqObj->apiRunOpSimulationGetResponse(res);
    }
}


}
