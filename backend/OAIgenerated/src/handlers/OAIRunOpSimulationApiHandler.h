/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIRunOpSimulationApiHandler_H
#define OAI_OAIRunOpSimulationApiHandler_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include <QString>

namespace OpenAPI {

class OAIRunOpSimulationApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIRunOpSimulationApiHandler();
    virtual ~OAIRunOpSimulationApiHandler();


public Q_SLOTS:
    virtual void apiRunOpSimulationGet(QString config_path);
    

};

}

#endif // OAI_OAIRunOpSimulationApiHandler_H
