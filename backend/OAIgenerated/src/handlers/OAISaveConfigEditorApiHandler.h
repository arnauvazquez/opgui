/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISaveConfigEditorApiHandler_H
#define OAI_OAISaveConfigEditorApiHandler_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_configEditor_post_request.h"
#include <QString>

namespace OpenAPI {

class OAISaveConfigEditorApiHandler : public QObject
{
    Q_OBJECT

public:
    OAISaveConfigEditorApiHandler();
    virtual ~OAISaveConfigEditorApiHandler();


public Q_SLOTS:
    virtual void apiConfigEditorPost(OAI_api_configEditor_post_request oai_api_config_editor_post_request);
    

};

}

#endif // OAI_OAISaveConfigEditorApiHandler_H
