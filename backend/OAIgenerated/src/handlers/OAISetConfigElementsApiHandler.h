/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISetConfigElementsApiHandler_H
#define OAI_OAISetConfigElementsApiHandler_H

#include <QObject>

#include "OAIConfigElement.h"
#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include <QList>
#include <QString>

namespace OpenAPI {

class OAISetConfigElementsApiHandler : public QObject
{
    Q_OBJECT

public:
    OAISetConfigElementsApiHandler();
    virtual ~OAISetConfigElementsApiHandler();


public Q_SLOTS:
    virtual void apiConfigElementsPost(QList<OAIConfigElement> oai_config_element);
    

};

}

#endif // OAI_OAISetConfigElementsApiHandler_H
