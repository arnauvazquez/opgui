/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIConfigEditorObject.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIConfigEditorObject::OAIConfigEditorObject(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIConfigEditorObject::OAIConfigEditorObject() {
    this->initializeModel();
}

OAIConfigEditorObject::~OAIConfigEditorObject() {}

void OAIConfigEditorObject::initializeModel() {

    m_scenario_isSet = false;
    m_scenario_isValid = false;

    m_scenery_config_isSet = false;
    m_scenery_config_isValid = false;

    m_system_config_isSet = false;
    m_system_config_isValid = false;

    m_vehicles_catalog_isSet = false;
    m_vehicles_catalog_isValid = false;

    m_pedestrians_catalog_isSet = false;
    m_pedestrians_catalog_isValid = false;

    m_profiles_catalog_isSet = false;
    m_profiles_catalog_isValid = false;

    m_simulation_config_isSet = false;
    m_simulation_config_isValid = false;
}

void OAIConfigEditorObject::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIConfigEditorObject::fromJsonObject(QJsonObject json) {

    m_scenario_isValid = ::OpenAPI::fromJsonValue(scenario, json[QString("scenario")]);
    m_scenario_isSet = !json[QString("scenario")].isNull() && m_scenario_isValid;

    m_scenery_config_isValid = ::OpenAPI::fromJsonValue(scenery_config, json[QString("sceneryConfig")]);
    m_scenery_config_isSet = !json[QString("sceneryConfig")].isNull() && m_scenery_config_isValid;

    m_system_config_isValid = ::OpenAPI::fromJsonValue(system_config, json[QString("systemConfig")]);
    m_system_config_isSet = !json[QString("systemConfig")].isNull() && m_system_config_isValid;

    m_vehicles_catalog_isValid = ::OpenAPI::fromJsonValue(vehicles_catalog, json[QString("vehiclesCatalog")]);
    m_vehicles_catalog_isSet = !json[QString("vehiclesCatalog")].isNull() && m_vehicles_catalog_isValid;

    m_pedestrians_catalog_isValid = ::OpenAPI::fromJsonValue(pedestrians_catalog, json[QString("pedestriansCatalog")]);
    m_pedestrians_catalog_isSet = !json[QString("pedestriansCatalog")].isNull() && m_pedestrians_catalog_isValid;

    m_profiles_catalog_isValid = ::OpenAPI::fromJsonValue(profiles_catalog, json[QString("profilesCatalog")]);
    m_profiles_catalog_isSet = !json[QString("profilesCatalog")].isNull() && m_profiles_catalog_isValid;

    m_simulation_config_isValid = ::OpenAPI::fromJsonValue(simulation_config, json[QString("simulationConfig")]);
    m_simulation_config_isSet = !json[QString("simulationConfig")].isNull() && m_simulation_config_isValid;
}

QString OAIConfigEditorObject::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIConfigEditorObject::asJsonObject() const {
    QJsonObject obj;
    if (m_scenario_isSet) {
        obj.insert(QString("scenario"), ::OpenAPI::toJsonValue(scenario));
    }
    if (m_scenery_config_isSet) {
        obj.insert(QString("sceneryConfig"), ::OpenAPI::toJsonValue(scenery_config));
    }
    if (m_system_config_isSet) {
        obj.insert(QString("systemConfig"), ::OpenAPI::toJsonValue(system_config));
    }
    if (m_vehicles_catalog_isSet) {
        obj.insert(QString("vehiclesCatalog"), ::OpenAPI::toJsonValue(vehicles_catalog));
    }
    if (m_pedestrians_catalog_isSet) {
        obj.insert(QString("pedestriansCatalog"), ::OpenAPI::toJsonValue(pedestrians_catalog));
    }
    if (m_profiles_catalog_isSet) {
        obj.insert(QString("profilesCatalog"), ::OpenAPI::toJsonValue(profiles_catalog));
    }
    if (simulation_config.isSet()) {
        obj.insert(QString("simulationConfig"), ::OpenAPI::toJsonValue(simulation_config));
    }
    return obj;
}

QString OAIConfigEditorObject::getScenario() const {
    return scenario;
}
void OAIConfigEditorObject::setScenario(const QString &scenario) {
    this->scenario = scenario;
    this->m_scenario_isSet = true;
}

bool OAIConfigEditorObject::is_scenario_Set() const{
    return m_scenario_isSet;
}

bool OAIConfigEditorObject::is_scenario_Valid() const{
    return m_scenario_isValid;
}

QString OAIConfigEditorObject::getSceneryConfig() const {
    return scenery_config;
}
void OAIConfigEditorObject::setSceneryConfig(const QString &scenery_config) {
    this->scenery_config = scenery_config;
    this->m_scenery_config_isSet = true;
}

bool OAIConfigEditorObject::is_scenery_config_Set() const{
    return m_scenery_config_isSet;
}

bool OAIConfigEditorObject::is_scenery_config_Valid() const{
    return m_scenery_config_isValid;
}

QString OAIConfigEditorObject::getSystemConfig() const {
    return system_config;
}
void OAIConfigEditorObject::setSystemConfig(const QString &system_config) {
    this->system_config = system_config;
    this->m_system_config_isSet = true;
}

bool OAIConfigEditorObject::is_system_config_Set() const{
    return m_system_config_isSet;
}

bool OAIConfigEditorObject::is_system_config_Valid() const{
    return m_system_config_isValid;
}

QString OAIConfigEditorObject::getVehiclesCatalog() const {
    return vehicles_catalog;
}
void OAIConfigEditorObject::setVehiclesCatalog(const QString &vehicles_catalog) {
    this->vehicles_catalog = vehicles_catalog;
    this->m_vehicles_catalog_isSet = true;
}

bool OAIConfigEditorObject::is_vehicles_catalog_Set() const{
    return m_vehicles_catalog_isSet;
}

bool OAIConfigEditorObject::is_vehicles_catalog_Valid() const{
    return m_vehicles_catalog_isValid;
}

QString OAIConfigEditorObject::getPedestriansCatalog() const {
    return pedestrians_catalog;
}
void OAIConfigEditorObject::setPedestriansCatalog(const QString &pedestrians_catalog) {
    this->pedestrians_catalog = pedestrians_catalog;
    this->m_pedestrians_catalog_isSet = true;
}

bool OAIConfigEditorObject::is_pedestrians_catalog_Set() const{
    return m_pedestrians_catalog_isSet;
}

bool OAIConfigEditorObject::is_pedestrians_catalog_Valid() const{
    return m_pedestrians_catalog_isValid;
}

QString OAIConfigEditorObject::getProfilesCatalog() const {
    return profiles_catalog;
}
void OAIConfigEditorObject::setProfilesCatalog(const QString &profiles_catalog) {
    this->profiles_catalog = profiles_catalog;
    this->m_profiles_catalog_isSet = true;
}

bool OAIConfigEditorObject::is_profiles_catalog_Set() const{
    return m_profiles_catalog_isSet;
}

bool OAIConfigEditorObject::is_profiles_catalog_Valid() const{
    return m_profiles_catalog_isValid;
}

OAISimulationConfigFile OAIConfigEditorObject::getSimulationConfig() const {
    return simulation_config;
}
void OAIConfigEditorObject::setSimulationConfig(const OAISimulationConfigFile &simulation_config) {
    this->simulation_config = simulation_config;
    this->m_simulation_config_isSet = true;
}

bool OAIConfigEditorObject::is_simulation_config_Set() const{
    return m_simulation_config_isSet;
}

bool OAIConfigEditorObject::is_simulation_config_Valid() const{
    return m_simulation_config_isValid;
}

bool OAIConfigEditorObject::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_scenario_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_scenery_config_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_system_config_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_vehicles_catalog_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_pedestrians_catalog_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_profiles_catalog_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (simulation_config.isSet()) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIConfigEditorObject::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_simulation_config_isValid && true;
}

} // namespace OpenAPI
