/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIConfigEditorObject.h
 *
 * 
 */

#ifndef OAIConfigEditorObject_H
#define OAIConfigEditorObject_H

#include <QJsonObject>

#include "OAISimulationConfigFile.h"
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIConfigEditorObject : public OAIObject {
public:
    OAIConfigEditorObject();
    OAIConfigEditorObject(QString json);
    ~OAIConfigEditorObject() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getScenario() const;
    void setScenario(const QString &scenario);
    bool is_scenario_Set() const;
    bool is_scenario_Valid() const;

    QString getSceneryConfig() const;
    void setSceneryConfig(const QString &scenery_config);
    bool is_scenery_config_Set() const;
    bool is_scenery_config_Valid() const;

    QString getSystemConfig() const;
    void setSystemConfig(const QString &system_config);
    bool is_system_config_Set() const;
    bool is_system_config_Valid() const;

    QString getVehiclesCatalog() const;
    void setVehiclesCatalog(const QString &vehicles_catalog);
    bool is_vehicles_catalog_Set() const;
    bool is_vehicles_catalog_Valid() const;

    QString getPedestriansCatalog() const;
    void setPedestriansCatalog(const QString &pedestrians_catalog);
    bool is_pedestrians_catalog_Set() const;
    bool is_pedestrians_catalog_Valid() const;

    QString getProfilesCatalog() const;
    void setProfilesCatalog(const QString &profiles_catalog);
    bool is_profiles_catalog_Set() const;
    bool is_profiles_catalog_Valid() const;

    OAISimulationConfigFile getSimulationConfig() const;
    void setSimulationConfig(const OAISimulationConfigFile &simulation_config);
    bool is_simulation_config_Set() const;
    bool is_simulation_config_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString scenario;
    bool m_scenario_isSet;
    bool m_scenario_isValid;

    QString scenery_config;
    bool m_scenery_config_isSet;
    bool m_scenery_config_isValid;

    QString system_config;
    bool m_system_config_isSet;
    bool m_system_config_isValid;

    QString vehicles_catalog;
    bool m_vehicles_catalog_isSet;
    bool m_vehicles_catalog_isValid;

    QString pedestrians_catalog;
    bool m_pedestrians_catalog_isSet;
    bool m_pedestrians_catalog_isValid;

    QString profiles_catalog;
    bool m_profiles_catalog_isSet;
    bool m_profiles_catalog_isValid;

    OAISimulationConfigFile simulation_config;
    bool m_simulation_config_isSet;
    bool m_simulation_config_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIConfigEditorObject)

#endif // OAIConfigEditorObject_H
