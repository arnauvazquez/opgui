/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIConfigElement.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIConfigElement::OAIConfigElement(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIConfigElement::OAIConfigElement() {
    this->initializeModel();
}

OAIConfigElement::~OAIConfigElement() {}

void OAIConfigElement::initializeModel() {

    m_id_isSet = false;
    m_id_isValid = false;

    m_value_isSet = false;
    m_value_isValid = false;

    m_type_isSet = false;
    m_type_isValid = false;
}

void OAIConfigElement::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIConfigElement::fromJsonObject(QJsonObject json) {

    m_id_isValid = ::OpenAPI::fromJsonValue(id, json[QString("id")]);
    m_id_isSet = !json[QString("id")].isNull() && m_id_isValid;

    m_value_isValid = ::OpenAPI::fromJsonValue(value, json[QString("value")]);
    m_value_isSet = !json[QString("value")].isNull() && m_value_isValid;

    m_type_isValid = ::OpenAPI::fromJsonValue(type, json[QString("type")]);
    m_type_isSet = !json[QString("type")].isNull() && m_type_isValid;
}

QString OAIConfigElement::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIConfigElement::asJsonObject() const {
    QJsonObject obj;
    if (m_id_isSet) {
        obj.insert(QString("id"), ::OpenAPI::toJsonValue(id));
    }
    if (m_value_isSet) {
        obj.insert(QString("value"), ::OpenAPI::toJsonValue(value));
    }
    if (m_type_isSet) {
        obj.insert(QString("type"), ::OpenAPI::toJsonValue(type));
    }
    return obj;
}

QString OAIConfigElement::getId() const {
    return id;
}
void OAIConfigElement::setId(const QString &id) {
    this->id = id;
    this->m_id_isSet = true;
}

bool OAIConfigElement::is_id_Set() const{
    return m_id_isSet;
}

bool OAIConfigElement::is_id_Valid() const{
    return m_id_isValid;
}

QString OAIConfigElement::getValue() const {
    return value;
}
void OAIConfigElement::setValue(const QString &value) {
    this->value = value;
    this->m_value_isSet = true;
}

bool OAIConfigElement::is_value_Set() const{
    return m_value_isSet;
}

bool OAIConfigElement::is_value_Valid() const{
    return m_value_isValid;
}

QString OAIConfigElement::getType() const {
    return type;
}
void OAIConfigElement::setType(const QString &type) {
    this->type = type;
    this->m_type_isSet = true;
}

bool OAIConfigElement::is_type_Set() const{
    return m_type_isSet;
}

bool OAIConfigElement::is_type_Valid() const{
    return m_type_isValid;
}

bool OAIConfigElement::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_id_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_value_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_type_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIConfigElement::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_id_isValid && m_value_isValid && m_type_isValid && true;
}

} // namespace OpenAPI
