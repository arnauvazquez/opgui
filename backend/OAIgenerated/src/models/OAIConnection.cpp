/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIConnection.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIConnection::OAIConnection(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIConnection::OAIConnection() {
    this->initializeModel();
}

OAIConnection::~OAIConnection() {}

void OAIConnection::initializeModel() {

    m_id_isSet = false;
    m_id_isValid = false;

    m_source_isSet = false;
    m_source_isValid = false;

    m_targets_isSet = false;
    m_targets_isValid = false;
}

void OAIConnection::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIConnection::fromJsonObject(QJsonObject json) {

    m_id_isValid = ::OpenAPI::fromJsonValue(id, json[QString("id")]);
    m_id_isSet = !json[QString("id")].isNull() && m_id_isValid;

    m_source_isValid = ::OpenAPI::fromJsonValue(source, json[QString("source")]);
    m_source_isSet = !json[QString("source")].isNull() && m_source_isValid;

    m_targets_isValid = ::OpenAPI::fromJsonValue(targets, json[QString("targets")]);
    m_targets_isSet = !json[QString("targets")].isNull() && m_targets_isValid;
}

QString OAIConnection::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIConnection::asJsonObject() const {
    QJsonObject obj;
    if (m_id_isSet) {
        obj.insert(QString("id"), ::OpenAPI::toJsonValue(id));
    }
    if (source.isSet()) {
        obj.insert(QString("source"), ::OpenAPI::toJsonValue(source));
    }
    if (targets.size() > 0) {
        obj.insert(QString("targets"), ::OpenAPI::toJsonValue(targets));
    }
    return obj;
}

qint32 OAIConnection::getId() const {
    return id;
}
void OAIConnection::setId(const qint32 &id) {
    this->id = id;
    this->m_id_isSet = true;
}

bool OAIConnection::is_id_Set() const{
    return m_id_isSet;
}

bool OAIConnection::is_id_Valid() const{
    return m_id_isValid;
}

OAISource OAIConnection::getSource() const {
    return source;
}
void OAIConnection::setSource(const OAISource &source) {
    this->source = source;
    this->m_source_isSet = true;
}

bool OAIConnection::is_source_Set() const{
    return m_source_isSet;
}

bool OAIConnection::is_source_Valid() const{
    return m_source_isValid;
}

QList<OAITarget> OAIConnection::getTargets() const {
    return targets;
}
void OAIConnection::setTargets(const QList<OAITarget> &targets) {
    this->targets = targets;
    this->m_targets_isSet = true;
}

bool OAIConnection::is_targets_Set() const{
    return m_targets_isSet;
}

bool OAIConnection::is_targets_Valid() const{
    return m_targets_isValid;
}

bool OAIConnection::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_id_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (source.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (targets.size() > 0) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIConnection::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_id_isValid && m_source_isValid && m_targets_isValid && true;
}

} // namespace OpenAPI
