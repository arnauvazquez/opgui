/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIEntityRepository.h
 *
 * Entity repository configuration. This object is required, but its fields can be empty strings.
 */

#ifndef OAIEntityRepository_H
#define OAIEntityRepository_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIEntityRepository : public OAIObject {
public:
    OAIEntityRepository();
    OAIEntityRepository(QString json);
    ~OAIEntityRepository() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getFileNamePrefix() const;
    void setFileNamePrefix(const QString &file_name_prefix);
    bool is_file_name_prefix_Set() const;
    bool is_file_name_prefix_Valid() const;

    QString getWritePersistentEntities() const;
    void setWritePersistentEntities(const QString &write_persistent_entities);
    bool is_write_persistent_entities_Set() const;
    bool is_write_persistent_entities_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString file_name_prefix;
    bool m_file_name_prefix_isSet;
    bool m_file_name_prefix_isValid;

    QString write_persistent_entities;
    bool m_write_persistent_entities_isSet;
    bool m_write_persistent_entities_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIEntityRepository)

#endif // OAIEntityRepository_H
