/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIExperiment.h
 *
 * 
 */

#ifndef OAIExperiment_H
#define OAIExperiment_H

#include <QJsonObject>

#include "OAIExperiment_libraries.h"

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIExperiment : public OAIObject {
public:
    OAIExperiment();
    OAIExperiment(QString json);
    ~OAIExperiment() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    qint32 getExperimentId() const;
    void setExperimentId(const qint32 &experiment_id);
    bool is_experiment_id_Set() const;
    bool is_experiment_id_Valid() const;

    qint32 getNumberOfInvocations() const;
    void setNumberOfInvocations(const qint32 &number_of_invocations);
    bool is_number_of_invocations_Set() const;
    bool is_number_of_invocations_Valid() const;

    qint32 getRandomSeed() const;
    void setRandomSeed(const qint32 &random_seed);
    bool is_random_seed_Set() const;
    bool is_random_seed_Valid() const;

    OAIExperiment_libraries getLibraries() const;
    void setLibraries(const OAIExperiment_libraries &libraries);
    bool is_libraries_Set() const;
    bool is_libraries_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    qint32 experiment_id;
    bool m_experiment_id_isSet;
    bool m_experiment_id_isValid;

    qint32 number_of_invocations;
    bool m_number_of_invocations_isSet;
    bool m_number_of_invocations_isValid;

    qint32 random_seed;
    bool m_random_seed_isSet;
    bool m_random_seed_isValid;

    OAIExperiment_libraries libraries;
    bool m_libraries_isSet;
    bool m_libraries_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIExperiment)

#endif // OAIExperiment_H
