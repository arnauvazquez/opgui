/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIExportedSimConfigSettings.h
 *
 * 
 */

#ifndef OAIExportedSimConfigSettings_H
#define OAIExportedSimConfigSettings_H

#include <QJsonObject>

#include <QList>
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIExportedSimConfigSettings : public OAIObject {
public:
    OAIExportedSimConfigSettings();
    OAIExportedSimConfigSettings(QString json);
    ~OAIExportedSimConfigSettings() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getPath() const;
    void setPath(const QString &path);
    bool is_path_Set() const;
    bool is_path_Valid() const;

    QList<QString> getSelectedExperiments() const;
    void setSelectedExperiments(const QList<QString> &selected_experiments);
    bool is_selected_experiments_Set() const;
    bool is_selected_experiments_Valid() const;

    QString getXmlContent() const;
    void setXmlContent(const QString &xml_content);
    bool is_xml_content_Set() const;
    bool is_xml_content_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString path;
    bool m_path_isSet;
    bool m_path_isValid;

    QList<QString> selected_experiments;
    bool m_selected_experiments_isSet;
    bool m_selected_experiments_isValid;

    QString xml_content;
    bool m_xml_content_isSet;
    bool m_xml_content_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIExportedSimConfigSettings)

#endif // OAIExportedSimConfigSettings_H
