/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIFileTreeExtended.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIFileTreeExtended::OAIFileTreeExtended(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIFileTreeExtended::OAIFileTreeExtended() {
    this->initializeModel();
}

OAIFileTreeExtended::~OAIFileTreeExtended() {}

void OAIFileTreeExtended::initializeModel() {

    m_base_path_isSet = false;
    m_base_path_isValid = false;

    m_file_system_tree_isSet = false;
    m_file_system_tree_isValid = false;
}

void OAIFileTreeExtended::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIFileTreeExtended::fromJsonObject(QJsonObject json) {

    m_base_path_isValid = ::OpenAPI::fromJsonValue(base_path, json[QString("basePath")]);
    m_base_path_isSet = !json[QString("basePath")].isNull() && m_base_path_isValid;

    m_file_system_tree_isValid = ::OpenAPI::fromJsonValue(file_system_tree, json[QString("fileSystemTree")]);
    m_file_system_tree_isSet = !json[QString("fileSystemTree")].isNull() && m_file_system_tree_isValid;
}

QString OAIFileTreeExtended::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIFileTreeExtended::asJsonObject() const {
    QJsonObject obj;
    if (m_base_path_isSet) {
        obj.insert(QString("basePath"), ::OpenAPI::toJsonValue(base_path));
    }
    if (file_system_tree.isSet()) {
        obj.insert(QString("fileSystemTree"), ::OpenAPI::toJsonValue(file_system_tree));
    }
    return obj;
}

QString OAIFileTreeExtended::getBasePath() const {
    return base_path;
}
void OAIFileTreeExtended::setBasePath(const QString &base_path) {
    this->base_path = base_path;
    this->m_base_path_isSet = true;
}

bool OAIFileTreeExtended::is_base_path_Set() const{
    return m_base_path_isSet;
}

bool OAIFileTreeExtended::is_base_path_Valid() const{
    return m_base_path_isValid;
}

OAITreeNode OAIFileTreeExtended::getFileSystemTree() const {
    return file_system_tree;
}
void OAIFileTreeExtended::setFileSystemTree(const OAITreeNode &file_system_tree) {
    this->file_system_tree = file_system_tree;
    this->m_file_system_tree_isSet = true;
}

bool OAIFileTreeExtended::is_file_system_tree_Set() const{
    return m_file_system_tree_isSet;
}

bool OAIFileTreeExtended::is_file_system_tree_Valid() const{
    return m_file_system_tree_isValid;
}

bool OAIFileTreeExtended::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_base_path_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (file_system_tree.isSet()) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIFileTreeExtended::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_base_path_isValid && m_file_system_tree_isValid && true;
}

} // namespace OpenAPI
