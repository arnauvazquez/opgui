/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIFileTreeRequest.h
 *
 * 
 */

#ifndef OAIFileTreeRequest_H
#define OAIFileTreeRequest_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIFileTreeRequest : public OAIObject {
public:
    OAIFileTreeRequest();
    OAIFileTreeRequest(QString json);
    ~OAIFileTreeRequest() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getBasePathGeneric() const;
    void setBasePathGeneric(const QString &base_path_generic);
    bool is_base_path_generic_Set() const;
    bool is_base_path_generic_Valid() const;

    bool isIsFolder() const;
    void setIsFolder(const bool &is_folder);
    bool is_is_folder_Set() const;
    bool is_is_folder_Valid() const;

    QString getExtension() const;
    void setExtension(const QString &extension);
    bool is_extension_Set() const;
    bool is_extension_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString base_path_generic;
    bool m_base_path_generic_isSet;
    bool m_base_path_generic_isValid;

    bool is_folder;
    bool m_is_folder_isSet;
    bool m_is_folder_isValid;

    QString extension;
    bool m_extension_isSet;
    bool m_extension_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIFileTreeRequest)

#endif // OAIFileTreeRequest_H
