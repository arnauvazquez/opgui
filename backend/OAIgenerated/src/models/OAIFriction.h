/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIFriction.h
 *
 * 
 */

#ifndef OAIFriction_H
#define OAIFriction_H

#include <QJsonObject>


#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIFriction : public OAIObject {
public:
    OAIFriction();
    OAIFriction(QString json);
    ~OAIFriction() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    float getProbability() const;
    void setProbability(const float &probability);
    bool is_probability_Set() const;
    bool is_probability_Valid() const;

    float getValue() const;
    void setValue(const float &value);
    bool is_value_Set() const;
    bool is_value_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    float probability;
    bool m_probability_isSet;
    bool m_probability_isValid;

    float value;
    bool m_value_isSet;
    bool m_value_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIFriction)

#endif // OAIFriction_H
