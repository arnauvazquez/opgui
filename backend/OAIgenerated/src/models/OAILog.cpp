/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAILog.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAILog::OAILog(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAILog::OAILog() {
    this->initializeModel();
}

OAILog::~OAILog() {}

void OAILog::initializeModel() {

    m_logging_cycles_to_csv_isSet = false;
    m_logging_cycles_to_csv_isValid = false;

    m_file_name_isSet = false;
    m_file_name_isValid = false;

    m_logging_groups_isSet = false;
    m_logging_groups_isValid = false;
}

void OAILog::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAILog::fromJsonObject(QJsonObject json) {

    m_logging_cycles_to_csv_isValid = ::OpenAPI::fromJsonValue(logging_cycles_to_csv, json[QString("loggingCyclesToCsv")]);
    m_logging_cycles_to_csv_isSet = !json[QString("loggingCyclesToCsv")].isNull() && m_logging_cycles_to_csv_isValid;

    m_file_name_isValid = ::OpenAPI::fromJsonValue(file_name, json[QString("fileName")]);
    m_file_name_isSet = !json[QString("fileName")].isNull() && m_file_name_isValid;

    m_logging_groups_isValid = ::OpenAPI::fromJsonValue(logging_groups, json[QString("loggingGroups")]);
    m_logging_groups_isSet = !json[QString("loggingGroups")].isNull() && m_logging_groups_isValid;
}

QString OAILog::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAILog::asJsonObject() const {
    QJsonObject obj;
    if (m_logging_cycles_to_csv_isSet) {
        obj.insert(QString("loggingCyclesToCsv"), ::OpenAPI::toJsonValue(logging_cycles_to_csv));
    }
    if (m_file_name_isSet) {
        obj.insert(QString("fileName"), ::OpenAPI::toJsonValue(file_name));
    }
    if (logging_groups.size() > 0) {
        obj.insert(QString("loggingGroups"), ::OpenAPI::toJsonValue(logging_groups));
    }
    return obj;
}

bool OAILog::isLoggingCyclesToCsv() const {
    return logging_cycles_to_csv;
}
void OAILog::setLoggingCyclesToCsv(const bool &logging_cycles_to_csv) {
    this->logging_cycles_to_csv = logging_cycles_to_csv;
    this->m_logging_cycles_to_csv_isSet = true;
}

bool OAILog::is_logging_cycles_to_csv_Set() const{
    return m_logging_cycles_to_csv_isSet;
}

bool OAILog::is_logging_cycles_to_csv_Valid() const{
    return m_logging_cycles_to_csv_isValid;
}

QString OAILog::getFileName() const {
    return file_name;
}
void OAILog::setFileName(const QString &file_name) {
    this->file_name = file_name;
    this->m_file_name_isSet = true;
}

bool OAILog::is_file_name_Set() const{
    return m_file_name_isSet;
}

bool OAILog::is_file_name_Valid() const{
    return m_file_name_isValid;
}

QList<OAILoggingGroup> OAILog::getLoggingGroups() const {
    return logging_groups;
}
void OAILog::setLoggingGroups(const QList<OAILoggingGroup> &logging_groups) {
    this->logging_groups = logging_groups;
    this->m_logging_groups_isSet = true;
}

bool OAILog::is_logging_groups_Set() const{
    return m_logging_groups_isSet;
}

bool OAILog::is_logging_groups_Valid() const{
    return m_logging_groups_isValid;
}

bool OAILog::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_logging_cycles_to_csv_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_file_name_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (logging_groups.size() > 0) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAILog::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_logging_cycles_to_csv_isValid && m_file_name_isValid && m_logging_groups_isValid && true;
}

} // namespace OpenAPI
