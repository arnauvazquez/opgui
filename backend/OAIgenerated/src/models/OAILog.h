/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAILog.h
 *
 * Logging configuration.
 */

#ifndef OAILog_H
#define OAILog_H

#include <QJsonObject>

#include "OAILoggingGroup.h"
#include <QList>
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAILog : public OAIObject {
public:
    OAILog();
    OAILog(QString json);
    ~OAILog() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    bool isLoggingCyclesToCsv() const;
    void setLoggingCyclesToCsv(const bool &logging_cycles_to_csv);
    bool is_logging_cycles_to_csv_Set() const;
    bool is_logging_cycles_to_csv_Valid() const;

    QString getFileName() const;
    void setFileName(const QString &file_name);
    bool is_file_name_Set() const;
    bool is_file_name_Valid() const;

    QList<OAILoggingGroup> getLoggingGroups() const;
    void setLoggingGroups(const QList<OAILoggingGroup> &logging_groups);
    bool is_logging_groups_Set() const;
    bool is_logging_groups_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    bool logging_cycles_to_csv;
    bool m_logging_cycles_to_csv_isSet;
    bool m_logging_cycles_to_csv_isValid;

    QString file_name;
    bool m_file_name_isSet;
    bool m_file_name_isValid;

    QList<OAILoggingGroup> logging_groups;
    bool m_logging_groups_isSet;
    bool m_logging_groups_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAILog)

#endif // OAILog_H
