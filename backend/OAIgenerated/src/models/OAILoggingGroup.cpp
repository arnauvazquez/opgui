/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAILoggingGroup.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAILoggingGroup::OAILoggingGroup(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAILoggingGroup::OAILoggingGroup() {
    this->initializeModel();
}

OAILoggingGroup::~OAILoggingGroup() {}

void OAILoggingGroup::initializeModel() {

    m_enabled_isSet = false;
    m_enabled_isValid = false;

    m_group_isSet = false;
    m_group_isValid = false;

    m_values_isSet = false;
    m_values_isValid = false;
}

void OAILoggingGroup::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAILoggingGroup::fromJsonObject(QJsonObject json) {

    m_enabled_isValid = ::OpenAPI::fromJsonValue(enabled, json[QString("enabled")]);
    m_enabled_isSet = !json[QString("enabled")].isNull() && m_enabled_isValid;

    m_group_isValid = ::OpenAPI::fromJsonValue(group, json[QString("group")]);
    m_group_isSet = !json[QString("group")].isNull() && m_group_isValid;

    m_values_isValid = ::OpenAPI::fromJsonValue(values, json[QString("values")]);
    m_values_isSet = !json[QString("values")].isNull() && m_values_isValid;
}

QString OAILoggingGroup::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAILoggingGroup::asJsonObject() const {
    QJsonObject obj;
    if (m_enabled_isSet) {
        obj.insert(QString("enabled"), ::OpenAPI::toJsonValue(enabled));
    }
    if (m_group_isSet) {
        obj.insert(QString("group"), ::OpenAPI::toJsonValue(group));
    }
    if (m_values_isSet) {
        obj.insert(QString("values"), ::OpenAPI::toJsonValue(values));
    }
    return obj;
}

bool OAILoggingGroup::isEnabled() const {
    return enabled;
}
void OAILoggingGroup::setEnabled(const bool &enabled) {
    this->enabled = enabled;
    this->m_enabled_isSet = true;
}

bool OAILoggingGroup::is_enabled_Set() const{
    return m_enabled_isSet;
}

bool OAILoggingGroup::is_enabled_Valid() const{
    return m_enabled_isValid;
}

QString OAILoggingGroup::getGroup() const {
    return group;
}
void OAILoggingGroup::setGroup(const QString &group) {
    this->group = group;
    this->m_group_isSet = true;
}

bool OAILoggingGroup::is_group_Set() const{
    return m_group_isSet;
}

bool OAILoggingGroup::is_group_Valid() const{
    return m_group_isValid;
}

QString OAILoggingGroup::getValues() const {
    return values;
}
void OAILoggingGroup::setValues(const QString &values) {
    this->values = values;
    this->m_values_isSet = true;
}

bool OAILoggingGroup::is_values_Set() const{
    return m_values_isSet;
}

bool OAILoggingGroup::is_values_Valid() const{
    return m_values_isValid;
}

bool OAILoggingGroup::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_enabled_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_group_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_values_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAILoggingGroup::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_enabled_isValid && m_group_isValid && m_values_isValid && true;
}

} // namespace OpenAPI
