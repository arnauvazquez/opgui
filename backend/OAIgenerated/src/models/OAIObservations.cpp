/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIObservations.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIObservations::OAIObservations(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIObservations::OAIObservations() {
    this->initializeModel();
}

OAIObservations::~OAIObservations() {}

void OAIObservations::initializeModel() {

    m_log_isSet = false;
    m_log_isValid = false;

    m_entity_repository_isSet = false;
    m_entity_repository_isValid = false;
}

void OAIObservations::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIObservations::fromJsonObject(QJsonObject json) {

    m_log_isValid = ::OpenAPI::fromJsonValue(log, json[QString("log")]);
    m_log_isSet = !json[QString("log")].isNull() && m_log_isValid;

    m_entity_repository_isValid = ::OpenAPI::fromJsonValue(entity_repository, json[QString("entityRepository")]);
    m_entity_repository_isSet = !json[QString("entityRepository")].isNull() && m_entity_repository_isValid;
}

QString OAIObservations::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIObservations::asJsonObject() const {
    QJsonObject obj;
    if (log.isSet()) {
        obj.insert(QString("log"), ::OpenAPI::toJsonValue(log));
    }
    if (entity_repository.isSet()) {
        obj.insert(QString("entityRepository"), ::OpenAPI::toJsonValue(entity_repository));
    }
    return obj;
}

OAILog OAIObservations::getLog() const {
    return log;
}
void OAIObservations::setLog(const OAILog &log) {
    this->log = log;
    this->m_log_isSet = true;
}

bool OAIObservations::is_log_Set() const{
    return m_log_isSet;
}

bool OAIObservations::is_log_Valid() const{
    return m_log_isValid;
}

OAIEntityRepository OAIObservations::getEntityRepository() const {
    return entity_repository;
}
void OAIObservations::setEntityRepository(const OAIEntityRepository &entity_repository) {
    this->entity_repository = entity_repository;
    this->m_entity_repository_isSet = true;
}

bool OAIObservations::is_entity_repository_Set() const{
    return m_entity_repository_isSet;
}

bool OAIObservations::is_entity_repository_Valid() const{
    return m_entity_repository_isValid;
}

bool OAIObservations::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (log.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (entity_repository.isSet()) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIObservations::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_log_isValid && m_entity_repository_isValid && true;
}

} // namespace OpenAPI
