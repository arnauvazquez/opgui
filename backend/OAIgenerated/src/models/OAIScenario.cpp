/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIScenario.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIScenario::OAIScenario(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIScenario::OAIScenario() {
    this->initializeModel();
}

OAIScenario::~OAIScenario() {}

void OAIScenario::initializeModel() {

    m_open_scenario_file_isSet = false;
    m_open_scenario_file_isValid = false;
}

void OAIScenario::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIScenario::fromJsonObject(QJsonObject json) {

    m_open_scenario_file_isValid = ::OpenAPI::fromJsonValue(open_scenario_file, json[QString("openScenarioFile")]);
    m_open_scenario_file_isSet = !json[QString("openScenarioFile")].isNull() && m_open_scenario_file_isValid;
}

QString OAIScenario::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIScenario::asJsonObject() const {
    QJsonObject obj;
    if (m_open_scenario_file_isSet) {
        obj.insert(QString("openScenarioFile"), ::OpenAPI::toJsonValue(open_scenario_file));
    }
    return obj;
}

QString OAIScenario::getOpenScenarioFile() const {
    return open_scenario_file;
}
void OAIScenario::setOpenScenarioFile(const QString &open_scenario_file) {
    this->open_scenario_file = open_scenario_file;
    this->m_open_scenario_file_isSet = true;
}

bool OAIScenario::is_open_scenario_file_Set() const{
    return m_open_scenario_file_isSet;
}

bool OAIScenario::is_open_scenario_file_Valid() const{
    return m_open_scenario_file_isValid;
}

bool OAIScenario::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_open_scenario_file_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIScenario::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_open_scenario_file_isValid && true;
}

} // namespace OpenAPI
