/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIScenario.h
 *
 * 
 */

#ifndef OAIScenario_H
#define OAIScenario_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIScenario : public OAIObject {
public:
    OAIScenario();
    OAIScenario(QString json);
    ~OAIScenario() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getOpenScenarioFile() const;
    void setOpenScenarioFile(const QString &open_scenario_file);
    bool is_open_scenario_file_Set() const;
    bool is_open_scenario_file_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString open_scenario_file;
    bool m_open_scenario_file_isSet;
    bool m_open_scenario_file_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIScenario)

#endif // OAIScenario_H
