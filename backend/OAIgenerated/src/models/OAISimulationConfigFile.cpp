/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAISimulationConfigFile.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAISimulationConfigFile::OAISimulationConfigFile(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAISimulationConfigFile::OAISimulationConfigFile() {
    this->initializeModel();
}

OAISimulationConfigFile::~OAISimulationConfigFile() {}

void OAISimulationConfigFile::initializeModel() {

    m_profiles_catalog_isSet = false;
    m_profiles_catalog_isValid = false;

    m_experiment_isSet = false;
    m_experiment_isValid = false;

    m_scenario_isSet = false;
    m_scenario_isValid = false;

    m_environment_isSet = false;
    m_environment_isValid = false;

    m_observations_isSet = false;
    m_observations_isValid = false;

    m_spawners_isSet = false;
    m_spawners_isValid = false;
}

void OAISimulationConfigFile::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAISimulationConfigFile::fromJsonObject(QJsonObject json) {

    m_profiles_catalog_isValid = ::OpenAPI::fromJsonValue(profiles_catalog, json[QString("profilesCatalog")]);
    m_profiles_catalog_isSet = !json[QString("profilesCatalog")].isNull() && m_profiles_catalog_isValid;

    m_experiment_isValid = ::OpenAPI::fromJsonValue(experiment, json[QString("experiment")]);
    m_experiment_isSet = !json[QString("experiment")].isNull() && m_experiment_isValid;

    m_scenario_isValid = ::OpenAPI::fromJsonValue(scenario, json[QString("scenario")]);
    m_scenario_isSet = !json[QString("scenario")].isNull() && m_scenario_isValid;

    m_environment_isValid = ::OpenAPI::fromJsonValue(environment, json[QString("environment")]);
    m_environment_isSet = !json[QString("environment")].isNull() && m_environment_isValid;

    m_observations_isValid = ::OpenAPI::fromJsonValue(observations, json[QString("observations")]);
    m_observations_isSet = !json[QString("observations")].isNull() && m_observations_isValid;

    m_spawners_isValid = ::OpenAPI::fromJsonValue(spawners, json[QString("spawners")]);
    m_spawners_isSet = !json[QString("spawners")].isNull() && m_spawners_isValid;
}

QString OAISimulationConfigFile::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAISimulationConfigFile::asJsonObject() const {
    QJsonObject obj;
    if (m_profiles_catalog_isSet) {
        obj.insert(QString("profilesCatalog"), ::OpenAPI::toJsonValue(profiles_catalog));
    }
    if (experiment.isSet()) {
        obj.insert(QString("experiment"), ::OpenAPI::toJsonValue(experiment));
    }
    if (scenario.isSet()) {
        obj.insert(QString("scenario"), ::OpenAPI::toJsonValue(scenario));
    }
    if (environment.isSet()) {
        obj.insert(QString("environment"), ::OpenAPI::toJsonValue(environment));
    }
    if (observations.isSet()) {
        obj.insert(QString("observations"), ::OpenAPI::toJsonValue(observations));
    }
    if (spawners.size() > 0) {
        obj.insert(QString("spawners"), ::OpenAPI::toJsonValue(spawners));
    }
    return obj;
}

QString OAISimulationConfigFile::getProfilesCatalog() const {
    return profiles_catalog;
}
void OAISimulationConfigFile::setProfilesCatalog(const QString &profiles_catalog) {
    this->profiles_catalog = profiles_catalog;
    this->m_profiles_catalog_isSet = true;
}

bool OAISimulationConfigFile::is_profiles_catalog_Set() const{
    return m_profiles_catalog_isSet;
}

bool OAISimulationConfigFile::is_profiles_catalog_Valid() const{
    return m_profiles_catalog_isValid;
}

OAIExperiment OAISimulationConfigFile::getExperiment() const {
    return experiment;
}
void OAISimulationConfigFile::setExperiment(const OAIExperiment &experiment) {
    this->experiment = experiment;
    this->m_experiment_isSet = true;
}

bool OAISimulationConfigFile::is_experiment_Set() const{
    return m_experiment_isSet;
}

bool OAISimulationConfigFile::is_experiment_Valid() const{
    return m_experiment_isValid;
}

OAIScenario OAISimulationConfigFile::getScenario() const {
    return scenario;
}
void OAISimulationConfigFile::setScenario(const OAIScenario &scenario) {
    this->scenario = scenario;
    this->m_scenario_isSet = true;
}

bool OAISimulationConfigFile::is_scenario_Set() const{
    return m_scenario_isSet;
}

bool OAISimulationConfigFile::is_scenario_Valid() const{
    return m_scenario_isValid;
}

OAIEnvironment OAISimulationConfigFile::getEnvironment() const {
    return environment;
}
void OAISimulationConfigFile::setEnvironment(const OAIEnvironment &environment) {
    this->environment = environment;
    this->m_environment_isSet = true;
}

bool OAISimulationConfigFile::is_environment_Set() const{
    return m_environment_isSet;
}

bool OAISimulationConfigFile::is_environment_Valid() const{
    return m_environment_isValid;
}

OAIObservations OAISimulationConfigFile::getObservations() const {
    return observations;
}
void OAISimulationConfigFile::setObservations(const OAIObservations &observations) {
    this->observations = observations;
    this->m_observations_isSet = true;
}

bool OAISimulationConfigFile::is_observations_Set() const{
    return m_observations_isSet;
}

bool OAISimulationConfigFile::is_observations_Valid() const{
    return m_observations_isValid;
}

QList<OAISpawner> OAISimulationConfigFile::getSpawners() const {
    return spawners;
}
void OAISimulationConfigFile::setSpawners(const QList<OAISpawner> &spawners) {
    this->spawners = spawners;
    this->m_spawners_isSet = true;
}

bool OAISimulationConfigFile::is_spawners_Set() const{
    return m_spawners_isSet;
}

bool OAISimulationConfigFile::is_spawners_Valid() const{
    return m_spawners_isValid;
}

bool OAISimulationConfigFile::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_profiles_catalog_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (experiment.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (scenario.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (environment.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (observations.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (spawners.size() > 0) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAISimulationConfigFile::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_profiles_catalog_isValid && m_experiment_isValid && m_scenario_isValid && m_environment_isValid && m_observations_isValid && true;
}

} // namespace OpenAPI
