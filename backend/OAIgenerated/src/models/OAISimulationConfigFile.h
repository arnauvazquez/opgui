/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAISimulationConfigFile.h
 *
 * 
 */

#ifndef OAISimulationConfigFile_H
#define OAISimulationConfigFile_H

#include <QJsonObject>

#include "OAIEnvironment.h"
#include "OAIExperiment.h"
#include "OAIObservations.h"
#include "OAIScenario.h"
#include "OAISpawner.h"
#include <QList>
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAISimulationConfigFile : public OAIObject {
public:
    OAISimulationConfigFile();
    OAISimulationConfigFile(QString json);
    ~OAISimulationConfigFile() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getProfilesCatalog() const;
    void setProfilesCatalog(const QString &profiles_catalog);
    bool is_profiles_catalog_Set() const;
    bool is_profiles_catalog_Valid() const;

    OAIExperiment getExperiment() const;
    void setExperiment(const OAIExperiment &experiment);
    bool is_experiment_Set() const;
    bool is_experiment_Valid() const;

    OAIScenario getScenario() const;
    void setScenario(const OAIScenario &scenario);
    bool is_scenario_Set() const;
    bool is_scenario_Valid() const;

    OAIEnvironment getEnvironment() const;
    void setEnvironment(const OAIEnvironment &environment);
    bool is_environment_Set() const;
    bool is_environment_Valid() const;

    OAIObservations getObservations() const;
    void setObservations(const OAIObservations &observations);
    bool is_observations_Set() const;
    bool is_observations_Valid() const;

    QList<OAISpawner> getSpawners() const;
    void setSpawners(const QList<OAISpawner> &spawners);
    bool is_spawners_Set() const;
    bool is_spawners_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString profiles_catalog;
    bool m_profiles_catalog_isSet;
    bool m_profiles_catalog_isValid;

    OAIExperiment experiment;
    bool m_experiment_isSet;
    bool m_experiment_isValid;

    OAIScenario scenario;
    bool m_scenario_isSet;
    bool m_scenario_isValid;

    OAIEnvironment environment;
    bool m_environment_isSet;
    bool m_environment_isValid;

    OAIObservations observations;
    bool m_observations_isSet;
    bool m_observations_isValid;

    QList<OAISpawner> spawners;
    bool m_spawners_isSet;
    bool m_spawners_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAISimulationConfigFile)

#endif // OAISimulationConfigFile_H
