/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAISpawner.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAISpawner::OAISpawner(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAISpawner::OAISpawner() {
    this->initializeModel();
}

OAISpawner::~OAISpawner() {}

void OAISpawner::initializeModel() {

    m_library_isSet = false;
    m_library_isValid = false;

    m_type_isSet = false;
    m_type_isValid = false;

    m_priority_isSet = false;
    m_priority_isValid = false;

    m_profile_isSet = false;
    m_profile_isValid = false;
}

void OAISpawner::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAISpawner::fromJsonObject(QJsonObject json) {

    m_library_isValid = ::OpenAPI::fromJsonValue(library, json[QString("library")]);
    m_library_isSet = !json[QString("library")].isNull() && m_library_isValid;

    m_type_isValid = ::OpenAPI::fromJsonValue(type, json[QString("type")]);
    m_type_isSet = !json[QString("type")].isNull() && m_type_isValid;

    m_priority_isValid = ::OpenAPI::fromJsonValue(priority, json[QString("priority")]);
    m_priority_isSet = !json[QString("priority")].isNull() && m_priority_isValid;

    m_profile_isValid = ::OpenAPI::fromJsonValue(profile, json[QString("profile")]);
    m_profile_isSet = !json[QString("profile")].isNull() && m_profile_isValid;
}

QString OAISpawner::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAISpawner::asJsonObject() const {
    QJsonObject obj;
    if (m_library_isSet) {
        obj.insert(QString("library"), ::OpenAPI::toJsonValue(library));
    }
    if (m_type_isSet) {
        obj.insert(QString("type"), ::OpenAPI::toJsonValue(type));
    }
    if (m_priority_isSet) {
        obj.insert(QString("priority"), ::OpenAPI::toJsonValue(priority));
    }
    if (m_profile_isSet) {
        obj.insert(QString("profile"), ::OpenAPI::toJsonValue(profile));
    }
    return obj;
}

QString OAISpawner::getLibrary() const {
    return library;
}
void OAISpawner::setLibrary(const QString &library) {
    this->library = library;
    this->m_library_isSet = true;
}

bool OAISpawner::is_library_Set() const{
    return m_library_isSet;
}

bool OAISpawner::is_library_Valid() const{
    return m_library_isValid;
}

QString OAISpawner::getType() const {
    return type;
}
void OAISpawner::setType(const QString &type) {
    this->type = type;
    this->m_type_isSet = true;
}

bool OAISpawner::is_type_Set() const{
    return m_type_isSet;
}

bool OAISpawner::is_type_Valid() const{
    return m_type_isValid;
}

qint32 OAISpawner::getPriority() const {
    return priority;
}
void OAISpawner::setPriority(const qint32 &priority) {
    this->priority = priority;
    this->m_priority_isSet = true;
}

bool OAISpawner::is_priority_Set() const{
    return m_priority_isSet;
}

bool OAISpawner::is_priority_Valid() const{
    return m_priority_isValid;
}

QString OAISpawner::getProfile() const {
    return profile;
}
void OAISpawner::setProfile(const QString &profile) {
    this->profile = profile;
    this->m_profile_isSet = true;
}

bool OAISpawner::is_profile_Set() const{
    return m_profile_isSet;
}

bool OAISpawner::is_profile_Valid() const{
    return m_profile_isValid;
}

bool OAISpawner::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_library_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_type_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_priority_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_profile_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAISpawner::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_library_isValid && m_type_isValid && m_priority_isValid && true;
}

} // namespace OpenAPI
