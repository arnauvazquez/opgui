/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAISpawner.h
 *
 * 
 */

#ifndef OAISpawner_H
#define OAISpawner_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAISpawner : public OAIObject {
public:
    OAISpawner();
    OAISpawner(QString json);
    ~OAISpawner() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getLibrary() const;
    void setLibrary(const QString &library);
    bool is_library_Set() const;
    bool is_library_Valid() const;

    QString getType() const;
    void setType(const QString &type);
    bool is_type_Set() const;
    bool is_type_Valid() const;

    qint32 getPriority() const;
    void setPriority(const qint32 &priority);
    bool is_priority_Set() const;
    bool is_priority_Valid() const;

    QString getProfile() const;
    void setProfile(const QString &profile);
    bool is_profile_Set() const;
    bool is_profile_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString library;
    bool m_library_isSet;
    bool m_library_isValid;

    QString type;
    bool m_type_isSet;
    bool m_type_isValid;

    qint32 priority;
    bool m_priority_isSet;
    bool m_priority_isValid;

    QString profile;
    bool m_profile_isSet;
    bool m_profile_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAISpawner)

#endif // OAISpawner_H
