/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAITreeNode.h
 *
 * 
 */

#ifndef OAITreeNode_H
#define OAITreeNode_H

#include <QJsonObject>

#include <QList>
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAITreeNode : public OAIObject {
public:
    OAITreeNode();
    OAITreeNode(QString json);
    ~OAITreeNode() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getName() const;
    void setName(const QString &name);
    bool is_name_Set() const;
    bool is_name_Valid() const;

    QList<OAITreeNode> getChildren() const;
    void setChildren(const QList<OAITreeNode> &children);
    bool is_children_Set() const;
    bool is_children_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString name;
    bool m_name_isSet;
    bool m_name_isValid;

    QList<OAITreeNode> children;
    bool m_children_isSet;
    bool m_children_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAITreeNode)

#endif // OAITreeNode_H
