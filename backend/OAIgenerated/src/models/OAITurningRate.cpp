/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAITurningRate.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAITurningRate::OAITurningRate(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAITurningRate::OAITurningRate() {
    this->initializeModel();
}

OAITurningRate::~OAITurningRate() {}

void OAITurningRate::initializeModel() {

    m_incoming_isSet = false;
    m_incoming_isValid = false;

    m_outgoing_isSet = false;
    m_outgoing_isValid = false;

    m_weight_isSet = false;
    m_weight_isValid = false;
}

void OAITurningRate::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAITurningRate::fromJsonObject(QJsonObject json) {

    m_incoming_isValid = ::OpenAPI::fromJsonValue(incoming, json[QString("incoming")]);
    m_incoming_isSet = !json[QString("incoming")].isNull() && m_incoming_isValid;

    m_outgoing_isValid = ::OpenAPI::fromJsonValue(outgoing, json[QString("outgoing")]);
    m_outgoing_isSet = !json[QString("outgoing")].isNull() && m_outgoing_isValid;

    m_weight_isValid = ::OpenAPI::fromJsonValue(weight, json[QString("weight")]);
    m_weight_isSet = !json[QString("weight")].isNull() && m_weight_isValid;
}

QString OAITurningRate::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAITurningRate::asJsonObject() const {
    QJsonObject obj;
    if (m_incoming_isSet) {
        obj.insert(QString("incoming"), ::OpenAPI::toJsonValue(incoming));
    }
    if (m_outgoing_isSet) {
        obj.insert(QString("outgoing"), ::OpenAPI::toJsonValue(outgoing));
    }
    if (m_weight_isSet) {
        obj.insert(QString("weight"), ::OpenAPI::toJsonValue(weight));
    }
    return obj;
}

QString OAITurningRate::getIncoming() const {
    return incoming;
}
void OAITurningRate::setIncoming(const QString &incoming) {
    this->incoming = incoming;
    this->m_incoming_isSet = true;
}

bool OAITurningRate::is_incoming_Set() const{
    return m_incoming_isSet;
}

bool OAITurningRate::is_incoming_Valid() const{
    return m_incoming_isValid;
}

QString OAITurningRate::getOutgoing() const {
    return outgoing;
}
void OAITurningRate::setOutgoing(const QString &outgoing) {
    this->outgoing = outgoing;
    this->m_outgoing_isSet = true;
}

bool OAITurningRate::is_outgoing_Set() const{
    return m_outgoing_isSet;
}

bool OAITurningRate::is_outgoing_Valid() const{
    return m_outgoing_isValid;
}

double OAITurningRate::getWeight() const {
    return weight;
}
void OAITurningRate::setWeight(const double &weight) {
    this->weight = weight;
    this->m_weight_isSet = true;
}

bool OAITurningRate::is_weight_Set() const{
    return m_weight_isSet;
}

bool OAITurningRate::is_weight_Valid() const{
    return m_weight_isValid;
}

bool OAITurningRate::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_incoming_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_outgoing_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_weight_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAITurningRate::isValid() const {
    // only required properties are required for the object to be considered valid
    return true;
}

} // namespace OpenAPI
