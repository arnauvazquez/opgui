/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAITurningRate.h
 *
 * 
 */

#ifndef OAITurningRate_H
#define OAITurningRate_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAITurningRate : public OAIObject {
public:
    OAITurningRate();
    OAITurningRate(QString json);
    ~OAITurningRate() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getIncoming() const;
    void setIncoming(const QString &incoming);
    bool is_incoming_Set() const;
    bool is_incoming_Valid() const;

    QString getOutgoing() const;
    void setOutgoing(const QString &outgoing);
    bool is_outgoing_Set() const;
    bool is_outgoing_Valid() const;

    double getWeight() const;
    void setWeight(const double &weight);
    bool is_weight_Set() const;
    bool is_weight_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString incoming;
    bool m_incoming_isSet;
    bool m_incoming_isValid;

    QString outgoing;
    bool m_outgoing_isSet;
    bool m_outgoing_isValid;

    double weight;
    bool m_weight_isSet;
    bool m_weight_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAITurningRate)

#endif // OAITurningRate_H
