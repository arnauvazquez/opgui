/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIVisibilityDistance.h
 *
 * 
 */

#ifndef OAIVisibilityDistance_H
#define OAIVisibilityDistance_H

#include <QJsonObject>


#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIVisibilityDistance : public OAIObject {
public:
    OAIVisibilityDistance();
    OAIVisibilityDistance(QString json);
    ~OAIVisibilityDistance() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    float getProbability() const;
    void setProbability(const float &probability);
    bool is_probability_Set() const;
    bool is_probability_Valid() const;

    qint32 getValue() const;
    void setValue(const qint32 &value);
    bool is_value_Set() const;
    bool is_value_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    float probability;
    bool m_probability_isSet;
    bool m_probability_isValid;

    qint32 value;
    bool m_value_isSet;
    bool m_value_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIVisibilityDistance)

#endif // OAIVisibilityDistance_H
