/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIWeather.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIWeather::OAIWeather(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIWeather::OAIWeather() {
    this->initializeModel();
}

OAIWeather::~OAIWeather() {}

void OAIWeather::initializeModel() {

    m_probability_isSet = false;
    m_probability_isValid = false;

    m_value_isSet = false;
    m_value_isValid = false;
}

void OAIWeather::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIWeather::fromJsonObject(QJsonObject json) {

    m_probability_isValid = ::OpenAPI::fromJsonValue(probability, json[QString("probability")]);
    m_probability_isSet = !json[QString("probability")].isNull() && m_probability_isValid;

    m_value_isValid = ::OpenAPI::fromJsonValue(value, json[QString("value")]);
    m_value_isSet = !json[QString("value")].isNull() && m_value_isValid;
}

QString OAIWeather::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIWeather::asJsonObject() const {
    QJsonObject obj;
    if (m_probability_isSet) {
        obj.insert(QString("probability"), ::OpenAPI::toJsonValue(probability));
    }
    if (m_value_isSet) {
        obj.insert(QString("value"), ::OpenAPI::toJsonValue(value));
    }
    return obj;
}

float OAIWeather::getProbability() const {
    return probability;
}
void OAIWeather::setProbability(const float &probability) {
    this->probability = probability;
    this->m_probability_isSet = true;
}

bool OAIWeather::is_probability_Set() const{
    return m_probability_isSet;
}

bool OAIWeather::is_probability_Valid() const{
    return m_probability_isValid;
}

QString OAIWeather::getValue() const {
    return value;
}
void OAIWeather::setValue(const QString &value) {
    this->value = value;
    this->m_value_isSet = true;
}

bool OAIWeather::is_value_Set() const{
    return m_value_isSet;
}

bool OAIWeather::is_value_Valid() const{
    return m_value_isValid;
}

bool OAIWeather::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_probability_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_value_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIWeather::isValid() const {
    // only required properties are required for the object to be considered valid
    return true;
}

} // namespace OpenAPI
