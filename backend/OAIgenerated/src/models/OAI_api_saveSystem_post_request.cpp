/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAI_api_saveSystem_post_request.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAI_api_saveSystem_post_request::OAI_api_saveSystem_post_request(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAI_api_saveSystem_post_request::OAI_api_saveSystem_post_request() {
    this->initializeModel();
}

OAI_api_saveSystem_post_request::~OAI_api_saveSystem_post_request() {}

void OAI_api_saveSystem_post_request::initializeModel() {

    m_system_isSet = false;
    m_system_isValid = false;

    m_path_isSet = false;
    m_path_isValid = false;

    m_is_new_isSet = false;
    m_is_new_isValid = false;
}

void OAI_api_saveSystem_post_request::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAI_api_saveSystem_post_request::fromJsonObject(QJsonObject json) {

    m_system_isValid = ::OpenAPI::fromJsonValue(system, json[QString("system")]);
    m_system_isSet = !json[QString("system")].isNull() && m_system_isValid;

    m_path_isValid = ::OpenAPI::fromJsonValue(path, json[QString("path")]);
    m_path_isSet = !json[QString("path")].isNull() && m_path_isValid;

    m_is_new_isValid = ::OpenAPI::fromJsonValue(is_new, json[QString("isNew")]);
    m_is_new_isSet = !json[QString("isNew")].isNull() && m_is_new_isValid;
}

QString OAI_api_saveSystem_post_request::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAI_api_saveSystem_post_request::asJsonObject() const {
    QJsonObject obj;
    if (system.isSet()) {
        obj.insert(QString("system"), ::OpenAPI::toJsonValue(system));
    }
    if (m_path_isSet) {
        obj.insert(QString("path"), ::OpenAPI::toJsonValue(path));
    }
    if (m_is_new_isSet) {
        obj.insert(QString("isNew"), ::OpenAPI::toJsonValue(is_new));
    }
    return obj;
}

OAISystemUI OAI_api_saveSystem_post_request::getSystem() const {
    return system;
}
void OAI_api_saveSystem_post_request::setSystem(const OAISystemUI &system) {
    this->system = system;
    this->m_system_isSet = true;
}

bool OAI_api_saveSystem_post_request::is_system_Set() const{
    return m_system_isSet;
}

bool OAI_api_saveSystem_post_request::is_system_Valid() const{
    return m_system_isValid;
}

QString OAI_api_saveSystem_post_request::getPath() const {
    return path;
}
void OAI_api_saveSystem_post_request::setPath(const QString &path) {
    this->path = path;
    this->m_path_isSet = true;
}

bool OAI_api_saveSystem_post_request::is_path_Set() const{
    return m_path_isSet;
}

bool OAI_api_saveSystem_post_request::is_path_Valid() const{
    return m_path_isValid;
}

bool OAI_api_saveSystem_post_request::isIsNew() const {
    return is_new;
}
void OAI_api_saveSystem_post_request::setIsNew(const bool &is_new) {
    this->is_new = is_new;
    this->m_is_new_isSet = true;
}

bool OAI_api_saveSystem_post_request::is_is_new_Set() const{
    return m_is_new_isSet;
}

bool OAI_api_saveSystem_post_request::is_is_new_Valid() const{
    return m_is_new_isValid;
}

bool OAI_api_saveSystem_post_request::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (system.isSet()) {
            isObjectUpdated = true;
            break;
        }

        if (m_path_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_is_new_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAI_api_saveSystem_post_request::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_system_isValid && m_path_isValid && m_is_new_isValid && true;
}

} // namespace OpenAPI
