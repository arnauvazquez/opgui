/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAI_api_saveSystem_post_request.h
 *
 * 
 */

#ifndef OAI_api_saveSystem_post_request_H
#define OAI_api_saveSystem_post_request_H

#include <QJsonObject>

#include "OAISystemUI.h"
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAI_api_saveSystem_post_request : public OAIObject {
public:
    OAI_api_saveSystem_post_request();
    OAI_api_saveSystem_post_request(QString json);
    ~OAI_api_saveSystem_post_request() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    OAISystemUI getSystem() const;
    void setSystem(const OAISystemUI &system);
    bool is_system_Set() const;
    bool is_system_Valid() const;

    QString getPath() const;
    void setPath(const QString &path);
    bool is_path_Set() const;
    bool is_path_Valid() const;

    bool isIsNew() const;
    void setIsNew(const bool &is_new);
    bool is_is_new_Set() const;
    bool is_is_new_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    OAISystemUI system;
    bool m_system_isSet;
    bool m_system_isValid;

    QString path;
    bool m_path_isSet;
    bool m_path_isValid;

    bool is_new;
    bool m_is_new_isSet;
    bool m_is_new_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAI_api_saveSystem_post_request)

#endif // OAI_api_saveSystem_post_request_H
