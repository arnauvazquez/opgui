/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAIGetConfigEditorApiRequest.h"

namespace OpenAPI {

OAIGetConfigEditorApiRequest::OAIGetConfigEditorApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetConfigEditorApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAIGetConfigEditorApiRequest::~OAIGetConfigEditorApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAIGetConfigEditorApiRequest::~OAIGetConfigEditorApiRequest()";
}

QMap<QString, QString>
OAIGetConfigEditorApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAIGetConfigEditorApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAIGetConfigEditorApiRequest::getRawSocket(){
    return socket;
}


void OAIGetConfigEditorApiRequest::apiConfigEditorGetRequest(){
    qDebug() << "/api/configEditor";
    connect(this, &OAIGetConfigEditorApiRequest::apiConfigEditorGet, handler.data(), &OAIGetConfigEditorApiHandler::apiConfigEditorGet);

    
    QString path;
    if(socket->queryString().keys().contains("path")){
        fromStringValue(socket->queryString().value("path"), path);
    }
    


    Q_EMIT apiConfigEditorGet(path);
}



void OAIGetConfigEditorApiRequest::apiConfigEditorGetResponse(const OAIConfigEditorObject& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetConfigEditorApiRequest::apiConfigEditorGetError(const OAIConfigEditorObject& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetConfigEditorApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAIGetConfigEditorApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
