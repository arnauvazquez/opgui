/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetConfigEditorApiRequest_H
#define OAI_OAIGetConfigEditorApiRequest_H

#include <QObject>
#include <QStringList>
#include <QMultiMap>
#include <QNetworkReply>
#include <QSharedPointer>

#include <qhttpengine/socket.h>
#include "OAIConfigEditorObject.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include <QString>
#include "OAIGetConfigEditorApiHandler.h"

namespace OpenAPI {

class OAIGetConfigEditorApiRequest : public QObject
{
    Q_OBJECT

public:
    OAIGetConfigEditorApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetConfigEditorApiHandler> handler);
    virtual ~OAIGetConfigEditorApiRequest();

    void apiConfigEditorGetRequest();
    

    void apiConfigEditorGetResponse(const OAIConfigEditorObject& res);
    

    void apiConfigEditorGetError(const OAIConfigEditorObject& res, QNetworkReply::NetworkError error_type, QString& error_str);
    

    void sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type);

    void sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type);

    QMap<QString, QString> getRequestHeaders() const;

    QHttpEngine::Socket* getRawSocket();

    void setResponseHeaders(const QMultiMap<QString,QString>& headers);

Q_SIGNALS:
    void apiConfigEditorGet(QString path);
    

private:
    QMap<QString, QString> requestHeaders;
    QMap<QString, QString> responseHeaders;
    QHttpEngine::Socket  *socket;
    QSharedPointer<OAIGetConfigEditorApiHandler> handler;

    inline void setSocketResponseHeaders(){
        QHttpEngine::Socket::HeaderMap resHeaders;
        for(auto itr = responseHeaders.begin(); itr != responseHeaders.end(); ++itr) {
            resHeaders.insert(itr.key().toUtf8(), itr.value().toUtf8());
        }
        socket->setHeaders(resHeaders);
    }
};

}

#endif // OAI_OAIGetConfigEditorApiRequest_H
