/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAIGetConfigElementsApiRequest.h"

namespace OpenAPI {

OAIGetConfigElementsApiRequest::OAIGetConfigElementsApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetConfigElementsApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAIGetConfigElementsApiRequest::~OAIGetConfigElementsApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAIGetConfigElementsApiRequest::~OAIGetConfigElementsApiRequest()";
}

QMap<QString, QString>
OAIGetConfigElementsApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAIGetConfigElementsApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAIGetConfigElementsApiRequest::getRawSocket(){
    return socket;
}


void OAIGetConfigElementsApiRequest::apiConfigElementsGetRequest(){
    qDebug() << "/api/configElements";
    connect(this, &OAIGetConfigElementsApiRequest::apiConfigElementsGet, handler.data(), &OAIGetConfigElementsApiHandler::apiConfigElementsGet);

    


    Q_EMIT apiConfigElementsGet();
}



void OAIGetConfigElementsApiRequest::apiConfigElementsGetResponse(const QList<OAIConfigElement>& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toArray());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetConfigElementsApiRequest::apiConfigElementsGetError(const QList<OAIConfigElement>& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toArray());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetConfigElementsApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAIGetConfigElementsApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
