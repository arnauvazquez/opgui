/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetConfigElementsApiRequest_H
#define OAI_OAIGetConfigElementsApiRequest_H

#include <QObject>
#include <QStringList>
#include <QMultiMap>
#include <QNetworkReply>
#include <QSharedPointer>

#include <qhttpengine/socket.h>
#include "OAIConfigElement.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include <QList>
#include <QString>
#include "OAIGetConfigElementsApiHandler.h"

namespace OpenAPI {

class OAIGetConfigElementsApiRequest : public QObject
{
    Q_OBJECT

public:
    OAIGetConfigElementsApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetConfigElementsApiHandler> handler);
    virtual ~OAIGetConfigElementsApiRequest();

    void apiConfigElementsGetRequest();
    

    void apiConfigElementsGetResponse(const QList<OAIConfigElement>& res);
    

    void apiConfigElementsGetError(const QList<OAIConfigElement>& res, QNetworkReply::NetworkError error_type, QString& error_str);
    

    void sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type);

    void sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type);

    QMap<QString, QString> getRequestHeaders() const;

    QHttpEngine::Socket* getRawSocket();

    void setResponseHeaders(const QMultiMap<QString,QString>& headers);

Q_SIGNALS:
    void apiConfigElementsGet();
    

private:
    QMap<QString, QString> requestHeaders;
    QMap<QString, QString> responseHeaders;
    QHttpEngine::Socket  *socket;
    QSharedPointer<OAIGetConfigElementsApiHandler> handler;

    inline void setSocketResponseHeaders(){
        QHttpEngine::Socket::HeaderMap resHeaders;
        for(auto itr = responseHeaders.begin(); itr != responseHeaders.end(); ++itr) {
            resHeaders.insert(itr.key().toUtf8(), itr.value().toUtf8());
        }
        socket->setHeaders(resHeaders);
    }
};

}

#endif // OAI_OAIGetConfigElementsApiRequest_H
