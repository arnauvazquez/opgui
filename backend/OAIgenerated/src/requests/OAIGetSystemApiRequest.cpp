/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAIGetSystemApiRequest.h"

namespace OpenAPI {

OAIGetSystemApiRequest::OAIGetSystemApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetSystemApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAIGetSystemApiRequest::~OAIGetSystemApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAIGetSystemApiRequest::~OAIGetSystemApiRequest()";
}

QMap<QString, QString>
OAIGetSystemApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAIGetSystemApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAIGetSystemApiRequest::getRawSocket(){
    return socket;
}


void OAIGetSystemApiRequest::apiGetSystemGetRequest(){
    qDebug() << "/api/getSystem";
    connect(this, &OAIGetSystemApiRequest::apiGetSystemGet, handler.data(), &OAIGetSystemApiHandler::apiGetSystemGet);

    
    QString path;
    if(socket->queryString().keys().contains("path")){
        fromStringValue(socket->queryString().value("path"), path);
    }
    


    Q_EMIT apiGetSystemGet(path);
}



void OAIGetSystemApiRequest::apiGetSystemGetResponse(const OAISystemUI& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetSystemApiRequest::apiGetSystemGetError(const OAISystemUI& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetSystemApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAIGetSystemApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
