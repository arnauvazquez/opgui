/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetSystemApiRequest_H
#define OAI_OAIGetSystemApiRequest_H

#include <QObject>
#include <QStringList>
#include <QMultiMap>
#include <QNetworkReply>
#include <QSharedPointer>

#include <qhttpengine/socket.h>
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAISystemUI.h"
#include <QString>
#include "OAIGetSystemApiHandler.h"

namespace OpenAPI {

class OAIGetSystemApiRequest : public QObject
{
    Q_OBJECT

public:
    OAIGetSystemApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetSystemApiHandler> handler);
    virtual ~OAIGetSystemApiRequest();

    void apiGetSystemGetRequest();
    

    void apiGetSystemGetResponse(const OAISystemUI& res);
    

    void apiGetSystemGetError(const OAISystemUI& res, QNetworkReply::NetworkError error_type, QString& error_str);
    

    void sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type);

    void sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type);

    QMap<QString, QString> getRequestHeaders() const;

    QHttpEngine::Socket* getRawSocket();

    void setResponseHeaders(const QMultiMap<QString,QString>& headers);

Q_SIGNALS:
    void apiGetSystemGet(QString path);
    

private:
    QMap<QString, QString> requestHeaders;
    QMap<QString, QString> responseHeaders;
    QHttpEngine::Socket  *socket;
    QSharedPointer<OAIGetSystemApiHandler> handler;

    inline void setSocketResponseHeaders(){
        QHttpEngine::Socket::HeaderMap resHeaders;
        for(auto itr = responseHeaders.begin(); itr != responseHeaders.end(); ++itr) {
            resHeaders.insert(itr.key().toUtf8(), itr.value().toUtf8());
        }
        socket->setHeaders(resHeaders);
    }
};

}

#endif // OAI_OAIGetSystemApiRequest_H
