/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAIRunOpSimulationApiRequest.h"

namespace OpenAPI {

OAIRunOpSimulationApiRequest::OAIRunOpSimulationApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIRunOpSimulationApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAIRunOpSimulationApiRequest::~OAIRunOpSimulationApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAIRunOpSimulationApiRequest::~OAIRunOpSimulationApiRequest()";
}

QMap<QString, QString>
OAIRunOpSimulationApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAIRunOpSimulationApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAIRunOpSimulationApiRequest::getRawSocket(){
    return socket;
}


void OAIRunOpSimulationApiRequest::apiRunOpSimulationGetRequest(){
    qDebug() << "/api/runOpSimulation";
    connect(this, &OAIRunOpSimulationApiRequest::apiRunOpSimulationGet, handler.data(), &OAIRunOpSimulationApiHandler::apiRunOpSimulationGet);

    
    QString config_path;
    if(socket->queryString().keys().contains("config_path")){
        fromStringValue(socket->queryString().value("config_path"), config_path);
    }
    


    Q_EMIT apiRunOpSimulationGet(config_path);
}



void OAIRunOpSimulationApiRequest::apiRunOpSimulationGetResponse(const OAIDefault200Response& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIRunOpSimulationApiRequest::apiRunOpSimulationGetError(const OAIDefault200Response& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIRunOpSimulationApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAIRunOpSimulationApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
