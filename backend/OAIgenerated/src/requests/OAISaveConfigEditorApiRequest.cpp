/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAISaveConfigEditorApiRequest.h"

namespace OpenAPI {

OAISaveConfigEditorApiRequest::OAISaveConfigEditorApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAISaveConfigEditorApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAISaveConfigEditorApiRequest::~OAISaveConfigEditorApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAISaveConfigEditorApiRequest::~OAISaveConfigEditorApiRequest()";
}

QMap<QString, QString>
OAISaveConfigEditorApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAISaveConfigEditorApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAISaveConfigEditorApiRequest::getRawSocket(){
    return socket;
}


void OAISaveConfigEditorApiRequest::apiConfigEditorPostRequest(){
    qDebug() << "/api/configEditor";
    connect(this, &OAISaveConfigEditorApiRequest::apiConfigEditorPost, handler.data(), &OAISaveConfigEditorApiHandler::apiConfigEditorPost);

    
 
    
    QJsonDocument doc;
    socket->readJson(doc);
    QJsonObject obj = doc.object();
    OAI_api_configEditor_post_request oai_api_config_editor_post_request;
    ::OpenAPI::fromJsonValue(oai_api_config_editor_post_request, obj);
    

    Q_EMIT apiConfigEditorPost(oai_api_config_editor_post_request);
}



void OAISaveConfigEditorApiRequest::apiConfigEditorPostResponse(const OAIDefault200Response& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAISaveConfigEditorApiRequest::apiConfigEditorPostError(const OAIDefault200Response& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAISaveConfigEditorApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAISaveConfigEditorApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
