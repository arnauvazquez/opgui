/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAISaveSystemApiRequest.h"

namespace OpenAPI {

OAISaveSystemApiRequest::OAISaveSystemApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAISaveSystemApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAISaveSystemApiRequest::~OAISaveSystemApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAISaveSystemApiRequest::~OAISaveSystemApiRequest()";
}

QMap<QString, QString>
OAISaveSystemApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAISaveSystemApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAISaveSystemApiRequest::getRawSocket(){
    return socket;
}


void OAISaveSystemApiRequest::apiSaveSystemPostRequest(){
    qDebug() << "/api/saveSystem";
    connect(this, &OAISaveSystemApiRequest::apiSaveSystemPost, handler.data(), &OAISaveSystemApiHandler::apiSaveSystemPost);

    
 
    
    QJsonDocument doc;
    socket->readJson(doc);
    QJsonObject obj = doc.object();
    OAI_api_saveSystem_post_request oai_api_save_system_post_request;
    ::OpenAPI::fromJsonValue(oai_api_save_system_post_request, obj);
    

    Q_EMIT apiSaveSystemPost(oai_api_save_system_post_request);
}



void OAISaveSystemApiRequest::apiSaveSystemPostResponse(const OAIDefault200Response& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAISaveSystemApiRequest::apiSaveSystemPostError(const OAIDefault200Response& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAISaveSystemApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAISaveSystemApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
