/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAISetConfigElementsApiRequest.h"

namespace OpenAPI {

OAISetConfigElementsApiRequest::OAISetConfigElementsApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAISetConfigElementsApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAISetConfigElementsApiRequest::~OAISetConfigElementsApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAISetConfigElementsApiRequest::~OAISetConfigElementsApiRequest()";
}

QMap<QString, QString>
OAISetConfigElementsApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAISetConfigElementsApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAISetConfigElementsApiRequest::getRawSocket(){
    return socket;
}


void OAISetConfigElementsApiRequest::apiConfigElementsPostRequest(){
    qDebug() << "/api/configElements";
    connect(this, &OAISetConfigElementsApiRequest::apiConfigElementsPost, handler.data(), &OAISetConfigElementsApiHandler::apiConfigElementsPost);

    
 
    QJsonDocument doc;
    QList<OAIConfigElement> oai_config_element;
    if(socket->readJson(doc)){
        QJsonArray jsonArray = doc.array();
        foreach(QJsonValue obj, jsonArray) {
            OAIConfigElement o;
            ::OpenAPI::fromJsonValue(o, obj);
            oai_config_element.append(o);
        }
    }
    

    Q_EMIT apiConfigElementsPost(oai_config_element);
}



void OAISetConfigElementsApiRequest::apiConfigElementsPostResponse(const OAIDefault200Response& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAISetConfigElementsApiRequest::apiConfigElementsPostError(const OAIDefault200Response& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAISetConfigElementsApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAISetConfigElementsApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
