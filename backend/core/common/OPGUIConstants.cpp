/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUIConstants.h"

const QList<QString> OPGUIConstants::trafficRulesListConst = {"DE"};
const QList<QString> OPGUIConstants::weathersListConst = {"Clear", "rainy", "snowy"};
const QList<QString> OPGUIConstants::worldLibrariesListConst = {"World", "World_OSI"};
const QList<QString> OPGUIConstants::dataBufferLibrariesListConst = {"BasicDataBuffer","DataBuffer"};
const QList<QString> OPGUIConstants::stochasticsLibrariesListConst = {"Stochastics"};
const QString OPGUIConstants::defaultWorldLibrary = "World";
const QString OPGUIConstants::defaultDataBufferLibrary = "BasicDataBuffer";
const QString OPGUIConstants::defaultStochasticsLibrary = "Stochastics";  
const QList<QString> OPGUIConstants::defaultLoggingGroupsConst = {"Trace",
                                                                "RoadPosition",
                                                                "RoadPositionExtended",
                                                                "Sensor",
                                                                "Vehicle",
                                                                "Visualization"};                                                              
const QList<QString> OPGUIConstants::writePersistentEntitiesModesConst = {"Consolidated","Separate","Skip"};
const QList<QString> OPGUIConstants::spawnerLibrariesListConst = {"SpawnerPreRunCommon","SpawnerRuntimeCommon"};
const QList<QString> OPGUIConstants::spawnerTypesListConst = {"PreRun","Runtime"};
const QString OPGUIConstants::simulationConfigSchema = "0.8.2";
const QString OPGUIConstants::ProfilesCatalogSchema = "0.6.0";
const QString OPGUIConstants::simConfigFileName = "simulationConfig.xml";
