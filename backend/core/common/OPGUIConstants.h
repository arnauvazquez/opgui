/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QList>

class OPGUIConstants {
public:
    static const QList<QString> trafficRulesListConst;
    static const QList<QString> weathersListConst;
    static const QList<QString> worldLibrariesListConst;
    static const QList<QString> dataBufferLibrariesListConst;
    static const QList<QString> stochasticsLibrariesListConst;
    static const QList<QString> defaultLoggingGroupsConst;
    static const QList<QString> writePersistentEntitiesModesConst;
    static const QList<QString> spawnerLibrariesListConst;
    static const QList<QString> spawnerTypesListConst;

    static constexpr double EPSILON = 0.00001;

    static const QString simulationConfigSchema;
    static const QString ProfilesCatalogSchema;
    static const QString simConfigFileName;
    static const QString defaultWorldLibrary;
    static const QString defaultDataBufferLibrary;
    static const QString defaultStochasticsLibrary;
};

