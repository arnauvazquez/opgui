/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QProcess>
#include <QException>
#include <QtConcurrent>
#include <QStack>
#include <QDir>
#include <QAtomicInt> 
#include <QObject>
#include <QDateTime>
#include <QUrl>
#include <QString>

#include "OPGUICore.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUIPCMSimulation.h"
#include "OPGUISystemEditorImport.h"
#include "OPGUISystemEditorExport.h"
#include "OPGUIGlobalSetupEditor.h"
#include "OPGUIFileSystemManager.h"
#include "OPGUIQtLogger.h"
#include "OPGUIValueList.h"
#include "OPGUISimulationExecution.h"
#include "OPGUIConfigEditorImport.h"
#include "OPGUIConfigEditorExport.h"

namespace OPGUICore
{
    QAtomicInt isRunning = 0;  
    OPGUIPCMSimulation::PCMSimulation *sim = nullptr;

    bool isSimulationRunning()
    {
        return isRunning.loadRelaxed()==0?false:true;
    }

    bool api_is_simulation_running(OpenAPI::OAIBoolean200Response &resp, QString &errorMsg)
    {
        LOG_INFO("Initialising : isSimulationRunning");

        resp.setResponse(OPGUISimulationExecution::getInstance().isSimulationRunning());

        return true;
    }

    void setSimulation(OPGUIPCMSimulation::PCMSimulation *newSim) {
        sim = newSim;
    }

   OPGUIPCMSimulation::PCMSimulation* getSimulation() {
        return sim;
    }
    
    bool api_verify_path(const OpenAPI::OAIPathRequest& req, OpenAPI::OAIVerifyPath_200_response &resp, QString &errorMsg)
    {
        LOG_INFO("Initialising : api_verify_path");

        if(!req.isValid() || req.getPath().isEmpty()) {
            errorMsg = QString("Request or Path of folder is empty.");
            LOG_ERROR(errorMsg);
            return false;
        }

        OPGUIFileSystemManager::FileSystemManager fileSystemManager;
        QString realPath;
        bool isEmpty=true;

        if(!fileSystemManager.verifyPath(req.getPath(),req.isIsFullPath(),realPath,isEmpty,errorMsg)) {
            LOG_ERROR("Failed to verify path information.");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }

        resp.setRealPath(realPath);
        resp.setEmpty(isEmpty);
        resp.setOk(true);

        return true;

    }

    bool api_delete_information(const OpenAPI::OAIPathRequest& req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg)
    {
        LOG_INFO("Initialising : api_delete_information");

        if(!req.isValid() || req.getPath().isEmpty()) {
            errorMsg = QString("Request or Path of folder is empty.");
            LOG_ERROR(errorMsg);
            return false;
        }

        if(OPGUIFileSystemManager::FileSystemManager fileSystemManager; !fileSystemManager.deleteInformation(req.getPath(),errorMsg)) {
            LOG_ERROR("Failed to delete information.");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }
        
        resp.setResponse("Successfully deleted information under path: " + req.getPath());
        return true;
    }

    bool api_run_opSimulation(const QString &configPath, OpenAPI::OAIDefault200Response &resp, QString &errorMsg) {
        LOG_INFO("Initialising : api_run_opSimulation");

        QString path = configPath;

        if(path.isEmpty()) {
            path = OPGUICoreGlobalConfig::getInstance().fullPathOpSimulationManagerXml();
        }

        if(OPGUISimulationExecution::getInstance().executeSimulation(path, errorMsg)) {
            QString msg = "Simulations started correctly";
            resp.setResponse(msg);
            LOG_INFO(msg);
            return true;
        }

        resp.setResponse(errorMsg);
        LOG_ERROR(errorMsg);
        return false;
    }

    bool api_export_opSimulationManager_xml(OpenAPI::OAIOpSimulationManagerXmlRequest req,OpenAPI::OAIDefault200Response &resp, QString &errorMsg)
    {
        LOG_INFO("Initialising : api_export_opSimulationManager_xml");

        auto const& config = OPGUICoreGlobalConfig::getInstance();

        OPGUIPCMSimulation::PCMSimulation localSim;

        if (!req.isValid()){
            LOG_ERROR("Internal Error, api_export_opSimulationManager_xml request was not correctly set");
            return false;
        }

        req.setLogFileSimulationManager(config.pathLogFile());
        req.setLibraries(config.fullPathModulesFolder());
        req.setSimulation(config.fullPathOpSimulationExe());

        if(localSim.GenerateSimulationXml(req.getSimulationConfigs(),req.getLogLevel(),req.getLogFileSimulationManager(),
            req.getLibraries(),req.getSimulation(),OPGUICoreGlobalConfig::getInstance().pathOpenpassCore(),errorMsg))
        {
            resp.setResponse("opsimulation manager data exported correctly!");
            LOG_INFO("Xml file exported.");
            return true;
        }
        else
        {
            resp.setResponse("Xml file export failed");
            LOG_INFO("Xml file export failed.");
            return true;
        }
    }

    bool api_send_PCM_file(const OpenAPI::OAIPathRequest& req, OpenAPI::OAISelectedExperimentsRequest &resp, QString &errorMsg)
    {   
        auto const& config = OPGUICoreGlobalConfig::getInstance();
        LOG_INFO("Initialising : api_send_PCM_file");

        if(sim != nullptr)
        {
            LOG_INFO("Old Instance of PCMSimulation plugin found, deleting.");
            delete sim;
        }

        sim = new OPGUIPCMSimulation::PCMSimulation();

        if(!req.isValid() || req.getPath().isEmpty()){
            errorMsg="Path to pcm database in request is empty / invalid.";
            LOG_ERROR(errorMsg);
            return false;
        }

        auto pcm_db_path = req.getPath();
        LOG_INFO("Recieved path of PCM DB file : " + pcm_db_path);


        if(!sim->Init(pcm_db_path,config.fullPathModulesFolder(),config.pathConvertedCases(),config.fullPathOpSimulationExe(),errorMsg)){
            LOG_ERROR("There was an error during initialization of simulation properties:" + errorMsg);
            return false;
        }

        QStringList pcm_case_list;

        if( sim->LoadCasesFromPcmFile(pcm_db_path,pcm_case_list,errorMsg))
        {
            LOG_INFO("PCM Cases found : " + pcm_case_list.join(", "));
            resp.setSelectedExperiments(pcm_case_list);
            return true;
        }
        else
        {
            resp.setSelectedExperiments(QList<QString>());
            LOG_ERROR(errorMsg);
            return false;
        }
    }

    bool api_path_to_converted_cases(const OpenAPI::OAIPathRequest& req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg)
    {
        auto& config = OPGUICoreGlobalConfig::getInstance();
        LOG_INFO("Initialising : api_path_to_converted_cases");
        if(!req.isValid() || req.getPath().isEmpty())
        {
            errorMsg = "Error in setting path of converted cases. Path is empty / invalid.";
            LOG_ERROR(errorMsg);
            return false;
        }

        auto path_to_converted_cases = req.getPath();
        LOG_INFO("Path to converted cases request:" + path_to_converted_cases);

        if(sim != nullptr)
        {
            LOG_INFO("PCM Plugin already Initialised, changing the path to converted cases for current instance.");
            sim->SetPathToConvertedCases(path_to_converted_cases);              
        }
        config.setPathConvertedCases(path_to_converted_cases);
        LOG_DEBUG("PATH_CONVERTED_CASES has been correctly set in memory as:"+path_to_converted_cases); 

        if(config.modifyOrAddValueToConfigFile("PATH_CONVERTED_CASES", path_to_converted_cases))
        {
            LOG_INFO("PATH_CONVERTED_CASES has been set in configuration file as:"+path_to_converted_cases);
            resp.setResponse("PATH_CONVERTED_CASES has been correctly set.");
            return true;
        }
        else
        {
            errorMsg = "Failed to set PATH_CONVERTED_CASES in configuration file as:"+path_to_converted_cases;
            LOG_ERROR(errorMsg);
            return false;
        }
    }

    bool api_convert_to_configs(const OpenAPI::OAIConfigsRequest& req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg) 
    {
        auto const& config = OPGUICoreGlobalConfig::getInstance();
        LOG_INFO("Initialising : api_convert_to_configs");
        if(!req.isValid()){
            errorMsg = QString("Invalid JSON request");
            LOG_ERROR(errorMsg);
            return false;
        }

        if(req.getSelectedExperiments().empty())
        {
            errorMsg = QString("Config generation failed as no experiments where selected, please select at least one experiment.");
            LOG_ERROR(errorMsg);
            return false;
        }

        if(req.getSelectedSystems().empty() || req.getSelectedSystems().size()<2)
        {
            errorMsg = QString("Config generation failed since list of agents not properly set, please set at least two agents for the experiments.");
            LOG_ERROR(errorMsg);
            return false;
        }

        if(sim != nullptr)
        {
            QStringList listExperiments = req.getSelectedExperiments();

            //add the systems using the request... 
            sim->setAgentsCar1(QList<QString>{req.getSelectedSystems()[0].getFile()});
            sim->setAgentsCar2(QList<QString>{req.getSelectedSystems()[1].getFile()});
            if(req.getSelectedSystems().size()==2)
                sim->setAgentsOther(QList<QString>{req.getSelectedSystems()[1].getFile()});
            else
                sim->setAgentsOther(QList<QString>{req.getSelectedSystems()[2].getFile()});

            if(!sim->UIGenerateConfigOfPCMCases(listExperiments,errorMsg))
            {
                errorMsg = QString("Config generation failed with error:"+errorMsg);
                LOG_ERROR(errorMsg);
                return false;
            }

            resp.setResponse("Succesfully created config files under:" + config.pathConvertedCases());
            
            LOG_INFO("Results of config generation stored in : " + config.pathConvertedCases());
            return true;
        }
        else
        {
            errorMsg = QString("Config generation failed since PCM plugin was not initialised");
            LOG_ERROR(errorMsg);
            
            return false;
        }
    }

    bool api_export_to_simulations(const OpenAPI::OAISelectedExperimentsRequest& req, OpenAPI::OAIExportedSimConfigSettings &resp, QString &errorMsg)
    {
        auto const& config = OPGUICoreGlobalConfig::getInstance();

        //selected experiments is unused
        LOG_INFO("Initialising : api_export_to_simulations");
        if(!req.isValid() || req.getSelectedExperiments().isEmpty())
        {
            errorMsg = "List of experiments for config conversion is invalid / empty."; 
            LOG_ERROR(errorMsg);
            return false;
        }

        if(sim != nullptr)
        {
            sim->setPathModulesFolder(config.fullPathModulesFolder());
            sim->setPathOpSimulationExe(config.fullPathOpSimulationExe());

            if(sim->GenerateSimulationXmlUsingSharedConfig(errorMsg)){
                LOG_INFO("Simulation Manager Config File created as:"+sim->pathGeneratedOPSimulationManagerConfig);
                //resp.setResponse("Simulation Manager Config File created as:"+sim->pathGeneratedOPSimulationManagerConfig);
                resp.setPath(sim->pathGeneratedOPSimulationManagerConfig); 
                resp.setSelectedExperiments(req.getSelectedExperiments());

                QFile file(sim->pathGeneratedOPSimulationManagerConfig);
                QString xmlContent;

                if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                    // Handle the error
                    LOG_ERROR("Failed to open file");
                    return false;
                } else {
                    QTextStream stream(&file);
                    xmlContent = stream.readAll();
                    QByteArray base64ByteArray = xmlContent.toUtf8().toBase64();
                    auto base64QString = QString(base64ByteArray);
                    resp.setXmlContent(base64QString);
                    file.close();
                }

                return true;
            }
            else{
                LOG_ERROR(errorMsg);
                return false;
            }
        }
        else
        {
            errorMsg = "PCM Plugin is not initialised."; 
            LOG_ERROR(errorMsg);
            return false;
        }
    }

    bool api_get_components(QList<OpenAPI::OAIComponentUI> &resp, QString &errorMsg){
        auto const& config = OPGUICoreGlobalConfig::getInstance();
        LOG_INFO("Initialising : api_get_components");

        OPGUISystemEditor::OPGUISystemEditorImport editor;

        if(editor.loadComponentsFromDirectory(config.fullPathComponentsFolder(), resp, errorMsg)) {
            return true;
        } else {
            LOG_ERROR("Failed to load components under path: " + config.fullPathComponentsFolder());
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }
    };

    bool api_get_fileTree(const OpenAPI::OAIFileTreeRequest& req,OpenAPI::OAIFileTreeExtended &resp, QString &errorMsg){
        LOG_INFO("Initialising : api_get_fileTree");
        if(!req.isValid())
        {
            errorMsg = QString("Received JSON object file tree request is empty/invalid");
            LOG_ERROR(errorMsg);
            
            return false;
        }

        OPGUIFileSystemManager::FileSystemManager fileSystemManager;

        if(fileSystemManager.getFileTree(req,resp,errorMsg)) {
            return true;
        } else {
            LOG_ERROR("Failed to get file tree");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }
    };

    bool api_get_configElements(QList<OpenAPI::OAIConfigElement> &resp,QString &errorMsg){
        auto const& config = OPGUICoreGlobalConfig::getInstance();
        LOG_INFO("Initialising : api_get_config_elements");

        OPGUIGlobalSetupEditor::GlobalSetupEditor globalSetupEditor;

        if(globalSetupEditor.getUserConfigElements(resp,errorMsg)) {
            return true;
        } else {
            LOG_ERROR("Failed to load configuration elements under path: " + config.pathConfigFile());
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }
    };

    bool api_set_configElements(QList<OpenAPI::OAIConfigElement> req,OpenAPI::OAIDefault200Response &resp,QString &errorMsg){
        auto const& config = OPGUICoreGlobalConfig::getInstance();
        QString response;
        LOG_INFO("Initialising : api_set_config_elements");

        if(req.isEmpty())
        {
            errorMsg = QString("Received JSON list of config elements is empty");
            LOG_ERROR(errorMsg);
            return false;
        }

        OPGUIGlobalSetupEditor::GlobalSetupEditor globalSetupEditor;

        if(globalSetupEditor.setUserConfigElements(req,errorMsg,response)) {
            resp.setResponse(response);
            return true;
        } else {
            LOG_ERROR("Failed to save configuration elements under path:: " + config.pathConfigFile());
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }

    };

    bool api_get_system(const QString &req, OpenAPI::OAISystemUI &resp, QString &errorMsg){
        LOG_INFO("Initialising : api_get_system");
        
        if(req.isEmpty()) {
            errorMsg = QString("Path of file is empty.");
            LOG_ERROR(errorMsg);
            return false;
        }

        QString decodedPath = QUrl::fromPercentEncoding(req.toUtf8());

        OPGUISystemEditor::OPGUISystemEditorImport editor;

        if(editor.getSystemFromXml(decodedPath, resp, errorMsg)) {
            return true;
        } else {
            LOG_ERROR("Failed to get system from XML file");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }   
    }

    bool api_save_system(const OpenAPI::OAI_api_saveSystem_post_request& req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg){
        LOG_INFO("Initialising : api_save_system");
        if(req.getPath().isEmpty())
        {
            errorMsg = QString("Path of file is empty.");
            LOG_ERROR(errorMsg);
            return false;
        }
        if(!req.is_system_Set())
        {
            errorMsg = QString("System to save not valid/empty.");
            LOG_ERROR(errorMsg);
            return false;
        }


        if(OPGUISystemEditor::OPGUISystemEditorExport editor; !editor.saveSystemToXmlFile(req.getSystem(),req.getPath(),req.isIsNew(),errorMsg)) {
            LOG_ERROR("Failed to save system in XML file");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }

        resp.setResponse("System correctly saved under path:"+req.getPath());
        return true;
    };

    bool api_delete_system(const QString &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg){
        LOG_INFO("Initialising: api_delete_system");

        if (req.isEmpty()) {
            errorMsg = QString("Path of file is empty.");
            LOG_ERROR(errorMsg);
            return false;
        }

        QString decodedPath = QUrl::fromPercentEncoding(req.toUtf8());


        if(OPGUIFileSystemManager::FileSystemManager fileSystemManager; !fileSystemManager.deleteFile(decodedPath,errorMsg)) {
            LOG_ERROR("Failed to get file contents");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }
        
        resp.setResponse("System file successfully deleted at path: " + decodedPath);
        return true;
    };

    bool api_get_file_contents(const QString &reqPath,const bool reqEncoded, OpenAPI::OAIDefault200Response &resp, QString &errorMsg){
        LOG_INFO("Initialising: api_get_file_contents");
        if (reqPath.isEmpty()) {
            errorMsg = QString("Path of file is empty.");
            LOG_ERROR(errorMsg);
            return false;
        }

        QString decodedPath = QUrl::fromPercentEncoding(reqPath.toUtf8());

        QString decodedContents;


        if(OPGUIFileSystemManager::FileSystemManager fileSystemManager; !fileSystemManager.getFileContents(decodedPath,reqEncoded,decodedContents,errorMsg)) {
            LOG_ERROR("Failed to get file contents");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }

        resp.setResponse(decodedContents);

        return true;
    };

    bool api_get_value_list(const QString &listName, OpenAPI::OAI_api_getValueList_get_200_response &resp, QString &errorMsg) {
        LOG_INFO("Initialising: api_get_value_list");
        if (listName.isEmpty()) {
            errorMsg = "List name is empty.";
            LOG_ERROR(errorMsg);
            return false;
        }

        QList<QString> values;

        if(OPGUIValueList::getInstance().getValues(listName, values, errorMsg)){
             resp.setListValues(values);
             return true;
        }
        else {
            LOG_ERROR("Failed to get list values");
            if (!errorMsg.isEmpty()) {
                LOG_ERROR("Details: " + errorMsg); 
            }
            return false;
        }
    }

    bool api_save_config_editor(const OpenAPI::OAI_api_configEditor_post_request &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg) {
        // Validate the request path
        QString path = req.getPath();
        if (path.isEmpty()) {
            errorMsg = "Path is empty";
            LOG_ERROR(errorMsg);
            return false;
        }

        // Get the configuration object from the request
        OpenAPI::OAIConfigEditorObject configEditorObj = req.getConfigEditorObject();
        if (!configEditorObj.isValid()) {
            errorMsg = "Invalid configuration object in request";
            LOG_ERROR(errorMsg);
            return false;
        }

        // Delegate the saving logic to OPGUIConfigEditorExport
        OPGUIConfigEditor::OPGUIConfigEditorExport configExporter;
        if (!configExporter.saveConfigEditor(configEditorObj, path, errorMsg)) {
            LOG_ERROR("Failed to save configuration: " + errorMsg);
            return false;
        }

        QString successResponse = "Successfully saved configurations to " + path;

        resp.setResponse(successResponse);
        LOG_INFO(successResponse);

        return true;
    }

    bool api_import_config_editor(const QString &path, OpenAPI::OAIConfigEditorObject &resp, QString &errorMsg) {
        LOG_INFO("Initialising: api_import_config_editor");
        
        if (path.isEmpty()) {
            errorMsg = "Path is empty";
            LOG_ERROR(errorMsg);
            return false;
        }

        // Create instance of the importer
        OPGUIConfigEditor::OPGUIConfigEditorImport configImporter;

        // Initialize a config object
        OpenAPI::OAIConfigEditorObject configObj;

        QString decodedPath = QUrl::fromPercentEncoding(path.toUtf8());

        // Process the config from the provided path
        if (!configImporter.loadConfiguration(decodedPath,configObj, errorMsg)) {
            LOG_ERROR("Failed to import configuration: " + errorMsg);
            return false;
        }

        // Set the response object
        resp = configObj;

        LOG_INFO("Successfully imported configuration from " + path);
        return true;
    }
}