/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include <QList>

/**
 * Namespace OPGUIInit encapsulates initialization functions for the opGUI application,
 * ensuring the environment is correctly set up before use.
 */
namespace OPGUIInit {

    /**
     * Initializes the opGUI application by setting up necessary configurations,
     * checking for required files, and preparing the runtime environment.
     *
     * @return Returns true if the initialization was successful, false otherwise.
     */
    bool Initialise_OPGUI();

    /**
     * Checks whether the given string contains any placeholder characters.
     * Placeholder characters might be used to mark where dynamic content should be inserted.
     *
     * @param str The string to be checked for placeholders.
     * @return Returns true if placeholders are detected, false otherwise.
     */
    bool containsPlaceholder(const QString& str);

    /**
     * Verifies the existence of the specified directory and creates it if it does not exist
     * and if creation is requested.
     *
     * @param path The path to the directory to check or create.
     * @param description Description of the directory for logging purposes.
     * @param createIfNotExists Indicates whether to create the directory if it does not exist.
     * @return Returns true if the directory exists or was successfully created, false if creation failed or it does not exist.
     */
    bool checkAndCreateDir(const QString& path, const QString& description, bool createIfNotExists);

    /**
     * Checks for the existence of a specified file.
     *
     * @param filePath The path of the file to check.
     * @param description Description of the file for logging purposes.
     * @return Returns true if the file exists, false otherwise.
     */
    bool checkFileExists(const QString& filePath, const QString& description);

} // namespace OPGUIInit
