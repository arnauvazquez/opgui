/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "helpers.h"

#include <QTextStream>
#include <QFile>
#include <QIODevice>
#include <QDir>

namespace Helpers {

QString normalizePathToUnix(const QString& path) {
    QString normalizedPath = path;

    // Replace backslashes with forward slashes
    normalizedPath.replace("\\", "/");

    // Remove leading 'C:' if present, but keep the initial '/'
    if (normalizedPath.startsWith("C:", Qt::CaseInsensitive)) {
        normalizedPath.remove(0, 2);
    }

    return normalizedPath;
}

QString readXmlFile(const QString &filePath){
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return QString();  
    }
    QTextStream in(&file);
    QString xmlContent = in.readAll();
    file.close();
    return xmlContent;
}

QString joinPaths(const QString& path1, const QString& path2) {
    QDir dir(path1);
    return QDir::cleanPath(path1 + QStringLiteral("/") + path2);
}

}