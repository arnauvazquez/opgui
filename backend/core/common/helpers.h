/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>


/**
 * @namespace Helpers
 * Contains utility functions that perform common tasks and operations used across the application.
 */
namespace Helpers {

    /**
     * Normalizes a file path from Windows-style to Unix-style.
     * This function converts backslashes to slashes and removes the "C:" prefix if present,
     * making paths more consistent for processing in environments that use Unix-style paths.
     *
     * @param path The Windows-style file path to be normalized.
     * @return A QString representing the normalized Unix-style path.
     */
    QString normalizePathToUnix(const QString& path);

     /**
     * Reads an XML file and returns its contents as a QString.
     *
     * @param filePath The path to the XML file.
     * @return The content of the XML file as a QString.
     */
    QString readXmlFile(const QString &filePath);

    /**
     * Joins two file paths and returns the combined path.
     *
     * @param path1 The first part of the path.
     * @param path2 The second part of the path.
     * @return The combined file path as a QString.
     */
    QString joinPaths(const QString& path1, const QString& path2);
}
