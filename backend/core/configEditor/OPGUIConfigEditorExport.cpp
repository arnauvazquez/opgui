/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include <QDir>
#include "OPGUIConfigEditorExport.h"
#include "OPGUISimulationConfigFileExport.h"
#include "OPGUIQtLogger.h"
#include "OPGUIConstants.h"

namespace OPGUIConfigEditor {

bool OPGUIConfigEditorExport::saveConfigEditor(
    const OpenAPI::OAIConfigEditorObject &configEditorObj,
    const QString &path,
    QString &errorMsg) const 
{
    // Ensure the directory exists
    QFileInfo fileInfo(path);
    QString dirPath = fileInfo.dir().absolutePath();
    if (!ensureDirectoryExists(dirPath, errorMsg)) {
        return false;
    }

    // For now, we only handle the simulation config file
    const OpenAPI::OAISimulationConfigFile &simConfig = configEditorObj.getSimulationConfig();
    
    // Create the simulation config file exporter
    OPGUISimulationConfigFileExport simConfigExporter;
    
    // Export the simulation config file
    if (!simConfigExporter.saveConfigFileToXmlFile(simConfig, path, errorMsg)) {
        LOG_ERROR(QString("Failed to save simulation config file to %1: %2").arg(path, errorMsg));
        return false;
    }

    LOG_INFO(QString("Successfully saved simulation config file to %1").arg(path));
    return true;
}

bool OPGUIConfigEditorExport::ensureDirectoryExists(const QString &path, QString &errorMsg) const {
    QDir dir(path);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            errorMsg = QString("Failed to create directory: %1").arg(path);
            LOG_ERROR(errorMsg);
            return false;
        }
        LOG_INFO(QString("Created directory: %1").arg(path));
    }
    return true;
}

} // namespace OPGUIConfigEditor