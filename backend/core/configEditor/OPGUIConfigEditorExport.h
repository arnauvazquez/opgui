/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include "OAIConfigEditorObject.h"

namespace OPGUIConfigEditor {

class OPGUIConfigEditorExport {
public:
    OPGUIConfigEditorExport() = default;

    /**
     * Saves the configuration editor object to the specified path
     * Currently only handles the simulationConfigFile member
     *
     * @param configEditorObj The configuration object to save
     * @param path The path where to save the configuration
     * @param errorMsg Error message if operation fails
     * @return bool True if successful, false otherwise
     */
    bool saveConfigEditor(const OpenAPI::OAIConfigEditorObject &configEditorObj, const QString &path, QString &errorMsg) const;

private:
    /**
     * Creates the directory at the specified path if it doesn't exist
     *
     * @param path The directory path to create
     * @param errorMsg Error message if operation fails
     * @return bool True if successful or directory already exists, false otherwise
     */
    bool ensureDirectoryExists(const QString &path, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor