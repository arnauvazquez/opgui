/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include "OPGUIConfigEditorImport.h"
#include "OPGUIQtLogger.h"
#include "OPGUISimulationConfigFileImport.h"

namespace OPGUIConfigEditor {

bool OPGUIConfigEditorImport::loadConfiguration(const QString& path, OpenAPI::OAIConfigEditorObject& configObj, QString& errorMsg) const {
    LOG_INFO("Starting configuration import from: " + path);

    if (path.isEmpty()) {
        errorMsg = "Configuration file path is empty";
        LOG_ERROR(errorMsg);
        return false;
    }

    //leave the rest of objects temporary empty
    configObj.setScenario("");
    configObj.setSceneryConfig("");
    configObj.setSystemConfig("");
    configObj.setVehiclesCatalog("");
    configObj.setPedestriansCatalog("");
    configObj.setProfilesCatalog("");

    // Load simulation config
    if (!loadSimulationConfig(path, configObj, errorMsg)) {
        LOG_ERROR("Failed to load simulation configuration: " + errorMsg);
        return false;
    }

    LOG_INFO("Successfully loaded configuration from: " + path);
    return true;
}

bool OPGUIConfigEditorImport::loadSimulationConfig(const QString& configsPath, OpenAPI::OAIConfigEditorObject& configObj, QString& errorMsg) const {
    OPGUISimulationConfigFileImport simConfigImporter;
    OpenAPI::OAISimulationConfigFile simConfig;

    if (!simConfigImporter.getConfigFromXml(configsPath, simConfig, errorMsg)) {
        errorMsg = QString("Failed to import simulation config from %1: %2").arg(configsPath, errorMsg);
        return false;
    }

    configObj.setSimulationConfig(simConfig);
    LOG_DEBUG("Successfully loaded simulation configuration");
    return true;
}

} // namespace OPGUIConfigEditor