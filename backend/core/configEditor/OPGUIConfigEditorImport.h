/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include "OAIConfigEditorObject.h"
#include "OAISimulationConfigFile.h"
#include "OPGUISimulationConfigFileImport.h"

namespace OPGUIConfigEditor {

/**
 * @class OPGUIConfigEditorImport
 * @brief Handles importing and processing of simulation configuration files.
 */
class OPGUIConfigEditorImport {
public:
    /**
     * @brief Default constructor.
     */
    OPGUIConfigEditorImport() = default;

    /**
     * @brief Loads and processes a configuration from the specified path.
     * 
     * @param path The file path to load the configuration from.
     * @param configObj The configuration object to populate.
     * @param errorMsg Output parameter for error messages.
     * @return true if successful, false otherwise.
     */
    bool loadConfiguration(const QString& path, OpenAPI::OAIConfigEditorObject& configObj, QString& errorMsg) const;

private:
    /**
     * @brief Loads and processes the simulation configuration part.
     * 
     * @param simConfigPath Path to the configurations folder.
     * @param configObj The configuration object to update.
     * @param errorMsg Output parameter for error messages.
     * @return true if successful, false otherwise.
     */
    bool loadSimulationConfig(const QString& configsPath, OpenAPI::OAIConfigEditorObject& configObj, QString& errorMsg) const;
};

} // namespace OPGUIConfigEditor