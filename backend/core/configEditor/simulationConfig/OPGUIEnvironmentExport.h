/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QList>
#include <QString>
#include <QXmlStreamWriter>

#include "OAIEnvironment.h"
#include "OAITimeOfDay.h"
#include "OAIVisibilityDistance.h"
#include "OAIFriction.h"
#include "OAIWeather.h"
#include "OAITurningRate.h"

/**
 * Namespace OPGUIConfigEditor encapsulates the functionality related to configuration editing
 * within the OPGUI application.
 */
namespace OPGUIConfigEditor {

/**
 * Class OPGUIEnvironmentExport handles the export of environmental settings into an XML format.
 * It provides functionalities to serialize various environmental components such as weather conditions,
 * time of day settings, visibility distances, friction values, and traffic rules.
 */
class OPGUIEnvironmentExport {
public:
    /**
     * Constructor for the OPGUIEnvironmentExport class.
     */
    OPGUIEnvironmentExport();

    /**
     * Exports the given environment settings into an XML document.
     *
     * @param xmlWriter Reference to an XML stream writer to write the data.
     * @param environment The environmental settings to be exported.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportEnvironment(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIEnvironment &environment, QString &errorMsg) const;

private:
    // Helper functions for exporting specific environmental components.
    bool exportTimeOfDays(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAITimeOfDay> &timeOfDays, QString &errorMsg) const;
    bool exportVisibilityDistances(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAIVisibilityDistance> &visibilityDistances, QString &errorMsg) const;
    bool exportFrictions(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAIFriction> &frictions, QString &errorMsg) const;
    bool exportWeathers(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAIWeather> &weathers, QString &errorMsg) const;
    bool exportTrafficRules(QXmlStreamWriter &xmlWriter, const QString &trafficRules, QString &errorMsg) const;
    bool exportTurningRates(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAITurningRate> &turningRates, QString &errorMsg) const;

    // Helper functions for validating JSON structures before exporting.
    bool isValidEnvironmentJSON(const OpenAPI::OAIEnvironment &environment, QString &errorMsg) const;
    bool isValidTimeOfDaysJSON(const QList<OpenAPI::OAITimeOfDay> &timeOfDays, QString &errorMsg) const;
    bool isValidVisibilityDistancesJSON(const QList<OpenAPI::OAIVisibilityDistance> &visibilityDistances, QString &errorMsg) const;
    bool isValidFrictionsJSON(const QList<OpenAPI::OAIFriction> &frictions, QString &errorMsg) const;
    bool isValidWeathersJSON(const QList<OpenAPI::OAIWeather> &weathers, QString &errorMsg) const;
    bool isValidTrafficRulesJSON(const QString &trafficRules, QString &errorMsg) const;
    bool isValidTurningRatesJSON(const QList<OpenAPI::OAITurningRate> &turningRates, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor