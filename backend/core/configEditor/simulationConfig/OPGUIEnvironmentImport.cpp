/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QList>
#include <QString>
#include <cmath>

#include "OPGUIEnvironmentImport.h"
#include "OPGUIValueList.h"
#include "OPGUIConstants.h"

namespace OPGUIConfigEditor {

OPGUIEnvironmentImport::OPGUIEnvironmentImport() = default;

//import functions
bool OPGUIEnvironmentImport::importEnvironment(const QDomElement &environmentEl, OpenAPI::OAIEnvironment &environment, QString &errorMsg) const{
    if (!isValidEnvironmentXml(environmentEl, errorMsg)) {
        return false;
    }

    QList<OpenAPI::OAITimeOfDay> timeOfDays;
    if (QDomElement timeOfDaysEl = environmentEl.firstChildElement("TimeOfDays"); !importTimeOfDays(timeOfDaysEl, timeOfDays, errorMsg)) {
        return false;
    }
    environment.setTimeOfDays(timeOfDays); 

    QList<OpenAPI::OAIVisibilityDistance> visibilityDistances;
    if (QDomElement visibilityDistancesEl = environmentEl.firstChildElement("VisibilityDistances"); !importVisibilityDistances(visibilityDistancesEl, visibilityDistances, errorMsg)) {
        return false;
    }
    environment.setVisibilityDistances(visibilityDistances);

    QList<OpenAPI::OAIFriction> frictions;
    if (QDomElement frictionsEl = environmentEl.firstChildElement("Frictions"); !importFrictions(frictionsEl, frictions, errorMsg)) {
        return false;
    }
    environment.setFrictions(frictions); 

    QList<OpenAPI::OAIWeather> weathers;
    if (QDomElement weathersEl = environmentEl.firstChildElement("Weathers"); !importWeathers(weathersEl, weathers, errorMsg)) {
        return false;
    }
    environment.setWeathers(weathers); 

    QString trafficRules;
    if (QDomElement trafficRulesEl = environmentEl.firstChildElement("TrafficRules"); !importTrafficRules(trafficRulesEl, trafficRules, errorMsg)) {
        return false;
    }
    environment.setTrafficRules(trafficRules); 

    QList<OpenAPI::OAITurningRate> turningRates;
    // Check if the TurningRates element exists and is valid
    if (QDomElement turningRatesEl = environmentEl.firstChildElement("TurningRates"); !turningRatesEl.isNull()) {
        if (!importTurningRates(turningRatesEl, turningRates, errorMsg)) {
            return false;
        }

        // Only set TurningRates if the imported list is not empty
        //if (!turningRates.isEmpty()) {
            environment.setTurningRates(turningRates);
        //}
    }

    return true; 
};

bool OPGUIEnvironmentImport::importTimeOfDays(const QDomElement &timeOfDaysEl, QList<OpenAPI::OAITimeOfDay> &timeOfDays, QString &errorMsg) const {
    if (!isValidTimeOfDaysXml(timeOfDaysEl, errorMsg)) {
        return false;
    }

    QDomNodeList timeOfDayList = timeOfDaysEl.elementsByTagName("TimeOfDay");
    for (int i = 0; i < timeOfDayList.count(); ++i) {
        QDomElement timeOfDayEl = timeOfDayList.at(i).toElement();

        double probability = QString::number(timeOfDayEl.attribute("Probability").toDouble(), 'f', 2).toDouble();
        int value = timeOfDayEl.attribute("Value").toInt();

        OpenAPI::OAITimeOfDay timeOfDay;
        timeOfDay.setProbability(static_cast<float>(probability));
        timeOfDay.setValue(value);

        timeOfDays.append(timeOfDay);
    }

    return true;
};

bool OPGUIEnvironmentImport::importVisibilityDistances(const QDomElement &visibilityDistancesEl, QList<OpenAPI::OAIVisibilityDistance> &visibilityDistances, QString &errorMsg) const {
    if (!isValidVisibilityDistancesXml(visibilityDistancesEl, errorMsg)) {
        return false;
    }

    QDomNodeList distanceList = visibilityDistancesEl.elementsByTagName("VisibilityDistance");
    for (int i = 0; i < distanceList.count(); ++i) {
        QDomElement distanceEl = distanceList.at(i).toElement();

        double probability = QString::number(distanceEl.attribute("Probability").toDouble(), 'f', 2).toDouble();
        int value = distanceEl.attribute("Value").toInt();

        OpenAPI::OAIVisibilityDistance visibilityDistance;
        visibilityDistance.setProbability(static_cast<float>(probability));
        visibilityDistance.setValue(value);

        visibilityDistances.append(visibilityDistance);
    }

    return true;
};

bool OPGUIEnvironmentImport::importFrictions(const QDomElement &frictionsEl, QList<OpenAPI::OAIFriction> &frictions, QString &errorMsg) const{
    if (!isValidFrictionsXml(frictionsEl, errorMsg)) {
        return false;
    }

    QDomNodeList frictionList = frictionsEl.elementsByTagName("Friction");
    for (int i = 0; i < frictionList.count(); ++i) {
        QDomElement frictionEl = frictionList.at(i).toElement();

        double probability = QString::number(frictionEl.attribute("Probability").toDouble(), 'f', 2).toDouble();
        double value = frictionEl.attribute("Value").toDouble();

        OpenAPI::OAIFriction friction;
        friction.setProbability(static_cast<float>(probability));
        friction.setValue(static_cast<float>(value));

        frictions.append(friction);
    }

    return true;
};

bool OPGUIEnvironmentImport::importWeathers(const QDomElement &weathersEl, QList<OpenAPI::OAIWeather> &weathers, QString &errorMsg) const{
    if (!isValidWeathersXml(weathersEl, errorMsg)) {
        return false;
    }

    QDomNodeList weatherList = weathersEl.elementsByTagName("Weather");
    for (int i = 0; i < weatherList.count(); ++i) {
        QDomElement weatherEl = weatherList.at(i).toElement();

        double probability = QString::number(weatherEl.attribute("Probability").toDouble(), 'f', 2).toDouble();
        QString value = weatherEl.attribute("Value");

        OpenAPI::OAIWeather weather;
        weather.setProbability(static_cast<float>(probability));
        weather.setValue(value);

        weathers.append(weather);
    }
    return true;
};

bool OPGUIEnvironmentImport::importTrafficRules(const QDomElement &trafficRulesEl, QString &trafficRules, QString &errorMsg) const{
    if (!isValidTrafficRulesXml(trafficRulesEl, errorMsg)) {
        return false;
    }

    trafficRules = trafficRulesEl.text().trimmed();
    return true;
}

bool OPGUIEnvironmentImport::importTurningRates(const QDomElement &turningRatesEl, QList<OpenAPI::OAITurningRate> &turningRates, QString &errorMsg) const {
    if (!isValidTurningRatesXml(turningRatesEl, errorMsg)) {
        return false;
    }

    QDomNodeList turningRateList = turningRatesEl.elementsByTagName("TurningRate");
    for (int i = 0; i < turningRateList.count(); ++i) {
        QDomElement turningRateEl = turningRateList.at(i).toElement();
        
        QString incoming = turningRateEl.attribute("Incoming");
        QString outgoing = turningRateEl.attribute("Outgoing");
        double weight = turningRateEl.attribute("Weight").toDouble();

        OpenAPI::OAITurningRate turningRate;

        turningRate.setIncoming(incoming);
        turningRate.setOutgoing(outgoing);
        turningRate.setWeight(static_cast<float>(weight));

        turningRates.append(turningRate);
    }

    return true;
}

//validation functions
bool OPGUIEnvironmentImport::isValidTimeOfDaysXml(const QDomElement &timeOfDaysEl, QString &errorMsg) const{
    if (timeOfDaysEl.isNull()) {
        errorMsg = "TimeOfDays element is missing.";
        return false;
    }

    QDomNodeList timeOfDayList = timeOfDaysEl.elementsByTagName("TimeOfDay");
    if (timeOfDayList.isEmpty()) {
        errorMsg = "No TimeOfDay elements found.";
        return false;
    }

    double probabilitySum = 0.0;
    for (int i = 0; i < timeOfDayList.count(); ++i) {
        QDomElement timeOfDayEl = timeOfDayList.at(i).toElement();

        if (!timeOfDayEl.hasAttribute("Probability") || !timeOfDayEl.hasAttribute("Value")) {
            errorMsg = QString("TimeOfDay element at index %1 is missing Probability or Value attributes.").arg(i);
            return false;
        }

        bool ok;
        double probability = timeOfDayEl.attribute("Probability").toDouble(&ok);
        if (!ok || probability < 0 || probability > 1) {
            errorMsg = QString("TimeOfDay element at index %1 has an invalid Probability value.").arg(i);
            return false;
        }

        int value = timeOfDayEl.attribute("Value").toInt(&ok);
        if (!ok || value < 1 || value > 24) {
            errorMsg = QString("TimeOfDay element at index %1 has an invalid Value.").arg(i);
            return false;
        }
        probabilitySum += probability;
    }

    if (std::fabs(probabilitySum - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of times of day does not equal 1. Total sum: %1").arg(probabilitySum);
        return false;
    }

    return true;
};

bool OPGUIEnvironmentImport::isValidVisibilityDistancesXml(const QDomElement &visibilityDistancesEl, QString &errorMsg) const{
    if (visibilityDistancesEl.isNull()) {
        errorMsg = "VisibilityDistances element is missing.";
        return false;
    }

    QDomNodeList visibilityDistanceList = visibilityDistancesEl.elementsByTagName("VisibilityDistance");
    if (visibilityDistanceList.isEmpty()) {
        errorMsg = "No VisibilityDistance elements found.";
        return false;
    }

    double probabilitySum = 0.0;
    for (int i = 0; i < visibilityDistanceList.count(); ++i) {
        QDomElement visibilityDistanceEl = visibilityDistanceList.at(i).toElement();
        
        // Check if required attributes exist
        if (!visibilityDistanceEl.hasAttribute("Probability") || !visibilityDistanceEl.hasAttribute("Value")) {
            errorMsg = QString("VisibilityDistance element at index %1 is missing Probability or Value attributes.").arg(i);
            return false;
        }

        bool ok;
        double probability = visibilityDistanceEl.attribute("Probability").toDouble(&ok);
        if (!ok || probability < 0 || probability > 1) {
            errorMsg = QString("VisibilityDistance element at index %1 has an invalid Probability value.").arg(i);
            return false;
        }

        int value = visibilityDistanceEl.attribute("Value").toInt(&ok);
        if (!ok || value < 0) {
            errorMsg = QString("VisibilityDistance element at index %1 has an invalid Value.").arg(i);
            return false;
        }

        probabilitySum += probability;
    }

    if (std::fabs(probabilitySum - 1.0) > OPGUIConstants::EPSILON) { // Adjusted comparison for potential floating-point precision issues
        errorMsg = QString("The sum of probabilities of visibility distances does not equal 1.0. Total sum: %1").arg(probabilitySum);
        return false;
    }

    return true;
};

bool OPGUIEnvironmentImport::isValidFrictionsXml(const QDomElement &frictionsEl, QString &errorMsg) const{
    if (frictionsEl.isNull()) {
        errorMsg = "Frictions element is missing.";
        return false;
    }

    QDomNodeList frictionList = frictionsEl.elementsByTagName("Friction");
    if (frictionList.isEmpty()) {
        errorMsg = "No Friction elements found.";
        return false;
    }

    double probabilitySum = 0.0;
    for (int i = 0; i < frictionList.count(); ++i) {
        QDomElement frictionEl = frictionList.at(i).toElement();
        
        // Check if required attributes exist
        if (!frictionEl.hasAttribute("Probability") || !frictionEl.hasAttribute("Value")) {
            errorMsg = QString("Friction element at index %1 is missing Probability or Value attributes.").arg(i);
            return false;
        }

        bool ok;
        double probability = frictionEl.attribute("Probability").toDouble(&ok);
        if (!ok || probability < 0 || probability > 1) {
            errorMsg = QString("Friction element at index %1 has an invalid Probability value.").arg(i);
            return false;
        }

        double value = frictionEl.attribute("Value").toDouble(&ok);
        if (!ok || value < 0.0 || value > 1.0) { // Assuming Value should be between 0.0 and 1.0
            errorMsg = QString("Friction element at index %1 has an invalid Value.").arg(i);
            return false;
        }

        probabilitySum += probability;
    }

    // Check if the sum of probabilities equals 1.0, using a tolerance for floating-point comparison
    if (std::fabs(probabilitySum - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of frictions does not equal 1.0. Total sum: %1").arg(probabilitySum);
        return false;
    }

    return true;
};

bool OPGUIEnvironmentImport::isValidWeathersXml(const QDomElement &weathersEl, QString &errorMsg) const{
    if (weathersEl.isNull()) {
        errorMsg = "Weathers element is missing.";
        return false;
    }

    QDomNodeList weatherList = weathersEl.elementsByTagName("Weather");
    if (weatherList.isEmpty()) {
        errorMsg = "No Weather elements found.";
        return false;
    }

    double probabilitySum = 0.0;
    for (int i = 0; i < weatherList.count(); ++i) {
        QDomElement weatherEl = weatherList.at(i).toElement();
        
        // Check if required attributes exist
        if (!weatherEl.hasAttribute("Probability") || !weatherEl.hasAttribute("Value")) {
            errorMsg = QString("Weather element at index %1 is missing Probability or Value attributes.").arg(i);
            return false;
        }

        bool ok;
        double probability = weatherEl.attribute("Probability").toDouble(&ok);
        if (!ok || probability < 0 || probability > 1) {
            errorMsg = QString("Weather element at index %1 has an invalid Probability value.").arg(i);
            return false;
        }

        QString value = weatherEl.attribute("Value");
        if (value.isEmpty()) {
            errorMsg = QString("Weather element at index %1 has an empty Value.").arg(i);
            return false;
        }

        if (!OPGUIValueList::getInstance().hasValue("weathers", value)) {
            errorMsg = QString("Invalid weather value '%1' at position %2. Available options are: %3.")
                .arg(value, QString::number(i), OPGUIValueList::getInstance().listToString("weathers"));
            return false;
        }

        probabilitySum += probability;
    }

    if (std::fabs(probabilitySum - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of weathers does not equal 1.0. Total sum: %1").arg(probabilitySum);
        return false;
    }

    return true;
};

bool OPGUIEnvironmentImport::isValidTrafficRulesXml(const QDomElement &trafficRulesEl, QString &errorMsg) const{
    if (trafficRulesEl.isNull()) {
        errorMsg = "TrafficRules element is missing.";
        return false;
    }

    QString value = trafficRulesEl.text().trimmed(); 

    if (value.isEmpty()) {
        errorMsg = "TrafficRules value is empty.";
        return false;
    }

    if (!OPGUIValueList::getInstance().hasValue("trafficRules", value)) {
        errorMsg = QString("Invalid Traffic Rules value '%1'. Available options are: %2.")
            .arg(value)
            .arg(OPGUIValueList::getInstance().listToString("trafficRules"));
        return false;
    }

    return true; 
};

bool OPGUIEnvironmentImport::isValidTurningRatesXml(const QDomElement &turningRatesEl, QString &errorMsg) const {
    QDomNodeList turningRateList = turningRatesEl.elementsByTagName("TurningRate");
    if (turningRateList.isEmpty()) {
        errorMsg = "No TurningRate elements found.";
        return true;
    }

    for (int i = 0; i < turningRateList.count(); ++i) {
        QDomElement turningRateEl = turningRateList.at(i).toElement();

        // Check if required attributes exist
        if (!turningRateEl.hasAttribute("Incoming") || !turningRateEl.hasAttribute("Outgoing") || !turningRateEl.hasAttribute("Weight")) {
            errorMsg = QString("TurningRate element at index %1 is missing Outgoing, Incoming or Weight attributes.").arg(i);
            return false;
        }

        if (turningRateEl.attribute("Incoming").isEmpty()) {
            errorMsg = QString("TurningRate element at index %1 has empty Incoming value.").arg(i);
            return false;
        }

        if (turningRateEl.attribute("Outgoing").isEmpty()) {
            errorMsg = QString("TurningRate element at index %1 has empty Outgoing value.").arg(i);
            return false;
        }

        bool ok;
        double weight = turningRateEl.attribute("Weight").toDouble(&ok);
        if (!ok || weight < 0) {
            errorMsg = QString("TurningRate element at index %1 has an invalid Weight value.").arg(i);
            return false;
        }
    }

    return true;
}

bool OPGUIEnvironmentImport::isValidEnvironmentXml(const QDomElement &environmentEl, QString &errorMsg) const{
    if (environmentEl.isNull()) {
        errorMsg = "Environment element is missing.";
        return false;
    }
    return true;
};

}