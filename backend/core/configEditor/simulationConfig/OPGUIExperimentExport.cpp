/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUIExperimentExport.h"
#include "OPGUIValueList.h"

namespace OPGUIConfigEditor {

bool OPGUIExperimentExport::exportExperiment(QXmlStreamWriter& xmlWriter, const OpenAPI::OAIExperiment& experiment, QString& errorMsg) const {
    if (!isValidExperimentJSON(experiment, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Experiment");

    xmlWriter.writeTextElement("ExperimentID", QString::number(experiment.getExperimentId()));
    xmlWriter.writeTextElement("NumberOfInvocations", QString::number(experiment.getNumberOfInvocations()));
    xmlWriter.writeTextElement("RandomSeed", QString::number(experiment.getRandomSeed()));

    // Export Libraries using getters
    exportLibraries(xmlWriter, experiment.getLibraries());

    xmlWriter.writeEndElement(); // Experiment

    return true;
}

bool OPGUIExperimentExport::isValidExperimentJSON(const OpenAPI::OAIExperiment& experiment, QString& errorMsg) const {
    if (experiment.getExperimentId() < 0) {
        errorMsg = "Experiment ID must be 0 or greater.";
        return false;
    }
    if (experiment.getNumberOfInvocations() < 1) {
        errorMsg = "Number of invocations must be a positive integer.";
        return false;
    }
    if (experiment.getRandomSeed() < 0) {
        errorMsg = "Random seed must be a non-negative integer.";
        return false;
    }

    // Validate Libraries object using getters
    if (!isValidLibrariesJSON(experiment.getLibraries(), errorMsg)) {
        return false;
    }

    return true;
}

void OPGUIExperimentExport::exportLibraries(QXmlStreamWriter& xmlWriter, const OpenAPI::OAIExperiment_libraries& libraries) const {
    xmlWriter.writeStartElement("Libraries");

    // Access library details using getters
    if (!libraries.getWorldLibrary().isEmpty()) {
        xmlWriter.writeTextElement("WorldLibrary", libraries.getWorldLibrary());
    }
    
    if (!libraries.getDataBufferLibrary().isEmpty()) {
        xmlWriter.writeTextElement("DataBufferLibrary", libraries.getDataBufferLibrary());
    }
    
    if (!libraries.getStochasticsLibrary().isEmpty()) {
        xmlWriter.writeTextElement("StochasticsLibrary", libraries.getStochasticsLibrary());
    }

    xmlWriter.writeEndElement(); // Libraries
}

bool OPGUIExperimentExport::isValidLibrariesJSON(const OpenAPI::OAIExperiment_libraries& libraries, QString& errorMsg) const {
    if (!libraries.getDataBufferLibrary().isEmpty()) {
        if (!OPGUIValueList::getInstance().hasValue("dataBufferLibraries", libraries.getDataBufferLibrary())) {
            errorMsg = QString("Invalid dataBuffer Library value %1. Available options are: %2.")
                .arg(libraries.getDataBufferLibrary(), 
                     OPGUIValueList::getInstance().listToString("dataBufferLibraries"));
            return false;
        }
    }

    if (!libraries.getStochasticsLibrary().isEmpty()) {
        if (!OPGUIValueList::getInstance().hasValue("stochasticsLibraries", libraries.getStochasticsLibrary())) {
            errorMsg = QString("Invalid stochastics Library value %1. Available options are: %2.")
                .arg(libraries.getStochasticsLibrary(), 
                     OPGUIValueList::getInstance().listToString("stochasticsLibraries"));
            return false;
        }
    }

    if (!libraries.getWorldLibrary().isEmpty()) {
        if (!OPGUIValueList::getInstance().hasValue("worldLibraries", libraries.getWorldLibrary())) {
            errorMsg = QString("Invalid WorldLibrary value %1. Available options are: %2.")
                .arg(libraries.getWorldLibrary(), 
                     OPGUIValueList::getInstance().listToString("worldLibraries"));
            return false;
        }
    }

    return true;
}

} // namespace OPGUIConfigEditor
