/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUIExperimentImport.h"
#include "OPGUIValueList.h"
#include "OPGUIConstants.h"

namespace OPGUIConfigEditor {

bool OPGUIExperimentImport::importExperiment(const QDomElement &experimentEl, OpenAPI::OAIExperiment &experiment, QString &errorMsg) const {
    if (!isValidExperimentXml(experimentEl, errorMsg)) {
        return false;
    }

    QDomElement experimentIDEl = experimentEl.firstChildElement("ExperimentID");
    QDomElement numberOfInvocationsEl = experimentEl.firstChildElement("NumberOfInvocations");
    QDomElement randomSeedEl = experimentEl.firstChildElement("RandomSeed");

    int experimentID = experimentIDEl.text().toInt();
    int numberOfInvocations = numberOfInvocationsEl.text().toInt();
    int randomSeed = randomSeedEl.text().toInt();

    experiment.setExperimentId(experimentID);
    experiment.setNumberOfInvocations(numberOfInvocations);
    experiment.setRandomSeed(randomSeed);

    QDomElement librariesEl = experimentEl.firstChildElement("Libraries");
    OpenAPI::OAIExperiment_libraries libraries;
    if (!importLibraries(librariesEl, libraries, errorMsg)) {
        return false;
    }
    experiment.setLibraries(libraries);

    return true;
}

bool OPGUIExperimentImport::importLibraries(const QDomElement &librariesEl, OpenAPI::OAIExperiment_libraries &libraries, QString &errorMsg) const {
    if (!isValidLibrariesXml(librariesEl, errorMsg)) {
        return false;
    }

    QDomElement worldLibraryEl = librariesEl.firstChildElement("WorldLibrary");
    QDomElement dataBufferLibraryEl = librariesEl.firstChildElement("DataBufferLibrary");
    QDomElement stochasticsLibraryEl = librariesEl.firstChildElement("StochasticsLibrary");

    // Set WorldLibrary value
    if (worldLibraryEl.isNull() || worldLibraryEl.text().isEmpty()) {
        libraries.setWorldLibrary(OPGUIConstants::defaultWorldLibrary);
    } else {
        libraries.setWorldLibrary(worldLibraryEl.text());
    }

    // Set DataBufferLibrary value
    if (dataBufferLibraryEl.isNull() || dataBufferLibraryEl.text().isEmpty()) {
        libraries.setDataBufferLibrary(OPGUIConstants::defaultDataBufferLibrary);
    } else {
        libraries.setDataBufferLibrary(dataBufferLibraryEl.text());
    }

    // Set StochasticsLibrary value
    if (stochasticsLibraryEl.isNull() || stochasticsLibraryEl.text().isEmpty()) {
        libraries.setStochasticsLibrary(OPGUIConstants::defaultStochasticsLibrary);
    } else {
        libraries.setStochasticsLibrary(stochasticsLibraryEl.text());
    }

    return true;
}

bool OPGUIExperimentImport::isValidExperimentXml(const QDomElement &experimentEl, QString &errorMsg) const {
    if (experimentEl.isNull()) {
        errorMsg = "Experiment element is missing.";
        return false;
    }

    if (!isValidExperimentID(experimentEl, errorMsg)) {
        return false;
    }
    if (!isValidNumberOfInvocations(experimentEl, errorMsg)) {
        return false;
    }
    if (!isValidRandomSeed(experimentEl, errorMsg)) {
        return false;
    }
    if (!isValidLibrariesXml(experimentEl.firstChildElement("Libraries"), errorMsg)) {
        return false;
    }

    return true;
}

bool OPGUIExperimentImport::isValidExperimentID(const QDomElement &experimentEl, QString &errorMsg) const {
    if (QDomElement experimentIDEl = experimentEl.firstChildElement("ExperimentID"); experimentIDEl.isNull()) {
        errorMsg = "ExperimentID element is missing.";
        return false;
    } else {
        bool ok;
        experimentIDEl.text().toInt(&ok);
        int experimentID = experimentIDEl.text().toInt(&ok);
        if (!ok || experimentID < 0) {
            errorMsg = "ExperimentID can't be a negative number.";
            return false;
        }
    }
    return true;
}

bool OPGUIExperimentImport::isValidNumberOfInvocations(const QDomElement &experimentEl, QString &errorMsg) const {
    if (QDomElement numberOfInvocationsEl = experimentEl.firstChildElement("NumberOfInvocations"); numberOfInvocationsEl.isNull()) {
        errorMsg = "NumberOfInvocations element is missing.";
        return false;
    } else {
        bool ok;
        int numberOfInvocations = numberOfInvocationsEl.text().toInt(&ok);
        if (!ok || numberOfInvocations < 1) {
            errorMsg = "NumberOfInvocations must be a positive integer.";
            return false;
        }
    }
    return true;
}

bool OPGUIExperimentImport::isValidRandomSeed(const QDomElement &experimentEl, QString &errorMsg) const {
    if (QDomElement randomSeedEl = experimentEl.firstChildElement("RandomSeed"); randomSeedEl.isNull()) {
        errorMsg = "RandomSeed element is missing.";
        return false;
    } else {
        bool ok;
        randomSeedEl.text().toUInt(&ok);
        if (!ok) {
            errorMsg = "Random seed must be a non-negative integer.";
            return false;
        }
    }
    return true;
}

bool OPGUIExperimentImport::isValidLibrariesXml(const QDomElement &librariesEl, QString &errorMsg) const {
    if (librariesEl.isNull()) {
        errorMsg = "Libraries element is missing.";
        return false;
    }

    if (!librariesEl.firstChildElement("WorldLibrary").isNull()) {
        QString worldLibraryText = librariesEl.firstChildElement("WorldLibrary").text();
        if (!worldLibraryText.isEmpty() && !OPGUIValueList::getInstance().hasValue("worldLibraries", worldLibraryText)) {
            errorMsg = QString("Invalid WorldLibrary value '%1'. Available options are: %2.")
                .arg(worldLibraryText)
                .arg(OPGUIValueList::getInstance().listToString("worldLibraries"));
            return false;
        }
    }

    if (!librariesEl.firstChildElement("DataBufferLibrary").isNull()) {
        QString dataBufferLibraryText = librariesEl.firstChildElement("DataBufferLibrary").text();
        if (!dataBufferLibraryText.isEmpty() && !OPGUIValueList::getInstance().hasValue("dataBufferLibraries", dataBufferLibraryText)) {
            errorMsg = QString("Invalid dataBuffer Library value '%1'. Available options are: %2.")
                .arg(dataBufferLibraryText)
                .arg(OPGUIValueList::getInstance().listToString("dataBufferLibraries"));
            return false;
        }
    }

    if (!librariesEl.firstChildElement("StochasticsLibrary").isNull()) {
        QString stochasticsLibraryText = librariesEl.firstChildElement("StochasticsLibrary").text();
        if (!stochasticsLibraryText.isEmpty() && !OPGUIValueList::getInstance().hasValue("stochasticsLibraries", stochasticsLibraryText)) {
            errorMsg = QString("Invalid stochastics Library value '%1'. Available options are: %2.")
                .arg(stochasticsLibraryText)
                .arg(OPGUIValueList::getInstance().listToString("stochasticsLibraries"));
            return false;
        }
    }

    return true;
}

} // namespace OPGUIConfigEditor
