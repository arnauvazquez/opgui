/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include "OPGUIObservationsExport.h"
#include "OPGUIValueList.h"
#include <QDebug>
#include "OPGUIQtLogger.h"


namespace OPGUIConfigEditor {

bool OPGUIObservationsExport::exportObservations(QXmlStreamWriter& xmlWriter, const OpenAPI::OAIObservations& observations, QString& errorMsg) const {

    if (!isValidObservationsJSON(observations, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Observations");

    // Assuming each observation has defined subobjects like `log` and `entityRepository`
    if (!exportLog(xmlWriter, observations.getLog(), errorMsg)) {
        return false;
    }

    if (!exportEntityRepository(xmlWriter, observations.getEntityRepository())) {
        return false;
    }

    xmlWriter.writeEndElement(); // Observations
    return true;
}

bool OPGUIObservationsExport::isValidObservationsJSON(const OpenAPI::OAIObservations& observations, QString& errorMsg) const {
    if (!isValidLogJSON(observations.getLog(), errorMsg)) {
        return false;
    }

    if (!isValidEntityRepositoryJSON(observations.getEntityRepository(), errorMsg)) {
        return false;
    }

    return true;
}

bool OPGUIObservationsExport::isValidLogJSON(const OpenAPI::OAILog& log, QString& errorMsg) const {
    // Check if fileName is not empty
    if (log.getFileName().isEmpty()) {
        errorMsg = "FileName cannot be empty.";
        return false;
    }

    // If the group is enabled, check that the values are not empty
    for (const auto& group : log.getLoggingGroups()) {
        if (group.getGroup().isEmpty()) {
            errorMsg = QString("Logging group has no name.");
            return false;
        }
        if (group.isEnabled() && group.getValues().isEmpty()) {
            errorMsg = QString("Logging group '%1' is enabled but has no values.").arg(group.getGroup());
            return false;
        }
    }
    return true;
}

bool OPGUIObservationsExport::isValidEntityRepositoryJSON(const OpenAPI::OAIEntityRepository& entityRepository, QString& errorMsg) const {
    if (!entityRepository.getWritePersistentEntities().isEmpty()) {
        const QString value = entityRepository.getWritePersistentEntities();
        QList<QString> validOptions;

        if (!OPGUIValueList::getInstance().getValues("WritePersistentEntitiesModes", validOptions, errorMsg)) {
            return false;
        }

        if (!validOptions.contains(value)) {
            QString allowedValues = validOptions.join(", ");
            errorMsg = QString("Invalid value for WritePersistentEntities: '%1'. Please use one of the allowed options: %2.")
                           .arg(value, allowedValues);
            return false;
        }
    }

    return true;
}

bool OPGUIObservationsExport::exportLog(QXmlStreamWriter& xmlWriter, const OpenAPI::OAILog& log, QString& errorMsg) const {
    xmlWriter.writeStartElement("Observation");
    xmlWriter.writeTextElement("Library", "Observation_Log");

    xmlWriter.writeStartElement("Parameters");

    // Exporting fileName as a String parameter
    xmlWriter.writeStartElement("String");
    xmlWriter.writeAttribute("Key", "OutputFilename");
    xmlWriter.writeAttribute("Value", log.getFileName());
    xmlWriter.writeEndElement(); // String

    // Exporting loggingCyclesToCsv as a Bool parameter
    xmlWriter.writeStartElement("Bool");
    xmlWriter.writeAttribute("Key", "LoggingCyclicsToCsv");
    xmlWriter.writeAttribute("Value", log.isLoggingCyclesToCsv() ? "true" : "false");
    xmlWriter.writeEndElement(); // Bool

    // Exporting details for all groups
    for (const auto& group : log.getLoggingGroups()) {
        QString values = group.getValues();
        xmlWriter.writeStartElement("StringVector");
        xmlWriter.writeAttribute("Key", "LoggingGroup_" + group.getGroup());
        xmlWriter.writeAttribute("Value", values);
        xmlWriter.writeEndElement(); // StringVector
    }

    // Include the list of only the enabled logging groups
    QStringList enabledGroups;
    for (const auto& group : log.getLoggingGroups()) {
        if (group.isEnabled()) {
            enabledGroups.append(group.getGroup());
        }
    }

    std::sort(enabledGroups.begin(), enabledGroups.end());

    if (!enabledGroups.isEmpty()) {
        xmlWriter.writeStartElement("StringVector");
        xmlWriter.writeAttribute("Key", "LoggingGroups");
        xmlWriter.writeAttribute("Value", enabledGroups.join(","));
        xmlWriter.writeEndElement(); // StringVector
    }

    xmlWriter.writeEndElement(); // Parameters
    xmlWriter.writeEndElement(); // Observation
    return true;
}

bool OPGUIObservationsExport::exportEntityRepository(QXmlStreamWriter& xmlWriter, const OpenAPI::OAIEntityRepository& entityRepository) const {
    if (entityRepository.getFileNamePrefix().isEmpty() && entityRepository.getWritePersistentEntities().isEmpty()) {
        return true; // Nothing to export since both are empty.
    }

    xmlWriter.writeStartElement("Observation");
    xmlWriter.writeTextElement("Library", "Observation_EntityRepository");

    xmlWriter.writeStartElement("Parameters");

    // Export only non-empty attributes.
    if (!entityRepository.getFileNamePrefix().isEmpty()) {
        xmlWriter.writeStartElement("String");
        xmlWriter.writeAttribute("Key", "FilenamePrefix");
        xmlWriter.writeAttribute("Value", entityRepository.getFileNamePrefix());
        xmlWriter.writeEndElement(); // String
    }

    if (!entityRepository.getWritePersistentEntities().isEmpty()) {
        xmlWriter.writeStartElement("String");
        xmlWriter.writeAttribute("Key", "WritePersistentEntities");
        xmlWriter.writeAttribute("Value", entityRepository.getWritePersistentEntities());
        xmlWriter.writeEndElement(); // String
    }

    xmlWriter.writeEndElement(); // Parameters
    xmlWriter.writeEndElement(); // Observation
    return true;
}

} // namespace OPGUIConfigEditor
