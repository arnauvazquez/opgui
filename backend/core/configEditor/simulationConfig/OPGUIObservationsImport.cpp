/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include "OPGUIObservationsImport.h"
#include "OPGUIValueList.h"
#include "OPGUIQtLogger.h"

namespace OPGUIConfigEditor {

bool OPGUIObservationsImport::importObservations(const QDomElement &observationsEl, OpenAPI::OAIObservations &observations, QString &errorMsg) const {
    if (!isValidObservationsXml(observationsEl, errorMsg)) {
        return false;
    }

    QDomElement observationEl = observationsEl.firstChildElement("Observation");
    if (observationEl.isNull()) {
        errorMsg = "No Observation elements found.";
        return false;
    }

    OpenAPI::OAILog log;
    OpenAPI::OAIEntityRepository entityRepository;
    bool logImported = false;
    bool entityRepoImported = false;

    while (!observationEl.isNull()) {
        QDomElement libraryEl = observationEl.firstChildElement("Library");
        QString libraryType = libraryEl.text();

        if (libraryType == "Observation_Log" && !logImported) {
            if (!importLog(observationEl.firstChildElement("Parameters"), log, errorMsg)) {
                return false;
            }
            logImported = true;
        } else if (libraryType == "Observation_EntityRepository" && !entityRepoImported) {
            if (!importEntityRepository(observationEl.firstChildElement("Parameters"), entityRepository, errorMsg)) {
                return false;
            }
            entityRepoImported = true;
        }

        observationEl = observationEl.nextSiblingElement("Observation");
    }

    if (!logImported) {
        errorMsg = "Log observation not found.";
        return false;
    }

    if (!entityRepoImported) {
        entityRepository.setFileNamePrefix("");
        entityRepository.setWritePersistentEntities("");
    }

    observations.setLog(log);
    observations.setEntityRepository(entityRepository);
    return true;
}

bool OPGUIObservationsImport::importEntityRepository(const QDomElement &paramsEl, OpenAPI::OAIEntityRepository &entityRepository, QString &errorMsg) const {
    if (paramsEl.isNull()) {
        return true;
    }

    if (!isValidEntityRepositoryXml(paramsEl, errorMsg)) {
        return false;
    }

    QString filenamePrefix="";
    QString writePersistentEntities="";

    QDomElement child = paramsEl.firstChildElement("String");
    while (!child.isNull()) {
        QString key = child.attribute("Key");
        QString value = child.attribute("Value");

        if (key == "FilenamePrefix") {
            filenamePrefix = value;    
        } else if (key == "WritePersistentEntities") {
            writePersistentEntities = value; 
        }

        entityRepository.setFileNamePrefix(filenamePrefix);
        entityRepository.setWritePersistentEntities(writePersistentEntities);

        child = child.nextSiblingElement("String");
    }
    return true;
}

bool OPGUIObservationsImport::importLog(const QDomElement &logEl, OpenAPI::OAILog &log, QString &errorMsg) const {
    if (!isValidLogXml(logEl, errorMsg)) {
        return false;
    }

    QDomElement cyclesEl = logEl.firstChildElement("Bool");
    bool loggingCyclesToCsv = (cyclesEl.attribute("Value").toLower() == "true");
    log.setLoggingCyclesToCsv(loggingCyclesToCsv);

    QDomElement fileNameEl = logEl.firstChildElement("String");
    QString fileName = fileNameEl.attribute("Value");
    log.setFileName(fileName);

    QList<OpenAPI::OAILoggingGroup> loggingGroups;
    if (!importLoggingGroups(logEl, loggingGroups, errorMsg)) {
        return false;
    }

    log.setLoggingGroups(loggingGroups);
    return true;
}

bool OPGUIObservationsImport::importLoggingGroups(const QDomElement &paramsEl, QList<OpenAPI::OAILoggingGroup> &loggingGroups, QString &errorMsg) const {
    if (!isValidLoggingGroupsXml(paramsEl, errorMsg)) {
        return false;
    }

    QString enabledGroupsList;
    QStringList enabledGroups;
    QDomElement logGroupEl = paramsEl.firstChildElement("StringVector");

    while (!logGroupEl.isNull()) {
        QString key = logGroupEl.attribute("Key");

        if (key == "LoggingGroups") {
            enabledGroupsList = logGroupEl.attribute("Value");
            enabledGroups = enabledGroupsList.split(',', Qt::SkipEmptyParts);
        }

        logGroupEl = logGroupEl.nextSiblingElement("StringVector");
    }

    logGroupEl = paramsEl.firstChildElement("StringVector");
    while (!logGroupEl.isNull()) {
        if (QString key = logGroupEl.attribute("Key"); key.startsWith("LoggingGroup_")) {
            QString group = key.mid(QString("LoggingGroup_").length());
            QString values = logGroupEl.attribute("Value");
            bool isEnabled = enabledGroups.contains(group);

            OpenAPI::OAILoggingGroup loggingGroup;
            loggingGroup.setGroup(group);
            loggingGroup.setValues(values);
            loggingGroup.setEnabled(isEnabled);

            loggingGroups.append(loggingGroup);
        }

        logGroupEl = logGroupEl.nextSiblingElement("StringVector");
    }
    return true;
}

bool OPGUIObservationsImport::isValidObservationsXml(const QDomElement &observationsEl, QString &errorMsg) const {
    if (observationsEl.isNull()) {
        errorMsg = "Observations element is missing.";
        return false;
    }
    return true;
}

bool OPGUIObservationsImport::isValidEntityRepositoryXml(const QDomElement &paramsEl, QString &errorMsg) const {
    if (paramsEl.isNull()) {
        return true;
    }

    QDomElement child = paramsEl.firstChildElement("String");

     QString writePersistentEntitiesVal = "";

    while (!child.isNull()) {
        QString key = child.attribute("Key");
        QString value = child.attribute("Value");

        if (key == "FilenamePrefix" && value.isEmpty()) {
                errorMsg = "FilenamePrefix value is empty.";
                return false;
        } else if (key == "WritePersistentEntities" && value.isEmpty()) {
                errorMsg = "WritePersistentEntities value is empty.";
                return false;
        } else if (key == "WritePersistentEntities"){
            writePersistentEntitiesVal = value;
        }

        child = child.nextSiblingElement("String");
    }

    if (!writePersistentEntitiesVal.isEmpty()) {
        QList<QString> validOptions;

        if (!OPGUIValueList::getInstance().getValues("WritePersistentEntitiesModes", validOptions, errorMsg)) {
            return false;
        }

        if (!validOptions.contains(writePersistentEntitiesVal)) {
            QString allowedValues = validOptions.join(", ");
            errorMsg = QString("Invalid value for WritePersistentEntities: '%1'. Please use one of the allowed options: %2.")
                           .arg(writePersistentEntitiesVal, allowedValues);
            return false;
        }
    }
    return true;
}

bool OPGUIObservationsImport::isValidLogXml(const QDomElement &logEl, QString &errorMsg) const {
    if (logEl.isNull()) {
        errorMsg = "Log element (parameters element) is missing.";
        return false;
    }

    if (QDomElement cyclesEl = logEl.firstChildElement("Bool"); cyclesEl.isNull() || cyclesEl.attribute("Key") != "LoggingCyclicsToCsv" || cyclesEl.attribute("Value").isEmpty()) {
        errorMsg = "LoggingCyclicsToCsv element is missing or empty.";
        return false;
    }

    if (QDomElement fileNameEl = logEl.firstChildElement("String"); fileNameEl.isNull() || fileNameEl.attribute("Key") != "OutputFilename" || fileNameEl.attribute("Value").isEmpty()) {
        errorMsg = "OutputFilename element is missing or empty.";
        return false;
    }

    if (!isValidLoggingGroupsXml(logEl, errorMsg)) {
        return false;
    }
    return true;
}

bool OPGUIObservationsImport::isValidLoggingGroupsXml(const QDomElement &logEl, QString &errorMsg) const {
    QDomElement logGroupEl = logEl.firstChildElement("StringVector");
    if (logGroupEl.isNull()) {
        errorMsg = "Logging groups elements are missing.";
        return false;
    }

    QStringList definedGroups;
    QString enabledGroupsList;
    bool foundEnabledGroups = false;

    while (!logGroupEl.isNull()) {
        QString key = logGroupEl.attribute("Key");
        if (!logGroupEl.hasAttribute("Key") || !logGroupEl.hasAttribute("Value")) {
            errorMsg = "Logging group element is missing required attributes.";
            return false;
        }

        if (key == "LoggingGroups") {
            enabledGroupsList = logGroupEl.attribute("Value");
            foundEnabledGroups = true;
        } else if (key.startsWith("LoggingGroup_")) {
            definedGroups.append(key.mid(13));  // Remove the prefix "LoggingGroup_"
        }

        logGroupEl = logGroupEl.nextSiblingElement("StringVector");
    }

    if (!foundEnabledGroups) {
        errorMsg = "LoggingGroups entry specifying enabled groups is missing.";
        return false;
    }

    QStringList enabledGroups = enabledGroupsList.split(",");
    for (const QString& group : enabledGroups) {
        if (!definedGroups.contains(group)) {
            errorMsg = QString("Enabled logging group '%1' is listed but not defined.").arg(group);
            return false;
        }
    }
    return true;
}

} // namespace OPGUIConfigEditor
