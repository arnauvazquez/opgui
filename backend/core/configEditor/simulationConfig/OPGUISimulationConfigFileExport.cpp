/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDomDocument>
#include <QBuffer>

#include "OPGUISimulationConfigFileExport.h"
#include "OPGUIEnvironmentExport.h"
#include "OPGUISpawnersExport.h"
#include "OPGUIExperimentExport.h"
#include "OPGUIObservationsExport.h"
#include "OPGUIQtLogger.h"
#include "OPGUIConstants.h"
#include "helpers.h"


namespace OPGUIConfigEditor {

bool OPGUISimulationConfigFileExport::saveConfigFileToXmlFile(const OpenAPI::OAISimulationConfigFile &config, const QString &path, QString &errorMsg) const {
    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.setAutoFormattingIndent(4);
    xmlWriter.setCodec("UTF-8");
    xmlWriter.writeStartDocument("1.0");
    
    if (!exportSimulationConfig(xmlWriter, config, errorMsg)) {
        errorMsg = QString("Failed exporting simulationConfig to file %1: %2").arg(path).arg(errorMsg);
        buffer.close();
        return false;
    }   

    xmlWriter.writeEndDocument();
    buffer.close();

    QString fullPath = Helpers::joinPaths(path,OPGUIConstants::simConfigFileName);

    QFile file(fullPath);    
    if (!file.open(QIODevice::WriteOnly)) {
        errorMsg = QString("Failed to open file %1 for writing").arg(fullPath);
        return false; 
    }
    file.write(xmlData);
    file.close();

    return true;
}

bool OPGUISimulationConfigFileExport::exportSimulationConfig(
    QXmlStreamWriter &xmlWriter, 
    const OpenAPI::OAISimulationConfigFile &config, 
    QString &errorMsg) const 
{
    xmlWriter.writeStartElement("simulationConfig");
    xmlWriter.writeAttribute("SchemaVersion", OPGUIConstants::simulationConfigSchema);

    // Export profiles catalog
    if (!exportProfilesCatalog(xmlWriter, config, errorMsg)) {
        return false;
    }

    // Export experiment
    OPGUIExperimentExport experimentExporter;
    if (!experimentExporter.exportExperiment(xmlWriter, config.getExperiment(), errorMsg)) {
        return false;
    }

    // Export scenario
    xmlWriter.writeStartElement("Scenario");
    xmlWriter.writeTextElement("OpenScenarioFile", config.getScenario().getOpenScenarioFile());
    xmlWriter.writeEndElement(); // Scenario

    // Export environment
    OPGUIEnvironmentExport environmentExporter;
    if (!environmentExporter.exportEnvironment(xmlWriter, config.getEnvironment(), errorMsg)) {
        return false;
    }

    // Export observations
    OPGUIObservationsExport observationsExporter;
    if (!observationsExporter.exportObservations(xmlWriter, config.getObservations(), errorMsg)) {
        return false;
    }

    // Export spawners
    OPGUISpawnerExport spawnerExporter;
    const auto &spawners = config.getSpawners();

    if (!spawners.isEmpty()) {
        if (!spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg)) {
            return false;
        }
    } else {
        xmlWriter.writeEmptyElement("Spawners");
    }

    xmlWriter.writeEndElement(); // simulationConfig
    return true;
}

bool OPGUISimulationConfigFileExport::isValidProfilesCatalog(
    const QString &catalogPath, 
    QString &errorMsg) const 
{
    if (catalogPath.isEmpty()) {
        errorMsg = "ProfilesCatalog path is empty.";
        return false;
    }
    return true;
}

bool OPGUISimulationConfigFileExport::exportProfilesCatalog(
    QXmlStreamWriter &xmlWriter, 
    const OpenAPI::OAISimulationConfigFile &config, 
    QString &errorMsg) const 
{
    QString catalogPath = config.getProfilesCatalog();
    if (!isValidProfilesCatalog(catalogPath, errorMsg)) {
        return false;
    }

    xmlWriter.writeTextElement("ProfilesCatalog", catalogPath);
    return true;
}

} // namespace OPGUIConfigEditor