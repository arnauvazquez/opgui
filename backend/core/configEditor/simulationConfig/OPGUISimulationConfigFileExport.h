/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QXmlStreamWriter>
#include "OAISimulationConfigFile.h"

namespace OPGUIConfigEditor {

/**
 * Class OPGUISimulationConfigFileExport handles exporting simulation configuration settings to XML format.
 */
class OPGUISimulationConfigFileExport {
public:
    /**
     * Default constructor.
     */
    OPGUISimulationConfigFileExport() = default;

    bool saveConfigFileToXmlFile(const OpenAPI::OAISimulationConfigFile &config, const QString &path, QString &errorMsg) const;

    /**
     * Exports simulation configuration to XML.
     *
     * @param xmlWriter The XML stream writer to write the data.
     * @param config The simulation configuration to export.
     * @param errorMsg Output parameter for error messages.
     * @return True if export succeeds, false otherwise.
     */
    bool exportSimulationConfig(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISimulationConfigFile &config, QString &errorMsg) const;

private:

    /**
     * Validates the profiles catalog value.
     *
     * @param catalogPath The catalog path to validate.
     * @param errorMsg Output parameter for error messages.
     * @return True if valid, false otherwise.
     */
    bool isValidProfilesCatalog(const QString &catalogPath, QString &errorMsg) const;

    /**
     * Exports profiles catalog information.
     *
     * @param xmlWriter The XML stream writer.
     * @param config The config object containing the catalog info.
     * @param errorMsg Output parameter for error messages.
     * @return True if export succeeds, false otherwise.
     */
    bool exportProfilesCatalog(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISimulationConfigFile &config, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor