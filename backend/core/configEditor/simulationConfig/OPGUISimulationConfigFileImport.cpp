/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUISimulationConfigFileImport.h"
#include "OPGUIEnvironmentImport.h"
#include "OPGUISpawnersImport.h"
#include "OPGUIExperimentImport.h"
#include "OPGUIObservationsImport.h"
#include "OPGUIQtLogger.h"
#include "helpers.h"
#include "OPGUIConstants.h"

namespace OPGUIConfigEditor {

bool OPGUISimulationConfigFileImport::importSimulationConfig(
    const QDomElement &configEl, 
    OpenAPI::OAISimulationConfigFile &config, 
    QString &errorMsg) const 
{

    if (configEl.isNull()) {
        errorMsg = "simulationConfig element is missing.";
        return false;
    }

    // Import profiles catalog
    if (!importProfilesCatalog(configEl, config, errorMsg)) {
        return false;
    }

    // Import experiment
    OPGUIExperimentImport experimentImporter;
    OpenAPI::OAIExperiment experiment;
    QDomElement experimentEl = configEl.firstChildElement("Experiment");
    if (experimentEl.isNull()) {
        errorMsg = "Experiment element is missing.";
        return false;
    }
    if (!experimentImporter.importExperiment(experimentEl, experiment, errorMsg)) {
        return false;
    }
    config.setExperiment(experiment);

    // Import scenario
    QDomElement scenarioEl = configEl.firstChildElement("Scenario");
    if (scenarioEl.isNull()) {
        errorMsg = "Scenario element is missing.";
        return false;
    }
    OpenAPI::OAIScenario scenario;
    QString scenarioFile = scenarioEl.firstChildElement("OpenScenarioFile").text();
    if (scenarioFile.isEmpty()) {
        errorMsg = "OpenScenarioFile value is empty.";
        return false;
    }
    scenario.setOpenScenarioFile(scenarioFile);
    config.setScenario(scenario);

    // Import environment
    OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomElement environmentEl = configEl.firstChildElement("Environment");
    if (environmentEl.isNull()) {
        errorMsg = "Environment element is missing.";
        return false;
    }
    if (!environmentImporter.importEnvironment(environmentEl, environment, errorMsg)) {
        return false;
    }
    config.setEnvironment(environment);

    // Import observations
    OPGUIObservationsImport observationsImporter;
    OpenAPI::OAIObservations observations;
    QDomElement observationsEl = configEl.firstChildElement("Observations");
    if (observationsEl.isNull()) {
        errorMsg = "Observations element is missing.";
        return false;
    }
    if (!observationsImporter.importObservations(observationsEl, observations, errorMsg)) {
        return false;
    }
    config.setObservations(observations);

    // Import spawners
    OPGUISpawnerImport spawnerImporter;
    QList<OpenAPI::OAISpawner> spawners;
    QDomElement spawnersEl = configEl.firstChildElement("Spawners");
    if (!spawnersEl.isNull()) {  // Check if the element is not null
        if (!spawnerImporter.importSpawners(spawnersEl, spawners, errorMsg)) {
            return false;
        }
        config.setSpawners(spawners);
    } else {
        config.setSpawners(spawners);
    }

    return true;
}

bool OPGUISimulationConfigFileImport::importProfilesCatalog(
    const QDomElement &configEl, 
    OpenAPI::OAISimulationConfigFile &config, 
    QString &errorMsg) const 
{
    QDomElement catalogEl = configEl.firstChildElement("ProfilesCatalog");
    if(catalogEl.isNull()){
        errorMsg="ProfilesCatalog element is missing";
        return false;
    }

    QString catalogPath = catalogEl.text();

    if (!isValidProfilesCatalog(catalogPath, errorMsg)) {
        return false;
    }

    config.setProfilesCatalog(catalogPath);
    return true;
}

bool OPGUISimulationConfigFileImport::isValidProfilesCatalog(
    const QString &catalogPath, 
    QString &errorMsg) const 
{
    if (catalogPath.isEmpty()) {
        errorMsg = "ProfilesCatalog path is empty.";
        return false;
    }
    
    return true;
}

bool OPGUISimulationConfigFileImport::getConfigFromXml(const QString &path, OpenAPI::OAISimulationConfigFile &config, QString &errorMsg) const {
    QString fullPath = Helpers::joinPaths(path,OPGUIConstants::simConfigFileName);

    LOG_DEBUG("Parsing " + fullPath + " simulation config XML file");

    QString xmlContent="";
    
    if (xmlContent = Helpers::readXmlFile(fullPath); xmlContent.isEmpty()) {
        errorMsg = QString("SimulationConfig XML file is empty: %1").arg(fullPath);
        return false;
    } 
    
    QDomDocument doc;
    if (!doc.setContent(xmlContent)) {
        errorMsg = QString("Invalid XML content in simulationConfig XML file: %1").arg(fullPath);
        return false;
    }

    QDomElement configElement = doc.documentElement();
    
    if (configElement.tagName() != "simulationConfig") {
        errorMsg = QString("The root element is not <simulationConfig> as expected in file: %1").arg(fullPath);
        return false;
    }

    if (!importSimulationConfig(configElement, config, errorMsg)) {
        errorMsg = QString("Failed to parse simulationConfig from file: %1. Error: %2").arg(fullPath).arg(errorMsg);
        return false;
    }

    return true;
}

} // namespace OPGUIConfigEditor