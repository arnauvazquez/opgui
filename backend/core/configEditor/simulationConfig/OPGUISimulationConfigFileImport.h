/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QDomElement>
#include "OAISimulationConfigFile.h"

namespace OPGUIConfigEditor {

/**
 * Class OPGUISimulationConfigFileImport handles importing simulation configuration settings from XML format.
 */
class OPGUISimulationConfigFileImport {
public:
    /**
     * Default constructor.
     */
    OPGUISimulationConfigFileImport() = default;

    bool getConfigFromXml(const QString &path, OpenAPI::OAISimulationConfigFile &config, QString &errorMsg) const;

    /**
     * Imports simulation configuration from XML.
     *
     * @param configEl The XML element containing the simulation config.
     * @param config The object to populate with the imported data.
     * @param errorMsg Output parameter for error messages.
     * @return True if import succeeds, false otherwise.
     */
    bool importSimulationConfig(const QDomElement &configEl, OpenAPI::OAISimulationConfigFile &config, QString &errorMsg) const;

private:
    /**
     * Imports the profiles catalog information.
     *
     * @param configEl The XML element containing catalog info.
     * @param config The config object to populate.
     * @param errorMsg Output parameter for error messages.
     * @return True if import succeeds, false otherwise.
     */
    bool importProfilesCatalog(const QDomElement &configEl, OpenAPI::OAISimulationConfigFile &config, QString &errorMsg) const;

    /**
     * Validates profiles catalog value.
     *
     * @param catalogPath The catalog path to validate.
     * @param errorMsg Output parameter for error messages.
     * @return True if valid, false otherwise.
     */
    bool isValidProfilesCatalog(const QString &catalogPath, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor