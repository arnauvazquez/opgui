/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include "OPGUISpawnersExport.h"
#include "OPGUIValueList.h"
#include "OPGUIQtLogger.h"

namespace OPGUIConfigEditor {

bool OPGUISpawnerExport::exportSpawner(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISpawner &spawner, QString &errorMsg) const {
    if (!isValidSpawnerJSON(spawner, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Spawner");
    
    xmlWriter.writeTextElement("Library", spawner.getLibrary());
    xmlWriter.writeTextElement("Type", spawner.getType());
    xmlWriter.writeTextElement("Priority", QString::number(spawner.getPriority()));
    
    if (!spawner.getProfile().isEmpty()) {
        xmlWriter.writeTextElement("Profile", spawner.getProfile());
    }

    xmlWriter.writeEndElement(); // Spawner

    return true;
}

bool OPGUISpawnerExport::exportSpawners(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAISpawner> &spawners, QString &errorMsg) const {
    if (!isValidSpawnersJSON(spawners, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Spawners");

    for (const auto &spawner : spawners) {
        if (!exportSpawner(xmlWriter, spawner, errorMsg)) {
            return false;
        }
    }

    xmlWriter.writeEndElement(); // Spawners

    return true;
}

bool OPGUISpawnerExport::isValidSpawnerJSON(const OpenAPI::OAISpawner &spawner, QString &errorMsg) const {
    // Check library first
    if (!isValidLibrary(spawner.getLibrary(), errorMsg)) {
        return false;
    }

    // Check type
    if (!isValidType(spawner.getType(), errorMsg)) {
        return false;
    }

    // Check priority
    if (!isValidPriority(spawner.getPriority(), errorMsg)) {
        return false;
    }

    // Check profile if present
    if (!spawner.getProfile().isEmpty() && !isValidProfile(spawner.getProfile(), errorMsg)) {
        return false;
    }

    // Finally check overall spawner validity
    if (!spawner.isValid()) {
        errorMsg = "Spawner object is not valid.";
        return false;
    }

    return true;
}

bool OPGUISpawnerExport::isValidSpawnersJSON(const QList<OpenAPI::OAISpawner> &spawners, QString &errorMsg) const {
    if (spawners.isEmpty()) {
        errorMsg = "Spawners list is empty.";
        return false;
    }

    // Validate each spawner individually
    for (int i = 0; i < spawners.size(); ++i) {
        QString spawnerError;
        if (!isValidSpawnerJSON(spawners.at(i), spawnerError)) {
            errorMsg = QString("Invalid spawner at index %1: %2").arg(i).arg(spawnerError);
            return false;
        }
    }

    return true;
}

bool OPGUISpawnerExport::isValidLibrary(const QString &library, QString &errorMsg) const {
    if (library.isEmpty()) {
        errorMsg = "Library value is empty.";
        return false;
    }

    if (!OPGUIValueList::getInstance().hasValue("spawnerLibraries", library)) {
        errorMsg = QString("Invalid spawner library value '%1'. Available options are: %2.")
                      .arg(library)
                      .arg(OPGUIValueList::getInstance().listToString("spawnerLibraries"));
        return false;
    }

    return true;
}

bool OPGUISpawnerExport::isValidType(const QString &type, QString &errorMsg) const {
    if (type.isEmpty()) {
        errorMsg = "Type value is empty.";
        return false;
    }

    if (!OPGUIValueList::getInstance().hasValue("spawnerTypes", type)) {
        errorMsg = QString("Invalid spawner type value '%1'. Available options are: %2.")
                      .arg(type)
                      .arg(OPGUIValueList::getInstance().listToString("spawnerTypes"));
        return false;
    }

    return true;
}

bool OPGUISpawnerExport::isValidPriority(const qint32 &priority, QString &errorMsg) const {
    if (priority < 0) {
        errorMsg = "Priority must be a non-negative integer.";
        return false;
    }

    return true;
}

bool OPGUISpawnerExport::isValidProfile(const QString &profile, QString &errorMsg) const {
    return true;
}

} // namespace OPGUIConfigEditor