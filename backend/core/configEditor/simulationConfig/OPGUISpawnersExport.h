/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QList>
#include <QString>
#include <QXmlStreamWriter>
#include "OAISpawner.h"

namespace OPGUIConfigEditor {

/**
 * Class OPGUISpawnerExport handles the export of spawner settings into an XML format.
 * It provides functionalities to serialize spawner components such as library,
 * type, priority, and profile settings.
 */
class OPGUISpawnerExport {
public:
    /**
     * Constructor for the OPGUISpawnerExport class.
     */
    OPGUISpawnerExport() = default;

    /**
     * Exports the given spawner settings into an XML document.
     *
     * @param xmlWriter Reference to an XML stream writer to write the data.
     * @param spawner The spawner settings to be exported.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportSpawner(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISpawner &spawner, QString &errorMsg) const;

    /**
     * Exports a list of spawners into an XML document.
     *
     * @param xmlWriter Reference to an XML stream writer to write the data.
     * @param spawners The list of spawners to be exported.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportSpawners(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAISpawner> &spawners, QString &errorMsg) const;

private:
    /**
     * Validates the spawner object before exporting.
     *
     * @param spawner The spawner object to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the spawner is valid, false otherwise.
     */
    bool isValidSpawnerJSON(const OpenAPI::OAISpawner &spawner, QString &errorMsg) const;

    /**
     * Validates the list of spawners before exporting.
     *
     * @param spawners The list of spawner objects to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if all spawners are valid, false otherwise.
     */
    bool isValidSpawnersJSON(const QList<OpenAPI::OAISpawner> &spawners, QString &errorMsg) const;

    /**
     * Validates the library field of a spawner.
     *
     * @param library The library value to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the library is valid, false otherwise.
     */
    bool isValidLibrary(const QString &library, QString &errorMsg) const;

    /**
     * Validates the type field of a spawner.
     *
     * @param type The type value to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the type is valid, false otherwise.
     */
    bool isValidType(const QString &type, QString &errorMsg) const;

    /**
     * Validates the priority field of a spawner.
     *
     * @param priority The priority value to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the priority is valid, false otherwise.
     */
    bool isValidPriority(const qint32 &priority, QString &errorMsg) const;

    /**
     * Validates the profile field of a spawner, if present.
     *
     * @param profile The profile value to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the profile is valid or empty, false otherwise.
     */
    bool isValidProfile(const QString &profile, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor
