/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include "OPGUISpawnersImport.h"
#include "OPGUIValueList.h"
#include "OPGUIQtLogger.h"

namespace OPGUIConfigEditor {

bool OPGUISpawnerImport::importSpawner(const QDomElement &spawnerEl, OpenAPI::OAISpawner &spawner, QString &errorMsg) const {
    if (!isValidSpawnerXml(spawnerEl, errorMsg)) {
        return false;
    }

    QDomElement libraryEl = spawnerEl.firstChildElement("Library");
    QDomElement typeEl = spawnerEl.firstChildElement("Type");
    QDomElement priorityEl = spawnerEl.firstChildElement("Priority");
    QDomElement profileEl = spawnerEl.firstChildElement("Profile");

    spawner.setLibrary(libraryEl.text());
    spawner.setType(typeEl.text());
    spawner.setPriority(priorityEl.text().toInt());

    if (!profileEl.isNull()) {
        spawner.setProfile(profileEl.text());
    }

    return true;
}

bool OPGUISpawnerImport::importSpawners(const QDomElement &spawnersEl, QList<OpenAPI::OAISpawner> &spawners, QString &errorMsg) const {
    if (!isValidSpawnersXml(spawnersEl, errorMsg)) {
        return false;
    }

    QDomNodeList spawnerList = spawnersEl.elementsByTagName("Spawner");
    for (int i = 0; i < spawnerList.count(); ++i) {
        QDomElement spawnerEl = spawnerList.at(i).toElement();
        OpenAPI::OAISpawner spawner;

        if (!importSpawner(spawnerEl, spawner, errorMsg)) {
            errorMsg = QString("Failed to import spawner at index %1: %2").arg(i).arg(errorMsg);
            return false;
        }

        spawners.append(spawner);
    }

    return true;
}

bool OPGUISpawnerImport::isValidSpawnerXml(const QDomElement &spawnerEl, QString &errorMsg) const {
    if (spawnerEl.isNull()) {
        errorMsg = "Spawner element is missing.";
        return false;
    }

    if (!isValidLibrary(spawnerEl, errorMsg)) {
        return false;
    }

    if (!isValidType(spawnerEl, errorMsg)) {
        return false;
    }

    if (!isValidPriority(spawnerEl, errorMsg)) {
        return false;
    }

    if (!isValidProfile(spawnerEl, errorMsg)) {
        return false;
    }

    return true;
}

bool OPGUISpawnerImport::isValidSpawnersXml(const QDomElement &spawnersEl, QString &errorMsg) const {
    if (spawnersEl.isNull()) {
        errorMsg = "Spawners element is missing.";
        return false;
    }

    // First check if we have the correct root element
    if (spawnersEl.tagName() != "Spawners") {
        errorMsg = QString("Invalid root element '%1'. Expected 'Spawners'.").arg(spawnersEl.tagName());
        return false;
    }

    return true;
}

bool OPGUISpawnerImport::isValidLibrary(const QDomElement &spawnerEl, QString &errorMsg) const {
    QDomElement libraryEl = spawnerEl.firstChildElement("Library");
    if (libraryEl.isNull()) {
        errorMsg = "Library element is missing.";
        return false;
    }

    QString library = libraryEl.text().trimmed();
    if (library.isEmpty()) {
        errorMsg = "Library value is empty.";
        return false;
    }

    if (!OPGUIValueList::getInstance().hasValue("spawnerLibraries", library)) {
        errorMsg = QString("Invalid spawner library value '%1'. Available options are: %2.")
                      .arg(library)
                      .arg(OPGUIValueList::getInstance().listToString("spawnerLibraries"));
        return false;
    }

    return true;
}

bool OPGUISpawnerImport::isValidType(const QDomElement &spawnerEl, QString &errorMsg) const {
    QDomElement typeEl = spawnerEl.firstChildElement("Type");
    if (typeEl.isNull()) {
        errorMsg = "Type element is missing.";
        return false;
    }

    QString type = typeEl.text().trimmed();
    if (type.isEmpty()) {
        errorMsg = "Type value is empty.";
        return false;
    }

    if (!OPGUIValueList::getInstance().hasValue("spawnerTypes", type)) {
        errorMsg = QString("Invalid spawner type value '%1'. Available options are: %2.")
                      .arg(type)
                      .arg(OPGUIValueList::getInstance().listToString("spawnerTypes"));
        return false;
    }

    return true;
}

bool OPGUISpawnerImport::isValidPriority(const QDomElement &spawnerEl, QString &errorMsg) const {
    QDomElement priorityEl = spawnerEl.firstChildElement("Priority");
    if (priorityEl.isNull()) {
        errorMsg = "Priority element is missing.";
        return false;
    }

    bool ok;
    int priority = priorityEl.text().toInt(&ok);
    if (!ok) {
        errorMsg = "Priority must be a valid integer.";
        return false;
    }

    if (priority < 0) {
        errorMsg = "Priority must be a non-negative integer.";
        return false;
    }

    return true;
}

bool OPGUISpawnerImport::isValidProfile(const QDomElement &spawnerEl, QString &errorMsg) const {
    QDomElement profileEl = spawnerEl.firstChildElement("Profile");
    if (profileEl.isNull()) {
        return true;  // Profile is optional
    }

    QString profile = profileEl.text().trimmed();
    if (profile.isEmpty()) {
        errorMsg = "Profile value is empty.";
        return false;
    }

    return true; // Accept any non-empty string as valid
}

} // namespace OPGUIConfigEditor