/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QList>
#include <QString>
#include <QDomElement>
#include "OAISpawner.h"

namespace OPGUIConfigEditor {

/**
 * Class OPGUISpawnerImport is responsible for importing spawner settings from an XML format.
 * It handles parsing and validating the XML for spawner components including library,
 * type, priority, and profile settings.
 */
class OPGUISpawnerImport {
public:
    /**
     * Constructor for the OPGUISpawnerImport class.
     */
    OPGUISpawnerImport() = default;

    /**
     * Imports spawner settings from an XML element into the OpenAPI::OAISpawner structure.
     *
     * @param spawnerEl The XML element containing the spawner settings.
     * @param spawner The structure where the parsed spawner data will be stored.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the import is successful, false otherwise.
     */
    bool importSpawner(const QDomElement &spawnerEl, OpenAPI::OAISpawner &spawner, QString &errorMsg) const;

    /**
     * Imports a list of spawners from an XML element.
     *
     * @param spawnersEl The XML element containing multiple spawner elements.
     * @param spawners The list where the parsed spawner data will be stored.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the import is successful, false otherwise.
     */
    bool importSpawners(const QDomElement &spawnersEl, QList<OpenAPI::OAISpawner> &spawners, QString &errorMsg) const;

private:
    /**
     * Validates the spawner XML element.
     *
     * @param spawnerEl The XML element to validate.
     * @param errorMsg Reference to a string to store any error message in case of failure.
     * @return Returns true if the XML is valid, false otherwise.
     */
    bool isValidSpawnerXml(const QDomElement &spawnerEl, QString &errorMsg) const;

    /**
     * Validates the list of spawners XML element.
     *
     * @param spawnersEl The XML element to validate.
     * @param errorMsg Reference to a string to store any error message in case of failure.
     * @return Returns true if the XML is valid, false otherwise.
     */
    bool isValidSpawnersXml(const QDomElement &spawnersEl, QString &errorMsg) const;

    /**
     * Validates the library element within a spawner.
     *
     * @param spawnerEl The spawner XML element containing the library element.
     * @param errorMsg Reference to a string to store any error message in case of failure.
     * @return Returns true if the library element is valid, false otherwise.
     */
    bool isValidLibrary(const QDomElement &spawnerEl, QString &errorMsg) const;

    /**
     * Validates the type element within a spawner.
     *
     * @param spawnerEl The spawner XML element containing the type element.
     * @param errorMsg Reference to a string to store any error message in case of failure.
     * @return Returns true if the type element is valid, false otherwise.
     */
    bool isValidType(const QDomElement &spawnerEl, QString &errorMsg) const;

    /**
     * Validates the priority element within a spawner.
     *
     * @param spawnerEl The spawner XML element containing the priority element.
     * @param errorMsg Reference to a string to store any error message in case of failure.
     * @return Returns true if the priority element is valid, false otherwise.
     */
    bool isValidPriority(const QDomElement &spawnerEl, QString &errorMsg) const;

    /**
     * Validates the profile element within a spawner.
     *
     * @param spawnerEl The spawner XML element containing the profile element.
     * @param errorMsg Reference to a string to store any error message in case of failure.
     * @return Returns true if the profile element is valid or not present (as it's optional), false otherwise.
     */
    bool isValidProfile(const QDomElement &spawnerEl, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor
