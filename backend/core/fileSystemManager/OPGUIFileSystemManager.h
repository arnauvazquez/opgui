/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include <QDomElement>
#include <QList>
#include <QDir>

#include "OAIFileTreeRequest.h"
#include "OAIFileTreeExtended.h"

namespace OPGUIFileSystemManager {

/**
 * Enum BasePathGeneric defines the base paths for different areas of the file system managed by the FileSystemManager.
 */
enum class BasePathGeneric {
    Workspace,  ///< Base path for workspace-related files.
    Core        ///< Base path for core system files.
};

/**
 * @brief Class FileSystemManager manages interactions with the file system including retrieving,
 * creating, and managing files and directories.
 *
 * This class provides functionalities to manipulate and retrieve data about the file system,
 * such as fetching directories, verifying paths, and handling file contents.
 */
class FileSystemManager {
public:
    /**
     * @brief Constructs the FileSystemManager object.
     */
    FileSystemManager();

    /**
     * @brief Retrieves a file tree based on the specified parameters in the request.
     *
     * @param fileTreeRequest The request containing the specifications for the file tree retrieval.
     * @param fileTreeExtended Output parameter that will contain the detailed information of the file tree.
     * @param errorMsg Output parameter that will store the error message if an error occurs.
     * @return Returns true if the file tree is retrieved successfully, false otherwise.
     */
    bool getFileTree(const OpenAPI::OAIFileTreeRequest &fileTreeRequest, OpenAPI::OAIFileTreeExtended &fileTreeExtended, QString &errorMsg);

    /**
     * @brief Converts a generic base path description into an actual file system path.
     *
     * @param basePathGenericStr String representation of the base path type.
     * @param errorMsg Output parameter for error messages.
     * @return Returns the actual file system path as a string.
     */
    QString getRealBasePath(const QString &basePathGenericStr, QString &errorMsg) const;

    /**
     * @brief Retrieves the contents of a specified file.
     *
     * @param filePath Path to the file whose contents are to be fetched.
     * @param isB64Encoded Specifies whether the contents should be returned as Base64 encoded.
     * @param contents Output parameter that will store the contents of the file.
     * @param errorMsg Output parameter for error messages.
     * @return Returns true if the file contents are successfully retrieved, false otherwise.
     */
    bool getFileContents(const QString &filePath, const bool isB64Encoded, QString &contents, QString &errorMsg) const;

    /**
     * @brief Constructs a tree structure from a directory, optionally filtering by folders or specific file extensions.
     *
     * @param directory Directory to scan.
     * @param isFolder Specifies whether to include only directories.
     * @param extension File extension filter, empty string means no filter.
     * @return Returns a tree node representing the directory structure.
     */
    OpenAPI::OAITreeNode buildTree(const QDir &directory, const bool isFolder, const QString &extension);

    /**
     * @brief Deletes a specified file from the filesystem.
     *
     * @param path Path to the file to be deleted.
     * @param errorMsg Output parameter for error messages.
     * @return Returns true if the file is successfully deleted, false otherwise.
     */
    bool deleteFile(const QString &path, QString &errorMsg) const;

    /**
     * @brief Deletes all information in a specified path.
     *
     * @param path Path where the information to be deleted is located.
     * @param errorMsg Output parameter for error messages.
     * @return Returns true if the information is successfully deleted, false otherwise.
     */
    bool deleteInformation(const QString &path, QString &errorMsg) const;

    /**
     * @brief Verifies if a specified path exists and determines if it is empty. Delegates to specific
     * methods based on whether the path is full or relative.
     *
     * @param path Path to verify.
     * @param isFullPath Specifies whether the path is a full path.
     * @param realPath Output parameter that will store the resolved real path.
     * @param isEmpty Output parameter that will indicate if the path is empty.
     * @param errorMsg Output parameter for error messages.
     * @return Returns true if the path is successfully verified, false otherwise.
     */
    bool verifyPath(const QString &path, bool isFullPath, QString &realPath, bool &isEmpty, QString &errorMsg) const;

    /**
     * @brief Verifies a full path. Checks if the path exists and whether it is empty if it's a directory.
     *
     * @param path The full path to verify.
     * @param realPath Output parameter that will store the resolved real path.
     * @param isEmpty Output parameter that will indicate if the directory is empty.
     * @param errorMsg Output parameter for error messages if the path does not exist.
     * @return Returns true if the path exists, false otherwise.
     */
    bool verifyFullPath(const QString& path, QString& realPath, bool& isEmpty, QString& errorMsg) const;

    /**
     * @brief Verifies if a given relative path exists within the workspace directory or its subdirectories.
     *
     * This function searches for the specified path within the workspace directory and its subdirectories.
     * If the path is found more than once, an error message is set and the function returns false.
     * If the path is found, the real path and its empty status are updated.
     *
     * @param path The relative path to search for.
     * @param realPath Reference to a string that will contain the absolute path if found.
     * @param isEmpty Reference to a boolean that will indicate if the directory is empty.
     * @param errorMsg Reference to a string that will contain an error message if the path is not found or found multiple times.
     * @return true if the path is found exactly once, false otherwise.
     */
    bool verifyRelativePath(const QString& path, QString& realPath, bool& isEmpty, QString& errorMsg) const;

    /**
     * @brief Processes a single directory, checking for the specified path and updating necessary information.
     *
     * This helper function iterates through the entries of the given directory, checks if the specified path is found,
     * and updates the real path and empty status if applicable. It also manages the count of found paths and pushes
     * subdirectories onto the stack for further processing.
     *
     * @param currentDir The directory currently being processed.
     * @param path The relative path to search for.
     * @param realPath Reference to a string that will contain the absolute path if found.
     * @param isEmpty Reference to a boolean that will indicate if the directory is empty.
     * @param count Reference to an integer that keeps track of the number of times the path is found.
     * @param dirsToCheck Reference to a stack of directories to be processed.
     * @param errorMsg Reference to a string that will contain an error message if the path is found multiple times.
     * @return true if processing continues successfully, false if the path is found multiple times.
     */
    bool processDirectory(const QDir& currentDir, const QString& path, QString& realPath, bool& isEmpty, int& count, QStack<QDir>& dirsToCheck, QString& errorMsg) const;

};

}  // namespace OPGUIFileSystemManager
