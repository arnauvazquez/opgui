/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QFile>
#include <QString>
#include <QTextStream>
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QJsonArray>
#include <QDir>

#include "OPGUIGlobalSetupEditor.h"
#include "OPGUIQtLogger.h"
#include "OPGUICoreGlobalConfig.h"
#include "OAIConfigElement.h"

namespace OPGUIGlobalSetupEditor {

GlobalSetupEditor::GlobalSetupEditor(){
    userSetableIds.append(qMakePair(QString("PATH_OPENPASS_CORE"),  QString("folder")));
    userSetableIds.append(qMakePair(QString("WORKSPACE"),  QString("folder")));
    userSetableIds.append(qMakePair(QString("MODULES_FOLDER"),  QString("folder")));
    userSetableIds.append(qMakePair(QString("COMPONENTS_FOLDER"),  QString("folder")));
    userSetableIds.append(qMakePair(QString("PATH_CONVERTED_CASES"),  QString("folder")));
    userSetableIds.append(qMakePair(QString("OPSIMULATION_EXE"),  QString("file")));
    userSetableIds.append(qMakePair(QString("OPSIMULATION_MANAGER_EXE"),  QString("file")));
    userSetableIds.append(qMakePair(QString("OPSIMULATION_MANAGER_XML"), QString("file")));
    userSetableIds.append(qMakePair(QString("PATH_LOG_FILE"),  QString("file")));
}

QList<OpenAPI::OAIConfigElement> GlobalSetupEditor::getConfigFileUserElements(QString &errorMsg) const{
    QList<OpenAPI::OAIConfigElement> configElementsList;
    QString configFilePath = OPGUICoreGlobalConfig::getInstance().pathConfigFile();
    QFile configFile(configFilePath);

    if (!configFile.exists()) {
        errorMsg = "Config file does not exist at path: " + configFilePath;
        LOG_ERROR(errorMsg);
        return configElementsList; 
    }

    if (!configFile.open(QIODevice::ReadOnly)) {
        errorMsg = "Unable to open config file for reading at path: " + configFilePath;
        LOG_ERROR(errorMsg);
        return configElementsList;
    }

    QByteArray jsonData = configFile.readAll();
    configFile.close();

    QJsonDocument doc = QJsonDocument::fromJson(jsonData);
    if (doc.isNull() || !doc.isObject()) {
        errorMsg = "Invalid or malformed JSON content in config file"
                   " File path: " + configFilePath +
                   " JSON Data: " + jsonData;
        LOG_ERROR(errorMsg);
        return configElementsList;
    }

    QJsonObject jsonObj = doc.object();
    for (const auto &configPair : userSetableIds) {
        QString key = configPair.first;
        if (jsonObj.contains(key)) {
            OpenAPI::OAIConfigElement element;
            element.setId(key);
            element.setValue(jsonObj.value(key).toString());
            element.setType(configPair.second);  // Setting the type from userSetableIds
            configElementsList.append(element);
        }
    }

    return configElementsList;
}

bool GlobalSetupEditor::getUserConfigElements(QList<OpenAPI::OAIConfigElement> &configElements, QString &errorMsg) const{
    
    QList<OpenAPI::OAIConfigElement> tempConfigElements = getConfigFileUserElements(errorMsg);

    if (!errorMsg.isEmpty()) {
        return false;
    }

    configElements = tempConfigElements;

    return true;
}

bool GlobalSetupEditor::setUserConfigElements(const QList<OpenAPI::OAIConfigElement> &newConfigElements, QString &errorMsg, QString &response) const {
    QList<OpenAPI::OAIConfigElement> currentConfigElementsList = getConfigFileUserElements(errorMsg);
    QList<OpenAPI::OAIConfigElement> elementsToUpdate;

    if (!errorMsg.isEmpty()) {
        return false;
    }

    for (const auto &newElement : newConfigElements) {
        if (!checkAndUpdateElements(newElement, currentConfigElementsList, elementsToUpdate, errorMsg)) {
            return false;
        }
    }

    if (updateElements(elementsToUpdate, errorMsg, response)) {
        if (!OPGUICoreGlobalConfig::getInstance().loadFromConfigFile()) {
            errorMsg = "Failed to reload the configuration file after updates.";
            LOG_ERROR(errorMsg);
            return false;
        }
    } else {
        response = "No values were modified.";
    }

    return true;
}

bool GlobalSetupEditor::checkAndUpdateElements(const OpenAPI::OAIConfigElement &newElement, QList<OpenAPI::OAIConfigElement> &currentConfigElementsList, QList<OpenAPI::OAIConfigElement> &elementsToUpdate, QString &errorMsg) const {
    for (auto &currentElement : currentConfigElementsList) {
        if (currentElement.getId() == newElement.getId()) {
            return checkAndPerformUpdate(currentElement, newElement, elementsToUpdate, errorMsg);
        }
    }
    return true;
}

bool GlobalSetupEditor::checkAndPerformUpdate(OpenAPI::OAIConfigElement &currentElement, const OpenAPI::OAIConfigElement &newElement, QList<OpenAPI::OAIConfigElement> &elementsToUpdate, QString &errorMsg) const {
    if (currentElement.getValue() != newElement.getValue()) {
        if (checkExists(newElement)) {
            currentElement.setValue(newElement.getValue());
            elementsToUpdate.append(currentElement);
        } else {
            errorMsg = QString("%1 '%2' in property '%3' does not exist.")
                            .arg(newElement.getType())
                            .arg(newElement.getValue())
                            .arg(newElement.getId());
            LOG_ERROR(errorMsg);
            return false;
        }
    }
    return true;
}

bool GlobalSetupEditor::updateElements(const QList<OpenAPI::OAIConfigElement> &elementsToUpdate, QString &errorMsg, QString &response) const {
    bool updated = false;
    for (const auto &element : elementsToUpdate) {
        QString debugMsg = QString("Updating element - ID: %1, Value: %2, Type: %3")
                        .arg(element.getId())
                        .arg(element.getValue())
                        .arg(element.getType());

        LOG_DEBUG(debugMsg);
        if (!modifyConfigElement(element, errorMsg)) {
            LOG_ERROR(errorMsg);
            return false;
        } else {
            updated = true;
            response += QString("Updated element: %1 with Value: %2\n")
                        .arg(element.getId())
                        .arg(element.getValue());
        }
    }
    return updated;
}

bool GlobalSetupEditor::checkExists(const OpenAPI::OAIConfigElement &element) const{
    const QString &value = element.getValue();
    QString type = element.getType();

    if (type == "file") {
        return QFile::exists(value);
    } else if (type == "folder") {
        return QDir(value).exists();
    } else {
        return true;
    }
}

bool GlobalSetupEditor::modifyConfigElement(const OpenAPI::OAIConfigElement& element, QString &errorMsg) const{
    QString configFilePath = OPGUICoreGlobalConfig::getInstance().pathConfigFile();
    QFile configFile(configFilePath);
    if (!configFile.open(QIODevice::ReadOnly)) {
        errorMsg = "Unable to open config file for reading at path: " + configFilePath;
        LOG_ERROR(errorMsg);
        return false;
    }

    QByteArray jsonData = configFile.readAll();
    configFile.close();

    QJsonDocument doc = QJsonDocument::fromJson(jsonData);
    if (doc.isNull() || !doc.isObject()) {
        errorMsg = "Error: JSON document parsed from config file is null. File path: " + configFilePath;
        LOG_ERROR(errorMsg);
        return false;
    }

    QJsonObject obj = doc.object();
    obj[element.getId()] = QJsonValue(element.getValue());
    doc.setObject(obj);

    if (!configFile.open(QIODevice::WriteOnly)) {
        errorMsg = "Unable to open config file for writing at path: " + configFilePath;
        LOG_ERROR(errorMsg);
        return false;
    }

    configFile.write(doc.toJson());
    configFile.close();

    return true;
}

}  // namespace OPGUIFileSystemManager




