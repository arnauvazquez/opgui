
/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

// OPGUISimulationExecution.cpp
#include "OPGUISimulationExecution.h"
#include "OPGUISimulationManagerImport.h"
#include "OPGUIQtLogger.h"

#include <QtConcurrent>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>

OPGUISimulationExecution& OPGUISimulationExecution::getInstance() {
    static OPGUISimulationExecution instance;
    return instance;
}

OPGUISimulationExecution::OPGUISimulationExecution() : 
    isRunning(false),
    totalSimulationsCount(0),
    completedSimulationsCount(0),  
    failedStartsCount(0) {
}

OPGUISimulationExecution::~OPGUISimulationExecution() {
    stopAllSimulations();
}

bool OPGUISimulationExecution::allSimulationsFinished() const {
    QMutexLocker lock(&statusMutex);

    int completed = completedSimulationsCount.loadRelaxed();
    int failed = failedStartsCount.loadRelaxed();
    int total = totalSimulationsCount.loadRelaxed();

    QString debugMsg = QString("Completed:%1, Failed:%2, Total:%3")
        .arg(completed)
        .arg(failed)
        .arg(total);
    LOG_INFO("Simulation count SUMMARY:" + debugMsg);

    return (completed + failed) == total;
}

bool OPGUISimulationExecution::executeSimulation(const QString& configPath, QString& errorMsg) {
    if (isRunning) {
        errorMsg = "Simulation already running";
        LOG_ERROR(errorMsg);
        return false;
    }

    if (!parseConfiguration(configPath, errorMsg)) {
        return false;
    }

    return startParallelExecution(errorMsg);
}

bool OPGUISimulationExecution::parseConfiguration(const QString& configPath, QString& errorMsg) {
   QFile file(configPath);
   if (!file.open(QIODevice::ReadOnly)) {
       errorMsg = "Could not open configuration file: " + configPath;
       return false;
   }

   QByteArray data = file.readAll();
   file.close();

   QDomDocument doc;
   if (!doc.setContent(data, &errorMsg)) {
       errorMsg = "Could not set content of virtual document";
       return false;
   }

   QDomElement root = doc.documentElement();
   OPGUISimulationManagerImport importer;
   if (!importer.importSimulationManager(root, currentConfig, errorMsg)) {
       return false;
   }

   return true;
}

bool OPGUISimulationExecution::startParallelExecution(QString& errorMsg) {
   LOG_INFO("Starting parallel simulation execution");
   
    if(!validateExecutable(currentConfig.getSimulation(),errorMsg)){
        return false;
    }

    resetSimulationState();

    logSimulationStart();

    std::vector<std::pair<int, OpenAPI::OAISimulationConfig>> indexedConfigs = prepareSimulationConfigs();
    if (!initializeSimulation(indexedConfigs, errorMsg)) {
        return false;
    }

    for (const auto& [index, simConfig] : indexedConfigs) {
        QtConcurrent::run([this, simConfig, index]() {
            QProcess* process = new QProcess();
            
            QStringList arguments;
            arguments << "--logLevel" << QString::number(currentConfig.getLogLevel());
            arguments << "--logFile" << simConfig.getLogFileSimulation();
            arguments << "--lib" << currentConfig.getLibraries();
            arguments << "--configs" << simConfig.getConfigurations();
            arguments << "--results" << simConfig.getResults();

            QString argString;
            for(int i = 0; i < arguments.size(); i += 2) {
                if(i > 0) {
                    argString += " ";
                }
                    argString += arguments[i] + " " + arguments[i+1];
            }
            LOG_INFO("Simulation called with: "+currentConfig.getSimulation()+" "+ argString);

            // Create results directory if not exists
            QDir().mkpath(QFileInfo(simConfig.getResults()).absolutePath());
            logSimulationEvent(QString("Created result folder %1").arg(simConfig.getResults()));

            process->start(currentConfig.getSimulation(), arguments);

            if (!process->waitForStarted()) {
                QString error = QString("Failed to start simulation process %1/%2")
                            .arg(index + 1)
                            .arg(totalSimulationsCount.loadRelaxed());
                LOG_ERROR(error);
                logSimulationEvent(error);
                
                QMutexLocker lock(&statusMutex);
                simulationStatuses[0].error = error;
                process->deleteLater();

                failedStartsCount.fetchAndAddRelaxed(1);
                if (allSimulationsFinished()) {
                    isRunning = false;
                    QString errorMsg = "## opSimulationManager finished with errors ##";
                    LOG_ERROR(errorMsg);
                    logSimulationEvent(errorMsg);
                }
                return;
            }
            
            qint64 pid = process->processId();
            
            // Log process start
            QString startMessage = QString("Proc %1: (Simulation %2/%3) ###Start###")
                        .arg(pid)
                        .arg(index + 1)
                        .arg(totalSimulationsCount.loadRelaxed());
            logSimulationEvent(startMessage);
            LOG_INFO(startMessage);
            
            // Log command line
            QString cmdLine = QString("Proc %1: (Simulation %2/%3) Called as %4 %5")
                    .arg(pid)
                    .arg(index + 1)
                    .arg(totalSimulationsCount.loadRelaxed())
                    .arg(currentConfig.getSimulation())
                    .arg(argString);
            logSimulationEvent(cmdLine);
            LOG_INFO(cmdLine);

            {
                QMutexLocker lock(&statusMutex);
                simulationStatuses[pid] = {
                    pid,
                    0,
                    QString(),
                    false,
                    simConfig.getLogFileSimulation()
                };
            }
            
            activeProcesses.append(process);
            process->waitForFinished(-1);
            
            {
                QMutexLocker lock(&statusMutex);
                simulationStatuses[pid].exitCode = process->exitCode();
                simulationStatuses[pid].isFinished = true;
            }
        
            QString procMessage = QString("Proc %1: (Simulation %2/%3) ###finished with exit code %4")
                        .arg(pid)
                        .arg(index + 1)
                        .arg(totalSimulationsCount.loadRelaxed())
                        .arg(process->exitCode());
            logSimulationEvent(procMessage);
            LOG_INFO(procMessage);
        
            cleanupSimulation(process);
       });
   }

   return waitForSimulationStarts(errorMsg);
}

void OPGUISimulationExecution::cleanupSimulation(QProcess* process) {
    completedSimulationsCount.fetchAndAddRelaxed(1);
    activeProcesses.removeOne(process);
    process->deleteLater();
    
    if (allSimulationsFinished()) {
        isRunning = false;
        QString procMessage = "## opSimulationManager finished ##";
        logSimulationEvent(procMessage);
        LOG_INFO(procMessage);
    }
}

void OPGUISimulationExecution::logSimulationEvent(const QString& message) {
   QString timestamp = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
   QString logEntry = QString("%1 [DebugCore] %2\n").arg(timestamp).arg(message);
   
   QFile logFile(currentConfig.getLogFileSimulationManager());
   if (logFile.open(QIODevice::Append | QIODevice::Text)) {
       QTextStream stream(&logFile);
       stream << logEntry;
       logFile.close();
   }
}

void OPGUISimulationExecution::stopAllSimulations() {
    for (QProcess* process : activeProcesses) {
        process->kill();
        process->deleteLater();
    }

    {
        QMutexLocker lock(&processListMutex);
        activeProcesses.clear();
    }
    isRunning = false;
}

bool OPGUISimulationExecution::isSimulationRunning() const {
    return isRunning;
}

QList<OPGUISimulationExecution::SimulationStatus> OPGUISimulationExecution::getSimulationStatuses() {
    QMutexLocker lock(&statusMutex);
    return simulationStatuses.values();
}

void OPGUISimulationExecution::resetSimulationState() {
    completedSimulationsCount.storeRelaxed(0);
    failedStartsCount.storeRelaxed(0);
    totalSimulationsCount.storeRelaxed(0);

    {
        QMutexLocker lock(&statusMutex);
        simulationStatuses.clear();
    }

    {
        QMutexLocker lock(&processListMutex);
        activeProcesses.clear();
    }
}

void OPGUISimulationExecution::logSimulationStart() {
    logSimulationEvent("## opSimulationManager start ##");
    logSimulationEvent(QString("log level: %1").arg(currentConfig.getLogLevel()));
    logSimulationEvent(QString("log file opSimulationManager: %1").arg(currentConfig.getLogFileSimulationManager()));
    logSimulationEvent(QString("simulation: %1").arg(currentConfig.getSimulation()));
    logSimulationEvent(QString("libraries: %1").arg(currentConfig.getLibraries()));
    logSimulationEvent(QString("number of simulations: %1").arg(currentConfig.getSimulationConfigs().size()));
}

bool OPGUISimulationExecution::validateExecutable(const QString& exePath, QString& errorMsg) {
    QFileInfo simulationExe(exePath);
    if (!simulationExe.exists()) {
        errorMsg = "Simulation executable not found at: " + exePath;
        return false;
    }
    if (!simulationExe.isExecutable()) {
        errorMsg = "File exists but is not executable: " + exePath;
        return false;
    }
    return true;
}

std::vector<std::pair<int, OpenAPI::OAISimulationConfig>> OPGUISimulationExecution::prepareSimulationConfigs() {
    QList<OpenAPI::OAISimulationConfig> configs = currentConfig.getSimulationConfigs();
    std::vector<std::pair<int, OpenAPI::OAISimulationConfig>> indexedConfigs;
    for (int i = 0; i < configs.size(); ++i) {
        indexedConfigs.emplace_back(i, configs[i]);
    }
    return indexedConfigs;
}

bool OPGUISimulationExecution::initializeSimulation(const std::vector<std::pair<int, OpenAPI::OAISimulationConfig>>& indexedConfigs, 
                         QString& errorMsg) {
    totalSimulationsCount.storeRelaxed(indexedConfigs.size());

    if (indexedConfigs.size() > 0) {
        isRunning = true;
        return true;
    } else {
        LOG_ERROR("No simulations to run.");
        errorMsg = "No simulations to run.";
        return false;
    }
}

bool OPGUISimulationExecution::waitForSimulationStarts(QString& errorMsg) {
    int maxWaitMs = 1000;
    int checkIntervalMs = 100;
    int elapsedMs = 0;

    while (elapsedMs < maxWaitMs) {
        QThread::msleep(checkIntervalMs);
        elapsedMs += checkIntervalMs;

        if (failedStartsCount.loadRelaxed() == totalSimulationsCount.loadRelaxed()) {
            errorMsg = "All simulations failed to start";
            isRunning = false;
            return false;
        }

        if (failedStartsCount.loadRelaxed() < totalSimulationsCount.loadRelaxed()) {
            break;
        }
    }
    return true;
}