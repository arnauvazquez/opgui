/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QProcess>
#include <QList>
#include <QMap>
#include <QRecursiveMutex>
#include <QAtomicInt>
#include "OAIOpSimulationManagerXmlRequest.h"

/**
 * @brief Manages the execution of simulations, handling concurrent processes and their statuses.
 */
class OPGUISimulationExecution {
public:
    /**
     * @brief Holds the status information for a simulation process.
     */
    struct SimulationStatus {
        qint64 processId;    ///< The process ID of the simulation.
        int exitCode;        ///< The exit code of the simulation process.
        QString error;       ///< Error message if the simulation failed to start or run.
        bool isFinished;     ///< Indicates whether the simulation has finished.
        QString logFile;     ///< Path to the simulation's log file.

    };

    QAtomicInt totalSimulationsCount;           ///< Total number of simulations to run.
    QAtomicInt completedSimulationsCount;       ///< Number of simulations that have completed.
    QAtomicInt failedStartsCount;               ///< Number of simulations that failed to start.
    QList<QProcess*> activeProcesses;           ///< List of currently active simulation processes.
    bool isRunning;                            ///< Indicates if simulations are currently running.
    QMap<qint64, SimulationStatus> simulationStatuses;  ///< Statuses of all simulations.
    mutable QRecursiveMutex statusMutex;        ///< Mutex to protect access to simulationStatuses.
    mutable QRecursiveMutex processListMutex;   ///< Mutex to protect access to activeProcesses.

    /**
     * @brief Retrieves the singleton instance of OPGUISimulationExecution.
     * @return Reference to the singleton instance.
     */
    static OPGUISimulationExecution& getInstance();
    
    /**
     * @brief Executes simulations based on the provided configuration file.
     * @param configPath The path to the simulation configuration file.
     * @param errorMsg Output parameter to receive error messages if execution fails.
     * @return True if the simulations started successfully, false otherwise.
     */
    bool executeSimulation(const QString& configPath, QString& errorMsg);

    /**
     * @brief Checks if simulations are currently running.
     * @return True if simulations are running, false otherwise.
     */
    bool isSimulationRunning() const;

    /**
     * @brief Stops all currently running simulations.
     */
    void stopAllSimulations();

    /**
     * @brief Retrieves the statuses of all simulations.
     * @return A list containing the status of each simulation.
     */
    QList<SimulationStatus> getSimulationStatuses();

    /**
     * @brief Logs a simulation event to the simulation manager's log file.
     * @param message The message to be logged.
     */
    void logSimulationEvent(const QString& message);

    /**
     * @brief Parses the simulation configuration from the given file path.
     * @param configPath The path to the configuration file.
     * @param errorMsg Output parameter to receive error messages if parsing fails.
     * @return True if parsing was successful, false otherwise.
     */
    bool parseConfiguration(const QString& configPath, QString& errorMsg);

    /**
     * @brief Validates that the simulation executable exists and is executable.
     * @param exePath The path to the simulation executable.
     * @param errorMsg Output parameter to receive error messages if validation fails.
     * @return True if the executable is valid, false otherwise.
     */
    bool validateExecutable(const QString& exePath, QString& errorMsg);

    /**
     * @brief Resets the simulation state, clearing counters and statuses.
     */
    void resetSimulationState();

    /**
     * @brief Logs the start of the simulation execution.
     */
    void logSimulationStart();

    /**
     * @brief The current simulation configuration.
     */
    OpenAPI::OAIOpSimulationManagerXmlRequest currentConfig;

    /**
     * @brief Waits for simulations to start or fail within a timeout period.
     * @param errorMsg Output parameter to receive error messages if waiting fails.
     * @return True if simulations started, false if they all failed to start.
     */
    bool waitForSimulationStarts(QString& errorMsg);

    /**
     * @brief Checks if all simulations have finished.
     * @return True if all simulations have completed or failed, false otherwise.
     */
    bool allSimulationsFinished() const;

    /**
     * @brief Cleans up resources after a simulation process finishes.
     * @param process The QProcess object representing the simulation process.
     */
    void cleanupSimulation(QProcess* process);

    /**
     * @brief Initializes simulations with the given configurations.
     * @param indexedConfigs The configurations for simulations with their indices.
     * @param errorMsg Output parameter to receive error messages if initialization fails.
     * @return True if initialization was successful, false otherwise.
     */
    bool initializeSimulation(const std::vector<std::pair<int, OpenAPI::OAISimulationConfig>>& indexedConfigs, 
                              QString& errorMsg);

private:
    /**
     * @brief Constructor for OPGUISimulationExecution.
     */
    OPGUISimulationExecution();

    /**
     * @brief Destructor for OPGUISimulationExecution.
     */
    ~OPGUISimulationExecution();

    /**
     * @brief Starts the simulations in parallel.
     * @param errorMsg Output parameter to receive error messages if execution fails.
     * @return True if simulations started successfully, false otherwise.
     */
    bool startParallelExecution(QString& errorMsg);

    /**
     * @brief Prepares simulation configurations for execution.
     * @return A vector of index and configuration pairs for simulations.
     */
    std::vector<std::pair<int, OpenAPI::OAISimulationConfig>> prepareSimulationConfigs();

    // Disable copy and move operations
    OPGUISimulationExecution(const OPGUISimulationExecution&) = delete;
    OPGUISimulationExecution& operator=(const OPGUISimulationExecution&) = delete;
    OPGUISimulationExecution(OPGUISimulationExecution&&) = delete;
    OPGUISimulationExecution& operator=(OPGUISimulationExecution&&) = delete;
};
