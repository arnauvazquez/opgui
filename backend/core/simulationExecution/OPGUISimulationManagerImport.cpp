/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUISimulationManagerImport.h"

bool OPGUISimulationManagerImport::importSimulationManager(const QDomElement &rootEl, OpenAPI::OAIOpSimulationManagerXmlRequest &config, QString &errorMsg) const {
    if (!isValidSimulationManagerXml(rootEl, errorMsg)) {
        return false;
    }

    config.setLogLevel(rootEl.firstChildElement("logLevel").text().toInt());
    config.setLogFileSimulationManager(rootEl.firstChildElement("logFileSimulationManager").text());
    config.setLibraries(rootEl.firstChildElement("libraries").text());
    config.setSimulation(rootEl.firstChildElement("simulation").text());

    QList<OpenAPI::OAISimulationConfig> configs;
    if (QDomElement configsEl = rootEl.firstChildElement("simulationConfigs"); !importSimulationConfigs(configsEl, configs, errorMsg)) {
        return false;
    }
    config.setSimulationConfigs(configs);

    return true;
}

bool OPGUISimulationManagerImport::importSimulationConfigs(const QDomElement &configsEl, QList<OpenAPI::OAISimulationConfig> &configs, QString &errorMsg) const {
    if (!isValidSimulationConfigsXml(configsEl, errorMsg)) {
        return false;
    }

    QDomNodeList configNodes = configsEl.elementsByTagName("simulationConfig");
    for (int i = 0; i < configNodes.count(); ++i) {
        QDomElement configEl = configNodes.at(i).toElement();
        OpenAPI::OAISimulationConfig config;
        
        config.setConfigurations(configEl.firstChildElement("configurations").text());
        config.setLogFileSimulation(configEl.firstChildElement("logFileSimulation").text());
        config.setResults(configEl.firstChildElement("results").text());
        
        configs.append(config);
    }

    return true;
}

bool OPGUISimulationManagerImport::isValidSimulationManagerXml(const QDomElement &rootEl, QString &errorMsg) const {
    if (rootEl.isNull() || rootEl.tagName() != "opSimulationManager") {
        errorMsg = "Invalid or missing opSimulationManager root element";
        return false;
    }

    QStringList requiredElements = {"logLevel", "logFileSimulationManager", "libraries", "simulation", "simulationConfigs"};
    for (const auto& element : requiredElements) {
        if (rootEl.firstChildElement(element).isNull()) {
            errorMsg = QString("Missing required element: %1").arg(element);
            return false;
        }
    }

    bool ok;
    int logLevel = rootEl.firstChildElement("logLevel").text().toInt(&ok);
    if (!ok || logLevel < 0) {
        errorMsg = "Invalid logLevel value";
        return false;
    }

    return true;
}

bool OPGUISimulationManagerImport::isValidSimulationConfigsXml(const QDomElement &configsEl, QString &errorMsg) const {
    if (configsEl.isNull()) {
        errorMsg = "Missing simulationConfigs element";
        return false;
    }

    QDomNodeList configNodes = configsEl.elementsByTagName("simulationConfig");
    if (configNodes.isEmpty()) {
        errorMsg = "No simulationConfig elements found";
        return false;
    }

    for (int i = 0; i < configNodes.count(); ++i) {
        QDomElement configEl = configNodes.at(i).toElement();
        QStringList requiredElements = {"configurations", "logFileSimulation", "results"};
        
        for (const auto& element : requiredElements) {
            if (configEl.firstChildElement(element).isNull()) {
                errorMsg = QString("Missing %1 in simulationConfig at index %2").arg(element).arg(i);
                return false;
            }
        }
    }

    return true;
}