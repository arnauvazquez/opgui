/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QDomElement>
#include "OAIOpSimulationManagerXmlRequest.h"

/**
 * @brief Handles the import and validation of simulation manager configurations from XML.
 */
class OPGUISimulationManagerImport {
public:
    /**
     * @brief Imports the simulation manager configuration from a QDomElement.
     * 
     * Parses the root XML element and populates the provided configuration object.
     * 
     * @param rootEl The root XML element containing the simulation manager configuration.
     * @param config Output parameter where the parsed configuration will be stored.
     * @param errorMsg Output parameter to receive error messages if the import fails.
     * @return True if the import was successful, false otherwise.
     */
    bool importSimulationManager(
        const QDomElement &rootEl,
        OpenAPI::OAIOpSimulationManagerXmlRequest &config,
        QString &errorMsg
    ) const;

private:
    /**
     * @brief Imports simulation configurations from a given XML element.
     * 
     * Parses the XML element containing simulation configurations and populates the provided list.
     * 
     * @param configsEl The XML element containing simulation configurations.
     * @param configs Output parameter where the list of simulation configurations will be stored.
     * @param errorMsg Output parameter to receive error messages if the import fails.
     * @return True if the configurations were imported successfully, false otherwise.
     */
    bool importSimulationConfigs(
        const QDomElement &configsEl,
        QList<OpenAPI::OAISimulationConfig> &configs,
        QString &errorMsg
    ) const;

    /**
     * @brief Validates the root XML element of the simulation manager configuration.
     * 
     * Checks whether the provided XML element meets the expected structure and content for a simulation manager.
     * 
     * @param rootEl The root XML element to validate.
     * @param errorMsg Output parameter to receive error messages if validation fails.
     * @return True if the XML element is valid, false otherwise.
     */
    bool isValidSimulationManagerXml(
        const QDomElement &rootEl,
        QString &errorMsg
    ) const;

    /**
     * @brief Validates the XML element containing simulation configurations.
     * 
     * Ensures that the XML element has the correct structure and required child elements for simulation configurations.
     * 
     * @param configsEl The XML element to validate.
     * @param errorMsg Output parameter to receive error messages if validation fails.
     * @return True if the configurations XML is valid, false otherwise.
     */
    bool isValidSimulationConfigsXml(
        const QDomElement &configsEl,
        QString &errorMsg
    ) const;
};
