
/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QDomElement>
#include <QList>
#include <QXmlStreamWriter>

#include "OAISystemUI.h"
#include "OAIComponentUI.h"
#include "OAISchedule.h"
#include "OAIPosition.h"
#include "OAIParameter.h"
#include "OAIInput.h"
#include "OAIOutput.h"
#include "OAIConnection.h"
#include "OAIComment.h"

namespace OPGUISystemEditor {

/**
 * Manages the OpenAPI System User Interface components
 *
 * Provides methods for reading, parsing, exporting, and validating
 * XML representations of OpenAPI System UI components and their associated elements.
 */
class OPGUISystemEditorExport {
public:
    /**
     * Default constructor for OPGUISystemEditorExport
     */
    OPGUISystemEditorExport();

    /**
     * Saves a system to an XML file
     *
     * @param system The system to save
     * @param path The file path to save the XML
     * @param isNew Indicates if this is a new file creation
     * @param errorMsg Error message if the save fails
     * @return True if successful, false otherwise
     */
    bool saveSystemToXmlFile(const OpenAPI::OAISystemUI &system, const QString &path, const bool isNew, QString &errorMsg) const;

    /**
     * Exports an OAISchedule object to XML
     *
     * @param xmlWriter XML stream writer
     * @param schedule The schedule to export
     * @param isInsideSystem Indicates if the schedule is within a system
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportScheduleToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISchedule &schedule, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Exports an OAIParameter object to XML
     *
     * @param xmlWriter XML stream writer
     * @param param The parameter to export
     * @param isInsideSystem Indicates if the parameter is within a system
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportParameterToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIParameter &param, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Exports an OAIInput object to XML
     *
     * @param xmlWriter XML stream writer
     * @param input The input to export
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportInputToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIInput &input, QString &errorMsg) const;

    /**
     * Exports an OAIOutput object to XML
     *
     * @param xmlWriter XML stream writer
     * @param output The output to export
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportOutputToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIOutput &output, QString &errorMsg) const;

    /**
     * Exports an OAIPosition object to XML
     *
     * @param xmlWriter XML stream writer
     * @param position The position to export
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportPositionToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIPosition &position, QString &errorMsg) const;

    /**
     * Exports an OAIComponentUI object to XML
     *
     * @param xmlWriter XML stream writer
     * @param component The component to export
     * @param isInsideSystem Indicates if the component is within a system
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportComponentToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIComponentUI &component, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Exports an OAIComment object to XML
     *
     * @param xmlWriter XML stream writer
     * @param comment The comment to export
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportCommentToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIComment &comment, QString &errorMsg) const;

    /**
     * Exports an OAISystemUI object to XML
     *
     * @param xmlWriter XML stream writer
     * @param system The system to export
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportSystemToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISystemUI &system, QString &errorMsg) const;

    /**
     * Exports an OAIConnection object to XML
     *
     * @param xmlWriter XML stream writer
     * @param connection The connection to export
     * @param components List of components related to the connection
     * @param errorMsg Error message if the export fails
     * @return True if successful, false otherwise
     */
    bool exportConnectionToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIConnection &connection, const QList<OpenAPI::OAIComponentUI> &components, QString &errorMsg) const;

    /* Object Validation Functions */

    /**
     * Validates the structure and integrity of an OAISystemUI object.
     * Ensures that the system object meets all required constraints and is complete for further processing or exporting.
     *
     * @param system The system object to validate.
     * @param errorMsg String to hold the description of the error if validation fails.
     * @return True if the system object is valid, false otherwise.
     */
    bool isValidSystem(const OpenAPI::OAISystemUI &system, QString &errorMsg) const;

    /**
     * Validates an OAIComponentUI object.
     * Checks if the component within a system (or standalone) adheres to expected format and contains all necessary data.
     *
     * @param component The component to validate.
     * @param isInsideSystem Boolean flag indicating whether the component is part of a system.
     * @param errorMsg String to hold the description of the error if validation fails.
     * @return True if the component is valid, false otherwise.
     */
    bool isValidComponent(const OpenAPI::OAIComponentUI &component, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Validates an OAISchedule object.
     * Ensures that the schedule details are logically consistent and complete.
     *
     * @param schedule The schedule object to validate.
     * @param isInsideSystem Boolean flag indicating whether the schedule is part of a system.
     * @param errorMsg String to hold the description of the error if validation fails.
     * @return True if the schedule is valid, false otherwise.
     */
    bool isValidSchedule(const OpenAPI::OAISchedule &schedule, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Validates an OAIParameter object.
     * Checks that parameters specified are consistent with the system's requirements.
     *
     * @param param The parameter object to validate.
     * @return True if the parameter is valid, false otherwise.
     */
    bool isValidParameter(const OpenAPI::OAIParameter &param) const;

    /**
     * Validates an OAIInput object.
     * Confirms that input configurations are correct and viable for the system.
     *
     * @param input The input object to validate.
     * @return True if the input is valid, false otherwise.
     */
    bool isValidInput(const OpenAPI::OAIInput &input) const;

    /**
     * Validates an OAIOutput object.
     * Ensures output specifications are correctly formatted and applicable.
     *
     * @param output The output object to validate.
     * @return True if the output is valid, false otherwise.
     */
    bool isValidOutput(const OpenAPI::OAIOutput &output) const;

    /**
     * Validates an OAIPosition object.
     * Checks for the correctness of position details within a system or component.
     *
     * @param position The position object to validate.
     * @return True if the position is valid, false otherwise.
     */
    bool isValidPosition(const OpenAPI::OAIPosition &position) const;

    /**
     * Validates an OAIConnection object.
     * Ensures that connections between components are logically consistent and properly defined.
     *
     * @param connection The connection object to validate.
     * @param components List of all components related to this connection for cross-referencing.
     * @param errorMsg String to hold the description of the error if validation fails.
     * @return True if the connection is valid, false otherwise.
     */
    bool isValidConnection(const OpenAPI::OAIConnection &connection, const QList<OpenAPI::OAIComponentUI> &components, QString &errorMsg) const;

    /**
     * Validates an OAIComment object.
     * Ensures that any comments associated with a system or component are appropriately formatted.
     *
     * @param comment The comment object to validate.
     * @param errorMsg String to hold the description of the error if validation fails.
     * @return True if the comment is valid, false otherwise.
     */
    bool isValidComment(const OpenAPI::OAIComment &comment, QString &errorMsg) const;

};

}  // namespace OPGUISystemEditor
