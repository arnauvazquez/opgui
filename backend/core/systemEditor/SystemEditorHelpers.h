/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QList>
#include <QStringList>
#include "OAIComponentUI.h"
#include "OAIInput.h"
#include "OAIOutput.h"

namespace OPGUISystemEditor {

/**
 * Finds a component .dll library file by its library name in a certain folder
 * 
 * @param pathLibraries Path where the libraries are found.
 * @param component Component where the library needs to be found.
 * @param errorMsg Output parameter to hold any error message if the operation fails.
 * @return Returns true id library is found
 */
bool existComponentLibraryDll(const QString &pathLibraries, const QString &library, QString &errorMsg);

/**
 * Finds a component by its ID from a list of components.
 * 
 * @param id The ID of the component to find.
 * @param components A list of components to search through.
 * @return Pointer to the found component or nullptr if not found.
 */
const OpenAPI::OAIComponentUI* findComponentById(const int id, const QList<OpenAPI::OAIComponentUI>& components);

/**
 * Finds a component by its title from a list of components.
 * 
 * @param title The title of the component to find.
 * @param components A list of components to search through.
 * @return Pointer to the found component or nullptr if not found.
 */
const OpenAPI::OAIComponentUI* findComponentByTitle(const QString &title, const QList<OpenAPI::OAIComponentUI>& components);

/**
 * Finds an input by its ID from a list of inputs.
 * 
 * @param id The ID of the input to find.
 * @param inputs A list of inputs to search through.
 * @return Pointer to the found input or nullptr if not found.
 */
const OpenAPI::OAIInput* findInputById(const int id, const QList<OpenAPI::OAIInput>& inputs);

/**
 * Finds an output by its ID from a list of outputs.
 * 
 * @param id The ID of the output to find.
 * @param outputs A list of outputs to search through.
 * @return Pointer to the found output or nullptr if not found.
 */
const OpenAPI::OAIOutput* findOutputById(const int id, const QList<OpenAPI::OAIOutput>& outputs);

QStringList splitString(const QString& str, QChar delimiter);

} // namespace OPGUISystemEditor
