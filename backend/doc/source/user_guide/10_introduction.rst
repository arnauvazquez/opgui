..
  *******************************************************************************
    Copyright (c) 2023 Hexad GmbH


    This program and the accompanying materials are made available under the
    terms of the Eclipse Public License 2.0 which is available at
    http://www.eclipse.org/legal/epl-2.0.

    SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _introduction:

Introduction
============

In the following pages, we will provide a comprehensive explanation of the process for generating simulations in an effective manner.

We primarily operate within an efficient order on a positive trajectory, although this approach allows for some degree of flexibility.

System Requirements
===================

The software has been tested for Google Chrome, other browsers can be used but proper functioning cannot be assured. The prerequisites for the successful execution of opGUI closely resemble those for openPASS.

PRE screen
==========

To configure the PRE window, in the next pages we will provide an explanation for generating the required data.

.. toctree::
   :glob:
   :maxdepth: 1

   pre/*

RUN screen
==========

To configure the RUN window, in the next pages we will provide an explanation for generating the required data.

.. toctree::
   :glob:
   :maxdepth: 1

   run/*

System Editor Screen
====================

For the system editor window, in the next pages we will provide an explanation for generating the required data.

.. toctree::
   :glob:
   :maxdepth: 1

   sys_edit/*

Global Setup
============

This contains the paths that the whole applications uses and allows the user to change them according to their systems.

.. toctree::
   :glob:
   :maxdepth: 1

   global_setup/*