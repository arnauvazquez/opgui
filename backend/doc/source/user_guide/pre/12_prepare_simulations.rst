..
  *******************************************************************************
    Copyright (c) 2023 Hexad GmbH


    This program and the accompanying materials are made available under the
    terms of the Eclipse Public License 2.0 which is available at
    http://www.eclipse.org/legal/epl-2.0.

    SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _prepare_simulations:

Prepare the simulation configurations
=====================================

Navigation
----------

For configuring simulations, there are two navigational paths available. You can either access the options through the side menu or when on the homepage, select it directly from one of the central options displayed on the screen.

.. image:: ../../_static/use_cases/UC3/homepage.jpg


Tabs
----

For the moment we can not start configs from scratch. So only "Based on PCM" is available.

.. image:: ../../_static/use_cases/UC3/pre-tabs.jpg


Based on PCM Database
==========================================

In the image below, you can observe the default screen that you will encounter after navigating to "Prepare Simulation Configuration." and then clicking on the “Based on PCM-Database” Tab (currently the default option)

.. image:: ../../_static/use_cases/UC3/initial-view.jpg

Within this screen, you will discover four distinct sections.

Sections
--------

1. Agents
~~~~~~~~~
These agents are imported from the global setup and play a pivotal role as contributing elements in the simulation.

Should users wish to modify the agents, they can be done within this section, tailored specifically for this simulation.

.. image:: ../../_static/use_cases/UC3/pre-agent-1.jpg


2. IDs
~~~~~~
In this section, you can import an iDs list file, apply filters, and make selections for exporting.

.. image:: ../../_static/use_cases/UC3/pre-ids.jpg


3. Export
~~~~~~~~~
Finally, at the bottom, there is a bar that provides various options for exporting configs for the selected iDs.

.. image:: ../../_static/use_cases/UC3/pre-export.jpg


Experiment ID Section
---------------------

In the this section, you can import an IDs XML file as we saw in the last guide section. Now we will see all the available options.

1. Import XML
~~~~~~~~~~~~~
Before beginning to work with the iDs, it is necessary to import a PCM database. A modal window will open, displaying your computer's file system, allowing you to navigate and select the appropriate file containing all the necessary iDs for import.

.. image:: ../../_static/use_cases/UC3/pcm-browse.jpg


2. Select IDs
~~~~~~~~~~~~~
To proceed with exporting, it's necessary to select certain iDs. By default, all iDs are unselected, which is why the export option remains unavailable.

You will have various methods to select the iDs.

* Checkboxes

You can select the iDs by clicking on the respective checkboxes. You can repeat this action as many times as needed.

.. image:: ../../_static/use_cases/UC3/pcm-some-cases-selected.jpg


* Dropdown

If you open the dropdown menu located above the bar, which is situated after the XML Path section, you will find the option to either select or unselect all the iDs.

.. image:: ../../_static/use_cases/UC3/pcm-select.jpg


* Search

Within the same bar, you can utilize the search input to filter the iDs and locate the specific iD you're looking for.

.. image:: ../../_static/use_cases/UC3/pcm-search.jpg


* Filters

Lastly, you can also select iDs by importing a CSV file containing the list of desired iDs, which serves as a method to filter the iDs.

Export section
--------------

After selecting at least one iD, the export options will become accessible.

Remember, it's essential to ensure that you've selected all the necessary iDs before proceeding with the export.

.. image:: ../../_static/use_cases/UC3/pcm-some-cases-selected.jpg


Path to converted cases
-----------------------
If necessary, you have the option to modify the export file by utilizing the corresponding input within the export bar. However, please note that this action is not obligatory.

A new modal will open, displaying your computer's folder system. This way, you can select the new appropriate location.

.. image:: ../../_static/use_cases/UC3/pcm-save-other-folder.jpg


Ultimately, the new location will be displayed in the corresponding section.

.. image:: ../../_static/use_cases/UC3/pcm-new-save-location.jpg


Convert to configs
------------------
Prior to exporting, it's essential to convert all the information into an XML file. You can achieve this by clicking on the designated button assigned for this purpose.

.. image:: ../../_static/use_cases/UC3/pcm-exported-to-configs.jpg


Export to simulation
--------------------
The final step involves exporting to simulation after clicking on the Export so simulation button this dialog will appear.

.. image:: ../../_static/use_cases/UC3/pcm-exported-to-simulation.jpg

An XML file will be generated and saved in the designated location. By navigating through your computer's file system, you will be able to locate the generated XML file.

.. image:: ../../_static/use_cases/UC3/pcm-saved-exported-content.jpg

Based on New Configuration
==========================
Based on new configuration allows us to create a new configuration from scratch.
A configuration is composed of different files. In here each XML file is represented by a hexagon.
To edit a hexagon simply click on it and a form will open.

Simulation Config
-----------------
This is the main hexagon. From here we will configure the changes and odds of the simulation. Such as weather, time of day, friction and more.
This hexagon has four distinct sections

Scenario
~~~~~~~~~~~
This section contains information about the scenario setup for the experiment. This information does not change between invocations.

Open Scenario File (list)
^^^^^^^^^^^^^^^^^^^^^^^^^^
Name of the scenario file

Environment
~~~~~~~~~~~
This section contains information about the world and the general environment inside the simulation. Every invocation re-rolls the environment probabilities. All probabilities need to add up to 1.0.

Time of day (list)
^^^^^^^^^^^^^^^^^^
This is a list composed of the hour of the day the experiment will take place into and the probability of it happening.

Visibility Distances (list)
^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is a list composed of driver's view distance [meters] and and the probability of that view distance.

Frictions (list)
^^^^^^^^^^^^^^^^
Friction on the road. Used by DynamicsRegularDriving and LimiterAccelerationVehicleComponents.
This is a list composed of friction (number between 0 and 1) and the probability of that friction.

Weathers (list)
^^^^^^^^^^^^^^^^
This is a list composed of weather (clear, rainy, snowy, etc) and the probability of that weather.

Turning Rates (list)
^^^^^^^^^^^^^^^^^^^^
If routes are not defined in the Scenario, they are randomly generated. Starting at the current agent position at each junction a random connector is chosen based on the weights given in the TurningRates element, until the route terminates at a road without successors or a maximum depth is reached. If for a pair of incoming road / connector no weight is given, it is defaulted to 1

- **Incoming**: Id of the incoming road

- **Outgoing**: Id of the connector

- **Weight**: Weight for this pair

Traffic Rules
^^^^^^^^^^^^^^
Defines which set of TrafficRules to use from the ProfilesCatalog

Experiment
~~~~~~~~~~~

Experiment ID (number)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Set the ID of the experiment, must be a number equal or bigger than 0.

Number of Invocations (number)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Number of invocation in the experiment. For each invocation probabilities are rerolled.

Random Seed (number)
^^^^^^^^^^^^^^^^^^^^^
Random seed for the entire experiment. Must be within the bounds of an unsigned integer.

Libraries
^^^^^^^^^^^^^^^^
Name of the core module Libraries to use. If not specified the default name is assumed.

- **World Library** (selector)

- **Stochastic Library** (selector)

- **Data Buffer Library** (selector)

Observation Log
~~~~~~~~~~~~~~~~

Log Cycles to CSV (boolean)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If true, cycles are written in an additional file (one CSV file per run)

Log File Name (text)
^^^^^^^^^^^^^^^^^^^^^^
If the .log is missing at the end, the system will add it automatically.

Logging Groups (list)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Which actions will be part of the log file when the application is run.

**Enabled**: The logs are recorded only if they are enabled.

**Group**: The group id of the log.

**Values**: the values of the group to be recorded, separate each with a "," and don't leave any spaces.

Adding custom values: You can add custom logging groups by clicking the ADD button.

Observation Entity Repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

File Name Prefix (text)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The prefix that will be automatically added before the logging group name (Example: entity_Sensor).

Write Persistent Entities (text) (optional)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Can be filled with one of the following values:

- **Consolidated** (default): Logged together with non persistent entities and hence duplicated for each run.

- **Separate**: Written once into ``{FilenamePrefix}_Persistent.csv``

- **Skip**: No output.

Spawner (list)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A list of the Spawners.
In this section the spawners are defined with their Profile (defined in the ProfilesCatalog). The same library can be loaded multiple times with different profiles. A spawner is either of type “PreRun”, meaning it is triggered only once at the start of the simulation, or “Runtime”, meaning it is triggered in every timestep. If different spawners are to be triggered at the same time the spawner with the highest priority is triggered first

- **Library** (selector): the library that the spawner will use.
- **Type** (selector): Select if the Spawner will run previously (PreRun) or at Runtime.
- **Priority** (number): The priority of the spawner.
- **Profile** (text): the profile name for this spawner.