/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef XMLSCENARIO_H
#define XMLSCENARIO_H

#include "XoscTrajectory.h"
#include "pcm_trajectory.h"
#include "pcm_participantData.h"

class XoscScenario : public XmlBaseClass
{
public:
    XoscScenario() = default;
    virtual ~XoscScenario() = default;

    bool WriteToXml( QXmlStreamWriter *xmlWriter );

    bool WriteToXml( QXmlStreamWriter *xmlWriter,const std::vector<PCM_ParticipantData *> &participants);

    void AddTrajectory(int agentId,
                       const PCM_Trajectory *trajectory);

private:
    std::vector<XoscTrajectory> trajectories;     //!< vector of all trajectories of all agents
};

#endif // XMLSCENARIO_H
