/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIApiRouterOP.h"


    OAIApiRouterOP::OAIApiRouterOP()
    {
        OAIApiRouterOP::createApiHandlers();
    }

    OAIApiRouterOP::~OAIApiRouterOP() = default;

    void OAIApiRouterOP::createApiHandlers() {
        QSharedPointer<OpenAPI::OAIConvertToConfigsApiHandlerOP> mOAIConvertToConfigsApiHandlerOP = QSharedPointer<OpenAPI::OAIConvertToConfigsApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIDeleteInformationApiHandlerOP> mOAIDeleteInformationApiHandlerOP = QSharedPointer<OpenAPI::OAIDeleteInformationApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIExportOpsimulationManagerXmlApiHandlerOP> mOAIExportOpsimulationManagerXmlApiHandlerOP = QSharedPointer<OpenAPI::OAIExportOpsimulationManagerXmlApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIExportToSimulationApiHandlerOP> mOAIExportToSimulationApiHandlerOP = QSharedPointer<OpenAPI::OAIExportToSimulationApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIPathToConvertedCasesApiHandlerOP> mOAIPathToConvertedCasesApiHandlerOP = QSharedPointer<OpenAPI::OAIPathToConvertedCasesApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIRunOpSimulationApiHandlerOP> mOAIRunOpSimulationApiHandlerOP = QSharedPointer<OpenAPI::OAIRunOpSimulationApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAISendPCMFileApiHandlerOP> mOAISendPCMFileApiHandlerOP = QSharedPointer<OpenAPI::OAISendPCMFileApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIVerifyPathApiHandlerOP> mOAIVerifyPathApiHandlerOP = QSharedPointer<OpenAPI::OAIVerifyPathApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIComponentsApiHandlerOP> mOAIComponentsApiHandlerOP = QSharedPointer<OpenAPI::OAIComponentsApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIGetFileTreeApiHandlerOP> mOAIGetFileTreeApiHandlerOP = QSharedPointer<OpenAPI::OAIGetFileTreeApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIGetConfigElementsApiHandlerOP> mOAIGetConfigElementsApiHandlerOP = QSharedPointer<OpenAPI::OAIGetConfigElementsApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAISetConfigElementsApiHandlerOP> mOAISetConfigElementsApiHandlerOP = QSharedPointer<OpenAPI::OAISetConfigElementsApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIDeleteSystemApiHandlerOP> mOAIDeleteSystemApiHandlerOP = QSharedPointer<OpenAPI::OAIDeleteSystemApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIGetSystemApiHandlerOP> mOAIGetSystemApiHandlerOP = QSharedPointer<OpenAPI::OAIGetSystemApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAISaveSystemApiHandlerOP> mOAISaveSystemApiHandlerOP = QSharedPointer<OpenAPI::OAISaveSystemApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIGetFileContentsApiHandlerOP> mOAIGetFileContentsApiHandlerOP = QSharedPointer<OpenAPI::OAIGetFileContentsApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIGetValueListApiHandlerOP> mOAIGetValueListApiHandlerOP = QSharedPointer<OpenAPI::OAIGetValueListApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIIsSimulationRunningApiHandlerOP> mOAIIsSimulationRunningApiHandlerOP = QSharedPointer<OpenAPI::OAIIsSimulationRunningApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAISaveConfigEditorApiHandlerOP> mOAISaveConfigEditorApiHandlerOP = QSharedPointer<OpenAPI::OAISaveConfigEditorApiHandlerOP>::create();
        QSharedPointer<OpenAPI::OAIGetConfigEditorApiHandlerOP> mOAIGetConfigEditorApiHandlerOP = QSharedPointer<OpenAPI::OAIGetConfigEditorApiHandlerOP>::create();

        setOAIConvertToConfigsApiHandler(mOAIConvertToConfigsApiHandlerOP);
        setOAIDeleteInformationApiHandler(mOAIDeleteInformationApiHandlerOP);
        setOAIExportOpsimulationManagerXmlApiHandler(mOAIExportOpsimulationManagerXmlApiHandlerOP);
        setOAIExportToSimulationApiHandler(mOAIExportToSimulationApiHandlerOP);
        setOAIPathToConvertedCasesApiHandler(mOAIPathToConvertedCasesApiHandlerOP);
        setOAIRunOpSimulationApiHandler(mOAIRunOpSimulationApiHandlerOP);
        setOAISendPCMFileApiHandler(mOAISendPCMFileApiHandlerOP);
        setOAIVerifyPathApiHandler(mOAIVerifyPathApiHandlerOP);
        setOAIComponentsApiHandler(mOAIComponentsApiHandlerOP);
        setOAIGetFileTreeApiHandler(mOAIGetFileTreeApiHandlerOP);
        setOAIGetConfigElementsApiHandler(mOAIGetConfigElementsApiHandlerOP);
        setOAISetConfigElementsApiHandler(mOAISetConfigElementsApiHandlerOP);
        setOAIDeleteSystemApiHandler(mOAIDeleteSystemApiHandlerOP);
        setOAIGetSystemApiHandler(mOAIGetSystemApiHandlerOP);
        setOAISaveSystemApiHandler(mOAISaveSystemApiHandlerOP);
        setOAIGetFileContentsApiHandler(mOAIGetFileContentsApiHandlerOP);
        setOAIGetValueListApiHandler(mOAIGetValueListApiHandlerOP);
        setOAIIsSimulationRunningApiHandler(mOAIIsSimulationRunningApiHandlerOP);
        setOAISaveConfigEditorApiHandler(mOAISaveConfigEditorApiHandlerOP);
        setOAIGetConfigEditorApiHandler(mOAIGetConfigEditorApiHandlerOP);
    }
