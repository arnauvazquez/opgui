/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIGetConfigEditorApiHandlerOP.h"
#include "OAIGetConfigEditorApiRequest.h"
#include "OAIConfigEditorObject.h"
#include "OAICustomHelpers.h"
#include "OPGUICore.h" 

namespace OpenAPI {

OAIGetConfigEditorApiHandlerOP::OAIGetConfigEditorApiHandlerOP(){

}

OAIGetConfigEditorApiHandlerOP::~OAIGetConfigEditorApiHandlerOP(){

}

void OAIGetConfigEditorApiHandlerOP::apiConfigEditorGet(QString path) {
    auto reqObj = qobject_cast<OAIGetConfigEditorApiRequest*>(sender());
    if (reqObj != nullptr)
    {
        OpenAPI::OAIConfigEditorObject res;
        QString errorMsg; 

        if (OPGUICore::api_import_config_editor(path,res, errorMsg))
        {
            reqObj->apiConfigEditorGetResponse(res);
        }
        else
        {
            int code=500;
            OAIError500 error;
            error.setMessage("Server error");
            if (!errorMsg.isEmpty()) {
                error.setMessage(error.getMessage() + " - Details: " + errorMsg);
            }
            handleSocketResponse(reqObj, error.asJsonObject(), code);
        }
    }
    else{
            int code=400;
            OAIError400 error;
            error.setMessage("Bad client request");
            handleSocketResponse(reqObj, error.asJsonObject(), code);
    }
}

}
