/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetConfigEditorApiHandler_OP_H
#define OAI_OAIGetConfigEditorApiHandler_OP_H

#include <QObject>

#include "OAIConfigEditorObject.h"
#include "OAIGetConfigEditorApiHandler.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include <QString>

namespace OpenAPI {

class OAIGetConfigEditorApiHandlerOP : public OAIGetConfigEditorApiHandler
{
    Q_OBJECT

public:
    OAIGetConfigEditorApiHandlerOP();
    virtual ~OAIGetConfigEditorApiHandlerOP();


public Q_SLOTS:
    virtual void apiConfigEditorGet(QString path);
    

};

}

#endif // OAI_OAIGetConfigEditorApiHandler_OP_H
