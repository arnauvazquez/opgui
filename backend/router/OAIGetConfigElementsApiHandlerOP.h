/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetConfigElementsApiHandler_OP_H
#define OAI_OAIGetConfigElementsApiHandler_OP_H

#include <QObject>
#include <QList>
#include <QString>

#include "OAIConfigElement.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAIDefault200Response.h"
#include "OAIGetConfigElementsApiHandler.h"


namespace OpenAPI {

class OAIGetConfigElementsApiHandlerOP : public OAIGetConfigElementsApiHandler
{
    Q_OBJECT

public:
    OAIGetConfigElementsApiHandlerOP();
    ~OAIGetConfigElementsApiHandlerOP() override;


public slots:
    void apiConfigElementsGet() override;
    

};

}

#endif // OAI_OAIGetConfigElementsApiHandler_OP_H
