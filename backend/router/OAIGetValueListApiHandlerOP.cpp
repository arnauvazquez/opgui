/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAICustomHelpers.h"
#include "OAIGetValueListApiHandlerOP.h"
#include "OAIGetValueListApiRequest.h"
#include "OPGUICore.h"

namespace OpenAPI {

OAIGetValueListApiHandlerOP::OAIGetValueListApiHandlerOP() = default;

OAIGetValueListApiHandlerOP::~OAIGetValueListApiHandlerOP() = default;

void OAIGetValueListApiHandlerOP::apiGetValueListGet(QString name) {
    auto reqObj = qobject_cast<OAIGetValueListApiRequest*>(sender());

    if( reqObj != nullptr )
    {
        OAI_api_getValueList_get_200_response res;
        QString errorMsg; 
        
        if(OPGUICore::api_get_value_list(name,res, errorMsg))
        {
            reqObj->apiGetValueListGetResponse(res);
        }
        else
        {
            int code=500;
            OAIError500 error;
            error.setMessage("Server error");
            if (!errorMsg.isEmpty()) {
                error.setMessage(error.getMessage() + " - Details: " + errorMsg);
            }
            handleSocketResponse(reqObj, error.asJsonObject(), code);
        }

    }
    else{
            int code=400;
            OAIError400 error;
            error.setMessage("Bad client request");
            handleSocketResponse(reqObj, error.asJsonObject(), code);
    }
}


}
