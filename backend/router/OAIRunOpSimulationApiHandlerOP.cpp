/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>
#include <QNetworkReply>

#include "OAIRunOpSimulationApiHandlerOP.h"
#include "OAIRunOpSimulationApiRequest.h"
#include "OAICustomHelpers.h"
#include "OAIDefault200Response.h"
#include "OAIError500.h"
#include "OPGUICore.h"

namespace OpenAPI {

    OAIRunOpSimulationApiHandlerOP::OAIRunOpSimulationApiHandlerOP()  = default;

    OAIRunOpSimulationApiHandlerOP::~OAIRunOpSimulationApiHandlerOP()  = default;

    void OAIRunOpSimulationApiHandlerOP::apiRunOpSimulationGet(QString config_path) {
        auto reqObj = qobject_cast<OAIRunOpSimulationApiRequest*>(sender());
        OAIDefault200Response res;
        QString errorMsg;

        if (reqObj != nullptr)
        {
            if (OPGUICore::api_run_opSimulation(config_path,res,errorMsg))
            {
                reqObj->apiRunOpSimulationGetResponse(res);
            }
            else
            {
                int code=500;
                OAIError500 error;
                error.setMessage("Server error");
                if (!errorMsg.isEmpty()) {
                    error.setMessage(error.getMessage() + " - Details: " + errorMsg);
                }
                handleSocketResponse(reqObj, error.asJsonObject(), code);
            }
        }
    }

}
