/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISaveConfigEditorApiHandler_OP_H
#define OAI_OAISaveConfigEditorApiHandler_OP_H

#include <QObject>

#include "OAISaveConfigEditorApiHandler.h"
#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_configEditor_post_request.h"
#include <QString>

namespace OpenAPI {

class OAISaveConfigEditorApiHandlerOP : public OAISaveConfigEditorApiHandler
{
    Q_OBJECT

public:
    OAISaveConfigEditorApiHandlerOP();
    virtual ~OAISaveConfigEditorApiHandlerOP();


public Q_SLOTS:
    virtual void apiConfigEditorPost(OAI_api_configEditor_post_request oai_api_config_editor_post_request);
    

};

}

#endif // OAI_OAISaveConfigEditorApiHandler_OP_H
