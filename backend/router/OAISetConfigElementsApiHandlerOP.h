/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISetConfigElementsApiHandler_OP_H
#define OAI_OAISetConfigElementsApiHandler_OP_H

#include <QObject>
#include <QList>
#include <QString>

#include "OAIConfigElement.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAIDefault200Response.h"
#include "OAISetConfigElementsApiHandler.h"


namespace OpenAPI {

class OAISetConfigElementsApiHandlerOP : public OAISetConfigElementsApiHandler
{
    Q_OBJECT

public:
    OAISetConfigElementsApiHandlerOP();
    ~OAISetConfigElementsApiHandlerOP() override;


public slots:
    void apiConfigElementsPost(QList<OAIConfigElement> oai_config_element) override;
    

};

}

#endif // OAI_OAISetConfigElementsApiHandler_OP_H
