/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <gtest/gtest.h>
#include <QString>

#include "OPGUICoreGlobalConfig.h"

class GLOBAL_CONFIG : public ::testing::Test {
  public:
    QString backupFilePath;
    QString originalFilePath;

    OPGUICoreGlobalConfig* config;

    void SetUp() override;
    void TearDown() override;
};