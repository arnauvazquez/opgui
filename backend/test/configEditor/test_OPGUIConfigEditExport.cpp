/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDebug>
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QBuffer>
#include <QJsonDocument>

#include "test_OPGUIConfigEditExport.h"
#include "OAIConfigEditorObject.h"
#include "OAIHelpers.h"
#include "OPGUIConfigEditorExport.h"
#include "test_helpers.h"
#include "OPGUICoreGlobalConfig.h"

// Sample config editor object in JSON format
QString configEditorObjectJSON = QStringLiteral(
    R"({
        "scenario": "/path/to/scenario.xosc",
        "sceneryConfig": "/path/to/scenery.xml",
        "systemConfig": "/path/to/system.xml",
        "vehiclesCatalog": "/path/to/vehicles.xml",
        "pedestriansCatalog": "/path/to/pedestrians.xml",
        "profilesCatalog": "/path/to/profiles.xml",
        "simulationConfig": {
            "profilesCatalog": "/path/to/profiles/catalog.xml",
            "experiment": {
                "experimentID": 123,
                "numberOfInvocations": 1,
                "randomSeed": 532725206,
                "libraries": {
                    "worldLibrary": "World_OSI",
                    "dataBufferLibrary": "DataBuffer",
                    "stochasticsLibrary": "Stochastics"
                }
            },
            "scenario": {
                "openScenarioFile": "/path/to/scenario.xosc"
            },
            "environment": {
                "visibilityDistances": [
                    {
                        "probability": 0.7,
                        "value": 350
                    },
                    {
                        "probability": 0.3,
                        "value": 500
                    }
                ],
                "timeOfDays": [
                    {
                        "probability": 0.7,
                        "value": 15
                    },
                    {
                        "probability": 0.3,
                        "value": 22
                    }
                ],
                "frictions": [
                    {
                        "probability": 0.33,
                        "value": 0.85
                    },
                    {
                        "probability": 0.33,
                        "value": 0.9
                    },
                    {
                        "probability": 0.34,
                        "value": 0.75
                    }
                ],
                "weathers": [
                    {
                        "probability": 0.5,
                        "value": "Clear"
                    },
                    {
                        "probability": 0.5,
                        "value": "rainy"
                    }
                ],
                "trafficRules": "DE",
                "turningRates": [
                    {
                        "incoming": "lane1",
                        "outgoing": "lane2",
                        "weight": 0.3
                    },
                    {
                        "incoming": "lane2",
                        "outgoing": "lane3",
                        "weight": 0.7
                    }
                ]
            },
            "observations": {
                "log": {
                    "loggingCyclesToCsv": true,
                    "fileName": "output.log",
                    "loggingGroups": [
                        {
                            "enabled": true,
                            "group": "Trace",
                            "values": "group1,group2"
                        },
                        {
                            "enabled": true,
                            "group": "RoadPosition",
                            "values": "pos1,pos2"
                        }
                    ]
                },
                "entityRepository": {
                    "fileNamePrefix": "entity_",
                    "writePersistentEntities": "Consolidated"
                }
            },
            "spawners": [
                {
                    "library": "SpawnerPreRunCommon",
                    "type": "PreRun",
                    "priority": 0,
                    "profile": "CustomProfileName"
                },
                {
                    "library": "SpawnerRuntimeCommon",
                    "type": "Runtime",
                    "priority": 1
                }
            ]
        }
    })"
);

void CONFIG_EDITOR_EXPORT_TEST::SetUp() {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    this->workspacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirFullPath = TestHelpers::joinPaths(this->workspacePath, "test");
    this->sampleConfigPath = TestHelpers::joinPaths(this->testDirFullPath, "configuration");

    QDir dir(this->workspacePath);
    ASSERT_TRUE(dir.exists()) 
        << "The workspace directory does not exist: " << this->workspacePath.toStdString();

    // If test dir exists, remove it
    QDir dirTest(this->testDirFullPath);
    if (dirTest.exists()) {
        ASSERT_TRUE(dirTest.removeRecursively()) 
            << "Failed to remove the test directory: " << this->testDirFullPath.toStdString();
    }

    ASSERT_TRUE(dir.mkpath(this->sampleConfigPath)) 
        << "Failed to create configuration directory path: " << this->sampleConfigPath.toStdString();
}

void CONFIG_EDITOR_EXPORT_TEST::TearDown() {
    QDir dirTest(this->testDirFullPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) 
            << "Failed to remove test directory: " << this->testDirFullPath.toStdString();
    }
}

bool CONFIG_EDITOR_EXPORT_TEST::parseConfigEditorObjectJson(const QString& jsonStr, OpenAPI::OAIConfigEditorObject& configEditorObj) {
    QJsonDocument doc = QJsonDocument::fromJson(jsonStr.toUtf8());
    if (doc.isNull()) {
        qDebug() << "Failed to create JSON doc.";
        return false; 
    }

    if (!doc.isObject()) {
        qDebug() << "JSON obtained is not an object";
        return false;  
    }
    
    QJsonObject jsonObject = doc.object();
    OpenAPI::fromJsonValue(configEditorObj, jsonObject);

    return true;
}

TEST_F(CONFIG_EDITOR_EXPORT_TEST, SaveConfigEditor_Valid_POSITIVE) {
    OPGUIConfigEditor::OPGUIConfigEditorExport exporter;
    QString errorMsg;

    // Parse JSON into config editor object
    OpenAPI::OAIConfigEditorObject configEditorObj;
    ASSERT_TRUE(parseConfigEditorObjectJson(configEditorObjectJSON, configEditorObj))
        << "Failed to parse config editor object JSON.";

    // Test saving to a valid path
    ASSERT_TRUE(exporter.saveConfigEditor(configEditorObj, this->sampleConfigPath, errorMsg))
        << "Failed to save config editor object: " << errorMsg.toStdString();

    // Verify that the necessary files were created
    QDir configDir(this->sampleConfigPath);
    ASSERT_TRUE(configDir.exists()) << "Configuration directory was not created";

    // Verify simulation config file
    QString simConfigPath = TestHelpers::joinPaths(this->sampleConfigPath, "simulationConfig.xml");
    ASSERT_TRUE(QFile::exists(simConfigPath)) << "Simulation config file was not created under "+simConfigPath.toStdString();
}

TEST_F(CONFIG_EDITOR_EXPORT_TEST, SaveConfigEditor_InvalidPath_NEGATIVE) {
    OPGUIConfigEditor::OPGUIConfigEditorExport exporter;
    QString errorMsg;

    // Parse JSON into config editor object
    OpenAPI::OAIConfigEditorObject configEditorObj;
    ASSERT_TRUE(parseConfigEditorObjectJson(configEditorObjectJSON, configEditorObj))
        << "Failed to parse config editor object JSON.";

    // Test saving to an invalid path
    QString invalidPath = "/nonexistent/path/config";
    ASSERT_FALSE(exporter.saveConfigEditor(configEditorObj, invalidPath, errorMsg))
        << "Save operation should have failed for invalid path";
    ASSERT_FALSE(errorMsg.isEmpty()) << "Error message should not be empty for invalid path";
}

TEST_F(CONFIG_EDITOR_EXPORT_TEST, SaveConfigEditor_EmptyObject_NEGATIVE) {
    OPGUIConfigEditor::OPGUIConfigEditorExport exporter;
    QString errorMsg;

    // Create an empty config editor object
    OpenAPI::OAIConfigEditorObject emptyConfig;

    // Test saving empty object
    QString testPath = TestHelpers::joinPaths(this->sampleConfigPath, "empty_config");
    ASSERT_FALSE(exporter.saveConfigEditor(emptyConfig, testPath, errorMsg))
        << "Save operation should have failed for empty config object";
    ASSERT_FALSE(errorMsg.isEmpty()) << "Error message should not be empty for invalid config";
}

TEST_F(CONFIG_EDITOR_EXPORT_TEST, SaveConfigEditor_InvalidSimulationConfig_NEGATIVE) {
    OPGUIConfigEditor::OPGUIConfigEditorExport exporter;
    QString errorMsg;

    // Create a config editor object with invalid simulation config
    QString invalidJSON = configEditorObjectJSON;
    invalidJSON.replace("\"experimentID\": 123", "\"experimentID\": \"invalid\"");

    OpenAPI::OAIConfigEditorObject invalidConfig;
    ASSERT_TRUE(parseConfigEditorObjectJson(invalidJSON, invalidConfig))
        << "Failed to parse invalid config editor object JSON.";

    // Test saving invalid config
    QString testPath = TestHelpers::joinPaths(this->sampleConfigPath, "invalid_config");
    ASSERT_FALSE(exporter.saveConfigEditor(invalidConfig, testPath, errorMsg))
        << "Save operation should have failed for invalid simulation config";
    ASSERT_FALSE(errorMsg.isEmpty()) << "Error message should not be empty for invalid simulation config";
}
