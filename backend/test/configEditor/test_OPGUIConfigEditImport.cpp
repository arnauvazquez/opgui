/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIConfigEditImport.h"
#include "OPGUIConfigEditorImport.h"
#include "OPGUICoreGlobalConfig.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

// Using the same test XML from simulation config tests
QString simConfigXmlToImport = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
       <simulationConfig SchemaVersion="0.8.2">
        <ProfilesCatalog>/path/to/profiles/catalog.xml</ProfilesCatalog>
        <Experiment>
            <ExperimentID>123</ExperimentID>
            <NumberOfInvocations>1</NumberOfInvocations>
            <RandomSeed>532725206</RandomSeed>
            <Libraries>
                <WorldLibrary>World_OSI</WorldLibrary>
                <DataBufferLibrary>DataBuffer</DataBufferLibrary>
                <StochasticsLibrary>Stochastics</StochasticsLibrary>
            </Libraries>
        </Experiment>
        <Scenario>
            <OpenScenarioFile>/path/to/scenario.xosc</OpenScenarioFile>
        </Scenario>
        <Environment>
            <TimeOfDays>
                <TimeOfDay Probability="0.5" Value="15"/>
                <TimeOfDay Probability="0.5" Value="22"/>
            </TimeOfDays>
            <VisibilityDistances>
                <VisibilityDistance Probability="0.5" Value="350"/>
                <VisibilityDistance Probability="0.5" Value="500"/>
            </VisibilityDistances>
            <Frictions>
                <Friction Probability="0.5" Value="0.5"/>
                <Friction Probability="0.5" Value="1"/>
            </Frictions>
            <Weathers>
                <Weather Probability="0.5" Value="Clear"/>
                <Weather Probability="0.5" Value="rainy"/>
            </Weathers>
            <TrafficRules>DE</TrafficRules>
            <TurningRates>
                <TurningRate Incoming="lane1" Outgoing="lane2" Weight="100"/>
                <TurningRate Incoming="lane2" Outgoing="lane3" Weight="1000"/>
            </TurningRates>
        </Environment>
        <Observations>
            <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="output.log"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="true"/>
                    <StringVector Key="LoggingGroup_Trace" Value="group1,group2"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="pos1,pos2"/>
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Trace"/>
                </Parameters>
            </Observation>
            <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="FilenamePrefix" Value="entity_"/>
                    <String Key="WritePersistentEntities" Value="Consolidated"/>
                </Parameters>
            </Observation>
        </Observations>
        <Spawners>
            <Spawner>
                <Library>SpawnerPreRunCommon</Library>
                <Type>PreRun</Type>
                <Priority>0</Priority>
                <Profile>CustomProfileName</Profile>
            </Spawner>
            <Spawner>
                <Library>SpawnerRuntimeCommon</Library>
                <Type>Runtime</Type>
                <Priority>1</Priority>
            </Spawner>
        </Spawners>
    </simulationConfig>)"
);

// Expected JSON structure for comparison
QString configEditObjectJSONToCompare = QStringLiteral(
    R"({"scenario": "",
        "sceneryConfig": "",
        "systemConfig": "",
        "vehiclesCatalog": "",
        "pedestriansCatalog": "",
        "profilesCatalog": "",
        "simulationConfig":{
        "profilesCatalog": "/path/to/profiles/catalog.xml",
        "experiment": {
            "experimentID": 123,
            "numberOfInvocations": 1,
            "randomSeed": 532725206,
            "libraries": {
                "worldLibrary": "World_OSI",
                "dataBufferLibrary": "DataBuffer",
                "stochasticsLibrary": "Stochastics"
            }
        },
        "scenario": {
            "openScenarioFile": "/path/to/scenario.xosc"
        },
        "environment": {
            "visibilityDistances": [
                {
                    "probability": 0.5,
                    "value": 350
                },
                {
                    "probability": 0.5,
                    "value": 500
                }
            ],
            "timeOfDays": [
                {
                    "probability": 0.5,
                    "value": 15
                },
                {
                    "probability": 0.5,
                    "value": 22
                }
            ],
            "frictions": [
                {
                    "probability": 0.5,
                    "value": 0.5
                },
                {
                    "probability": 0.5,
                    "value": 1
                }
            ],
            "weathers": [
                {
                    "probability": 0.5,
                    "value": "Clear"
                },
                {
                    "probability": 0.5,
                    "value": "rainy"
                }
            ],
            "trafficRules": "DE",
            "turningRates": [
                {
                    "incoming": "lane1",
                    "outgoing": "lane2",
                    "weight": 100
                },
                {
                    "incoming": "lane2",
                    "outgoing": "lane3",
                    "weight": 1000
                }
            ]
        },
         "observations": {
            "log": {
                "loggingCyclesToCsv": true,
                "fileName": "output.log",
                "loggingGroups": [
                    {
                        "enabled": true,
                        "group": "Trace",
                        "values": "group1,group2"
                    },
                    {
                        "enabled": true,
                        "group": "RoadPosition",
                        "values": "pos1,pos2"
                    }
                ]
            },
            "entityRepository": {
                "fileNamePrefix": "entity_",
                "writePersistentEntities": "Consolidated"
            }
        },
        "spawners": [
            {
                "library": "SpawnerPreRunCommon",
                "type": "PreRun",
                "priority": 0,
                "profile": "CustomProfileName"
            },
            {
                "library": "SpawnerRuntimeCommon",
                "type": "Runtime",
                "priority": 1
            }
        ]
    }
    })"
);

void CONFIG_EDITOR_IMPORT_TEST::SetUp() {
    // Create test directory structure
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    QString workspacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirPath = TestHelpers::joinPaths(workspacePath, "test_configs");

    this->testSimConfigFileName = "simulationConfig.xml";
    QString simConfigFileFullPath = TestHelpers::joinPaths(this->testDirPath , this->testSimConfigFileName);

    // Create test directory
    QDir dir;
    if (!dir.exists(this->testDirPath)) {
        ASSERT_TRUE(dir.mkpath(this->testDirPath)) << "Failed to create test directory";
    }

    // Create test config file with the test XML content
    ASSERT_TRUE(TestHelpers::createAndCheckFile(simConfigFileFullPath, simConfigXmlToImport))
        << "Failed to create test configuration file";
}

void CONFIG_EDITOR_IMPORT_TEST::TearDown() {
    // Clean up test directory and files
    QDir dir(this->testDirPath);
    if (dir.exists()) {
        EXPECT_TRUE(dir.removeRecursively()) << "Failed to clean up test directory";
    }
}

TEST_F(CONFIG_EDITOR_IMPORT_TEST, valid_config_loaded_correctly_POSITIVE) {
    QString errorMsg;

    OPGUIConfigEditor::OPGUIConfigEditorImport importer;
    OpenAPI::OAIConfigEditorObject configObj;

    bool result = importer.loadConfiguration(this->testDirPath, configObj, errorMsg);

    ASSERT_TRUE(result) << "Import failed with error: " << errorMsg.toStdString();
    
    // Convert the loaded config object to JSON for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(configObj).toObject());
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    // Load expected JSON
    QJsonDocument expectedDoc = QJsonDocument::fromJson(configEditObjectJSONToCompare.toUtf8());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    EXPECT_EQ(actualJsonString, expectedJsonString)
        << "The imported config does not match the expected JSON.\n"
        << "Expected JSON:\n" << QJsonDocument::fromJson(expectedJsonString.toUtf8()).toJson(QJsonDocument::Indented).toStdString() << "\n"
        << "Actual JSON:\n" << QJsonDocument::fromJson(actualJsonString.toUtf8()).toJson(QJsonDocument::Indented).toStdString();
}

TEST_F(CONFIG_EDITOR_IMPORT_TEST, empty_path_NEGATIVE) {
    QString errorMsg;
    OpenAPI::OAIConfigEditorObject configObj;
    
    OPGUIConfigEditor::OPGUIConfigEditorImport importer;
    bool result = importer.loadConfiguration("", configObj, errorMsg);

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("Configuration file path is empty"))
        << "Expected error about empty path, got: " << errorMsg.toStdString();
}

TEST_F(CONFIG_EDITOR_IMPORT_TEST, nonexistent_file_NEGATIVE) {
    QString testFilePath = "nonexistent_config.xml";
    QString errorMsg;
    OpenAPI::OAIConfigEditorObject configObj;
    
    OPGUIConfigEditor::OPGUIConfigEditorImport importer;
    bool result = importer.loadConfiguration(testFilePath, configObj, errorMsg);

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("Failed to import simulation config"))
        << "Expected error about import failure, got: " << errorMsg.toStdString();
}

TEST_F(CONFIG_EDITOR_IMPORT_TEST, empty_file_NEGATIVE) {
    QString testFilePath = "empty_config.xml";
    QString errorMsg;
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(testFilePath, ""))
        << "Failed to create empty test file";

    OpenAPI::OAIConfigEditorObject configObj;
    OPGUIConfigEditor::OPGUIConfigEditorImport importer;
    bool result = importer.loadConfiguration(testFilePath, configObj, errorMsg);

    EXPECT_TRUE(TestHelpers::deleteFileIfExists(testFilePath))
        << "Failed to cleanup test file";

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("Failed to import simulation config"))
        << "Expected error about empty file, got: " << errorMsg.toStdString();
}

TEST_F(CONFIG_EDITOR_IMPORT_TEST, invalid_xml_content_NEGATIVE) {
    QString testFilePath = "invalid_config.xml";
    QString errorMsg;
    
    QString invalidXml = "This is not valid XML content";
    ASSERT_TRUE(TestHelpers::createAndCheckFile(testFilePath, invalidXml))
        << "Failed to create test file with invalid XML";

    OpenAPI::OAIConfigEditorObject configObj;
    OPGUIConfigEditor::OPGUIConfigEditorImport importer;
    bool result = importer.loadConfiguration(testFilePath, configObj, errorMsg);

    EXPECT_TRUE(TestHelpers::deleteFileIfExists(testFilePath))
        << "Failed to cleanup test file";

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("Failed to import simulation config"))
        << "Expected error about invalid XML content, got: " << errorMsg.toStdString();
}

TEST_F(CONFIG_EDITOR_IMPORT_TEST, wrong_root_element_NEGATIVE) {
    QString testFilePath = "wrong_root_config.xml";
    QString errorMsg;
    
    QString wrongRootXml = R"(<?xml version="1.0" encoding="UTF-8"?>
        <wrongRoot>
            <ProfilesCatalog>/path/to/profiles/catalog.xml</ProfilesCatalog>
        </wrongRoot>)";
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(testFilePath, wrongRootXml))
        << "Failed to create test file with wrong root element";

    OpenAPI::OAIConfigEditorObject configObj;
    OPGUIConfigEditor::OPGUIConfigEditorImport importer;
    bool result = importer.loadConfiguration(testFilePath, configObj, errorMsg);

    EXPECT_TRUE(TestHelpers::deleteFileIfExists(testFilePath))
        << "Failed to cleanup test file";

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("Failed to import simulation config"))
        << "Expected error about wrong root element, got: " << errorMsg.toStdString();
}
