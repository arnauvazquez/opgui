/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIEnvironmentExport.h"
#include "OPGUIEnvironmentExport.h"
#include "OAIEnvironment.h"
#include "OAIWeather.h"
#include "OAIFriction.h"
#include "OAITimeOfDay.h"
#include "OAIFriction.h"
#include "OAIVisibilityDistance.h"
#include "OAIHelpers.h"

QString environmentXmlToCompare = QStringLiteral(
    R"(<Environment>
            <TimeOfDays>
                <TimeOfDay Probability="0.7" Value="15"/>
                <TimeOfDay Probability="0.3" Value="22"/>
            </TimeOfDays>
            <VisibilityDistances>
                <VisibilityDistance Probability="0.7" Value="350"/>
                <VisibilityDistance Probability="0.3" Value="500"/>
            </VisibilityDistances>
            <Frictions>
                <Friction Probability="0.33" Value="0.85"/>
                <Friction Probability="0.33" Value="0.9"/>
                <Friction Probability="0.34" Value="0.75"/>
            </Frictions>
            <Weathers>
                <Weather Probability="0.5" Value="Clear"/>
                <Weather Probability="0.5" Value="rainy"/>
            </Weathers>
            <TrafficRules>DE</TrafficRules>
            <TurningRates>
                <TurningRate Incoming="lane1" Outgoing="lane2" Weight="100"/>
                <TurningRate Incoming="lane2" Outgoing="lane3" Weight="1000"/>
            </TurningRates>
        </Environment>)"
);

QString environmentJSONToExp = QStringLiteral(
    R"({
        "visibilityDistances": [
                {
                    "probability": 0.7,
                    "value": 350
                },
                {
                    "probability": 0.3,
                    "value": 500
                }
            ],
            "timeOfDays": [
                {
                    "probability": 0.7,
                    "value": 15
                },
                {
                    "probability": 0.3,
                    "value": 22
                }
            ],
            "frictions": [
                {
                    "probability": 0.33,
                    "value": 0.85
                },
                {
                    "probability": 0.33,
                    "value": 0.9
                },
                {
                    "probability": 0.34,
                    "value": 0.75
                }
            ],
            "weathers": [
                {
                    "probability": 0.5,
                    "value": "Clear"
                },
                {
                    "probability": 0.5,
                    "value": "rainy"
                }
            ],
            "trafficRules": "DE",
            "turningRates": [
                {
                    "incoming": "lane1",
                    "outgoing": "lane2",
                    "weight": 100
                },
                {
                    "incoming": "lane2",
                    "outgoing": "lane3",
                    "weight": 1000
                }
            ]
    })");

void ENVIRONMENT_EXPORT_TEST::SetUp()  {
}

void ENVIRONMENT_EXPORT_TEST::TearDown()  {
    
}

TEST_F(ENVIRONMENT_EXPORT_TEST, environment_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentExport environmentExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIEnvironment environment(environmentJSONToExp);

    bool result = environmentExporter.exportEnvironment(xmlWriter, environment, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export environment was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = environmentXmlToCompare.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" << generatedXmlString.toStdString() << "\nExpected XML:\n" << normalizedExpectedXml.toStdString();
}

TEST_F(ENVIRONMENT_EXPORT_TEST, environment_wrong_weathers_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentExport environmentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIEnvironment environment(environmentJSONToExp);

    //Empty weaethers array
    QList<OpenAPI::OAIWeather> weathers;
    weathers.clear();
    environment.setWeathers(weathers);
    result = environmentExporter.exportEnvironment(xmlWriter,environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Weathers array is empty."))<<"Error message was expected to contain 'Weathers array is empty.' but was "+errorMsg.toStdString();
    
    //Weather probability out of range
    weathers.clear();
    OpenAPI::OAIWeather weatherOutOfRange;
    weatherOutOfRange.setProbability(1.5); // Invalid probability
    weatherOutOfRange.setValue("Sunny");
    weathers.append(weatherOutOfRange);
    environment.setWeathers(weathers);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Probability out of range for weather at position 0"))<<"Error message was expected to contain 'Probability out of range for weather at position 0' but was "+errorMsg.toStdString();
    
    //Empty weather value
    weathers.clear();
    OpenAPI::OAIWeather weatherEmptyValue;
    weatherEmptyValue.setProbability(0.5); 
    weatherEmptyValue.setValue(""); // Empty weather
    weathers.append(weatherEmptyValue);
    environment.setWeathers(weathers);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Empty weather value at position 0"))<<"Error message was expected to contain 'Empty weather value at position 0' but was "+errorMsg.toStdString();
    
    //Invalid weather value
    weathers.clear();
    OpenAPI::OAIWeather weatherWrongValue;
    weatherWrongValue.setProbability(0.5); // Non valid value weather
    weatherWrongValue.setValue("WRONG_VALUE");
    weathers.append(weatherWrongValue);
    environment.setWeathers(weathers);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid weather value 'WRONG_VALUE' at position 0"))<<"Error message was expected to contain 'Invalid weather value 'WRONG_VALUE' at position 0' but was "+errorMsg.toStdString();
    
    //Sum of probabilities not equal 1
    weathers.clear();
    OpenAPI::OAIWeather weatherWrongProbSum;
    weatherWrongProbSum.setProbability(0.6); 
    weatherWrongProbSum.setValue("Clear");
    weathers.append(weatherWrongProbSum);
    weathers.append(weatherWrongProbSum); // Invalid probability weather as sum is 1.2
    environment.setWeathers(weathers);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of all weathers does not equal 1"))<<"Error message was expected to contain 'The sum of probabilities of all weathers does not equal 1' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(ENVIRONMENT_EXPORT_TEST, environment_wrong_frictions_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentExport environmentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIEnvironment environment(environmentJSONToExp);

    //Empty frictions array
    QList<OpenAPI::OAIFriction> frictions;
    frictions.clear();
    environment.setFrictions(frictions);
    result = environmentExporter.exportEnvironment(xmlWriter,environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Frictions array is empty."))<<"Error message was expected to contain 'Frictions array is empty.' but was "+errorMsg.toStdString();
    
    //Friction probability out of range
    frictions.clear();
    OpenAPI::OAIFriction frictionOutOfRange;
    frictionOutOfRange.setProbability(1.5); // Invalid friction probability
    frictionOutOfRange.setValue(0.8);
    frictions.append(frictionOutOfRange);
    environment.setFrictions(frictions);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Probability out of range for friction at position 0"))<<"Error message was expected to contain 'Probability out of range for friction at position 0' but was "+errorMsg.toStdString();
    
    //Invalid value
    frictions.clear();
    OpenAPI::OAIFriction frictionInvalidValue;
    frictionInvalidValue.setProbability(0.5); 
    frictionInvalidValue.setValue(1.2); // Invalid friction value
    frictions.append(frictionInvalidValue);
    environment.setFrictions(frictions);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Value out of range for friction at position 0"))<<"Error message was expected to contain 'Value out of range for friction at position 0' but was "+errorMsg.toStdString();
    
    
    //Sum of probabilities not equal 1
    frictions.clear();
    OpenAPI::OAIFriction frictionWrongProbSum;
    frictionWrongProbSum.setProbability(0.6); 
    frictionWrongProbSum.setValue(0.4);
    frictions.append(frictionWrongProbSum);
    frictions.append(frictionWrongProbSum); // Invalid probability weather as sum is 1.2
    environment.setFrictions(frictions);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of all frictions does not equal 1"))<<"Error message was expected to contain 'The sum of probabilities of all frictions does not equal 1' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(ENVIRONMENT_EXPORT_TEST, environment_wrong_times_of_day_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentExport environmentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIEnvironment environment(environmentJSONToExp);

    //Empty times of day array
    QList<OpenAPI::OAITimeOfDay> timesOfDay;
    timesOfDay.clear();
    environment.setTimeOfDays(timesOfDay);
    result = environmentExporter.exportEnvironment(xmlWriter,environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Times of day array is empty."))<<"Error message was expected to contain 'Times of day array is empty.' but was "+errorMsg.toStdString();
    
    //Time of day probability out of range
    timesOfDay.clear();
    OpenAPI::OAITimeOfDay timesOfDayOutOfRange;
    timesOfDayOutOfRange.setProbability(1.5); // Invalid time of day probability
    timesOfDayOutOfRange.setValue(19);
    timesOfDay.append(timesOfDayOutOfRange);
    environment.setTimeOfDays(timesOfDay);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Probability out of range for time of day at position 0"))<<"Error message was expected to contain 'Probability out of range for time of day at position 0' but was "+errorMsg.toStdString();
    
    //Empty time of day value
    timesOfDay.clear();
    OpenAPI::OAITimeOfDay timeOfDayInvalidValue;
    timeOfDayInvalidValue.setProbability(0.5); 
    timeOfDayInvalidValue.setValue(29); // Invalid time of day value
    timesOfDay.append(timeOfDayInvalidValue);
    environment.setTimeOfDays(timesOfDay);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Value out of range for time of day at position 0"))<<"Error message was expected to contain 'Value out of range for time of day at position 0' but was "+errorMsg.toStdString();
    
    
    //Sum of times of day not equal 1
    timesOfDay.clear();
    OpenAPI::OAITimeOfDay timesOfDayWrongProbSum;
    timesOfDayWrongProbSum.setProbability(0.6);
    timesOfDayWrongProbSum.setValue(1);
    timesOfDay.append(timesOfDayWrongProbSum);
    timesOfDay.append(timesOfDayWrongProbSum); // Invalid times of day probabilty as sum is 1.2
    environment.setTimeOfDays(timesOfDay);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of all times of day does not equal 1"))<<"Error message was expected to contain 'The sum of probabilities of all times of day does not equal 1' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(ENVIRONMENT_EXPORT_TEST, environment_wrong_visibility_distance_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentExport environmentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIEnvironment environment(environmentJSONToExp);

    QList<OpenAPI::OAIVisibilityDistance> visibilityDistances;

    //Empty Visibility distance array
    visibilityDistances.clear();
    environment.setVisibilityDistances(visibilityDistances);
    result = environmentExporter.exportEnvironment(xmlWriter,environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Visibility distances array is empty."))<<"Error message was expected to contain 'Visibility distances array is empty.' but was "+errorMsg.toStdString();
    
    //Visibility distance probability out of range
    visibilityDistances.clear();
    OpenAPI::OAIVisibilityDistance visibilityDistanceOutOfRange;
    visibilityDistanceOutOfRange.setProbability(1.5); // Invalid Visibility distance probability
    visibilityDistanceOutOfRange.setValue(19);
    visibilityDistances.append(visibilityDistanceOutOfRange);
    environment.setVisibilityDistances(visibilityDistances);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Probability out of range for visibility distance at position 0"))<<"Error message was expected to contain 'Probability out of range for visibility distance at position 0' but was "+errorMsg.toStdString();
    
    //Empty Visibility distance value
    visibilityDistances.clear();
    OpenAPI::OAIVisibilityDistance visibilityDistanceInvalidValue;
    visibilityDistanceInvalidValue.setProbability(0.5); 
    visibilityDistanceInvalidValue.setValue(-1); // Invalid Visibility distance value
    visibilityDistances.append(visibilityDistanceInvalidValue);
    environment.setVisibilityDistances(visibilityDistances);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Value out of range for visibility distance at position 0"))<<"Error message was expected to contain 'Value out of range for visibility distance at position 0' but was "+errorMsg.toStdString();
    
    
    //Sum of probabilities not equal 1
    visibilityDistances.clear();
    OpenAPI::OAIVisibilityDistance visibilityDistanceWrongProbSum;
    visibilityDistanceWrongProbSum.setProbability(0.6); 
    visibilityDistanceWrongProbSum.setValue(1);
    visibilityDistances.append(visibilityDistanceWrongProbSum);
    visibilityDistances.append(visibilityDistanceWrongProbSum); // Invalid Visibility distances probability as sum is 1.2
    environment.setVisibilityDistances(visibilityDistances);
    result = environmentExporter.exportEnvironment(xmlWriter, environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of all visibility distances does not equal 1"))<<"Error message was expected to contain 'The sum of probabilities of all visibility distances does not equal 1' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(ENVIRONMENT_EXPORT_TEST, environment_wrong_traffic_rules_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentExport environmentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIEnvironment environment(environmentJSONToExp);

    //Empty traffic rules
    environment.setTrafficRules("");
    result = environmentExporter.exportEnvironment(xmlWriter,environment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Traffic rules value is empty"))<<"Error message was expected to contain 'Traffic rules value is empty.' but was "+errorMsg.toStdString();
    
    //Wrong traffic rules
    environment.setTrafficRules("NO_EXISITNG_LIST");
    result = environmentExporter.exportEnvironment(xmlWriter,environment,errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of environment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid Traffic Rules value"))<<"Error message was expected to contain 'Invalid Traffic Rules value' but was "+errorMsg.toStdString();
    
    
    buffer.close();
}












