/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIEnvironmentImport.h"
#include "OPGUIEnvironmentImport.h"
#include "OAIEnvironment.h"
#include "OAIWeather.h"
#include "OAIFriction.h"
#include "OAITimeOfDay.h"
#include "OAIFriction.h"
#include "OAIVisibilityDistance.h"
#include "OAIHelpers.h"
#include "test_helpers.h"


QString environmentXmlToImp = QStringLiteral(
    R"(<Environment>
        <TimeOfDays>
            <TimeOfDay Probability="0.5" Value="15"/>
            <TimeOfDay Probability="0.5" Value="22"/>
        </TimeOfDays>
        <VisibilityDistances>
            <VisibilityDistance Probability="0.5" Value="350"/>
            <VisibilityDistance Probability="0.5" Value="500"/>
        </VisibilityDistances>
        <Frictions>
            <Friction Probability="0.5" Value="0.5"/>
            <Friction Probability="0.5" Value="1"/>
        </Frictions>
        <Weathers>
            <Weather Probability="0.5" Value="Clear"/>
            <Weather Probability="0.5" Value="rainy"/>
        </Weathers>
        <TrafficRules>DE</TrafficRules>
        <TurningRates>
            <TurningRate Incoming="lane1" Outgoing="lane2" Weight="100"/>
            <TurningRate Incoming="lane2" Outgoing="lane3" Weight="1000"/>
        </TurningRates>
        </Environment>)"
);

QString environmentJSONToCompare = QStringLiteral(
    R"({
        "visibilityDistances": [
            {
                "probability": 0.5,
                "value": 350
            },
            {
                "probability": 0.5,
                "value": 500
            }
        ],
        "timeOfDays": [
            {
                "probability": 0.5,
                "value": 15
            },
            {
                "probability": 0.5,
                "value": 22
            }
        ],
        "frictions": [
            {
                "probability": 0.5,
                "value": 0.5
            },
            {
                "probability": 0.5,
                "value": 1
            }
        ],
        "weathers": [
            {
                "probability": 0.5,
                "value": "Clear"
            },
            {
                "probability": 0.5,
                "value": "rainy"
            }
        ],
        "trafficRules": "DE",
        "turningRates": [
            {
                "incoming": "lane1",
                "outgoing": "lane2",
                "weight": 100
            },
            {
                "incoming": "lane2",
                "outgoing": "lane3",
                "weight": 1000
            }
        ]
    })");

void ENVIRONMENT_IMPORT_TEST::SetUp()  {
}

void ENVIRONMENT_IMPORT_TEST::TearDown()  {
    
}

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    QString errorMsg;

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(environmentXmlToImp)) << "Failed to parse XML data.";

    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIEnvironment environment; // Assuming this object will be populated

    bool result = environmentImporter.importEnvironment(root, environment, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Convert the environment object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(environment).toObject());

    // Convert the actual JSON document to a compact string for comparison
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    // Load the expected JSON for comparison
    QJsonDocument expectedDoc = QJsonDocument::fromJson(environmentJSONToCompare.toUtf8());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Visually compare the expected and actual JSON strings in case of an error
    if (expectedJsonString != actualJsonString) {
        FAIL() << "The imported environment does not match the expected JSON.\n"
               << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
               << "Actual JSON:\n" << actualJsonString.toStdString();
    }
} 

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_invalid_weather_several_reasons_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomDocument doc;
    QDomElement root;

    //no weathers element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "Weathers", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing array weathers.";
    ASSERT_TRUE(errorMsg.contains("Weathers element is missing."))<<"Error message was expected to contain 'Weathers element is missing.' but was "+errorMsg.toStdString();

    
    //empty weathers element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "Weather", "Weathers");
    incorrectXml = TestHelpers::removeXmlElement(incorrectXml, "Weather", "Weathers"); //remove twice as function only remove first matching child
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty array weathers.";
    ASSERT_TRUE(errorMsg.contains("No Weather elements found."))<<"Error message was expected to contain 'No Weather elements found.' but was "+errorMsg.toStdString();

    //No probability
    QString noProbWeather = QStringLiteral(
         R"(
            <Weathers>
                <Weather Value="Clear"/>
            </Weathers>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Weathers", noProbWeather);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing probability.";
    ASSERT_TRUE(errorMsg.contains("Weather element at index 0 is missing Probability or Value attributes."))<<"Error message was expected to contain 'Weather element at index 0 is missing Probability or Value attributes.' but was "+errorMsg.toStdString();
    
    //Wrong probability
    QString wrongProbWeather = QStringLiteral(
         R"(
            <Weathers>
                <Weather Value="Clear" Probability="1.7"/>
            </Weathers>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Weathers", wrongProbWeather);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect probability.";
    ASSERT_TRUE(errorMsg.contains("Weather element at index 0 has an invalid Probability value."))<<"Error message was expected to contain 'Weather element at index 0 has an invalid Probability value.' but was "+errorMsg.toStdString();

    //Emtpy value
    QString emptyValWeather = QStringLiteral(
         R"(
            <Weathers>
                <Weather Value="Clear" Probability="0.5"/>
                <Weather Value="" Probability="0.5"/>
            </Weathers>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Weathers", emptyValWeather);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing value ";
    ASSERT_TRUE(errorMsg.contains("Weather element at index 1 has an empty Value."))<<"Error message was expected to contain 'Weather element at index 1 has an empty Value.' but was "+errorMsg.toStdString();

    QString wrongValWeather = QStringLiteral(
         R"(
            <Weathers>
                <Weather Value="WRONG_VALUE" Probability="0.5"/>
                <Weather Value="Clear" Probability="0.5"/>
            </Weathers>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Weathers", wrongValWeather);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to wrong weather value probability.";
    ASSERT_TRUE(errorMsg.contains("Invalid weather value 'WRONG_VALUE' at position 0."))<<"Error message was expected to contain 'Invalid weather value 'WRONG_VALUE' at position 0.' but was "+errorMsg.toStdString();
    
    
    //wrong sum of probabilities
    QString wrongSumProbWeather = QStringLiteral(
        R"(
            <Weathers>
                <Weather Value="Clear" Probability="0.7"/>
                <Weather Value="rainy" Probability="0.5"/>
            </Weathers>
        )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Weathers", wrongSumProbWeather);//second weather has prob 0.5 so sum is 1.2
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect sum of probabilities.";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of weathers does not equal 1.0"))<<"Error message was expected to contain 'The sum of probabilities of weathers does not equal 1.0' but was "+errorMsg.toStdString();
}

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_invalid_frictions_several_reasons_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomDocument doc;
    QDomElement root;
    
    //no Frictions element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "Frictions", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing array frictions.";
    ASSERT_TRUE(errorMsg.contains("Frictions element is missing."))<<"Error message was expected to contain 'Frictions element is missing.' but was "+errorMsg.toStdString();

    
    //empty Frictions element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "Friction", "Frictions");
    incorrectXml = TestHelpers::removeXmlElement(incorrectXml, "Friction", "Frictions"); //remove twice as function only remove first matching child
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty array of frictions.";
    ASSERT_TRUE(errorMsg.contains("No Friction elements found."))<<"Error message was expected to contain 'No Friction elements found.' but was "+errorMsg.toStdString();

    //No probability
    QString noProbFriction = QStringLiteral(
         R"(
            <Frictions>
                <Friction Value="0.5"/>
            </Frictions>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Frictions", noProbFriction);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing probability.";
    ASSERT_TRUE(errorMsg.contains("Friction element at index 0 is missing Probability or Value attributes."))<<"Error message was expected to contain 'Friction element at index 0 is missing Probability or Value attributes.' but was "+errorMsg.toStdString();
   
    //Wrong probability
    QString wrongProbFriction = QStringLiteral(
         R"(<Frictions>
                <Friction Probability="1.2" Value="0.5"/>
            </Frictions>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Frictions", wrongProbFriction);
    qDebug()<<incorrectXml;
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect probability.";
    ASSERT_TRUE(errorMsg.contains("Friction element at index 0 has an invalid Probability value."))<<"Error message was expected to contain 'Friction element at index 0 has an invalid Probability value.' but was "+errorMsg.toStdString();
    
    //No value
    QString noValFriction = QStringLiteral(
         R"(
            <Frictions>
                <Friction Probability="0.5"/>
            </Frictions>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Frictions", noValFriction);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing probability.";
    ASSERT_TRUE(errorMsg.contains("Friction element at index 0 is missing Probability or Value attributes."))<<"Error message was expected to contain 'Friction element at index 0 is missing Probability or Value attributes.' but was "+errorMsg.toStdString();
    
    //wrong value
    QString wrongValFriction = QStringLiteral(
         R"(
            <Frictions>
                <Friction Probability="0.6" Value="0.9"/>
                <Friction Probability="0.4" Value="1.5"/>
            </Frictions>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Frictions", wrongValFriction);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to wrong friction value probability.";
    ASSERT_TRUE(errorMsg.contains("Friction element at index 1 has an invalid Value."))<<"Error message was expected to contain 'Friction element at index 1 has an invalid Value.' but was "+errorMsg.toStdString();
    
    //wrong sum of probabilities
    QString wrongProbSumFrictions = QStringLiteral(
         R"(
            <Frictions>
                <Friction Probability="0.5" Value="0.9"/>
                <Friction Probability="0.7" Value="0.8"/>
            </Frictions>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "Frictions", wrongProbSumFrictions); //second friction has prob 0.5 so sum is 1.2
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect sum of probabilities.";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of frictions does not equal 1.0"))<<"Error message was expected to contain 'The sum of probabilities of frictions does not equal 1.0' but was "+errorMsg.toStdString();
}

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_invalid_times_of_day_several_reasons_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomDocument doc;
    QDomElement root;

    //no TimeOfDays element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "TimeOfDays", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing array TimeOfDays.";
    ASSERT_TRUE(errorMsg.contains("TimeOfDays element is missing."))<<"Error message was expected to contain 'TimeOfDays element is missing.' but was "+errorMsg.toStdString();

    
    //empty TimeOfDay element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "TimeOfDay", "TimeOfDays");
    incorrectXml = TestHelpers::removeXmlElement(incorrectXml, "TimeOfDay", "TimeOfDays"); //remove twice as function only remove first matching child
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty array of TimeOfDays.";
    ASSERT_TRUE(errorMsg.contains("No TimeOfDay elements found."))<<"Error message was expected to contain 'No TimeOfDay elements found.' but was "+errorMsg.toStdString();

    //No probability
    QString noProbTimeOfDays = QStringLiteral(
         R"(
            <TimeOfDays>
                <TimeOfDay Value="15"/>
                <TimeOfDay Probability="0.5" Value="22"/>
            </TimeOfDays>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TimeOfDays", noProbTimeOfDays);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing probability.";
    ASSERT_TRUE(errorMsg.contains("TimeOfDay element at index 0 is missing Probability or Value attributes."))<<"Error message was expected to contain 'TimeOfDay element at index 0 is missing Probability or Value attributes.' but was "+errorMsg.toStdString();

    //Wrong probability
    QString wrongProbTimeOfDays = QStringLiteral(
         R"(
            <TimeOfDays>
                <TimeOfDay Probability="1.5" Value="15"/>
                <TimeOfDay Probability="0.5" Value="22"/>
            </TimeOfDays>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TimeOfDays", wrongProbTimeOfDays);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect times of day probability.";
    ASSERT_TRUE(errorMsg.contains("TimeOfDay element at index 0 has an invalid Probability value."))<<"Error message was expected to contain 'TimeOfDay element at index 0 has an invalid Probability value.' but was "+errorMsg.toStdString();

    //wrong value ++
    QString wrongValTimeOfDays = QStringLiteral(
         R"(
            <TimeOfDays>
                <TimeOfDay Probability="0.5" Value="15"/>
                <TimeOfDay Probability="0.5" Value="26"/>
            </TimeOfDays>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TimeOfDays", wrongValTimeOfDays); //26 is non existing hour of day
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to wrong times of day value.";
    ASSERT_TRUE(errorMsg.contains("TimeOfDay element at index 1 has an invalid Value."))<<"Error message was expected to contain 'TimeOfDay element at index 1 has an invalid Value.' but was "+errorMsg.toStdString();

    //wrong value --
    QString negValTimeOfDays = QStringLiteral(
         R"(
            <TimeOfDays>
                <TimeOfDay Probability="0.5" Value="15"/>
                <TimeOfDay Probability="0.5" Value="-2"/>
            </TimeOfDays>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TimeOfDays", negValTimeOfDays); //hours of days must be positive
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to wrong times of day value probability.";
    ASSERT_TRUE(errorMsg.contains("TimeOfDay element at index 1 has an invalid Value."))<<"Error message was expected to contain 'TimeOfDay element at index 1 has an invalid Value.' but was "+errorMsg.toStdString();

    //wrong sum of probabilities
    QString wrongSumProbTimeOfDays = QStringLiteral(
         R"(
            <TimeOfDays>
                <TimeOfDay Probability="0.4" Value="15"/>
                <TimeOfDay Probability="0.4" Value="2"/>
            </TimeOfDays>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TimeOfDays", wrongSumProbTimeOfDays); //second time of day has prob 0.5 so sum is 1.2
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect sum of times of day.";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of times of day does not equal 1."))<<"Error message was expected to contain 'The sum of probabilities of times of day does not equal 1.' but was "+errorMsg.toStdString();
}

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_invalid_visibility_distances_several_reasons_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomDocument doc;
    QDomElement root;

    //no VisibilityDistance element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "VisibilityDistances", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing array frictions.";
    ASSERT_TRUE(errorMsg.contains("VisibilityDistances element is missing."))<<"Error message was expected to contain 'VisibilityDistance element is missing.' but was "+errorMsg.toStdString();

    
    //empty VisibilityDistance element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "VisibilityDistance", "VisibilityDistances");
    incorrectXml = TestHelpers::removeXmlElement(incorrectXml, "VisibilityDistance", "VisibilityDistances"); //remove twice as function only remove first matching child
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty array of VisibilityDistance.";
    ASSERT_TRUE(errorMsg.contains("No VisibilityDistance elements found."))<<"Error message was expected to contain 'No VisibilityDistance elements found.' but was "+errorMsg.toStdString();

    //No probability
    QString noProbVisibilityDistance = QStringLiteral(
         R"(
             <VisibilityDistances>
                <VisibilityDistance Value="350"/>
                <VisibilityDistance Probability="0.5" Value="500"/>
             </VisibilityDistances>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "VisibilityDistances", noProbVisibilityDistance);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing probability.";
    ASSERT_TRUE(errorMsg.contains("VisibilityDistance element at index 0 is missing Probability or Value attributes."))<<"Error message was expected to contain 'VisibilityDistance element at index 0 is missing Probability or Value attributes.' but was "+errorMsg.toStdString();

    //Wrong probability
    QString wrongProbVisibilityDistance = QStringLiteral(
         R"(
             <VisibilityDistances>
                <VisibilityDistance Probability="1.5" Value="350"/>
                <VisibilityDistance Probability="0.5" Value="500"/>
             </VisibilityDistances>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "VisibilityDistances", wrongProbVisibilityDistance);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect probability.";
    ASSERT_TRUE(errorMsg.contains("VisibilityDistance element at index 0 has an invalid Probability value."))<<"Error message was expected to contain 'VisibilityDistance element at index 0 has an invalid Probability value.' but was "+errorMsg.toStdString();

    //wrong value
    QString wrongValVisibilityDistance = QStringLiteral(
         R"(
             <VisibilityDistances>
                <VisibilityDistance Probability="0.5" Value="500"/>
                <VisibilityDistance Probability="0.5" Value="-1"/>
             </VisibilityDistances>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "VisibilityDistances", wrongValVisibilityDistance);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to wrong friction value probability.";
    ASSERT_TRUE(errorMsg.contains("VisibilityDistance element at index 1 has an invalid Value."))<<"Error message was expected to contain 'VisibilityDistance element at index 1 has an invalid Value.' but was "+errorMsg.toStdString();


    //wrong sum of probabilities
    QString wrongSumProbVisibilityDistance = QStringLiteral(
         R"(
             <VisibilityDistances>
                <VisibilityDistance Probability="0.5" Value="500"/>
                <VisibilityDistance Probability="0.7" Value="250"/>
             </VisibilityDistances>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "VisibilityDistances", wrongSumProbVisibilityDistance); //second weather has prob 0.5 so sum is 1.2
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to incorrect sum of probabilities.";
    ASSERT_TRUE(errorMsg.contains("The sum of probabilities of visibility distances does not equal 1.0."))<<"Error message was expected to contain 'The sum of probabilities of VisibilityDistance does not equal 1.0' but was "+errorMsg.toStdString();
}

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_wrong_traffic_rules_several_reasons_NEGATIVE) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomDocument doc;
    QDomElement root;

    //no TrafficRules element
    incorrectXml = TestHelpers::removeXmlElement(environmentXmlToImp, "TrafficRules", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing element TrafficRules.";
    ASSERT_TRUE(errorMsg.contains("TrafficRules element is missing."))<<"Error message was expected to contain 'TrafficRules element is missing.' but was "+errorMsg.toStdString();

    //empty TrafficRules value
    QString emptyTrafficRulesValue = QStringLiteral(
         R"(   
                
         )");  
    incorrectXml =TestHelpers::replaceXmlElementContent(environmentXmlToImp, "TrafficRules", emptyTrafficRulesValue, "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing element TrafficRules.";
    ASSERT_TRUE(errorMsg.contains("TrafficRules value is empty."))<<"Error message was expected to contain 'TrafficRules value is empty.' but was "+errorMsg.toStdString();
    
    //wrong TrafficRules
    QString wrongTrafficRulesValue = QStringLiteral(
         R"(
                WRONG_VALUE
         )");
    incorrectXml =TestHelpers::replaceXmlElementContent(environmentXmlToImp, "TrafficRules", wrongTrafficRulesValue, "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to non existing element TrafficRules.";
    ASSERT_TRUE(errorMsg.contains("Invalid Traffic Rules value 'WRONG_VALUE'"))<<"Error message was expected to contain 'Invalid Traffic Rules value 'WRONG_VALUE' but was "+errorMsg.toStdString(); 
}

TEST_F(ENVIRONMENT_IMPORT_TEST, environment_invalid_turning_rates_several_reasons_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIEnvironmentImport environmentImporter;
    OpenAPI::OAIEnvironment environment;
    QDomDocument doc;
    QDomElement root;

    // Missing Incoming lane
    QString noIncLaneTurningRate = QStringLiteral(
         R"(
            <TurningRates>
                <TurningRate Outgoing="lane2" Incoming="lane3" Weight="0.7"/>
                <TurningRate Outgoing="lane2" Weight="0.7"/>
            </TurningRates>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TurningRates", noIncLaneTurningRate);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing Incoming element.";
    ASSERT_TRUE(errorMsg.contains("TurningRate element at index 1 is missing Outgoing, Incoming or Weight attributes.")) 
        << "Error message was expected to contain 'TurningRate element at index 1 is missing Outgoing, Incoming or Weight attributes.' but was " + errorMsg.toStdString();

    // Empty Incoming lane value
    QString emptyIncLaneTurningRate = QStringLiteral(
         R"(
            <TurningRates>
                <TurningRate Outgoing="lane2" Incoming="" Weight="0.7"/>
                <TurningRate Outgoing="lane2" Incoming="lane3" Weight="0.7"/>
            </TurningRates>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TurningRates", emptyIncLaneTurningRate);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty Incoming value.";
    ASSERT_TRUE(errorMsg.contains("TurningRate element at index 0 has empty Incoming value.")) 
        << "Error message was expected to contain 'TurningRate element at index 0 has empty Incoming value.' but was " + errorMsg.toStdString();

    // Empty Outgoing lane value
    QString emptyOutLaneTurningRate = QStringLiteral(
         R"(
            <TurningRates>
                <TurningRate Outgoing="" Incoming="lane1" Weight="0.7"/>
                <TurningRate Outgoing="lane2" Incoming="lane3" Weight="0.7"/>
            </TurningRates>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TurningRates", emptyOutLaneTurningRate);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty Outgoing value.";
    ASSERT_TRUE(errorMsg.contains("TurningRate element at index 0 has empty Outgoing value.")) 
        << "Error message was expected to contain 'TurningRate element at index 0 has empty Outgoing value.' but was " + errorMsg.toStdString();

    // Invalid Weight value (negative)
    QString invalidWeightTurningRate = QStringLiteral(
         R"(
            <TurningRates>
                <TurningRate Outgoing="lane1" Incoming="lane2" Weight="0.7"/>
                <TurningRate Outgoing="lane2" Incoming="lane3" Weight="-0.5"/>
            </TurningRates>
         )");
    incorrectXml =TestHelpers::replaceXmlElementArraySafe(environmentXmlToImp,"Environment", "TurningRates", invalidWeightTurningRate);
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement(); 
    result = environmentImporter.importEnvironment(root, environment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to negative Weight value.";
    ASSERT_TRUE(errorMsg.contains("TurningRate element at index 1 has an invalid Weight value.")) 
        << "Error message was expected to contain 'TurningRate element at index 1 has an invalid Weight value.' but was " + errorMsg.toStdString();
}












