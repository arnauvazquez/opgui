/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIExperimentExport.h"
#include "OPGUIExperimentExport.h"
#include "OAIExperiment.h"
#include "OAIExperiment_libraries.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

QString experimentXmlToCompare = QStringLiteral(
    R"(<Experiment>
        <ExperimentID>123</ExperimentID>
        <NumberOfInvocations>1</NumberOfInvocations>
        <RandomSeed>532725206</RandomSeed>
        <Libraries>
            <WorldLibrary>World_OSI</WorldLibrary>
            <DataBufferLibrary>DataBuffer</DataBufferLibrary>
            <StochasticsLibrary>Stochastics</StochasticsLibrary>
        </Libraries>
    </Experiment>)"
);

QString experimentJSONToExp = QStringLiteral(
    R"({
        "experimentID": 123,
        "numberOfInvocations": 1,
        "randomSeed": 532725206,
        "libraries": {
            "worldLibrary": "World_OSI",
            "dataBufferLibrary": "DataBuffer",
            "stochasticsLibrary": "Stochastics"
        }
    })");

void EXPERIMENT_EXPORT_TEST::SetUp()  {
}

void EXPERIMENT_EXPORT_TEST::TearDown()  {
    
}

TEST_F(EXPERIMENT_EXPORT_TEST, experiment_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

    bool result = experimentExporter.exportExperiment(xmlWriter, experiment, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export experiment was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = experimentXmlToCompare.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" << generatedXmlString.toStdString() << "\nExpected XML:\n" << normalizedExpectedXml.toStdString();
}

TEST_F(EXPERIMENT_EXPORT_TEST, experiment_wrong_experimentID_NEGATIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

     //Wrong experiment ID
    experiment.setExperimentId(-1100);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Experiment ID must be 0 or greater."))<<"Error message was expected to contain 'Experiment ID must be 0 or greater.' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(EXPERIMENT_EXPORT_TEST, experiment_wrong_NumberOfInvocations_NEGATIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

    //Wrong experiment ID
    experiment.setNumberOfInvocations(0);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Number of invocations must be a positive integer."))<<"Error message was expected to contain 'Number of invocations must be a positive integer.' but was "+errorMsg.toStdString();
    
     //Wrong experiment ID
    experiment.setNumberOfInvocations(-99999);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Number of invocations must be a positive integer."))<<"Error message was expected to contain 'Number of invocations must be a positive integer.' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(EXPERIMENT_EXPORT_TEST, experiment_wrong_RandomSeed_NEGATIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

     //Wrong experiment ID
    experiment.setRandomSeed(-99999);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg); 
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Random seed must be a non-negative integer."))<<"Error message was expected to contain 'Random seed must be a non-negative integer.' but was "+errorMsg.toStdString();
    
    buffer.close();
}


TEST_F(EXPERIMENT_EXPORT_TEST, experiment_wrong_world_libraries_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

    OpenAPI::OAIExperiment_libraries libraries;
    libraries.setWorldLibrary("World_OSI");
    libraries.setDataBufferLibrary("DataBuffer");
    libraries.setStochasticsLibrary("Stochastics");

    //Wrong world Library
    libraries.setWorldLibrary("NOT EXISTING IN LIST");
    experiment.setLibraries(libraries);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid WorldLibrary value"))<<"Error message was expected to contain 'Invalid WorldLibrary value' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(EXPERIMENT_EXPORT_TEST, experiment_wrong_data_buffer_libraries_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

    OpenAPI::OAIExperiment_libraries libraries;
    libraries.setWorldLibrary("World_OSI");
    libraries.setDataBufferLibrary("DataBuffer");
    libraries.setStochasticsLibrary("Stochastics");

    //Wrong data buffer Library.
    libraries.setDataBufferLibrary("NOT EXISTING IN LIST");
    experiment.setLibraries(libraries);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid dataBuffer Library value"))<<"Error message was expected to contain 'Invalid dataBuffer Library value' but was "+errorMsg.toStdString();
    
    buffer.close();
}

TEST_F(EXPERIMENT_EXPORT_TEST, experiment_wrong_stochastic_libraries_several_reasons_NEGATIVE) {
    OPGUIConfigEditor::OPGUIExperimentExport experimentExporter;
    QString errorMsg;
    bool result = false;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIExperiment experiment(experimentJSONToExp);

    OpenAPI::OAIExperiment_libraries libraries;
    libraries.setWorldLibrary("World_OSI");
    libraries.setDataBufferLibrary("DataBuffer");
    libraries.setStochasticsLibrary("Stochastics");
  
    //Wrong stochastics Library
    libraries.setStochasticsLibrary("NOT EXISTING IN LIST");
    experiment.setLibraries(libraries);
    result = experimentExporter.exportExperiment(xmlWriter,experiment,errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of experiment export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid stochastics Library value"))<<"Error message was expected to contain 'Invalid stochastics Library value' but was "+errorMsg.toStdString();
    
    
    buffer.close();
}














