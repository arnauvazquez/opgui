/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIObservationsImport.h"
#include "OPGUIObservationsImport.h"
#include "OAIObservations.h"
#include "OAILoggingGroup.h"
#include "OAILog.h"
#include "OAIEntityRepository.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include <QDebug>

QString observationsXmlToImp = QStringLiteral(
    R"(<Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            <Observation>
            <Library>Observation_EntityRepository</Library>
            <Parameters>
                <String Key="FilenamePrefix" Value="Repository"/>
                <String Key="WritePersistentEntities" Value="Consolidated"/>
            </Parameters>
            </Observation>
        </Observations>)"
);

QString observationsJSONToCompare = QStringLiteral(
    R"({
        "log": {
            "loggingCyclesToCsv": false,
            "fileName": "simulationOutput.xml",
            "loggingGroups": [
            {
                "enabled": true,
                "group": "Trace",
                "values": "XPosition,YPosition,YawAngle"
            },
            {
                "enabled": true,
                "group": "RoadPosition",
                "values": "AgentInFront,Lane,PositionRoute,Road,TCoordinate"
            },
            {
                "enabled": false,
                "group": "RoadPositionExtended",
                "values": "SecondaryLanes"
            },
            {
                "enabled": true,
                "group": "Sensor",
                "values": "Sensor*_DetectedAgents,Sensor*_VisibleAgents"
            },
            {
                "enabled": false,
                "group": "Vehicle",
                "values": "AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"
            },
            {
                "enabled": true,
                "group": "Visualization",
                "values": "AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"
            }
            ]
        },
        "entityRepository": {
            "fileNamePrefix": "Repository",
            "writePersistentEntities": "Consolidated"
        }
        })");

void OBSERVATIONS_IMPORT_TEST::SetUp()  {
}

void OBSERVATIONS_IMPORT_TEST::TearDown()  {
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsXmlToImp)) << "Failed to parse XML data.";

    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root, observations, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Convert the observations object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(observations).toObject());

    // Convert the actual JSON document to a compact string for comparison
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    // Load the expected JSON for comparison
    QJsonDocument expectedDoc = QJsonDocument::fromJson(observationsJSONToCompare.toUtf8());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Visually compare the expected and actual JSON strings in case of an error
    if (expectedJsonString != actualJsonString) {
        FAIL() << "The imported observations does not match the expected JSON.\n"
               << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
               << "Actual JSON:\n" << actualJsonString.toStdString();
    }
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_EntityRepository_not_existing_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Remove the second observation object
    QString observationsXmlToImpNoEntityRepoObs = QStringLiteral(
         R"(
            <Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsXmlToImpNoEntityRepoObs)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root, observations, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Load the expected JSON for comparison
    OpenAPI::OAIObservations observationsExpected(observationsJSONToCompare);
    OpenAPI::OAIEntityRepository emptyEntityRepository;
    emptyEntityRepository.setFileNamePrefix("");
    emptyEntityRepository.setWritePersistentEntities("");

    observationsExpected.setEntityRepository(emptyEntityRepository); // Set an empty entity repository

    // Convert the observations object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(observations).toObject());
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    QJsonDocument expectedDoc(::OpenAPI::toJsonValue(observationsExpected).toObject());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Visually compare the expected and actual JSON strings in case of an error
    if (expectedJsonString != actualJsonString) {
        FAIL() << "The imported observations does not match the expected JSON.\n"
               << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
               << "Actual JSON:\n" << actualJsonString.toStdString();
    }
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_EntityRepository_no_FilenamePrefix_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Remove the second observation object
    QString observationsXmlToImpNoFilenamePrefix = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
                <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="WritePersistentEntities" Value="Consolidated"/>
                </Parameters>
                </Observation>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsXmlToImpNoFilenamePrefix)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root, observations, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Load the expected JSON for comparison
    OpenAPI::OAIObservations observationsExpected(observationsJSONToCompare);

    OpenAPI::OAIEntityRepository entityRepNoFileNamePrefix = observations.getEntityRepository();
    entityRepNoFileNamePrefix.setFileNamePrefix("");
    observationsExpected.setEntityRepository(entityRepNoFileNamePrefix); // Set an empty fileNamePrefix

    // Convert the observations object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(observations).toObject());
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    QJsonDocument expectedDoc(::OpenAPI::toJsonValue(observationsExpected).toObject());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Visually compare the expected and actual JSON strings in case of an error
    if (expectedJsonString != actualJsonString) {
        FAIL() << "The imported observations does not match the expected JSON.\n"
               << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
               << "Actual JSON:\n" << actualJsonString.toStdString();
    }
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_EntityRepository_no_WritePersistentEntities_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Remove the second observation object
    QString observationsXmlToImpNoWritePersistentEntities = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
                <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="FilenamePrefix" Value="Repository"/>
                </Parameters>
                </Observation>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsXmlToImpNoWritePersistentEntities)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root, observations, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Load the expected JSON for comparison
    OpenAPI::OAIObservations observationsExpected(observationsJSONToCompare);

    OpenAPI::OAIEntityRepository entityRepNoWritePersistentEntities = observations.getEntityRepository();
    entityRepNoWritePersistentEntities.setWritePersistentEntities("");
    observationsExpected.setEntityRepository(entityRepNoWritePersistentEntities); // Set an empty fileNamePrefix

    // Convert the observations object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(observations).toObject());
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    QJsonDocument expectedDoc(::OpenAPI::toJsonValue(observationsExpected).toObject());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Visually compare the expected and actual JSON strings in case of an error
    if (expectedJsonString != actualJsonString) {
        FAIL() << "The imported observations does not match the expected JSON.\n"
               << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
               << "Actual JSON:\n" << actualJsonString.toStdString();
    }
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_No_Observations_element_imported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Remove the second observation object
    QString observationsWrong = QStringLiteral(
         R"(
            <WRONG_ELEMENT>
            </WRONG_ELEMENT>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsWrong)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root.firstChildElement("FORCE NULL"), observations, errorMsg);

    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Observations element is missing."))<<"Error message was expected to contain 'Observations element is missing.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_Empty_Observations_imported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Remove the second observation object
    QString observationsWrong = QStringLiteral(
         R"(
            <Observations>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsWrong)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root, observations, errorMsg);

    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("No Observation elements found."))<<"Error message was expected to contain 'No Observation elements found.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_Empty_ObservationsLoggingGroups_imported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;

    // Remove the second observation object
    QString observationsWrong = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="FilenamePrefix" Value="Repository"/>
                    <String Key="WritePersistentEntities" Value="Consolidated"/>
                </Parameters>
                </Observation>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(observationsWrong)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIObservations observations; // Assuming this object will be populated

    bool result = observationsImporter.importObservations(root, observations, errorMsg);

    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Log observation not found."))<<"Error message was expected to contain 'Log observation not found.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_ObservationsLoggingGroups_various_imported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;
    
    QString noParams = QStringLiteral(
         R"(
            <Observations>
            <Observation>
            <Library>Observation_Log</Library>
            </Observation>
        </Observations>
         )");

    QString noLoggingCyclicsToCsv = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
            </Observations>
         )");

    QString noOutputFilename = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
            </Observation>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;

    //No params
    ASSERT_TRUE(doc.setContent(noParams)) << "Failed to parse XML data.";
    QDomElement root = doc.documentElement(); // Get the root element of the document
    OpenAPI::OAIObservations observations; // Assuming this object will be populated
    bool result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Log element (parameters element) is missing."))<<"Error message was expected to contain 'Log element (parameters element) is missing.' but was "+errorMsg.toStdString();

    //No loggingCyclics
    ASSERT_TRUE(doc.setContent(noLoggingCyclicsToCsv)) << "Failed to parse XML data.";
    root = doc.documentElement();
    result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("LoggingCyclicsToCsv element is missing or empty."))<<"Error message was expected to contain 'LoggingCyclicsToCsv element is missing or empty.' but was "+errorMsg.toStdString();

    //No OutputFilename
    ASSERT_TRUE(doc.setContent(noOutputFilename)) << "Failed to parse XML data.";
    root = doc.documentElement();
    result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("OutputFilename element is missing or empty."))<<"Error message was expected to contain 'OutputFilename element is missing or empty.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_LoggingGroups_various_imported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;
    
    QString missingAtr = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
            </Observations>
         )");

    QString missingEnabled = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                </Parameters>
                </Observation>
            </Observations>
         )");

    QString enabledNotListed = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
            </Observations>
         )");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;

    //No params
    ASSERT_TRUE(doc.setContent(missingAtr)) << "Failed to parse XML data.";
    QDomElement root = doc.documentElement(); // Get the root element of the document
    OpenAPI::OAIObservations observations; // Assuming this object will be populated
    bool result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Logging group element is missing required attributes."))<<"Error message was expected to contain 'Logging group element is missing required attributes.' but was "+errorMsg.toStdString();

    //No loggingCyclics
    ASSERT_TRUE(doc.setContent(missingEnabled)) << "Failed to parse XML data.";
    root = doc.documentElement();
    result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("LoggingGroups entry specifying enabled groups is missing."))<<"Error message was expected to contain 'LoggingGroups entry specifying enabled groups is missing.' but was "+errorMsg.toStdString();

    //No OutputFilename
    ASSERT_TRUE(doc.setContent(enabledNotListed)) << "Failed to parse XML data.";
    root = doc.documentElement();
    result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("is listed but not defined."))<<"Error message was expected to contain 'is listed but not defined.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_IMPORT_TEST, observations_EntityRepositories_various_imported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsImport observationsImporter;
    QString errorMsg;
    
    QString emptyFileNamePrefix = QStringLiteral(
         R"(<Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            <Observation>
            <Library>Observation_EntityRepository</Library>
            <Parameters>
                <String Key="FilenamePrefix" Value=""/>
                <String Key="WritePersistentEntities" Value="Consolidated"/>
            </Parameters>
            </Observation>
        </Observations>)");

    QString emptyWritePersistentEntities = QStringLiteral(
         R"(<Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            <Observation>
            <Library>Observation_EntityRepository</Library>
            <Parameters>
                <String Key="FilenamePrefix" Value="Repository"/>
                <String Key="WritePersistentEntities"/>
            </Parameters>
            </Observation>
        </Observations>)");

    QString InvalidWritePersistentEntities = QStringLiteral(
         R"(<Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            <Observation>
            <Library>Observation_EntityRepository</Library>
            <Parameters>
                <String Key="FilenamePrefix" Value="Repository"/>
                <String Key="WritePersistentEntities" Value="WRONG_VALUE"/>
            </Parameters>
            </Observation>
        </Observations>)");

    // Parse the XML string into a QDomDocument
    QDomDocument doc;

    //No params
    ASSERT_TRUE(doc.setContent(emptyFileNamePrefix)) << "Failed to parse XML data.";
    QDomElement root = doc.documentElement(); // Get the root element of the document
    OpenAPI::OAIObservations observations; // Assuming this object will be populated
    bool result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("FilenamePrefix value is empty."))<<"Error message was expected to contain 'FilenamePrefix value is empty.' but was "+errorMsg.toStdString();

    //No loggingCyclics
    ASSERT_TRUE(doc.setContent(emptyWritePersistentEntities)) << "Failed to parse XML data.";
    root = doc.documentElement();
    result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("WritePersistentEntities value is empty."))<<"Error message was expected to contain 'WritePersistentEntities value is empty.' but was "+errorMsg.toStdString();

    //No OutputFilename
    ASSERT_TRUE(doc.setContent(InvalidWritePersistentEntities)) << "Failed to parse XML data.";
    root = doc.documentElement();
    result = observationsImporter.importObservations(root, observations, errorMsg);
    EXPECT_FALSE(result)<<"Test: Result of observations import was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid value for WritePersistentEntities:"))<<"Error message was expected to contain 'Invalid value for WritePersistentEntities:' but was "+errorMsg.toStdString();
}













