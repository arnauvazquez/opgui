/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDebug>
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUISimulationConfigFileExport.h"
#include "OAISimulationConfigFile.h"
#include "OAIHelpers.h"
#include "OPGUISimulationConfigFileExport.h"
#include "test_helpers.h"
#include "OPGUICoreGlobalConfig.h"

// Sample simulation configuration in JSON format
QString simulationConfigJSON = QStringLiteral(
    R"({
        "profilesCatalog": "/path/to/profiles/catalog.xml",
        "experiment": {
            "experimentID": 123,
            "numberOfInvocations": 1,
            "randomSeed": 532725206,
            "libraries": {
                "worldLibrary": "World_OSI",
                "dataBufferLibrary": "DataBuffer",
                "stochasticsLibrary": "Stochastics"
            }
        },
        "scenario": {
            "openScenarioFile": "/path/to/scenario.xosc"
        },
        "environment": {
            "visibilityDistances": [
                {
                    "probability": 0.7,
                    "value": 350
                },
                {
                    "probability": 0.3,
                    "value": 500
                }
            ],
            "timeOfDays": [
                {
                    "probability": 0.7,
                    "value": 15
                },
                {
                    "probability": 0.3,
                    "value": 22
                }
            ],
            "frictions": [
                {
                    "probability": 0.33,
                    "value": 0.85
                },
                {
                    "probability": 0.33,
                    "value": 0.9
                },
                {
                    "probability": 0.34,
                    "value": 0.75
                }
            ],
            "weathers": [
                {
                    "probability": 0.5,
                    "value": "Clear"
                },
                {
                    "probability": 0.5,
                    "value": "rainy"
                }
            ],
            "trafficRules": "DE",
            "turningRates": [
                {
                    "incoming": "lane1",
                    "outgoing": "lane2",
                    "weight": 100
                },
                {
                    "incoming": "lane2",
                    "outgoing": "lane3",
                    "weight": 1000
                }
            ]
        },
         "observations": {
            "log": {
                "loggingCyclesToCsv": true,
                "fileName": "output.log",
                "loggingGroups": [
                    {
                        "enabled": true,
                        "group": "Trace",
                        "values": "group1,group2"
                    },
                    {
                        "enabled": true,
                        "group": "RoadPosition",
                        "values": "pos1,pos2"
                    }
                ]
            },
            "entityRepository": {
                "fileNamePrefix": "entity_",
                "writePersistentEntities": "Consolidated"
            }
        },
        "spawners": [
            {
                "library": "SpawnerPreRunCommon",
                "type": "PreRun",
                "priority": 0,
                "profile": "CustomProfileName"
            },
            {
                "library": "SpawnerRuntimeCommon",
                "type": "Runtime",
                "priority": 1
            }
        ]
    })"
);

// Expected simulation configuration in XML format
QString simulationConfigXML = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
       <simulationConfig SchemaVersion="0.8.2">
        <ProfilesCatalog>/path/to/profiles/catalog.xml</ProfilesCatalog>
        <Experiment>
            <ExperimentID>123</ExperimentID>
            <NumberOfInvocations>1</NumberOfInvocations>
            <RandomSeed>532725206</RandomSeed>
            <Libraries>
                <WorldLibrary>World_OSI</WorldLibrary>
                <DataBufferLibrary>DataBuffer</DataBufferLibrary>
                <StochasticsLibrary>Stochastics</StochasticsLibrary>
            </Libraries>
        </Experiment>
        <Scenario>
            <OpenScenarioFile>/path/to/scenario.xosc</OpenScenarioFile>
        </Scenario>
        <Environment>
            <TimeOfDays>
                <TimeOfDay Probability="0.7" Value="15"/>
                <TimeOfDay Probability="0.3" Value="22"/>
            </TimeOfDays>
            <VisibilityDistances>
                <VisibilityDistance Probability="0.7" Value="350"/>
                <VisibilityDistance Probability="0.3" Value="500"/>
            </VisibilityDistances>
            <Frictions>
                <Friction Probability="0.33" Value="0.85"/>
                <Friction Probability="0.33" Value="0.9"/>
                <Friction Probability="0.34" Value="0.75"/>
            </Frictions>
            <Weathers>
                <Weather Probability="0.5" Value="Clear"/>
                <Weather Probability="0.5" Value="rainy"/>
            </Weathers>
            <TrafficRules>DE</TrafficRules>
            <TurningRates>
                <TurningRate Incoming="lane1" Outgoing="lane2" Weight="100"/>
                <TurningRate Incoming="lane2" Outgoing="lane3" Weight="1000"/>
            </TurningRates>
        </Environment>
        <Observations>
            <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="output.log"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="true"/>
                    <StringVector Key="LoggingGroup_Trace" Value="group1,group2"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="pos1,pos2"/>
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Trace"/>
                </Parameters>
            </Observation>
            <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="FilenamePrefix" Value="entity_"/>
                    <String Key="WritePersistentEntities" Value="Consolidated"/>
                </Parameters>
            </Observation>
        </Observations>
        <Spawners>
            <Spawner>
                <Library>SpawnerPreRunCommon</Library>
                <Type>PreRun</Type>
                <Priority>0</Priority>
                <Profile>CustomProfileName</Profile>
            </Spawner>
            <Spawner>
                <Library>SpawnerRuntimeCommon</Library>
                <Type>Runtime</Type>
                <Priority>1</Priority>
            </Spawner>
        </Spawners>
    </simulationConfig>)"
);

void SIMULATION_CONFIG_FILE_EXPORT_TEST::SetUp() {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    this->workspacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirFullPath = TestHelpers::joinPaths(this->workspacePath, "test");

    QDir dir(this->workspacePath);
    ASSERT_TRUE(dir.exists()) 
        << "The workspace directory does not exist: " << this->workspacePath.toStdString();

    // If test dir exists, remove it
    QDir dirTest(this->testDirFullPath);
    if (dirTest.exists()) {
        ASSERT_TRUE(dirTest.removeRecursively()) 
            << "Failed to remove the test directory: " << this->testDirFullPath.toStdString();
    }

    ASSERT_TRUE(dir.mkpath(this->testDirFullPath)) 
        << "Failed to create directory path: " << this->testDirFullPath.toStdString();

}

void SIMULATION_CONFIG_FILE_EXPORT_TEST::TearDown() {
    QDir dirTest(this->testDirFullPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove systems test directory: " << this->testDirFullPath.toStdString();
    }
}

bool SIMULATION_CONFIG_FILE_EXPORT_TEST::parseSimConfigFileJson(const QString& jsonStr, OpenAPI::OAISimulationConfigFile& oai_simConfigFile) {
    QJsonDocument doc = QJsonDocument::fromJson(jsonStr.toUtf8());
    if (doc.isNull()) {
        qDebug() << "Failed to create JSON doc.";
        return false; 
    }

    // Check if the JSON document is an object
    if (!doc.isObject()) {
        qDebug() << "JSON obtained is not an object";
        return false;  
    }
    
    QJsonObject jsonObject = doc.object();

    OpenAPI::fromJsonValue(oai_simConfigFile, jsonObject);

    return true;
}

TEST_F(SIMULATION_CONFIG_FILE_EXPORT_TEST, Export_SimulationConfigFile_Valid_POSITIVE) {
    OPGUIConfigEditor::OPGUISimulationConfigFileExport exporter;
    QString errorMsg;

    // Convert JSON to SimulationConfigFile object
    QJsonDocument doc = QJsonDocument::fromJson(simulationConfigJSON.toUtf8());
    ASSERT_FALSE(doc.isNull()) << "Failed to parse simulation configuration JSON.";

    QJsonObject jsonObject = doc.object();
    qDebug() << "Parsed JSON object:" << jsonObject;

    OpenAPI::OAISimulationConfigFile config;
    bool result = OpenAPI::fromJsonValue(config, jsonObject);

    if (!result) {
        qDebug() << "Failed keys in JSON:" << jsonObject.keys();
        qDebug() << "Object state:" << config.asJson();
    }

    ASSERT_TRUE(result) << "Failed to populate OAISimulationConfigFile from JSON.";
}

TEST_F(SIMULATION_CONFIG_FILE_EXPORT_TEST, Export_SimulationConfigFile_Invalid_Object_NEGATIVE) {
    OPGUIConfigEditor::OPGUISimulationConfigFileExport exporter;
    QString errorMsg;

    OpenAPI::OAISimulationConfigFile invalidConfig;

    QBuffer buffer;
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter writer(&buffer);
    writer.setAutoFormatting(true);

    bool result = exporter.exportSimulationConfig(writer, invalidConfig, errorMsg);
    EXPECT_FALSE(result) << "Export succeeded but should fail for invalid simulation config object.";
    EXPECT_FALSE(errorMsg.isEmpty()) << "Expected an error message but none was provided.";

    buffer.close();
}

TEST_F(SIMULATION_CONFIG_FILE_EXPORT_TEST, FileExportTest_POSITIVE) {
    QString errorMsg;
    OPGUIConfigEditor::OPGUISimulationConfigFileExport exporter;
    QString filePath = TestHelpers::joinPaths(this->testDirFullPath, "simulationConfig.xml");

    // Parse JSON into config object
    QJsonDocument doc = QJsonDocument::fromJson(simulationConfigJSON.toUtf8());
    ASSERT_FALSE(doc.isNull());

    OpenAPI::OAISimulationConfigFile config;
    ASSERT_TRUE(OpenAPI::fromJsonValue(config, doc.object()));
    
    ASSERT_TRUE(exporter.saveConfigFileToXmlFile(config, this->testDirFullPath,errorMsg))
        << errorMsg.toStdString();
    
    // Verify file content
    QString content = TestHelpers::readFile(filePath);
    QString cleanContent = TestHelpers::removeSpacesBetweenTags(content);
    QString cleanExpected = TestHelpers::removeSpacesBetweenTags(simulationConfigXML);
    EXPECT_EQ(cleanContent.simplified(), cleanExpected.simplified())
    << "Test: exportSimulationConfig not return expected response.\n"
    << "Expected Response: " << cleanExpected.toStdString() << "\n"
    << "Actual Response: " << cleanContent.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_EXPORT_TEST, FileExportInvalidPath_NEGATIVE) {
    QString errorMsg;
    OPGUIConfigEditor::OPGUISimulationConfigFileExport exporter;
    QString invalidPath = "::/nonexistent/path/config.xml";
    
    // Parse JSON into config object
    QJsonDocument doc = QJsonDocument::fromJson(simulationConfigJSON.toUtf8());
    ASSERT_FALSE(doc.isNull());

    OpenAPI::OAISimulationConfigFile config;
    ASSERT_TRUE(OpenAPI::fromJsonValue(config, doc.object()));

    // Attempt export to invalid path
    QFile file(invalidPath);
    ASSERT_FALSE(file.open(QIODevice::WriteOnly));
    QXmlStreamWriter xmlWriter(&file);

    EXPECT_FALSE(exporter.saveConfigFileToXmlFile(config,invalidPath,errorMsg))<<"Test: Result of save file should be failed but was succesfull";
    ASSERT_TRUE(errorMsg.contains("Failed to open file"))<<"Error message was expected to contain 'Failed to open file' but was "+errorMsg.toStdString();
}