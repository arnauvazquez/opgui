/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUISimulationConfigFileImport.h"
#include "OPGUISimulationConfigFileImport.h"
#include "OAISimulationConfigFile.h"
#include "OPGUICoreGlobalConfig.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

// Test XML with a complete valid configuration
QString simConfigXmlToImp = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
       <simulationConfig SchemaVersion="0.8.2">
        <ProfilesCatalog>/path/to/profiles/catalog.xml</ProfilesCatalog>
        <Experiment>
            <ExperimentID>123</ExperimentID>
            <NumberOfInvocations>1</NumberOfInvocations>
            <RandomSeed>532725206</RandomSeed>
            <Libraries>
                <WorldLibrary>World_OSI</WorldLibrary>
                <DataBufferLibrary>DataBuffer</DataBufferLibrary>
                <StochasticsLibrary>Stochastics</StochasticsLibrary>
            </Libraries>
        </Experiment>
        <Scenario>
            <OpenScenarioFile>/path/to/scenario.xosc</OpenScenarioFile>
        </Scenario>
        <Environment>
            <TimeOfDays>
                <TimeOfDay Probability="0.5" Value="15"/>
                <TimeOfDay Probability="0.5" Value="22"/>
            </TimeOfDays>
            <VisibilityDistances>
                <VisibilityDistance Probability="0.5" Value="350"/>
                <VisibilityDistance Probability="0.5" Value="500"/>
            </VisibilityDistances>
            <Frictions>
                <Friction Probability="0.5" Value="0.5"/>
                <Friction Probability="0.5" Value="1"/>
            </Frictions>
            <Weathers>
                <Weather Probability="0.5" Value="Clear"/>
                <Weather Probability="0.5" Value="rainy"/>
            </Weathers>
            <TrafficRules>DE</TrafficRules>
            <TurningRates>
                <TurningRate Incoming="lane1" Outgoing="lane2" Weight="100"/>
                <TurningRate Incoming="lane2" Outgoing="lane3" Weight="1000"/>
            </TurningRates>
        </Environment>
        <Observations>
            <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="output.log"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="true"/>
                    <StringVector Key="LoggingGroup_Trace" Value="group1,group2"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="pos1,pos2"/>
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Trace"/>
                </Parameters>
            </Observation>
            <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="FilenamePrefix" Value="entity_"/>
                    <String Key="WritePersistentEntities" Value="Consolidated"/>
                </Parameters>
            </Observation>
        </Observations>
        <Spawners>
            <Spawner>
                <Library>SpawnerPreRunCommon</Library>
                <Type>PreRun</Type>
                <Priority>0</Priority>
                <Profile>CustomProfileName</Profile>
            </Spawner>
            <Spawner>
                <Library>SpawnerRuntimeCommon</Library>
                <Type>Runtime</Type>
                <Priority>1</Priority>
            </Spawner>
        </Spawners>
    </simulationConfig>)"
);

// Expected JSON structure for comparison
QString simConfigJSONToCompare = QStringLiteral(
    R"({
        "profilesCatalog": "/path/to/profiles/catalog.xml",
        "experiment": {
            "experimentID": 123,
            "numberOfInvocations": 1,
            "randomSeed": 532725206,
            "libraries": {
                "worldLibrary": "World_OSI",
                "dataBufferLibrary": "DataBuffer",
                "stochasticsLibrary": "Stochastics"
            }
        },
        "scenario": {
            "openScenarioFile": "/path/to/scenario.xosc"
        },
        "environment": {
            "visibilityDistances": [
                {
                    "probability": 0.5,
                    "value": 350
                },
                {
                    "probability": 0.5,
                    "value": 500
                }
            ],
            "timeOfDays": [
                {
                    "probability": 0.5,
                    "value": 15
                },
                {
                    "probability": 0.5,
                    "value": 22
                }
            ],
            "frictions": [
                {
                    "probability": 0.5,
                    "value": 0.5
                },
                {
                    "probability": 0.5,
                    "value": 1
                }
            ],
            "weathers": [
                {
                    "probability": 0.5,
                    "value": "Clear"
                },
                {
                    "probability": 0.5,
                    "value": "rainy"
                }
            ],
            "trafficRules": "DE",
            "turningRates": [
                {
                    "incoming": "lane1",
                    "outgoing": "lane2",
                    "weight": 100
                },
                {
                    "incoming": "lane2",
                    "outgoing": "lane3",
                    "weight": 1000
                }
            ]
        },
         "observations": {
            "log": {
                "loggingCyclesToCsv": true,
                "fileName": "output.log",
                "loggingGroups": [
                    {
                        "enabled": true,
                        "group": "Trace",
                        "values": "group1,group2"
                    },
                    {
                        "enabled": true,
                        "group": "RoadPosition",
                        "values": "pos1,pos2"
                    }
                ]
            },
            "entityRepository": {
                "fileNamePrefix": "entity_",
                "writePersistentEntities": "Consolidated"
            }
        },
        "spawners": [
            {
                "library": "SpawnerPreRunCommon",
                "type": "PreRun",
                "priority": 0,
                "profile": "CustomProfileName"
            },
            {
                "library": "SpawnerRuntimeCommon",
                "type": "Runtime",
                "priority": 1
            }
        ]
    })"
);

void SIMULATION_CONFIG_FILE_IMPORT_TEST::SetUp() {
    // Create test directory structure
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    QString workspacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirPath = TestHelpers::joinPaths(workspacePath, "test_configs");

    this->testSimConfigFileName = "simulationConfig.xml";

    // Create test directory
    QDir dir;
    if (!dir.exists(this->testDirPath)) {
        ASSERT_TRUE(dir.mkpath(this->testDirPath)) << "Failed to create test directory";
    }
}

void SIMULATION_CONFIG_FILE_IMPORT_TEST::TearDown() {
    // Clean up test directory and files
    QDir dir(this->testDirPath);
    if (dir.exists()) {
        EXPECT_TRUE(dir.removeRecursively()) << "Failed to clean up test directory";
    }
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, simulation_config_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUISimulationConfigFileImport simConfigImporter;
    QString errorMsg;

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(simConfigXmlToImp)) << "Failed to parse XML data.";

    QDomElement root = doc.documentElement(); // Get the root element

    OpenAPI::OAISimulationConfigFile simConfig;
    bool result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Convert the simConfig object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(simConfig).toObject());
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    // Load the expected JSON for comparison
    QJsonDocument expectedDoc = QJsonDocument::fromJson(simConfigJSONToCompare.toUtf8());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Compare the JSON strings
    EXPECT_EQ(actualJsonString, expectedJsonString) 
        << "The imported config does not match the expected JSON.\n"
        << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
        << "Actual JSON:\n" << actualJsonString.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, missing_required_elements_NEGATIVE) {
    OPGUIConfigEditor::OPGUISimulationConfigFileImport simConfigImporter;
    QString errorMsg;
    QDomDocument doc;
    QDomElement root;
    OpenAPI::OAISimulationConfigFile simConfig;
    bool result;

    // Test missing ProfilesCatalog
    QString modifiedXml = TestHelpers::removeXmlElement(simConfigXmlToImp, "ProfilesCatalog", "");
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when ProfilesCatalog is missing";
    ASSERT_TRUE(errorMsg.contains("ProfilesCatalog element is missing"))<<"Error message was expected to contain 'ProfilesCatalog element is missing' but was "+errorMsg.toStdString();

    // Test missing Experiment
    modifiedXml = TestHelpers::removeXmlElement(simConfigXmlToImp, "Experiment", "");
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when Experiment is missing";
    ASSERT_TRUE(errorMsg.contains("Experiment element is missing"))<<"Error message was expected to contain 'Experiment element is missing' but was "+errorMsg.toStdString();

    // Test missing Scenario
    modifiedXml = TestHelpers::removeXmlElement(simConfigXmlToImp, "Scenario", "");
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when Scenario is missing";
    ASSERT_TRUE(errorMsg.contains("Scenario element is missing"))<<"Error message was expected to contain 'Scenario element is missing' but was "+errorMsg.toStdString();

    // Test missing Environment
    modifiedXml = TestHelpers::removeXmlElement(simConfigXmlToImp, "Environment", "");
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when Environment is missing";
    ASSERT_TRUE(errorMsg.contains("Environment element is missing"))<<"Error message was expected to contain 'Environment element is missing' but was "+errorMsg.toStdString();

    // Test missing Observations
    modifiedXml = TestHelpers::removeXmlElement(simConfigXmlToImp, "Observations", "");
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when Observations is missing";
    ASSERT_TRUE(errorMsg.contains("Observations element is missing"))<<"Error message was expected to contain 'Observations element is missing' but was "+errorMsg.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, invalid_profiles_catalog_NEGATIVE) {
    OPGUIConfigEditor::OPGUISimulationConfigFileImport simConfigImporter;
    QString errorMsg;
    QDomDocument doc;
    QDomElement root;
    OpenAPI::OAISimulationConfigFile simConfig;

    // Test empty ProfilesCatalog path
    QString modifiedXml = TestHelpers::replaceXmlElementContent(simConfigXmlToImp, "ProfilesCatalog", "", "");
    
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    bool result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    
    EXPECT_FALSE(result) << "Import should fail when ProfilesCatalog path is empty";
    EXPECT_TRUE(errorMsg.contains("ProfilesCatalog path is empty")) 
        << "Expected error message about empty path, got: " << errorMsg.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, invalid_scenario_NEGATIVE) {
    OPGUIConfigEditor::OPGUISimulationConfigFileImport simConfigImporter;
    QString errorMsg;
    QDomDocument doc;
    QDomElement root;
    OpenAPI::OAISimulationConfigFile simConfig;

    // Test missing OpenScenarioFile
    QString modifiedXml = TestHelpers::removeXmlElement(simConfigXmlToImp, "OpenScenarioFile", "Scenario");
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    bool result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when OpenScenarioFile is missing";
    EXPECT_TRUE(errorMsg.contains("OpenScenarioFile value is empty"));

    // Test empty OpenScenarioFile path
    modifiedXml = TestHelpers::replaceXmlElementContent(simConfigXmlToImp, "OpenScenarioFile", "", "Scenario");
    
    ASSERT_TRUE(doc.setContent(modifiedXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    
    result = simConfigImporter.importSimulationConfig(root, simConfig, errorMsg);
    EXPECT_FALSE(result) << "Import should fail when OpenScenarioFile path is empty";
    EXPECT_TRUE(errorMsg.contains("OpenScenarioFile value is empty")) 
        << "Expected error message about empty path, got: " << errorMsg.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, read_valid_config_file_POSITIVE) {
    QString errorMsg;

    QString simConfigFileFullPath = TestHelpers::joinPaths(this->testDirPath , this->testSimConfigFileName);
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(simConfigFileFullPath, simConfigXmlToImp))
        << "Failed to create test file";

    OPGUIConfigEditor::OPGUISimulationConfigFileImport importer;
    OpenAPI::OAISimulationConfigFile config;

    bool result = importer.getConfigFromXml(this->testDirPath, config, errorMsg);

    ASSERT_TRUE(result) << "Import failed with error: " << errorMsg.toStdString();
    
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(config).toObject());
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    QJsonDocument expectedDoc = QJsonDocument::fromJson(simConfigJSONToCompare.toUtf8());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    EXPECT_EQ(actualJsonString, expectedJsonString)
        << "The imported config does not match the expected JSON";
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, nonexistent_file_NEGATIVE) {
    QString testFilePath = "nonexistent_config.xml";
    QString errorMsg;
    
    OPGUIConfigEditor::OPGUISimulationConfigFileImport importer;
    OpenAPI::OAISimulationConfigFile config;

    bool result = importer.getConfigFromXml(testFilePath, config, errorMsg);

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("SimulationConfig XML file is empty"))
        << "Expected error about empty file, got: " << errorMsg.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, empty_file_NEGATIVE) {
    QString errorMsg;
    
    QString simConfigFileFullPath = TestHelpers::joinPaths(this->testDirPath , this->testSimConfigFileName);
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(simConfigFileFullPath, ""))
        << "Failed to create test file";

    OPGUIConfigEditor::OPGUISimulationConfigFileImport importer;
    OpenAPI::OAISimulationConfigFile config;

    bool result = importer.getConfigFromXml(this->testDirPath, config, errorMsg);

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("SimulationConfig XML file is empty"))
        << "Expected error about empty file, got: " << errorMsg.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, invalid_xml_content_NEGATIVE) {
    QString testFilePath = "simulationConfig.xml";
    QString errorMsg;
    
    QString invalidXml = "This is not valid XML content";
    
    QString simConfigFileFullPath = TestHelpers::joinPaths(this->testDirPath , this->testSimConfigFileName);
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(simConfigFileFullPath,invalidXml))
        << "Failed to create test file";

    OPGUIConfigEditor::OPGUISimulationConfigFileImport importer;
    OpenAPI::OAISimulationConfigFile config;

    bool result = importer.getConfigFromXml(this->testDirPath, config, errorMsg);

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("Invalid XML content"))
        << "Expected error about invalid XML content, got: " << errorMsg.toStdString();
}

TEST_F(SIMULATION_CONFIG_FILE_IMPORT_TEST, wrong_root_element_NEGATIVE) {
    QString testFilePath = "simulationConfig.xml";
    QString errorMsg;
    
    QString wrongRootXml = R"(<?xml version="1.0" encoding="UTF-8"?>
        <wrongRoot>
            <ProfilesCatalog>/path/to/profiles/catalog.xml</ProfilesCatalog>
        </wrongRoot>)";
    
    QString simConfigFileFullPath = TestHelpers::joinPaths(this->testDirPath , this->testSimConfigFileName);
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(simConfigFileFullPath,wrongRootXml))
        << "Failed to create test file";

    OPGUIConfigEditor::OPGUISimulationConfigFileImport importer;
    OpenAPI::OAISimulationConfigFile config;

    bool result = importer.getConfigFromXml(this->testDirPath, config, errorMsg);

    EXPECT_FALSE(result);
    EXPECT_TRUE(errorMsg.contains("The root element is not <simulationConfig>"))
        << "Expected error about wrong root element, got: " << errorMsg.toStdString();
}