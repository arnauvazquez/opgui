
/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUISpawnersExport.h"
#include "OPGUISpawnersExport.h"
#include "OAISpawner.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

QString spawnerJSONToExp = QStringLiteral(
    R"({
        "spawners": [
            {
                "library": "SpawnerPreRunCommon",
                "type": "PreRun",
                "priority": 0,
                "profile": "CustomProfileName"
            },
            {
                "library": "SpawnerRuntimeCommon",
                "type": "Runtime",
                "priority": 1
            }
        ]
    })"
);

QString spawnerXmlToCompare = QStringLiteral(
    R"(<Spawners>
        <Spawner>
            <Library>SpawnerPreRunCommon</Library>
            <Type>PreRun</Type>
            <Priority>0</Priority>
            <Profile>CustomProfileName</Profile>
        </Spawner>
        <Spawner>
            <Library>SpawnerRuntimeCommon</Library>
            <Type>Runtime</Type>
            <Priority>1</Priority>
        </Spawner>
    </Spawners>)"
);

void SPAWNER_EXPORT_TEST::SetUp() {
}

void SPAWNER_EXPORT_TEST::TearDown() {
}

TEST_F(SPAWNER_EXPORT_TEST, spawners_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    // Create spawners list from JSON
    QJsonDocument jsonDoc = QJsonDocument::fromJson(spawnerJSONToExp.toUtf8());
    QJsonArray spawnersArray = jsonDoc.object()["spawners"].toArray();
    QList<OpenAPI::OAISpawner> spawners;
    for (const auto& spawnerValue : spawnersArray) {
        OpenAPI::OAISpawner spawner(QString(QJsonDocument(spawnerValue.toObject()).toJson()));
        spawners.append(spawner);
    }

    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export spawners was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = spawnerXmlToCompare.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" 
                         << generatedXmlString.toStdString() << "\nExpected XML:\n" 
                         << normalizedExpectedXml.toStdString();
}

TEST_F(SPAWNER_EXPORT_TEST, empty_spawners_list_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&buffer);

    QList<OpenAPI::OAISpawner> spawners;
    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);
    
    buffer.close();

    EXPECT_FALSE(result) << "Export should fail with empty spawners list";
    EXPECT_TRUE(errorMsg.contains("Spawners list is empty")) 
        << "Error message should mention empty spawners list, but was: " << errorMsg.toStdString();
}

TEST_F(SPAWNER_EXPORT_TEST, spawner_invalid_library_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&buffer);

    OpenAPI::OAISpawner spawner;
    spawner.setLibrary("InvalidLibrary");
    spawner.setType("PreRun");
    spawner.setPriority(0);

    QList<OpenAPI::OAISpawner> spawners{spawner};
    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);
    
    buffer.close();

    EXPECT_FALSE(result) << "Export should fail with invalid library";
    EXPECT_TRUE(errorMsg.contains("Invalid spawner library value")) 
        << "Error message should mention invalid library, but was: " << errorMsg.toStdString();
}

TEST_F(SPAWNER_EXPORT_TEST, spawner_invalid_type_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&buffer);

    OpenAPI::OAISpawner spawner;
    spawner.setLibrary("SpawnerPreRunCommon");
    spawner.setType("InvalidType");
    spawner.setPriority(0);

    QList<OpenAPI::OAISpawner> spawners{spawner};
    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);
    
    buffer.close();

    EXPECT_FALSE(result) << "Export should fail with invalid type";
    EXPECT_TRUE(errorMsg.contains("Invalid spawner type value")) 
        << "Error message should mention invalid type, but was: " << errorMsg.toStdString();
}

TEST_F(SPAWNER_EXPORT_TEST, spawner_negative_priority_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&buffer);

    OpenAPI::OAISpawner spawner;
    spawner.setLibrary("SpawnerPreRunCommon");
    spawner.setType("PreRun");
    spawner.setPriority(-1);

    QList<OpenAPI::OAISpawner> spawners{spawner};
    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);
    
    buffer.close();

    EXPECT_FALSE(result) << "Export should fail with negative priority";
    EXPECT_TRUE(errorMsg.contains("Priority must be a non-negative integer")) 
        << "Error message should mention negative priority, but was: " << errorMsg.toStdString();
}

TEST_F(SPAWNER_EXPORT_TEST, spawner_empty_library_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&buffer);

    OpenAPI::OAISpawner spawner;
    spawner.setType("PreRun");
    spawner.setPriority(0);
    // Library intentionally left empty

    QList<OpenAPI::OAISpawner> spawners{spawner};
    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);
    
    buffer.close();

    EXPECT_FALSE(result) << "Export should fail with empty library";
    EXPECT_TRUE(errorMsg.contains("Library value is empty")) 
        << "Error message should mention empty library, but was: " << errorMsg.toStdString();
}

TEST_F(SPAWNER_EXPORT_TEST, spawner_empty_type_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerExport spawnerExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&buffer);

    OpenAPI::OAISpawner spawner;
    spawner.setLibrary("SpawnerPreRunCommon");
    spawner.setPriority(0);
    spawner.setType("");  // Empty type

    QList<OpenAPI::OAISpawner> spawners{spawner};
    bool result = spawnerExporter.exportSpawners(xmlWriter, spawners, errorMsg);
    
    buffer.close();

    EXPECT_FALSE(result) << "Export should fail with empty type";
    EXPECT_TRUE(errorMsg.contains("Type value is empty")) 
        << "Error message should mention empty type, but was: " << errorMsg.toStdString();
}
