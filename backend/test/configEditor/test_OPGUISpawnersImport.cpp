/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUISpawnersImport.h"
#include "OPGUISpawnersImport.h"
#include "OAISpawner.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

// Test XML with both a spawner with profile and one without
QString spawnerXmlToImp = QStringLiteral(
    R"(<Spawners>
        <Spawner>
            <Library>SpawnerPreRunCommon</Library>
            <Type>PreRun</Type>
            <Priority>0</Priority>
            <Profile>CustomProfileName</Profile>
        </Spawner>
        <Spawner>
            <Library>SpawnerRuntimeCommon</Library>
            <Type>Runtime</Type>
            <Priority>1</Priority>
        </Spawner>
    </Spawners>)"
);

// Expected JSON structure to compare against
QString spawnerJSONToCompare = QStringLiteral(
    R"({
        "spawners": [
            {
                "library": "SpawnerPreRunCommon",
                "type": "PreRun",
                "priority": 0,
                "profile": "CustomProfileName"
            },
            {
                "library": "SpawnerRuntimeCommon",
                "type": "Runtime",
                "priority": 1
            }
        ]
    })"
);

void SPAWNER_IMPORT_TEST::SetUp() {
}

void SPAWNER_IMPORT_TEST::TearDown() {
}

TEST_F(SPAWNER_IMPORT_TEST, spawners_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUISpawnerImport spawnerImporter;
    QString errorMsg;

    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(spawnerXmlToImp)) << "Failed to parse XML data.";
    
    QDomElement root = doc.documentElement();
    QList<OpenAPI::OAISpawner> spawners;

    bool result = spawnerImporter.importSpawners(root, spawners, errorMsg);
    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();
    ASSERT_EQ(spawners.size(), 2) << "Expected 2 spawners to be imported";

    // Verify first spawner (with profile)
    EXPECT_EQ(spawners[0].getLibrary(), QString("SpawnerPreRunCommon"));
    EXPECT_EQ(spawners[0].getType(), QString("PreRun"));
    EXPECT_EQ(spawners[0].getPriority(), 0);
    EXPECT_EQ(spawners[0].getProfile(), QString("CustomProfileName"));

    // Verify second spawner (without profile)
    EXPECT_EQ(spawners[1].getLibrary(), QString("SpawnerRuntimeCommon"));
    EXPECT_EQ(spawners[1].getType(), QString("Runtime"));
    EXPECT_EQ(spawners[1].getPriority(), 1);
    EXPECT_TRUE(spawners[1].getProfile().isEmpty());
}

TEST_F(SPAWNER_IMPORT_TEST, spawner_missing_required_elements_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerImport spawnerImporter;
    QString errorMsg;
    QString incorrectXml;
    QList<OpenAPI::OAISpawner> spawners;
    QDomDocument doc;
    QDomElement root;

    // Test: Missing Library element
    incorrectXml = TestHelpers::removeXmlElement(spawnerXmlToImp, "Library", "Spawner");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Library element is missing"));

    // Test: Missing Type element
    incorrectXml = TestHelpers::removeXmlElement(spawnerXmlToImp, "Type", "Spawner");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Type element is missing"));

    // Test: Missing Priority element
    incorrectXml = TestHelpers::removeXmlElement(spawnerXmlToImp, "Priority", "Spawner");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Priority element is missing"));
}

TEST_F(SPAWNER_IMPORT_TEST, spawner_invalid_values_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerImport spawnerImporter;
    QString errorMsg;
    QString incorrectXml;
    QList<OpenAPI::OAISpawner> spawners;
    QDomDocument doc;
    QDomElement root;

    // Test: Invalid Library value
    incorrectXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Library", "InvalidLibrary");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Invalid spawner library value"));

    // Test: Invalid Type value
    incorrectXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Type", "InvalidType");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Invalid spawner type value"));

    // Test: Invalid Priority value (negative)
    incorrectXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Priority", "-1");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Priority must be a non-negative integer"));

    // Test: Invalid Priority value (non-numeric)
    incorrectXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Priority", "notanumber");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Priority must be a valid integer"));
}

TEST_F(SPAWNER_IMPORT_TEST, spawner_profile_tests) {
    OPGUIConfigEditor::OPGUISpawnerImport spawnerImporter;
    QString errorMsg;
    QString modifiedXml;
    QList<OpenAPI::OAISpawner> spawners;
    QDomDocument doc;
    QDomElement root;

    // Test: Empty profile (should fail)
    modifiedXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Profile", "");
    ASSERT_TRUE(doc.setContent(modifiedXml, &errorMsg)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    EXPECT_TRUE(errorMsg.contains("Profile value is empty"));

    // Test: Valid custom profile name with debug output
    QString customProfile = "MyCustomProfile_123";
    modifiedXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Profile", customProfile);
    ASSERT_TRUE(doc.setContent(modifiedXml, &errorMsg)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    ASSERT_TRUE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    ASSERT_FALSE(spawners.isEmpty());
    EXPECT_EQ(spawners.first().getProfile(), customProfile);

    // Clear spawners list between tests
    spawners.clear();

    // Test: Long profile name
    QString longProfileName = QString("A").repeated(100);
    modifiedXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Profile", longProfileName);
    ASSERT_TRUE(doc.setContent(modifiedXml, &errorMsg)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    ASSERT_TRUE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    ASSERT_FALSE(spawners.isEmpty());
    EXPECT_EQ(spawners.first().getProfile(), longProfileName);

    // Clear spawners list between tests
    spawners.clear();

    // Test: Special characters in profile name
    QString specialCharProfile = "Profile-Name_123@Special.Chars";
    modifiedXml = TestHelpers::modifyXml(spawnerXmlToImp, "Spawners", "Spawner", 0, "Profile", specialCharProfile);
    ASSERT_TRUE(doc.setContent(modifiedXml, &errorMsg)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    ASSERT_TRUE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    ASSERT_FALSE(spawners.isEmpty());
    EXPECT_EQ(spawners.first().getProfile(), specialCharProfile);
}

TEST_F(SPAWNER_IMPORT_TEST, spawners_structural_issues_NEGATIVE) {
    OPGUIConfigEditor::OPGUISpawnerImport spawnerImporter;
    QString errorMsg;
    QList<OpenAPI::OAISpawner> spawners;
    QDomDocument doc;
    QDomElement root;

    // Test: Missing Spawners element
    errorMsg.clear();  // Clear previous error message
    ASSERT_TRUE(doc.setContent(QString("<InvalidRoot></InvalidRoot>"), &errorMsg));
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    qDebug() << "Invalid Root error:" << errorMsg;
    EXPECT_TRUE(errorMsg.contains("Invalid root element 'InvalidRoot'. Expected 'Spawners'")) 
        << "Actual error message: " << errorMsg.toStdString();

    // Test: Empty spawner element
    errorMsg.clear();  // Clear previous error message
    ASSERT_TRUE(doc.setContent(QString("<Spawners><Spawner></Spawner></Spawners>"), &errorMsg));
    root = doc.documentElement();
    EXPECT_FALSE(spawnerImporter.importSpawners(root, spawners, errorMsg));
    qDebug() << "Empty Spawner error:" << errorMsg;
    EXPECT_TRUE(errorMsg.contains("Library element is missing")) << "Actual error message: " << errorMsg.toStdString();
}