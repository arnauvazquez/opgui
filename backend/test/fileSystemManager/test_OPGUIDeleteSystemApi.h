/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>

#include "OPGUICore.h"
#include "OAIDefault200Response.h"

class DELETE_SYSTEM_TEST : public ::testing::Test {
  public:
    QString testDirFullPath;
    QString mockSystemFilePath;
    QString workspacePath;
    OpenAPI::OAIDefault200Response response;

    void SetUp() override;
    void TearDown() override;
};
