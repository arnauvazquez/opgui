/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>

#include "test_OPGUIGetFileContents.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUICore.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

QString simManagerXmlExample = QStringLiteral(
        R"(<?xml version="1.0" encoding="UTF-8"?>
            <opSimulationManager>
                <logLevel>3</logLevel>
                <simulation>/Users/someUser/opSimulation/bin/core/opSimulation</simulation>
                <logFileSimulationManager>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/opSimulationManager.log</logFileSimulationManager>
                <libraries>/Users/someUser/opSimulation/bin/core/modules</libraries>
                <simulationConfigs>
                    <simulationConfig>
                        <configurations>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210674/0-0-0/configs</configurations>
                        <logFileSimulation>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210674/0-0-0/opSimulation.log</logFileSimulation>
                        <results>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210674/0-0-0/results</results>
                    </simulationConfig>
                    <simulationConfig>
                        <configurations>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210678/0-0-0/configs</configurations>
                        <logFileSimulation>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210678/0-0-0/opSimulation.log</logFileSimulation>
                        <results>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210678/0-0-0/results</results>
                    </simulationConfig>
                    <simulationConfig>
                        <configurations>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210710/0-0-0/configs</configurations>
                        <logFileSimulation>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210710/0-0-0/opSimulation.log</logFileSimulation>
                        <results>/Users/someUser/Documents/workspaces/opGUI-workspace1/test_new_4/1210710/0-0-0/results</results>
                    </simulationConfig>
                </simulationConfigs>
            </opSimulationManager>)"
);

void GET_FILE_CONTENTS_TEST::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    this->workspacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirFullPath = TestHelpers::joinPaths(this->workspacePath, "test");

    this->mockSimManagerFilePath = TestHelpers::joinPaths(this->testDirFullPath, "mockSimManager.xml");

    QDir dir(this->workspacePath);
    ASSERT_TRUE(dir.exists()) 
        << "The workspace directory does not exist: " << this->workspacePath.toStdString();

    // If test dir exists, remove it
    QDir dirTest(this->testDirFullPath);
    if (dirTest.exists()) {
        ASSERT_TRUE(dirTest.removeRecursively()) 
            << "Failed to remove the test directory: " << this->testDirFullPath.toStdString();
    }

    ASSERT_TRUE(dir.mkpath(this->testDirFullPath)) 
        << "Failed to create directory path: " << this->testDirFullPath.toStdString();

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSimManagerFilePath, simManagerXmlExample))<<"Could not save file as: "+this->mockSimManagerFilePath.toStdString();
}

void GET_FILE_CONTENTS_TEST::TearDown()  {
    QDir dirTest(this->testDirFullPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove systems test directory: " << this->testDirFullPath.toStdString();
    }
}

TEST_F(GET_FILE_CONTENTS_TEST, Get_file_contents_no_existing_file_NEGATIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_get_file_contents("/some/fake/path/to/file.txt",false, this->response,errorMsg);

    ASSERT_FALSE(result) << "Get file contents was successfull but should have failed.";

    ASSERT_TRUE(errorMsg.contains("File does not exist at path"))<<"Error message was expected to contain 'File does not exist at path' but was "+errorMsg.toStdString(); 
}

TEST_F(GET_FILE_CONTENTS_TEST, Get_file_contents_empty_path_NEGATIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_get_file_contents("",false, this->response,errorMsg);

    ASSERT_FALSE(result) << "Get file contents was successfull but should have failed.";

    ASSERT_TRUE(errorMsg.contains("Path of file is empty."))<<"Error message was expected to contain 'Path of file is empty.' but was "+errorMsg.toStdString(); 
}

TEST_F(GET_FILE_CONTENTS_TEST, Get_file_contents_encoded_POSITIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_get_file_contents(this->mockSimManagerFilePath,true, this->response,errorMsg);

    ASSERT_TRUE(result) << "Get file contents failed";

    //check that the base64 code is the same as the exptected
    QByteArray byteArray = QByteArray::fromBase64(this->response.getResponse().toUtf8());
    QString decodedString = QString::fromUtf8(byteArray.constData());

    //check that the xml text in the response is the same in the file
    EXPECT_EQ(decodedString.simplified().toStdString(), simManagerXmlExample.simplified().toStdString())
    << "Test: Response was not the expected. Got: " << simManagerXmlExample.toStdString()
    << ", Expected: " << decodedString.toStdString();
}

TEST_F(GET_FILE_CONTENTS_TEST, Get_file_contents_encoded_NEGATIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_get_file_contents(this->mockSimManagerFilePath,false, this->response,errorMsg);

    ASSERT_TRUE(result) << "Get file contents failed";

    //check that the xml text in the response is the same in the file
    EXPECT_EQ(this->response.getResponse().simplified().toStdString(), simManagerXmlExample.simplified().toStdString())
    << "Test: Response was not the expected. Got: " << simManagerXmlExample.toStdString()
    << ", Expected: " << this->response.getResponse().toStdString();
}