/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <gtest/gtest.h>
#include <QString>

#include "OAITreeNode.h"
#include "OAIDefault200Response.h"

class GET_FILE_CONTENTS_TEST : public ::testing::Test {
  public:
    QString workspacePath;
    QString testDir;
    QString testDirFullPath;
    QString mockSimManagerFilePath;
    OpenAPI::OAIDefault200Response response;

    void SetUp() override;
    void TearDown() override;
};