/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>

#include "test_OPGUIGetFileSysTree.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUICore.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

QString fileTreeIsFolderJSON = QStringLiteral(
    R"({
        "name":"test",
        "children": [
            {
            "name": "parentA",
            "children": [
                {
                "name": "childA",
                "children": [
                    {
                    "name": ""
                    }
                ]
                }
            ]
            },
            {
            "name": "parentB",
            "children": [
                {
                "name": ""
                }
            ]
            }
        ]
    })"
);

QString fileTreeIsFolderFalseJSON = QStringLiteral(
    R"({
        "name":"test",
        "children": [
            {
            "name": "parentA",
            "children": [
                {
                "name": "childA",
                "children": [
                    {
                    "name": "testFile2.xosc"
                    }
                ]
                },
                {
                "name": "testFile1.xml"
                }
            ]
            },
            {
            "name": "parentB",
            "children": [
                {
                "name": ""
                }
            ]
            }
        ]
    })"
);

QString fileTreeIsFolderFalseExtXmlJSON = QStringLiteral(
    R"({
        "name":"test",
        "children": [
            {
            "name": "parentA",
            "children": [
                {
                "name": "childA",
                "children": [
                    {
                    "name": ""
                    }
                ]
                },
                {
                "name": "testFile1.xml"
                }
            ]
            },
            {
            "name": "parentB",
            "children": [
                {
                "name": ""
                }
            ]
            }
        ]
    })"
);

OpenAPI::OAITreeNode GET_FILE_SYSTEM_TREE_TEST::findTestNode(const OpenAPI::OAITreeNode &root) {
    // We assume that 'test' is a direct child of the root
    foreach (const OpenAPI::OAITreeNode &child, root.getChildren()) {
        if (child.getName() == "test") {
            // Instead of returning the child, we create a new node with the correct structure
            OpenAPI::OAITreeNode testNode;
            testNode.setName("test");
            testNode.setChildren(child.getChildren());
            return testNode;
        }
    }
    return OpenAPI::OAITreeNode(); // Return an empty node if "test" is not found
}

void GET_FILE_SYSTEM_TREE_TEST::SetUp() {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    // Define the workspace path and the test directory path
    this->workSpacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirFullPath = TestHelpers::joinPaths(this->workSpacePath, "test");

    // Check if workspace exists
    QDir workspaceDir(this->workSpacePath);
    ASSERT_TRUE(workspaceDir.exists()) << "Workspace directory doesn't exist: " << this->workSpacePath.toStdString();

    // Remove the test directory if it exists
    QDir testDir(this->testDirFullPath);
    if (testDir.exists()) {
        ASSERT_TRUE(testDir.removeRecursively()) << "Failed to remove existing test directory: " << this->testDirFullPath.toStdString();
    }

    // Recreate the test directory
    ASSERT_TRUE(workspaceDir.mkpath("test")) << "Failed to create test directory at: " << this->testDirFullPath.toStdString();

    // Set up the specified directory and file structure
    QDir testDirFullPathDir(this->testDirFullPath);
    testDirFullPathDir.mkpath("parentA/childA");  // Create parentA and childA directories
    testDirFullPathDir.mkpath("parentB");         // Create parentB directory

    // Create files inside parentA and childA
    QString file1Path = TestHelpers::joinPaths(TestHelpers::joinPaths(this->testDirFullPath, "parentA"), "testFile1.xml");
    QFile file1(file1Path);
    if (!file1.open(QIODevice::WriteOnly)) {
    // Handle error - Could not open file for writing
        qDebug() << "Could not open file for writing:" << file1Path;
    } else {
        file1.close();  // This will create the file if it doesn't exist
    }

    QString file2Path = TestHelpers::joinPaths(TestHelpers::joinPaths(TestHelpers::joinPaths(this->testDirFullPath, "parentA"), "childA"), "testFile2.xosc");
    QFile file2(file2Path);
    if (!file2.open(QIODevice::WriteOnly)) {
        // Handle error - Could not open file for writing
        qDebug() << "Could not open file for writing:" << file2Path;
    } else {
        file2.close();  // This will create the file if it doesn't exist
    }
}

void GET_FILE_SYSTEM_TREE_TEST::TearDown()  {
    QDir dir(this->testDirFullPath);
    if(dir.exists()){
        ASSERT_TRUE(dir.removeRecursively()) << "Failed to remove test directory during tear down: " << this->testDirFullPath.toStdString();
    }
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, check_correct_base_path_POSITIVE) {
    this->request.setBasePathGeneric("workspace");
    this->request.setIsFolder(false);
    this->request.setExtension("");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_TRUE(result) << "Get file tree failed but was expected to succeed";

    QString actualBasePath = response.getBasePath();
    QString expectedBasePath = this->workSpacePath;

    if (actualBasePath != expectedBasePath) {
        FAIL() << "The base path received and the expected do not match \nExpected:\n" 
            << expectedBasePath.toStdString() 
            << "\nActual:\n" 
            << actualBasePath.toStdString();
    }   
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, get_file_tree_get_files_all_POSITIVE) {
    this->request.setBasePathGeneric("workspace");
    this->request.setIsFolder(false);
    this->request.setExtension("");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_TRUE(result) << "Get file tree failed but was expected to succeed";

    OpenAPI::OAITreeNode root = response.getFileSystemTree();
    OpenAPI::OAITreeNode actualTree = findTestNode(root);

    QString actualJsonString = actualTree.asJson();

    QJsonDocument expectedDoc = QJsonDocument::fromJson(fileTreeIsFolderFalseJSON.toUtf8());
    QJsonDocument actualDoc = QJsonDocument::fromJson(actualJsonString.toUtf8());

    if (expectedDoc != actualDoc) {
        QString expectedJsonPretty = expectedDoc.toJson(QJsonDocument::Indented);
        QString actualJsonPretty = actualDoc.toJson(QJsonDocument::Indented);
        FAIL() << "The 'test' file system trees do not match. \nExpected:\n" 
            << expectedJsonPretty.toStdString() 
            << "\nActual:\n" 
            << actualJsonPretty.toStdString();
    }   
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, get_file_tree_get_folders_POSITIVE) {
    this->request.setBasePathGeneric("workspace");
    this->request.setIsFolder(true);
    this->request.setExtension("");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_TRUE(result) << "Get file tree failed but was expected to succeed";

    OpenAPI::OAITreeNode root = response.getFileSystemTree();
    OpenAPI::OAITreeNode actualTree = findTestNode(root);

    QString actualJsonString = actualTree.asJson();

    QJsonDocument expectedDoc = QJsonDocument::fromJson(fileTreeIsFolderJSON.toUtf8());
    QJsonDocument actualDoc = QJsonDocument::fromJson(actualJsonString.toUtf8());

    if (expectedDoc != actualDoc) {
        QString expectedJsonPretty = expectedDoc.toJson(QJsonDocument::Indented);
        QString actualJsonPretty = actualDoc.toJson(QJsonDocument::Indented);
        FAIL() << "The 'test' file system trees do not match. \nExpected:\n" 
            << expectedJsonPretty.toStdString() 
            << "\nActual:\n" 
            << actualJsonPretty.toStdString();
    }   
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, get_file_tree_get_files_xml_POSITIVE) {
    this->request.setBasePathGeneric("workspace");
    this->request.setIsFolder(false);
    this->request.setExtension("xml");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_TRUE(result) << "Get file tree failed but was expected to succeed";

    OpenAPI::OAITreeNode root = response.getFileSystemTree();
    OpenAPI::OAITreeNode actualTree = findTestNode(root);

    QString actualJsonString = actualTree.asJson();

    QJsonDocument expectedDoc = QJsonDocument::fromJson(fileTreeIsFolderFalseExtXmlJSON.toUtf8());
    QJsonDocument actualDoc = QJsonDocument::fromJson(actualJsonString.toUtf8());

    if (expectedDoc != actualDoc) {
        QString expectedJsonPretty = expectedDoc.toJson(QJsonDocument::Indented);
        QString actualJsonPretty = actualDoc.toJson(QJsonDocument::Indented);
        FAIL() << "The 'test' file system trees do not match. \nExpected:\n" 
            << expectedJsonPretty.toStdString() 
            << "\nActual:\n" 
            << actualJsonPretty.toStdString();
    }   
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, get_file_tree_get_core_POSITIVE) {
    this->request.setBasePathGeneric("core");
    this->request.setIsFolder(false);
    this->request.setExtension("xml");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_TRUE(result) << "Get file tree failed but was expected to succeed";

}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, get_file_tree_request_object_invalid_NEGATIVE) {
    OpenAPI::OAIFileTreeRequest validatedRequest;

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_FALSE(result) << "Get file tree was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Received JSON object file tree request is empty/invalid"))<<"Error message was expected to contain 'Received JSON object file tree request is empty/invalid' but was "+errorMsg.toStdString();
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, base_path_empty_NEGATIVE) {
    this->request.setBasePathGeneric("");
    this->request.setIsFolder(false);
    this->request.setExtension("xml");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_FALSE(result) << "Get file tree was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("The base path requested is empty"))<<"Error message was expected to contain 'The base path requested is empty' but was "+errorMsg.toStdString();
}

TEST_F(GET_FILE_SYSTEM_TREE_TEST, base_path_wrong_value_NEGATIVE) {
    this->request.setBasePathGeneric("wrong_value");
    this->request.setIsFolder(false);
    this->request.setExtension("xml");

    QJsonObject jsonRepresentation = this->request.asJsonObject();
    OpenAPI::OAIFileTreeRequest validatedRequest;
    validatedRequest.fromJsonObject(jsonRepresentation);

    QString errorMsg;

    bool result = OPGUICore::api_get_fileTree(validatedRequest, response, errorMsg);

    ASSERT_FALSE(result) << "Get file tree was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("The base path is not a valid value"))<<"Error message was expected to contain 'The base path is not a valid value' but was "+errorMsg.toStdString();
}
