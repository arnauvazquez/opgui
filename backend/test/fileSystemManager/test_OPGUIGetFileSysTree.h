/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <gtest/gtest.h>
#include <QString>

#include "OAIFileTreeRequest.h"
#include "OAIFileTreeExtended.h"
#include "OAITreeNode.h"

class GET_FILE_SYSTEM_TREE_TEST : public ::testing::Test {
  public:
    QString workSpacePath;
    QString testDir;
    QString testDirFullPath;
    OpenAPI::OAIFileTreeRequest request;
    OpenAPI::OAIFileTreeExtended response;

    void SetUp() override;
    void TearDown() override;
    OpenAPI::OAITreeNode findTestNode(const OpenAPI::OAITreeNode &root);
};