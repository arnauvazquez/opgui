/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#include "test_OPGUISimulationExecution.h"
#include "test_helpers.h"
#include <QDir>
#include <QFile>
#include <QThread>

QString configXmlComplete = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
    <opSimulationManager>
        <logLevel>3</logLevel>
        <simulation>/path/to/simulator</simulation>
        <logFileSimulationManager>/path/to/manager.log</logFileSimulationManager>
        <libraries>/path/to/libs</libraries>
        <simulationConfigs>
            <simulationConfig>
                <configurations>/path/to/config1</configurations>
                <logFileSimulation>/path/to/sim1.log</logFileSimulation>
                <results>/path/to/results1</results>
            </simulationConfig>
            <simulationConfig>
                <configurations>/path/to/config2</configurations>
                <logFileSimulation>/path/to/sim2.log</logFileSimulation>
                <results>/path/to/results2</results>
            </simulationConfig>
        </simulationConfigs>
    </opSimulationManager>)"
);

QString configXmlMissingElements = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
    <opSimulationManager>
        <logLevel>3</logLevel>
        <simulation>/path/to/simulator</simulation>
        <libraries>/path/to/libs</libraries>
    </opSimulationManager>)"
);

void TEST_OPGUI_SIMULATION_EXECUTION::SetUp() {
    testLogPath = TestHelpers::joinPaths(QDir::tempPath(), "test_simulation.log");
    TestHelpers::deleteFileIfExists(testLogPath);

    testExePath = TestHelpers::joinPaths(QDir::tempPath(), "test_executable");
    TestHelpers::deleteFileIfExists(testExePath);

    QFile execFile(testExePath);
    ASSERT_TRUE(execFile.open(QIODevice::WriteOnly));
    execFile.close();
    
    testConfig.setLogFileSimulationManager(testLogPath);
    auto& instance = OPGUISimulationExecution::getInstance();
    
    instance.currentConfig = testConfig;
}

void TEST_OPGUI_SIMULATION_EXECUTION::TearDown() {
    TestHelpers::deleteFileIfExists(testLogPath);
    TestHelpers::deleteFileIfExists(testExePath);
}

QString TEST_OPGUI_SIMULATION_EXECUTION::readLogFile() const {
    QFile file(testLogPath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return QString();
    }
    QString content = file.readAll();
    file.close();
    return content;
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, LogSimpleMessage) {
    QString testMessage = "Test simulation message";
    auto& instance = OPGUISimulationExecution::getInstance();
    
    instance.logSimulationEvent(testMessage);
    
    QString logContent = readLogFile();
    ASSERT_FALSE(logContent.isEmpty()) << "Log file should not be empty";
    ASSERT_TRUE(logContent.contains(testMessage)) << "Log should contain the test message";
    ASSERT_TRUE(logContent.contains("[DebugCore]")) << "Log should contain the debug tag";
    
    QDateTime now = QDateTime::currentDateTime();
    QString dateStr = now.toString("yyyy-MM-dd");
    ASSERT_TRUE(logContent.contains(dateStr)) << "Log should contain today's date";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, ParseConfigurationPositive) {
    QString configPath = TestHelpers::joinPaths(QDir::tempPath(), "test_config.xml");
    QFile configFile(configPath);
    ASSERT_TRUE(configFile.open(QIODevice::WriteOnly));
    configFile.write(configXmlComplete.toUtf8());
    configFile.close();

    QString errorMsg;
    auto& instance = OPGUISimulationExecution::getInstance();
    ASSERT_TRUE(instance.parseConfiguration(configPath, errorMsg)) << errorMsg.toStdString();

    const auto& config = instance.currentConfig;
    ASSERT_EQ(config.getLogLevel(), 3);
    ASSERT_EQ(config.getSimulation(), "/path/to/simulator");
    ASSERT_EQ(config.getLogFileSimulationManager(), "/path/to/manager.log");
    ASSERT_EQ(config.getLibraries(), "/path/to/libs");

    const auto& simConfigs = config.getSimulationConfigs();
    ASSERT_EQ(simConfigs.size(), 2);
    ASSERT_EQ(simConfigs[0].getConfigurations(), "/path/to/config1");
    ASSERT_EQ(simConfigs[1].getResults(), "/path/to/results2");

    TestHelpers::deleteFileIfExists(configPath);
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, ParseConfigurationNegativeMissingElements) {
    QString configPath = TestHelpers::joinPaths(QDir::tempPath(), "test_config.xml");
    QFile configFile(configPath);
    ASSERT_TRUE(configFile.open(QIODevice::WriteOnly));
    configFile.write(configXmlMissingElements.toUtf8());
    configFile.close();

    QString errorMsg;
    auto& instance = OPGUISimulationExecution::getInstance();
    ASSERT_FALSE(instance.parseConfiguration(configPath, errorMsg));
    ASSERT_TRUE(errorMsg.contains("Missing required element"));

    TestHelpers::deleteFileIfExists(configPath);
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, LogSimulationStart) {
    auto& instance = OPGUISimulationExecution::getInstance();
    instance.logSimulationStart();

    QString logContent = readLogFile();
    ASSERT_FALSE(logContent.isEmpty()) << "Log file should not be empty";
    ASSERT_TRUE(logContent.contains("[DebugCore]")) << "Log should contain the debug tag";
    ASSERT_TRUE(logContent.contains("opSimulationManager start")) << "Log should contain 'opSimulationManager start' message";

    QDateTime now = QDateTime::currentDateTime();
    QString dateStr = now.toString("yyyy-MM-dd");
    ASSERT_TRUE(logContent.contains(dateStr)) << "Log should contain today's date";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, ValidateExecutableNotExecutable) {
    QString exePath = testExePath;
    QString errorMsg;

    auto& instance = OPGUISimulationExecution::getInstance();
    bool success = instance.validateExecutable(exePath, errorMsg);
    ASSERT_FALSE(success) << "validateExecutable should have failed";
    ASSERT_TRUE(errorMsg.contains("File exists but is not executable")) << "Log should contain 'File exists but is not executable'";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, ValidateExecutableNotExists) {
    QString exePath = "/path/to/invalid/executable";
    QString errorMsg;

    auto& instance = OPGUISimulationExecution::getInstance();
    bool success = instance.validateExecutable(exePath, errorMsg);
    ASSERT_FALSE(success) << "validateExecutable should have failed";
    ASSERT_TRUE(errorMsg.contains("Simulation executable not found at")) << "Log should contain 'Simulation executable not found at'";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, WaitForSimulationStartsSuccess) {
    auto& instance = OPGUISimulationExecution::getInstance();
    instance.totalSimulationsCount = 3;
    instance.failedStartsCount = 0;
    instance.isRunning = true;

    QString errorMsg;
    ASSERT_TRUE(instance.waitForSimulationStarts(errorMsg)) << errorMsg.toStdString();
    ASSERT_TRUE(errorMsg.isEmpty()) << "Error message should be empty";
    ASSERT_TRUE(instance.isRunning) << "Simulation should still be running";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, WaitForSimulationStartsFailure) {
    auto& instance = OPGUISimulationExecution::getInstance();
    instance.totalSimulationsCount = 3;
    instance.failedStartsCount = 3;
    instance.isRunning = true;

    QString errorMsg;
    ASSERT_FALSE(instance.waitForSimulationStarts(errorMsg)) << errorMsg.toStdString();
    ASSERT_FALSE(errorMsg.isEmpty()) << "Error message should not be empty";
    ASSERT_FALSE(instance.isRunning) << "Simulation should no longer be running";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, ResetSimulationState) {
    auto& instance = OPGUISimulationExecution::getInstance();

    // Set up some initial state
    instance.totalSimulationsCount = 5;
    instance.completedSimulationsCount = 3;
    instance.failedStartsCount = 2;

    QProcess* process1 = new QProcess;
    QProcess* process2 = new QProcess;
    instance.activeProcesses.append(process1);
    instance.activeProcesses.append(process2);

    instance.simulationStatuses[1234] = {1234, 0, QString(), true, "log1.txt"};
    instance.simulationStatuses[5678] = {5678, 1, "Error", false, "log2.txt"};

    // Reset the simulation state
    instance.resetSimulationState();

    // Verify the state has been reset
    ASSERT_EQ(instance.totalSimulationsCount.loadRelaxed(), 0);
    ASSERT_EQ(instance.completedSimulationsCount.loadRelaxed(), 0);
    ASSERT_EQ(instance.failedStartsCount.loadRelaxed(), 0);

    ASSERT_TRUE(instance.activeProcesses.isEmpty());

    ASSERT_TRUE(instance.simulationStatuses.isEmpty());
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, AllSimulationsFinished) {
    auto& instance = OPGUISimulationExecution::getInstance();

    // Test case 1: All simulations completed
    instance.totalSimulationsCount = 10;
    instance.completedSimulationsCount = 10;
    instance.failedStartsCount = 0;
    ASSERT_TRUE(instance.allSimulationsFinished()) << "All simulations should be finished";

    // Test case 2: Some simulations failed
    instance.totalSimulationsCount = 10;
    instance.completedSimulationsCount = 7;
    instance.failedStartsCount = 3;
    ASSERT_TRUE(instance.allSimulationsFinished()) << "All simulations should be finished";

    // Test case 3: Not all simulations finished
    instance.totalSimulationsCount = 10;
    instance.completedSimulationsCount = 7;
    instance.failedStartsCount = 2;
    ASSERT_FALSE(instance.allSimulationsFinished()) << "Not all simulations should be finished";
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, CleanupSimulation) {
    auto& instance = OPGUISimulationExecution::getInstance();

    // Set up initial state
    instance.totalSimulationsCount = 5;
    instance.completedSimulationsCount = 4;
    instance.failedStartsCount = 0;
    instance.isRunning = true;

    QProcess* process = new QProcess;
    instance.activeProcesses.append(process);

    // Call the cleanupSimulation function
    instance.cleanupSimulation(process);

    // Verify the state after cleanup
    ASSERT_EQ(instance.completedSimulationsCount.loadRelaxed(), 5);
    ASSERT_TRUE(instance.activeProcesses.isEmpty());
    ASSERT_FALSE(instance.isRunning);

    QString logContent = readLogFile();
    ASSERT_FALSE(logContent.isEmpty());
    ASSERT_TRUE(logContent.contains("## opSimulationManager finished ##"));
}

TEST_F(TEST_OPGUI_SIMULATION_EXECUTION, InitializeSimulation) {
    auto& instance = OPGUISimulationExecution::getInstance();

    // Test case 1: Successful initialization with simulations
    std::vector<std::pair<int, OpenAPI::OAISimulationConfig>> indexedConfigs = {
        {0, OpenAPI::OAISimulationConfig()},
        {1, OpenAPI::OAISimulationConfig()},
        {2, OpenAPI::OAISimulationConfig()}
    };

    QString errorMsg;
    ASSERT_TRUE(instance.initializeSimulation(indexedConfigs, errorMsg));
    ASSERT_TRUE(errorMsg.isEmpty());
    ASSERT_EQ(instance.totalSimulationsCount.loadRelaxed(), 3);
    ASSERT_TRUE(instance.isRunning);

    // Test case 2: Unsuccessful initialization with no simulations
    indexedConfigs.clear();
    ASSERT_FALSE(instance.initializeSimulation(indexedConfigs, errorMsg));
    ASSERT_FALSE(errorMsg.isEmpty());
    ASSERT_EQ(instance.totalSimulationsCount.loadRelaxed(), 0);
}