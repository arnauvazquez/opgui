/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include "gtest/gtest.h"
#include "OPGUISimulationExecution.h"
#include <QFile>
#include <QDateTime>

class TEST_OPGUI_SIMULATION_EXECUTION : public ::testing::Test {
protected:
    QString testLogPath;
    QString testExePath;
    OpenAPI::OAIOpSimulationManagerXmlRequest testConfig;
    
    void SetUp() override;
    void TearDown() override;
    
    QString readLogFile() const;
};