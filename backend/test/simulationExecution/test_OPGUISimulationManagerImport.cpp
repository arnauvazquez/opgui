/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "test_OPGUISimulationManagerImport.h"
#include "OPGUICoreGlobalConfig.h"
#include "test_helpers.h"
#include <QDir>

QString simulationManagerXml = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
<opSimulationManager>
    <logLevel>3</logLevel>
    <simulation>opSimulation</simulation>
    <logFileSimulationManager>opSimulationManager.log</logFileSimulationManager>
    <libraries>modules</libraries>
    <simulationConfigs>
        <simulationConfig>
            <configurations>test_config/1210674/0-0-0/configs</configurations>
            <logFileSimulation>test_config/1210674/0-0-0/opSimulation.log</logFileSimulation>
            <results>test_config/1210674/0-0-0/results</results>
        </simulationConfig>
        <simulationConfig>
            <configurations>test_config/1210678/0-0-0/configs</configurations>
            <logFileSimulation>test_config/1210678/0-0-0/opSimulation.log</logFileSimulation>
            <results>test_config/1210678/0-0-0/results</results>
        </simulationConfig>
    </simulationConfigs>
</opSimulationManager>)"
);

void SIMULATION_MANAGER_IMPORT_TEST::SetUp() {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    QDir dirWkspc(OPGUICoreGlobalConfig::getInstance().workspace());
    ASSERT_TRUE(dirWkspc.exists()) << "Core directory not found under:" + 
        OPGUICoreGlobalConfig::getInstance().workspace().toStdString();
    
    this->testDirPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(), "test_simmanager");
    this->testSimulationManagerXmlPath = TestHelpers::joinPaths(this->testDirPath, "test_opSimulationManager.xml");

    // If test directory exists, remove it
    QDir dirTest(this->testDirPath);
    if(dirTest.exists()) {
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove existing test directory: " 
            << this->testDirPath.toStdString();
    }

    ASSERT_TRUE(dirTest.mkpath(this->testDirPath)) << "Failed to create test directory at: " 
        << this->testDirPath.toStdString();
}

void SIMULATION_MANAGER_IMPORT_TEST::TearDown() {
    QDir dirTest(this->testDirPath);
    if(dirTest.exists()) {
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove test directory: " 
            << this->testDirPath.toStdString();
    }
}

TEST_F(SIMULATION_MANAGER_IMPORT_TEST, Import_valid_simulation_manager_POSITIVE) {
    OpenAPI::OAIOpSimulationManagerXmlRequest config;
    QString errorMsg;

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->testSimulationManagerXmlPath, simulationManagerXml))
        << "Could not save xml file as: " + this->testSimulationManagerXmlPath.toStdString();

    QDomDocument doc;
    QFile file(this->testSimulationManagerXmlPath);
    ASSERT_TRUE(file.open(QIODevice::ReadOnly)) << "Could not open file: " 
        << this->testSimulationManagerXmlPath.toStdString();
    ASSERT_TRUE(doc.setContent(&file)) << "Could not parse XML content from file";
    file.close();

    QDomElement rootEl = doc.documentElement();
    bool result = this->importer.importSimulationManager(rootEl, config, errorMsg);

    ASSERT_TRUE(result) << "Import failed with error: " << errorMsg.toStdString();
    ASSERT_EQ(config.getLogLevel(), 3);
    ASSERT_EQ(config.getSimulation(), "opSimulation");
    ASSERT_EQ(config.getLogFileSimulationManager(), "opSimulationManager.log");
    ASSERT_EQ(config.getLibraries(), "modules");
    ASSERT_EQ(config.getSimulationConfigs().size(), 2);
}

TEST_F(SIMULATION_MANAGER_IMPORT_TEST, Import_empty_file_NEGATIVE) {
    OpenAPI::OAIOpSimulationManagerXmlRequest config;
    QString errorMsg;

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->testSimulationManagerXmlPath, ""))
        << "Could not save empty file as: " + this->testSimulationManagerXmlPath.toStdString();

    QDomDocument doc;
    QFile file(this->testSimulationManagerXmlPath);
    ASSERT_TRUE(file.open(QIODevice::ReadOnly));
    ASSERT_FALSE(doc.setContent(&file));
    file.close();

    QDomElement rootEl = doc.documentElement();
    bool result = this->importer.importSimulationManager(rootEl, config, errorMsg);

    ASSERT_FALSE(result);
    ASSERT_TRUE(errorMsg.contains("Invalid or missing opSimulationManager root element"));
}

TEST_F(SIMULATION_MANAGER_IMPORT_TEST, Import_missing_required_elements_NEGATIVE) {
    OpenAPI::OAIOpSimulationManagerXmlRequest config;
    QString errorMsg;

    QString modifiedXml = TestHelpers::removeXmlElement(simulationManagerXml, "logLevel");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->testSimulationManagerXmlPath, modifiedXml))
        << "Could not save xml file as: " + this->testSimulationManagerXmlPath.toStdString();

    QDomDocument doc;
    QFile file(this->testSimulationManagerXmlPath);
    ASSERT_TRUE(file.open(QIODevice::ReadOnly));
    ASSERT_TRUE(doc.setContent(&file));
    file.close();

    QDomElement rootEl = doc.documentElement();
    bool result = this->importer.importSimulationManager(rootEl, config, errorMsg);

    ASSERT_FALSE(result);
    ASSERT_TRUE(errorMsg.contains("Missing required element: logLevel"));
}

TEST_F(SIMULATION_MANAGER_IMPORT_TEST, Import_invalid_log_level_NEGATIVE) {
    OpenAPI::OAIOpSimulationManagerXmlRequest config;
    QString errorMsg;

    QString modifiedXml = TestHelpers::replaceXmlElementContent(simulationManagerXml, "logLevel", "-1");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->testSimulationManagerXmlPath, modifiedXml))
        << "Could not save xml file as: " + this->testSimulationManagerXmlPath.toStdString();

    QDomDocument doc;
    QFile file(this->testSimulationManagerXmlPath);
    ASSERT_TRUE(file.open(QIODevice::ReadOnly));
    ASSERT_TRUE(doc.setContent(&file));
    file.close();

    QDomElement rootEl = doc.documentElement();
    bool result = this->importer.importSimulationManager(rootEl, config, errorMsg);

    ASSERT_FALSE(result);
    ASSERT_TRUE(errorMsg.contains("Invalid logLevel value"));
}

TEST_F(SIMULATION_MANAGER_IMPORT_TEST, Import_missing_simulation_configs_NEGATIVE) {
    OpenAPI::OAIOpSimulationManagerXmlRequest config;
    QString errorMsg;

    QString modifiedXml = TestHelpers::removeXmlElement(simulationManagerXml, "simulationConfigs");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->testSimulationManagerXmlPath, modifiedXml))
        << "Could not save xml file as: " + this->testSimulationManagerXmlPath.toStdString();

    QDomDocument doc;
    QFile file(this->testSimulationManagerXmlPath);
    ASSERT_TRUE(file.open(QIODevice::ReadOnly));
    ASSERT_TRUE(doc.setContent(&file));
    file.close();

    QDomElement rootEl = doc.documentElement();
    bool result = this->importer.importSimulationManager(rootEl, config, errorMsg);

    ASSERT_FALSE(result);
    ASSERT_TRUE(errorMsg.contains("Missing required element: simulationConfigs"));
}

TEST_F(SIMULATION_MANAGER_IMPORT_TEST, Import_invalid_simulation_config_NEGATIVE) {
    OpenAPI::OAIOpSimulationManagerXmlRequest config;
    QString errorMsg;

    QString modifiedXml = TestHelpers::removeXmlElement(simulationManagerXml, "configurations", "simulationConfig");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->testSimulationManagerXmlPath, modifiedXml))
        << "Could not save xml file as: " + this->testSimulationManagerXmlPath.toStdString();

    QDomDocument doc;
    QFile file(this->testSimulationManagerXmlPath);
    ASSERT_TRUE(file.open(QIODevice::ReadOnly));
    ASSERT_TRUE(doc.setContent(&file));
    file.close();

    QDomElement rootEl = doc.documentElement();
    bool result = this->importer.importSimulationManager(rootEl, config, errorMsg);

    ASSERT_FALSE(result);
    ASSERT_TRUE(errorMsg.contains("Missing configurations in simulationConfig"));
}