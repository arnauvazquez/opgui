/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>

#include "OPGUICore.h"
#include "OPGUISimulationManagerImport.h"
#include "OAIOpSimulationManagerXmlRequest.h"

class SIMULATION_MANAGER_IMPORT_TEST : public ::testing::Test {
public:
    QString testDirPath;
    QString testSimulationManagerXmlPath;
    OPGUISimulationManagerImport importer;
    
    void SetUp() override;
    void TearDown() override;
};