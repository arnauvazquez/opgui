/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>

#include "OPGUICore.h"
#include "OAI_api_saveSystem_post_request.h"
#include "OAIDefault200Response.h"

class SAVE_SYSTEM_TEST : public ::testing::Test {
  public:
    QString testDirPath;
    QString testDirComponentsPath;
    QString testSystemFilePath;
    
    OpenAPI::OAIDefault200Response response;
    OpenAPI::OAIDefault200Response response200Expected;

    bool parseSystemJson(const QString& jsonStr, OpenAPI::OAISystemUI& oai_system_ui);
    void SetUp() override;
    void TearDown() override;
};
