/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import * as runtime from '../runtime';
import type { ConfigsRequest, Default200Response } from '../models';
export interface ApiConvertToConfigsPostRequest {
    configsRequest: ConfigsRequest;
}
/**
 *
 */
export declare class ConvertToConfigsApi extends runtime.BaseAPI {
    /**
     * send the IDs to convert into EXAMPLEID/configs for each selected experiment and also send the system paths that will be used
     * convert the selected PCM cases into configuration folders
     */
    apiConvertToConfigsPostRaw(requestParameters: ApiConvertToConfigsPostRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Default200Response>>;
    /**
     * send the IDs to convert into EXAMPLEID/configs for each selected experiment and also send the system paths that will be used
     * convert the selected PCM cases into configuration folders
     */
    apiConvertToConfigsPost(requestParameters: ApiConvertToConfigsPostRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Default200Response>;
}
