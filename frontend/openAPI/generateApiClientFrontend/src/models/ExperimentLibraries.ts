/* tslint:disable */
/* eslint-disable */
/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ExperimentLibraries
 */
export interface ExperimentLibraries {
    /**
     * 
     * @type {string}
     * @memberof ExperimentLibraries
     */
    worldLibrary: string;
    /**
     * 
     * @type {string}
     * @memberof ExperimentLibraries
     */
    dataBufferLibrary: string;
    /**
     * 
     * @type {string}
     * @memberof ExperimentLibraries
     */
    stochasticsLibrary: string;
}

/**
 * Check if a given object implements the ExperimentLibraries interface.
 */
export function instanceOfExperimentLibraries(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "worldLibrary" in value;
    isInstance = isInstance && "dataBufferLibrary" in value;
    isInstance = isInstance && "stochasticsLibrary" in value;

    return isInstance;
}

export function ExperimentLibrariesFromJSON(json: any): ExperimentLibraries {
    return ExperimentLibrariesFromJSONTyped(json, false);
}

export function ExperimentLibrariesFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExperimentLibraries {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'worldLibrary': json['worldLibrary'],
        'dataBufferLibrary': json['dataBufferLibrary'],
        'stochasticsLibrary': json['stochasticsLibrary'],
    };
}

export function ExperimentLibrariesToJSON(value?: ExperimentLibraries | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'worldLibrary': value.worldLibrary,
        'dataBufferLibrary': value.dataBufferLibrary,
        'stochasticsLibrary': value.stochasticsLibrary,
    };
}

