/* tslint:disable */
/* eslint-disable */
/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Scenario
 */
export interface Scenario {
    /**
     * 
     * @type {string}
     * @memberof Scenario
     */
    openScenarioFile: string;
}

/**
 * Check if a given object implements the Scenario interface.
 */
export function instanceOfScenario(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "openScenarioFile" in value;

    return isInstance;
}

export function ScenarioFromJSON(json: any): Scenario {
    return ScenarioFromJSONTyped(json, false);
}

export function ScenarioFromJSONTyped(json: any, ignoreDiscriminator: boolean): Scenario {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'openScenarioFile': json['openScenarioFile'],
    };
}

export function ScenarioToJSON(value?: Scenario | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'openScenarioFile': value.openScenarioFile,
    };
}

