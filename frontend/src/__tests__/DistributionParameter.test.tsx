/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {renderWithProviders} from "../utils/test.utils.tsx";
import {fireEvent, screen} from "@testing-library/react";
import DistributionParameter
    from "../components/interfaces/SystemEditor/DistributionParameter/DistributionParameter.tsx";
import {act} from "react-dom/test-utils";

const baseTestId = 'distribution'

const getBaseTestId = () => baseTestId
const getInputTestId = (extraTestId: string) => `${getBaseTestId()}-distribution-${extraTestId}`
const getInput = (inputName: string) => screen.getByTestId(getInputTestId(inputName)).querySelector('input')

describe('Testing Distribution Parameter Component', () => {

    it('Should not break with empty object', () => {
        renderWithProviders(<DistributionParameter variables={{}} data-testid={getBaseTestId()}/>);
        const folderTree = screen.getByTestId(getBaseTestId());

        expect(folderTree).toBeInTheDocument();
    });

    it('Should show all attributes of variables with correct values', () => {
        const variables = {
            mean: 2.4,
            max: 10,
        }
        renderWithProviders(<DistributionParameter variables={variables} data-testid={getBaseTestId()}/>);
        const meanInput = getInput('mean');
        const maxInput = getInput('max');

        expect(meanInput).toBeInTheDocument();
        expect(meanInput).toHaveValue(variables.mean.toString())
        expect(maxInput).toBeInTheDocument();
        expect(maxInput).toHaveValue(variables.max.toString())
    });

    it('Should update variable values correctly', () => {
        const variables = {
            mean: 2.4,
            max: 10,
        }
        renderWithProviders(<DistributionParameter variables={variables} data-testid={getBaseTestId()}/>);
        const meanInput = getInput('mean');

        expect(meanInput).toBeInTheDocument();
        act(() => {
            if (meanInput) fireEvent.change(meanInput, { target: { value: '3.7' }})
        })

        expect(meanInput).toHaveValue('3.7')
    });

    it('Should update variable if commas are used instead of points', () => {
        const variables = {
            mean: 2.4,
            max: 10,
        }
        renderWithProviders(<DistributionParameter variables={variables} data-testid={getBaseTestId()} />);
        const meanInput = getInput('mean');

        expect(meanInput).toBeInTheDocument();
        act(() => {
            if (meanInput) fireEvent.change(meanInput, { target: { value: '3,7' }})
        })

        expect(meanInput).toHaveValue('3,7')
    });
});