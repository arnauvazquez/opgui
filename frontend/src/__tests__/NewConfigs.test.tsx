/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {fireEvent, waitForElementToBeRemoved, within} from "@testing-library/react";
import {renderWithProviders} from "../utils/test.utils.tsx";
import NewConfig from "../components/interfaces/SetupConfigs/setups/NewConfig";
import {hexagonsData} from "../components/interfaces/SetupConfigs/setups/NewConfig/hexagonsData";
import {act} from "react-dom/test-utils";
import setupMocksCalls, {CallHandler} from "../utils/tests/setupMocksCalls.ts";
import handleResponse from "../utils/tests/handleResponse.ts";
import {DYNAMIC_SELECTORS} from "../components/commonComponents/DynamicInputs/interfaces.ts";
import SideForm from "../components/interfaces/SetupConfigs/setups/NewConfig/SideForm";
import initialFormData from "../components/interfaces/SetupConfigs/setups/NewConfig/initialFormData.ts";
import userEvent from "@testing-library/user-event";

const mockedUsedNavigate = jest.fn();
jest.mock("react-router", () => ({
    ...(jest.requireActual("react-router") as any),
    useNavigate: () => mockedUsedNavigate
}));
const setupValueLists = ()=> {

    const setupCalls: CallHandler[] = [
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.TRAFFIC_RULES}`,
            response: handleResponse({
                listValues: ['DE']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.DATA_BUFFER_LIBRARIES}`,
            response: handleResponse({
                listValues: ['DataBuffer']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.WORLD_LIBRARIES}`,
            response: handleResponse({
                listValues: ['WORLD', 'WORLD_OSI']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.STOCHASTIC_LIBRARIES}`,
            response: handleResponse({
                listValues: ['Stochastics']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.DEFAULT_LOGGING_GROUPS}`,
            response: handleResponse({
                listValues: ["Trace", "RoadPosition", "RoadPositionExtended", "Sensor", "Vehicle", "Visualization"]
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.SPAWNER_TYPES}`,
            response: handleResponse({
                listValues: ['example']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.SPAWNER_LIBRARIES}`,
            response: handleResponse({
                listValues: ['example']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.WEATHERS}`,
            response: handleResponse({
                listValues: ['example']
            })
        },
        {
            url: `api/getFileTree`,
            response: handleResponse({
                basePath: 'route1',
                fileSystemTree: {
                    name: 'route1',
                    children: [{
                        name: '',
                    }]
                }
            })
        },
    ]
    setupMocksCalls(setupCalls)
}


const formTestId = 'testId';
describe('Render test for New Configs Screen', () => {

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('It should render all hexagons ', () => {
        const { getByTestId } = renderWithProviders(<NewConfig />)
        for (let i= 0; i < hexagonsData.length; i++){
            const component = getByTestId(`hexagon-${i}`);
            expect(component).toBeInTheDocument();
            expect(component).not.toBeEmptyDOMElement();
        }
    });

    test('It should show modal when trying to send form with unsaved changes', async () => {
        setupValueLists()
        const { getByTestId } = renderWithProviders(<NewConfig initialRoute={'route1'}/>)
        await waitForElementToBeRemoved(()=> getByTestId('testid-fetching-groups'))
        const invocationsComponent = within(getByTestId('test-id-experiment.numberOfInvocations-input'))
        const input = invocationsComponent.getAllByRole('textbox')[0]
        const loadButton = getByTestId('test-id-button-convert')
        await userEvent.type(input, '90878');
        act(()=> fireEvent.click(loadButton))
        expect(loadButton).toBeEnabled()
        const modal = getByTestId(`test-id-modal-Save local changes before submitting?`)
        expect(modal).toBeInTheDocument()
    });

    test('It should show modal when trying to load with unsaved changes', async () => {
        setupValueLists()
        const { getByTestId } = renderWithProviders(<NewConfig initialRoute={'route1'}/>)
        await waitForElementToBeRemoved(()=> getByTestId('testid-fetching-groups'))
        const invocationsComponent = within(getByTestId('test-id-experiment.numberOfInvocations-input'))
        const input = invocationsComponent.getAllByRole('textbox')[0]
        const loadButton = getByTestId('test-id-button-load')
        await userEvent.type(input, '90878');
        act(()=> fireEvent.click(loadButton))
        expect(loadButton).toBeEnabled()
        const modal = getByTestId(`test-id-modal-Load new config files?`)
        expect(modal).toBeInTheDocument()
    });

    test('It should call onSubmit callback when submit button is clicked', () => {
        setupValueLists()
        const onSubmit = jest.fn();
        const localData = {
            testField: true,
            ...initialFormData,
        }
        const { getByTestId } = renderWithProviders(
            <SideForm
                title={'test'}
                testId={formTestId}
                formData={localData}
                onSubmit={onSubmit}
            />)
        const submitButton = getByTestId(`${formTestId}-button-save`)
        act(()=> {
            fireEvent.click(submitButton)
        })
        expect(onSubmit).toHaveBeenCalledTimes(1);
        expect(onSubmit).toHaveBeenCalledWith(localData)
    });
})