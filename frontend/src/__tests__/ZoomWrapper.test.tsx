/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { renderWithProviders } from "../utils/test.utils";
import ZoomWrapper, {ZoomWrapperProps} from "../components/commonComponents/ZoomWrapper/ZoomWrapper.tsx";

const TEST_ID = 'zoom-manager'

const render = (props: ZoomWrapperProps) => {
    const { children, ...rest } = props
    return renderWithProviders(
        <ZoomWrapper testId={TEST_ID} {...rest}>
            {children}
        </ZoomWrapper>
    )
}
describe('Render test for Home Screen component', () => {

    test('It render child properly', async()=> {
        const { getByTestId } = render({ children: <div data-testid={'test-child'} /> })
        const child = getByTestId('test-child')
        expect(child).toBeInTheDocument()
    })

    test('Zoom in property works', async()=> {
        const zoomIn = 1.5
        const { getByTestId } = render({ children: <div data-testid={'test-child'} />, zoomScale: zoomIn })
        const zoomWrapper = getByTestId(`${TEST_ID}-manager`)
        const computedStyle = getComputedStyle(zoomWrapper)
        expect(computedStyle.getPropertyValue('transform')).toBe(`scale(${zoomIn})`)
    })

    test('Zoom out property works', async()=> {
        const zoomOut = 0.5
        const { getByTestId } = render({ children: <div data-testid={'test-child'} />, zoomScale: zoomOut })
        const zoomWrapper = getByTestId(`${TEST_ID}-manager`)
        const computedStyle = getComputedStyle(zoomWrapper)
        expect(computedStyle.getPropertyValue('transform')).toBe(`scale(${zoomOut})`)
    })
})

