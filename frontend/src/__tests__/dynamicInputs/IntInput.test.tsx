import {fireEvent, screen} from "@testing-library/react";
import {IDynamicInputProps} from "../../components/commonComponents/DynamicInputs/interfaces.ts";
import {act} from "react-dom/test-utils";
import {RenderInput} from "../../utils/tests/RenderInputTest.tsx";
import getTestIdFromName from "../../utils/tests/getTestIdFromName.ts";
import IntInput from "../../components/commonComponents/DynamicInputs/IntInput";

const name = 'number'
const testId = getTestIdFromName(name)

describe('Testing Int Input', () => {

    it('Should not break with empty minimum props', () => {
        const props:IDynamicInputProps = {
            name: name,
            onChange: (_inputName: string, _value: any) => {},
            value: '5',
            inputProps: {
                label: '',
            }
        }
        RenderInput(props, IntInput);
        const input = screen.getByTestId(testId);

        expect(input).toBeInTheDocument();
    });

    it('Should start with initial value set', () => {
        const props:IDynamicInputProps = {
            name: name,
            onChange: (_inputName: string, _value: any) => {},
            value: '5',
            inputProps: {
                label: '',
            }
        }
        RenderInput(props, IntInput);
        const input = screen.getByTestId(testId);
        expect(input).toBeInTheDocument();
        const selector = input.querySelector('input')
        expect(selector).toBeInTheDocument();
        expect(selector).toHaveValue('5')
    });

    it('Should call the onChange callback when its value is changed', async () => {
        const callback = jest.fn()
        const props:IDynamicInputProps = {
            name: name,
            onChange: (inputName: string, value: any) => {
                callback(inputName, value)
            },
            value: '5',
            inputProps: {
                label: '',
            }
        }
        RenderInput(props, IntInput);
        const component = screen.getByTestId(testId);
        expect(component).toBeInTheDocument();
        const input = component.querySelector('input')
        if (input){
            await act(() => {
                fireEvent.change(input, { target: { value: '10' }});
            });
        }
        expect(input).toBeInTheDocument();
        expect(input).toHaveValue('10')
        expect(callback).toHaveBeenCalledWith('number', '10')
    });

})