import {
    cantRepeatValidation,
    sumMustBeValidation
} from "../../components/commonComponents/DynamicInputs/onChangeValidations.ts";

describe('On Change Validation', () => {

    it('Sum validation must return success', () => {
        const validation = sumMustBeValidation(1, 'probability')
        const dataToValidate = [{ probability: 0.1},{ probability: 0.4},{ probability: 0.5}]
        const result = validation('probability', dataToValidate)
        expect(result.error).toBe(false);
    });

    it('Sum validation must return error', () => {
        const validation = sumMustBeValidation(1, 'probability')
        const dataToValidate = [{ probability: 0.1},{ probability: 0.4},{ probability: 0.8}]
        const result = validation('probability', dataToValidate)
        expect(result.error).toBe(true);
    });

    it('Sum validation must return success', () => {
        const validation = cantRepeatValidation('timeOfDay')
        const dataToValidate = [{ timeOfDay: 1},{ timeOfDay: 2},{ timeOfDay: 3}]
        const result = validation('probability', dataToValidate)
        expect(result.error).toBe(false);
    });

    it('Sum validation must return error', () => {
        const validation = cantRepeatValidation('timeOfDay')
        const dataToValidate = [{ timeOfDay: 1},{ timeOfDay: 2},{ timeOfDay: 3}, { timeOfDay: 2}]
        const result = validation('probability', dataToValidate)
        expect(result.error).toBe(true);
    });

})