/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {fireEvent, render} from "@testing-library/react";
import ConnectionArrow, {
  ConnectionArrowProps
} from "../../components/interfaces/SystemEditor/TargetZone/ConnectionArrow.tsx";
import {act} from "react-dom/test-utils";

const testId = 'arrow'
const getDeleteButtonTestId = (connectionId: string | number)=> `${connectionId}-delete-button`;

const getWarningIconTestId = (connectionId: string | number) => `${connectionId}-invalid-icon`

const defaultProps: ConnectionArrowProps = {
  connection: {
    id: 0,
    indexComponentInput: 0,
    indexComponentOutput: 1,
    source: {
      component: 0,
      output: 0,
    },
    target: {
      component: 0,
      input: 0,
    }
  },
  zoomScale: 1,
  start: '',
  end: '',
}

const renderArrow = (props?: object) => render(
  <ConnectionArrow testId={testId} {...defaultProps} {...props} />
)

describe('Connection Arrow', () => {
  test('Should call onClick callback when clicked', () => {
    const onClick = jest.fn();
    const arrow = renderArrow({ onClick });
    const arrowComponent = arrow.getByTestId(testId);
    act(()=> fireEvent.click(arrowComponent))
    expect(onClick).toHaveBeenCalledTimes(1);
  });
  test('Should show delete icon when hovered', () => {
    const arrow = renderArrow();
    const arrowComponent = arrow.getByTestId(testId);
    act(()=> fireEvent.mouseOver(arrowComponent))
    const deleteButton = arrow.getByTestId(getDeleteButtonTestId(defaultProps.connection.id));
    expect(deleteButton).toBeInTheDocument();
  });
  test('Should display warning icon when connection is not valid', () => {
    const arrow = renderArrow({ isValid: false });
    const warningIcon = arrow.getByTestId(getWarningIconTestId(defaultProps.connection.id));
    expect(warningIcon).toBeInTheDocument();
  });
  test('Should NOT display warning icon when connection IS valid', async () => {
    const arrow = renderArrow({ isValid: true });
    const warningIcon = await arrow.queryByTestId(getWarningIconTestId(defaultProps.connection.id));
    expect(warningIcon).toBeNull();
  });
})