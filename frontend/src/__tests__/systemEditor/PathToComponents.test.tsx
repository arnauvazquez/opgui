/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import fetchMock from "jest-fetch-mock";
import {cleanup} from "@testing-library/react";
import {act} from "react-dom/test-utils";
import {renderWithProviders} from "../../utils/test.utils.tsx";
import SystemEditor from "../../components/interfaces/SystemEditor/SystemEditor.tsx";
import {screen} from "@testing-library/dom";


describe('Path To Components tests', () => {
  beforeEach(async () => {
    fetchMock.enableMocks();
  });
  afterEach(() => {
    fetchMock.resetMocks();
    cleanup();
  });
  test('Given all the components are empty accordion zone items should be empty', async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify([])
    );
    await act(async () => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
    const accordionComponent = screen.getByTestId('test-id-box-accordion-zone');
    expect(accordionComponent).toBeInTheDocument();
    expect(accordionComponent?.textContent).toBe('');
  });
  test('Given api call brings data when the component is loaded then accordion details should have content', async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify([
        {
          "id": 0,
          "inputs": [
            {
              "cardinality": 1,
              "id": 0,
              "title": "Longitudinal Signal",
              "type": "LongitudinalSignal",
              "unit": ""
            }
          ],
          "library": "ActionLongitudinalDriver",
          "position": {
            "x": 0,
            "y": 0
          },
          "title": "ActionLongitudinalDriver",
          "type": "Action"
        },
        {
          "id": 1,
          "inputs": [
            {
              "cardinality": 1,
              "id": 0,
              "title": "Desired trajectory",
              "type": "TrajectoryEvent",
              "unit": ""
            }
          ],
          "library": "Algorithm_RouteControl",
          "outputs": [
            {
              "id": 0,
              "title": "Control",
              "type": "ControlData",
              "unit": ""
            }
          ],
          "parameters": [
          ],
          "position": {
            "x": 0,
            "y": 0
          },
          "title": "Algorithm_RouteControl",
          "type": "Algorithm"
        },
        {
          "id": 2,
          "library": "ParametersVehicle",
          "outputs": [
            {
              "id": 1,
              "title": "Vehicle Parameters",
              "type": "ParametersVehicleSignal",
              "unit": ""
            }
          ],
          "position": {
            "x": 0,
            "y": 0
          },
          "title": "ParametersVehicle",
          "type": "Sensor"
        },
      ])
    );
    await act(async () => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
    const accordionActions = screen.getByTestId('test-id-accordion-Action');
    const accordionAlgorithms = screen.getByTestId('test-id-accordion-Algorithm');
    const accordionSensors = screen.getByTestId('test-id-accordion-Sensor');
    expect(accordionAlgorithms).toBeInTheDocument();
    expect(accordionSensors).toBeInTheDocument();
    expect(accordionActions).toBeInTheDocument();
    const actionsAccordionDetails = accordionActions.querySelector('.MuiAccordionDetails-root');
    const algorithmsAccordionDetails = accordionAlgorithms.querySelector('.MuiAccordionDetails-root');
    const sensorsAccordionDetails = accordionSensors.querySelector('.MuiAccordionDetails-root');
    expect(actionsAccordionDetails?.textContent).not.toBe('');
    expect(actionsAccordionDetails?.textContent).toBe('ActionLongitudinalDriver');
    expect(algorithmsAccordionDetails?.textContent).not.toBe('');
    expect(algorithmsAccordionDetails?.textContent).toBe('Algorithm_RouteControl');
    expect(sensorsAccordionDetails?.textContent).not.toBe('');
    expect(sensorsAccordionDetails?.textContent).toBe('ParametersVehicle');
  });
  test(
    'GIVEN interface is loading correctly ' +
    'WHEN the api call fails ' +
    'THEN a GET call is rejected', async () => {
      fetchMock.mockReject(Error('Bad Request'))
      await act(async () => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
      expect(fetchMock).toHaveBeenCalledTimes(1)
      expect(fetchMock).rejects.toThrowError(Error('Bad Request'))
    });
});
