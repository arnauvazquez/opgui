/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import fetchMock from "jest-fetch-mock";
import {cleanup, fireEvent} from "@testing-library/react";
import {screen} from "@testing-library/dom";
import {act} from "react-dom/test-utils";
import {
  renderSystemEditor,
  SelectAnotherSystem,
  SelectSystemFirst,
  setupMocks
} from "../../utils/tests/systemSelection.tsx";

const openFileTreeSystemButton = 'test-id-input-button-system'

describe('System bar and stepper tests', () => {
  const systems =
    [
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 0,
        "priority": 0,
        "title": "Follow Trajectory",
        "file": "Follow_Trajectory.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 1,
        "priority": 1,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 2,
        "priority": 2,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 3,
        "priority": 3,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 4,
        "priority": 4,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 5,
        "priority": 5,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 6,
        "priority": 6,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
      {
        "components": [
        ],
        "connections": [
        ],
        "id": 7,
        "priority": 7,
        "title": "Copy Trajectory",
        "file": "Agent_Copy.xml"
      },
    ]

  afterEach(() => {
    fetchMock.resetMocks();
    cleanup();
  });
  test('By default label should say 0 - 0 systems', async () => {
    setupMocks({ system: systems[0] })
    renderSystemEditor()
    expect(await screen.findByText(/0 - 0 Systems/i)).toBeInTheDocument();
  });
  test('By default stepper button should be disabled since no system have been loaded', async () => {
    setupMocks({ system: systems[0] })
    renderSystemEditor()
    const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
    const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
    expect(stepperBackButton).toBeInTheDocument();
    expect(stepperNextButton).toBeInTheDocument();
    expect(stepperBackButton).toBeDisabled()
    expect(stepperNextButton).toBeDisabled()
  });
  test('Given User click on Load Systems Button system should load', async () => {
    await SelectSystemFirst({ system: systems[0] });
    expect(await screen.findByText(/1 - 1 Systems/i)).toBeInTheDocument();
  });
  test('Given User click on Load Systems and theres 5 or more system stepper should be enabled', async () => {
    await SelectSystemFirst({ system: systems[0] });

    const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
    const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
    expect(stepperBackButton).toBeInTheDocument();
    expect(stepperNextButton).toBeInTheDocument();
    expect(stepperBackButton).toBeDisabled()
    expect(stepperNextButton).toBeDisabled()
    await SelectAnotherSystem({ system: systems[1] })
    await SelectAnotherSystem({ system: systems[2] })
    await SelectAnotherSystem({ system: systems[3] })
    await SelectAnotherSystem({ system: systems[4] })
    expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
    expect(stepperNextButton).toBeEnabled()
  });

  test('Given User click on Load Systems and theres 5 or more systems when clicking stepper should show 2-5 systems', async () => {
    await SelectSystemFirst({ system: systems[0] });
    await SelectAnotherSystem({ system: systems[1] })
    await SelectAnotherSystem({ system: systems[2] })
    await SelectAnotherSystem({ system: systems[3] })
    await SelectAnotherSystem({ system: systems[4] })
    const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const system = screen.getByTestId('test-id-system-1');
    expect(system).toBeInTheDocument();
    expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
    expect(stepperNextButton).toBeEnabled()
    act(() => {
      fireEvent.click(stepperNextButton);
    });
    expect(await screen.findByText(/5 - 5 Systems/i)).toBeInTheDocument();
  });

  test('Given User click on Load Systems and theres 8 systems when clicking stepper should show 8-8 systems', async () => {
    await SelectSystemFirst({ system: systems[0] });
    for (let i = 1 ; i<8 ; i++){
      await SelectAnotherSystem({ system: systems[i] })
    }
    const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const system = screen.getByTestId('test-id-system-1');
    expect(system).toBeInTheDocument();
    expect(await screen.findByText(/4 - 8 Systems/i)).toBeInTheDocument();
    expect(stepperNextButton).toBeEnabled()
    act(() => {
      fireEvent.click(stepperNextButton);
    });
    expect(await screen.findByText(/8 - 8 Systems/i)).toBeInTheDocument();
  });

  test('Given user already advance 1 step in the stepper and theres 5 systems when clicking back should show 4 - 5 Systems again', async () => {
    await SelectSystemFirst({ system: systems[0] });
    const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
    const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
    expect(stepperBackButton).toBeDisabled();
    for (let i = 1 ; i<5 ; i++){
      await SelectAnotherSystem({ system: systems[i] })
    }
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const system = screen.getByTestId('test-id-system-1');
    expect(system).toBeInTheDocument();
    expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
    expect(stepperNextButton).toBeEnabled()
    act(() => {
      fireEvent.click(stepperNextButton);
    });
    expect(await screen.findByText(/5 - 5 Systems/i)).toBeInTheDocument();
    expect(stepperBackButton).toBeEnabled();
    act(() => {
      fireEvent.click(stepperBackButton);
    });
    expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
    expect(stepperBackButton).toBeDisabled();
  });

  test('Given user already advance 1 step in the stepper and theres 8 systems when clicking back should show 4 - 8 Systems again', async () => {
    await SelectSystemFirst({ system: systems[0] });
    const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
    const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
    expect(stepperBackButton).toBeDisabled();
    for (let i = 1 ; i<8 ; i++){
      await SelectAnotherSystem({ system: systems[i] })
    }
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const system = screen.getByTestId('test-id-system-1');
    expect(system).toBeInTheDocument();
    expect(await screen.findByText(/4 - 8 Systems/i)).toBeInTheDocument();
    expect(stepperNextButton).toBeEnabled()
    act(() => {
      fireEvent.click(stepperNextButton);
    });
    expect(await screen.findByText(/8 - 8 Systems/i)).toBeInTheDocument();
    expect(stepperBackButton).toBeEnabled();
    act(() => {
      fireEvent.click(stepperBackButton);
    });
    expect(await screen.findByText(/4 - 8 Systems/i)).toBeInTheDocument();
    expect(stepperBackButton).toBeDisabled();
  });
  test('Given a Simulation when clicking on the chevron down icon Dropdown menu should open', async () => {
    await SelectSystemFirst({ system: systems[0] });
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
  });
  test('Given user clicks on add button should add a new system', async () => {
    await renderSystemEditor()
    const addSystemButton = screen.getByTestId('test-id-add-new-system');
    expect(addSystemButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(addSystemButton);
    });
    const systemAdded = await screen.findByText('System 1');
    expect(systemAdded).toBeInTheDocument();
  });
  test('Given user clicks on load button then on clear all and confirms no system should be shown in the screen', async () => {
    await SelectSystemFirst({ system: systems[0] });
    expect(await screen.findByText(/Follow Trajectory/i)).toBeInTheDocument();
    const buttonClearAll = screen.getByTestId('test-id-button-system-clearall');
    act(() => {
      fireEvent.click(buttonClearAll);
    });
    const buttonModalClear = screen.getByTestId('test-id-modalbutton-clearall');
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    act(() => {
      fireEvent.click(buttonModalClear);
    });
    expect(droppableZone).toBeEmptyDOMElement();
    expect(await screen.findByText(/0 - 0 Systems/i)).toBeInTheDocument();
  });
  test('Given user clicks on export button without creating or loading system should show error modal ', async () => {
    await renderSystemEditor()
    const buttonExport = screen.getByTestId('test-id-button-system-export');
    act(() => {
      fireEvent.click(buttonExport);
    });
    expect(await screen.findByText(/No System were loaded or created/i)).toBeInTheDocument();
  });
  test('Given user clicks on export button when renaming a system should show modal error ', async () => {
    await SelectSystemFirst({ system: systems[0] })
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const renameButton = screen.getByTestId('test-id-menu-item-rename');
    expect(renameButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(renameButton);
    });
    const buttonExport = screen.getByTestId('test-id-button-system-export');
    act(() => {
      fireEvent.click(buttonExport);
    });
    expect(await screen.findByText(/You are renaming a system at the moment/i)).toBeInTheDocument();
  });
  test('Given user clicks on export button everything should be ok ', async () => {
    await SelectSystemFirst({ system: systems[0] })
    const system = screen.getByTestId('test-id-system-0');
    expect(system).toHaveClass('selected');
    fetchMock.mockResponseOnce(JSON.stringify({
      "response": "Ok"
    }));
    const buttonExport = screen.getByTestId('test-id-button-system-export');
    act(() => {
      fireEvent.click(buttonExport);
    });
    expect(await screen.findByText(/Systems loaded correctly/i)).toBeInTheDocument();
  });
  test('Given user opens dropdown and click rename then rename input should show and then clicks on the confirm and the system name should change', async () => {
    await SelectSystemFirst({ system: systems[0] })
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const renameButton = screen.getByTestId('test-id-menu-item-rename');
    expect(renameButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(renameButton);
    });
    const renameField = screen.getByTestId('test-id-rename-field');
    expect(renameField).toBeInTheDocument();
    const renameInput = renameField.querySelector('input');
    expect(renameInput).toBeInTheDocument();
    expect(renameInput).toHaveProperty('value', 'Follow Trajectory');
    act(() => {
      fireEvent.change(renameInput!, { target: { value: 'newTitle' } });
    });
    expect(renameInput).toHaveProperty('value', 'newTitle');
    const renameConfirm = screen.getByTestId('test-id-rename-confirm');
    expect(renameConfirm).toBeInTheDocument();
    await act(() => {
      fireEvent.click(renameConfirm);
    });
    expect(await screen.findByText(/newTitle/i)).toBeInTheDocument();
  });
  test(`Given user opens dropdown and click rename then rename 
    input should show and then clicks on the confirm with empty name and should show modal with error`, async () => {
    await SelectSystemFirst({ system: systems[0] })
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const renameButton = screen.getByTestId('test-id-menu-item-rename');
    expect(renameButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(renameButton);
    });
    const renameField = screen.getByTestId('test-id-rename-field');
    expect(renameField).toBeInTheDocument();
    const renameInput = renameField.querySelector('input');
    expect(renameInput).toBeInTheDocument();
    expect(renameInput).toHaveProperty('value', 'Follow Trajectory');
    act(() => {
      fireEvent.change(renameInput!, { target: { value: '' } });
    });
    expect(renameInput).toHaveProperty('value', '');
    const renameConfirm = screen.getByTestId('test-id-rename-confirm');
    expect(renameConfirm).toBeInTheDocument();
    await act(() => {
      fireEvent.click(renameConfirm);
    });
    expect(await screen.findByText(/You have to give a name for the system./i)).toBeInTheDocument();
  });
  test('Given user opens dropdown and click rename then rename input should show and then clicks on the cancel icon the system name should not change', async () => {
    await SelectSystemFirst({ system: systems[0] })
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const renameButton = screen.getByTestId('test-id-menu-item-rename');
    expect(renameButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(renameButton);
    });
    const renameField = screen.getByTestId('test-id-rename-field');
    expect(renameField).toBeInTheDocument();
    const renameInput = renameField.querySelector('input');
    expect(renameInput).toBeInTheDocument();
    expect(renameInput).toHaveProperty('value', 'Follow Trajectory');
    act(() => {
      fireEvent.change(renameInput!, { target: { value: 'newTitle' } });
    });
    expect(renameInput).toHaveProperty('value', 'newTitle');
    const renameCancel = screen.getByTestId('test-id-rename-cancel');
    expect(renameCancel).toBeInTheDocument();
    await act(() => {
      fireEvent.click(renameCancel);
    });
    expect(await screen.findByText(/Follow Trajectory/i)).toBeInTheDocument();
  });
  test('Given user opens dropdown and click rename then click rename again should should modal cannot rename again', async () => {
    await SelectSystemFirst({ system: systems[0] })
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const renameButton = screen.getByTestId('test-id-menu-item-rename');
    expect(renameButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(renameButton);
    });
    act(() => {
      fireEvent.click(renameButton);
    });
    expect(await screen.findByText(/Youre already renaming another system/i)).toBeInTheDocument();
  });
  test('Should hide the accordeon on click ', () => {
    renderSystemEditor()
    const hideAccordeonButton = screen.getByTestId('test-id-button-hide-accordeon');
    act(() => {
      fireEvent.click(hideAccordeonButton);
    });
    const iconHiddenAccorder = screen.getByTestId('test-id-hidden-accordeon');
    expect(iconHiddenAccorder).toBeInTheDocument();
  });
  test('Should hide the connections on click ', () => {
    renderSystemEditor()
    const hideConnectionsButton = screen.getByTestId('test-id-button-hide-connections');
    act(() => {
      fireEvent.click(hideConnectionsButton);
    });
    const iconHideConnections = screen.getByTestId('test-id-hide-connections');
    expect(iconHideConnections).toBeInTheDocument();
  });
  test('Given user opens the dropdown menu and click duplicate the system should duplicate ', async () => {
    await SelectSystemFirst({ system: systems[0]})
    const loadButton = screen.getByTestId(openFileTreeSystemButton);
    await act(() => {
      fireEvent.click(loadButton);
    });
    const icon = screen.getByTestId('test-id-system-open-menu-0');
    expect(icon).toBeInTheDocument();
    act(() => {
      fireEvent.click(icon);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const duplicateButton = screen.getByTestId('test-id-menu-item-duplicate');
    expect(duplicateButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(duplicateButton);
    });
    const newSystems = await screen.findAllByText(/Follow Trajectory/i);
    expect(newSystems.length).toBe(2);
  });
});
