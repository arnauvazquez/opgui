/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { screen } from "@testing-library/dom";
import {cleanup} from "@testing-library/react";
import { renderWithProviders } from "../../utils/test.utils.tsx";
import SystemEditor from "../../components/interfaces/SystemEditor/SystemEditor.tsx";
import userEvent from '@testing-library/user-event'
import { act } from "react-dom/test-utils";
import fetchMock from "jest-fetch-mock";

jest.mock('react-xarrows', () => {
    return {
        __esModule: true,
        default: () => <span />,
        Xarrow: () => <div />,
        useXarrow: () => jest.fn(),
        Xwrapper: () => <div />,
        updateXarrow: () => jest.fn()
    };
});
describe('Render Test for System Editor Component', () => {
    beforeEach(async () => {
        fetchMock.enableMocks();
        fetchMock.mockResponseOnce(JSON.stringify([]));
        await act(() => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
    });

    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });
    test('Should render the footer with its content', () => {
        const footer = screen.getByTestId('test-id-footer-systemeditor');
        const clearAllButton = screen.getByTestId('test-id-button-system-clearall');
        const exportButton = screen.getByTestId('test-id-button-system-export');
        const exportAsButton = screen.getByTestId('test-id-button-system-export-as');
        expect(footer).toBeInTheDocument();
        expect(clearAllButton).toBeInTheDocument();
        expect(exportButton).toBeInTheDocument();
        expect(exportAsButton).toBeInTheDocument();
    });
    test('Should render accordion zone', () => {
        const accordionSideBar = screen.getByTestId('test-id-box-accordion-zone');
        expect(accordionSideBar).toBeInTheDocument();
    });
    test('Should render drag and drop zone', () => {
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        expect(droppableZone).toBeInTheDocument();
    });
    test('Should render system editor info icon', async () => {
        const infoIcon = screen.getByTestId('test-id-icon-system-info');
        expect(infoIcon).toBeInTheDocument();
        userEvent.hover(infoIcon);
        const tip = await screen.findByRole('tooltip');
        expect(tip).toBeInTheDocument();
        expect(tip.textContent).toBe('Drag and drop one component to the dashboard to create and editate systems.');
    });
});

