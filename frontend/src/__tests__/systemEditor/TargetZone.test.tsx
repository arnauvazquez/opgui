import {ComponentUITypeEnum} from "opGUI-client-frontend";
import {SelectSystemFirst} from "../../utils/tests/systemSelection.tsx";
import fetchMock from "jest-fetch-mock";
import {cleanup, fireEvent, waitFor, within} from "@testing-library/react";
import {screen} from "@testing-library/dom";
import {act} from "react-dom/test-utils";
import userEvent from "@testing-library/user-event";

describe('Target Zone tests', () => {
  const newDataDropped = JSON.stringify({
    "id": 2,
    "type": "Action",
    "library": "Action_Test",
    "title": "Action_Test",
    "schedule": {
      "offset": 0,
      "cycle": 10,
      "response": 0
    },
    "parameters": [
      {
        "id": 0,
        "type": "double",
        "title": "Spring coefficient",
        "unit": "",
        "value": "1200000"
      },
      {
        "id": 1,
        "type": "double",
        "title": "Damper coefficient",
        "unit": "",
        "value": "12000"
      }
    ],
    "inputs": [
      {
        "id": 0,
        "type": "VectorDouble",
        "title": "InertiaForce",
        "unit": "N",
        "cardinality": 1
      }
    ],
    "outputs": [
      {
        "id": 0,
        "type": "VectorDouble",
        "title": "VerticalForce",
        "unit": "N"
      }
    ],
    "position": {
      "x": 679.1953125,
      "y": 68.734375
    },
    "offsetY": 7.265625,
    "offsetX": 50.8046875,
    "height": 22,
    "width": 269,
    "dropped": false,
    "color": "#1D2D53"
  });
  const systemsWithComponents = [
    {
      "schedule": {
        "cycle": 10,
        "offset": 0,
        "priority": 0,
        "response": 0
      },
      "components": [
        {
          "id": 0,
          "inputs": [
            {
              "cardinality": 1,
              "id": 0,
              "title": "Desired trajectory",
              "type": "TrajectoryEvent",
              "unit": ""
            },
          ],
          "library": "Dynamics_CopyTrajectory",
          "position": {
            "x": 0,
            "y": 0
          },
          "schedule": {
            "cycle": 10,
            "offset": 0,
            "priority": 0,
            "response": 0
          },
          "parameters": [
            {
              "id": 0,
              "title": "Driver aggressiveness",
              "type": "double",
              "unit": "",
              "value": "1"
            },
            {
              "id": 1,
              "title": "Max. engine power",
              "type": "double",
              "unit": "W",
              "value": "100000"
            },
            {
              "id": 2,
              "title": "Boolean Param",
              "type": "bool",
              "unit": "",
              "value": "true"
            },
            {
              "id": 3,
              "title": "boolVector test",
              "type": "boolVector",
              "unit": "",
              "value": "[true,false,false,true,false]"
            },
            {
              "id": 4,
              "title": "intVector test",
              "type": "intVector",
              "unit": "",
              "value": "[2,3,4,-2]"
            },
            {
              "id": 5,
              "title": "intVector test 2",
              "type": "intVector",
              "unit": "",
              "value": "[1000,2000,3000,0]"
            },
            {
              "id": 6,
              "title": "doubleVector test",
              "type": "doubleVector",
              "unit": "",
              "value": "[1.0,2.0,3.0,0.5]"
            },
            {
              "id": 7,
              "title": "stringVector test",
              "type": "stringVector",
              "unit": "",
              "value": "[string 1,string 2,string 3,string 4]"
            },
            {
              "id": 8,
              "title": "String test",
              "type": "string",
              "unit": "",
              "value": "test"
            },
            {
              "id": 8,
              "title": "double test",
              "type": "double",
              "unit": "",
              "value": "0.123"
            }
          ],
          "title": "Dynamics_CopyTrajectory",
          "type": ComponentUITypeEnum.Action
        },
        {
          "id": 1,
          "library": "OpenScenarioActions",
          "outputs": [
            {
              "id": 0,
              "title": "Trajectory Event",
              "type": "TrajectoryEvent",
              "unit": ""
            },
            {
              "id": 1,
              "title": "Custom Lane Change Event",
              "type": "CustomLaneChangeEvent",
              "unit": ""
            },
            {
              "id": 2,
              "title": "Gaze Follower Event",
              "type": "GazeFollowerEvent",
              "unit": ""
            },
            {
              "id": 3,
              "title": "Speed Action Event",
              "type": "SpeedActionEvent",
              "unit": ""
            }
          ],
          "inputs": [
            {
              "cardinality": 1,
              "id": 0,
              "title": "Desired trajectory",
              "type": "TrajectoryEvent",
              "unit": ""
            }
          ],
          "position": {
            "x": 0,
            "y": 0
          },
          "schedule": {
            "cycle": 0,
            "offset": 0,
            "priority": 0,
            "response": 0
          },
          "title": "OpenScenarioActions",
          "type": ComponentUITypeEnum.Algorithm
        }
      ],
      "connections": [
        {
          "id": 0,
          "source": {
            "component": 1,
            "output": 0
          },
          "targets": [{
            "component": 0,
            "input": 0
          }]
        },
        {
          "id": 1,
          "source": {
            "component": 1,
            "output": 1
          },
          "targets": [{
            "component": 0,
            "input": 0
          }]
        }
      ],
      comments: [
        {
          title: 'Comment',
          message: 'This is a comment',
          position: {
            x: 300,
            y: 300,
          }
        }
      ],
      "id": 0,
      "priority": 0,
      "title": "Follow Trajectory",
      "file": "Follow_Trajectory.xml"
    },
  ];
  const getInitialElements = (index: number): number => systemsWithComponents[index].components.length + (systemsWithComponents[index].connections.length > 0 ? 1 : 0)
  const getInitialArrows = (index: number): number => systemsWithComponents[index].connections.length

  beforeEach(async () => {
    await SelectSystemFirst({ system: systemsWithComponents[0] })
  })

  afterEach(() => {
    fetchMock.resetMocks();
    cleanup();
  });

  // COMMENTS
  test('Should render comments', () => {
    const turnOnCommentsButton = screen.getByTestId('test-id-show-comments-button')
    act(() => {
      fireEvent.click(turnOnCommentsButton);
    });
    const component = screen.getByTestId('test-id-comment-0');
    expect(component).toBeInTheDocument();
  });

  test('Comment displays correct title and description', () => {
    const turnOnCommentsButton = screen.getByTestId('test-id-show-comments-button')
    act(() => {
      fireEvent.click(turnOnCommentsButton);
    });
    const component = screen.getByTestId('test-id-comment-0');
    const comment = systemsWithComponents[0].comments[0]
    expect(component).toBeInTheDocument();
    const titleInput = screen.getByTestId('test-id-comment-0-title').querySelector('input');
    expect(titleInput).toBeInTheDocument();
    expect(titleInput).toHaveProperty('value', comment.title);
    const messageInput = screen.getByTestId('test-id-comment-0-message').querySelector('textarea');
    expect(messageInput).toBeInTheDocument();
    expect(messageInput).toHaveDisplayValue(comment.message);
  });

  test('Should be able to edit comment', () => {
    const commentData = systemsWithComponents[0].comments[0]
    const newCommentData = {
      ...commentData,
      title: 'new title',
      message: 'new message'
    }
    const turnOnCommentsButton = screen.getByTestId('test-id-show-comments-button')
    act(() => {
      fireEvent.click(turnOnCommentsButton);
    });
    const component = screen.getByTestId('test-id-comment-0');
    expect(component).toBeInTheDocument();
    const editButton = screen.getByTestId('test-id-comment-0-edit');
    act(() => {
      fireEvent.click(editButton);
    });
    const titleInput = screen.getByTestId('test-id-comment-0-title').querySelector('input');
    expect(titleInput).not.toBe(null)
    expect(titleInput).toBeInTheDocument();
    expect(titleInput).toHaveProperty('value', commentData.title);
    act(() => {
      if (titleInput)
        fireEvent.change(titleInput, { target: { value: newCommentData.title }});
    });
    expect(titleInput).toHaveProperty('value', newCommentData.title);

    const messageInput = screen.getByTestId('test-id-comment-0-message').querySelector('textarea');
    expect(messageInput).toBeInTheDocument();
    expect(messageInput).toHaveProperty('value', commentData.message);
    act(() => {
      if (messageInput)
        fireEvent.change(messageInput, { target: { value: newCommentData.message }});
    });
    expect(messageInput).toHaveProperty('value', newCommentData.message);
  });

  // COMPONENTS
  test('Should render a component with schedule', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
  });
  test('Should change color of component borders', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const colorButton = screen.getByTestId('test-id-color-button-0');
    expect(colorButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(colorButton);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-dropdown-color');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const turquoise = screen.getByTestId('test-id-menu-item-0');
    expect(turquoise).toBeInTheDocument();
    act(() => {
      fireEvent.click(turquoise);
    });
  });
  test('Should show delete modal and not delete', async () => {
    const openMenuButton = screen.getByTestId('test-id-open-menu-0');
    expect(openMenuButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(openMenuButton);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-draggale-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const deleteButton = screen.getByTestId('test-id-menu-item-delete');
    expect(deleteButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(deleteButton);
    });
    const modal = screen.getByTestId('test-id-modal-Delete Component');
    expect(modal).toBeInTheDocument();
    const cancelButton = await screen.findByText(/Cancel/i);
    expect(cancelButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(cancelButton);
    });
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
  });
  test('Should show delete modal and delete', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const openMenuButton = screen.getByTestId('test-id-open-menu-0');
    expect(openMenuButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(openMenuButton);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-draggale-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const deleteButton = screen.getByTestId('test-id-menu-item-delete');
    expect(deleteButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(deleteButton);
    });
    const modal = screen.getByTestId('test-id-modal-Delete Component');
    expect(modal).toBeInTheDocument();
    const deleteComponentButton = screen.getByTestId('test-id-modalbutton-delete-component');
    expect(deleteComponentButton).toBeInTheDocument();
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    expect(droppableZone.childElementCount).toBe(getInitialElements(0));
    act(() => {
      fireEvent.click(deleteComponentButton);
    });
    // we write -2 because the element had all connections assigned to it, they have also been deleted
    expect(droppableZone.childElementCount).toBe(getInitialElements(0)-2);
  });
  test('Should be able to duplicate component', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const openMenuButton = screen.getByTestId('test-id-open-menu-0');
    expect(openMenuButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(openMenuButton);
    });
    const menuDropdown = screen.getByTestId('test-id-menu-draggale-dropdown');
    expect(menuDropdown).toBeInTheDocument();
    expect(menuDropdown).not.toBeEmptyDOMElement();
    const duplicateButton = screen.getByTestId('test-id-menu-item-duplicate');
    expect(duplicateButton).toBeInTheDocument();
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    expect(droppableZone.childElementCount).toBe(getInitialElements(0));
    await act(() => {
      fireEvent.click(duplicateButton);
    });
    expect(droppableZone.childElementCount).toBe(getInitialElements(0)+1);
  });
  test('Should change the value of the schedule of a component', () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const scheduleElement1 = screen.getByTestId('schedule-input-offset-0').querySelector('input');
    expect(scheduleElement1).toBeInTheDocument();
    act(() => {
      fireEvent.change(scheduleElement1!, { target: { value: '20' } });
    });
    expect(scheduleElement1).toHaveProperty('value', '20');
  });
  test('Should be empty when changing since its not a number', () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const scheduleElement1 = screen.getByTestId('schedule-input-offset-0').querySelector('input');
    expect(scheduleElement1).toBeInTheDocument();
    act(() => {
      fireEvent.input(scheduleElement1!, { target: { value: 'dsadsa' } });
    });
    expect(scheduleElement1).toHaveProperty('value', '');
  });
  test('Should change the value of the int parameter of a component', () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const parameter1 = screen.getByTestId('test-id-parameter-0-Driver aggressiveness').querySelector('input');
    expect(parameter1).toBeInTheDocument();
    act(() => {
      fireEvent.change(parameter1!, { target: { value: '20' } });
    });
    expect(parameter1).toHaveProperty('value', '20');
  });
  test('Should change the value of the string parameter of a component', () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const parameter1 = screen.getByTestId('test-id-parameter-0-String test').querySelector('input');
    expect(parameter1).toBeInTheDocument();
    act(() => {
      fireEvent.change(parameter1!, { target: { value: 'test after change' } });
    });
    expect(parameter1).toHaveProperty('value', 'test after change');
  });
  test('Should change the value of the double parameter of a component', () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const parameter1 = screen.getByTestId('test-id-parameter-0-String test').querySelector('input');
    expect(parameter1).toBeInTheDocument();
    act(() => {
      fireEvent.change(parameter1!, { target: { value: '1000.25' } });
    });
    expect(parameter1).toHaveProperty('value', '1000.25');
  });
  test('Should change the value of the boolean parameter of a component', () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    const parameterBoolean = screen.getByTestId('test-id-parameter-0-Boolean Param').querySelector('input');
    expect(parameterBoolean).toBeInTheDocument();
    act(() => {
      fireEvent.change(parameterBoolean!, { target: { value: 'true' } });
    });
    expect(parameterBoolean).toHaveProperty('value', 'true');
  });
  test('Should open the modal to edit intVector parameter and save changes', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the intVector parameter
    const intVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
    expect(intVectorElem).toBeInTheDocument();

    const editButton = intVectorElem.nextElementSibling; // or previousElementSibling, if applicable
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const intVectorModal = screen.getByRole('presentation');
    expect(intVectorModal).toBeInTheDocument();

    const modalInputs = within(intVectorModal).getAllByRole('textbox');
    expect(modalInputs.length).toBe(4);

    const newValues = ['-5', '3', '8','-1'];

    for (let i = 0; i < modalInputs.length; i++) {
      const inputElement = modalInputs[i] as HTMLInputElement;

      // Clear input before typing
      await userEvent.clear(inputElement);

      await userEvent.type(inputElement, newValues[i], {delay: 1});
      await waitFor(() => {
        inputElement.blur()
        expect(inputElement.value).toBe(newValues[i]);
      });
    }

    const saveButton = within(intVectorModal).getByText('Save');
    expect(saveButton).toBeInTheDocument();

    await waitFor(() => {
      fireEvent.click(saveButton);
    });

    expect(intVectorModal).not.toBeInTheDocument();

    expect(intVectorElem).toBeInTheDocument();
    const inputElem = intVectorElem.querySelector('input');
    expect(inputElem).toBeInTheDocument();
    expect(inputElem).toHaveValue('[-5,3,8,-1]');

  });
  test('Should open the modal to edit doubleVector parameter and save changes', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the boolVector parameter
    const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
    expect(doubleVectorElem).toBeInTheDocument();


    const editButton = doubleVectorElem.nextElementSibling; // or previousElementSibling, if applicable
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const doubleVectorModal = screen.getByRole('presentation');
    expect(doubleVectorModal).toBeInTheDocument();

    const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputs.length).toBe(4);

    const newValues = ['0.1','-2.0','1000.13','0'];

    for (let i = 0; i < modalInputs.length; i++) {
      // Ensure that modalInputs[i] is treated as an HTMLInputElement
      const inputElement = modalInputs[i] as HTMLInputElement;

      // Clear the existing value in the input
      userEvent.clear(inputElement);

      // Type the new value into the input
      userEvent.type(inputElement, newValues[i]);

      await waitFor(() => {
        // Access the 'value' property of the input element for comparison
        expect(inputElement.value).toBe(newValues[i]);
      });
    }

    const saveButton = within(doubleVectorModal).getByText('Save');
    expect(saveButton).toBeInTheDocument();

    act(() => {
      fireEvent.click(saveButton);
    });

    expect(doubleVectorModal).not.toBeInTheDocument();

    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[0.1,-2.0,1000.13,0]');
    });
  });
  test('Should open the modal to edit stringvector parameter and save changes', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the intVector parameter
    const stringVectorElem = screen.getByTestId('test-id-parameter-0-stringVector test');
    expect(stringVectorElem).toBeInTheDocument();

    const editButton = stringVectorElem.nextElementSibling;
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const stringVectorModal = screen.getByRole('presentation');
    expect(stringVectorModal).toBeInTheDocument();

    const modalInputs = within(stringVectorModal).getAllByRole('textbox');
    expect(modalInputs.length).toBe(4);

    const newValues = ["bla", "test val2", "test val 3 longer","123"];

    for (let i = 0; i < modalInputs.length; i++) {
      // Ensure that modalInputs[i] is treated as an HTMLInputElement
      const inputElement = modalInputs[i] as HTMLInputElement;

      // Clear the existing value in the input
      userEvent.clear(inputElement);

      // Type the new value into the input
      userEvent.type(inputElement, newValues[i]);

      await waitFor(() => {
        // Access the 'value' property of the input element for comparison
        expect(inputElement.value).toBe(newValues[i]);
      });
    }
    const inputElement = modalInputs[0] as HTMLInputElement;
    userEvent.clear(inputElement);
    userEvent.type(inputElement, newValues[0]);

    const saveButton = within(stringVectorModal).getByText('Save');
    expect(saveButton).toBeInTheDocument();

    act(() => {
      fireEvent.click(saveButton);
    });

    expect(stringVectorModal).not.toBeInTheDocument();

    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-stringVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[bla,test val2,test val 3 longer,123]');
    });
  });
  test('Should open the modal to edit boolvector parameter and save changes', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the intVector parameter
    const boolVectorElem = screen.getByTestId('test-id-parameter-0-boolVector test');
    expect(boolVectorElem).toBeInTheDocument();

    const editButton = boolVectorElem.nextElementSibling;
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within boolVector');
    }

    const boolVectorModal = screen.getByRole('presentation');
    expect(boolVectorModal).toBeInTheDocument();

    const modalInputs = within(boolVectorModal).getAllByRole('checkbox');
    expect(modalInputs.length).toBe(5);

    const newValues = [true,false,true,true,true];

    for (let i = 0; i < modalInputs.length; i++) {
      const inputElement = modalInputs[i] as HTMLInputElement; // Ensure it's treated as HTMLInputElement

      // If the element is a checkbox, set its checked state based on newValues[i]
      const shouldBeChecked = newValues[i] == true; // Assuming newValues[i] is 'true' or 'false' string
      if (inputElement.checked !== shouldBeChecked) {
        userEvent.click(inputElement); // Toggle the checkbox state
      }

      // Verify the checkbox's state
      await waitFor(() => {
        expect(inputElement.checked).toBe(shouldBeChecked);
      });
    }

    const saveButton = within(boolVectorModal).getByText('Save', { exact: false });

    act(() => {
      fireEvent.click(saveButton);
    });

    //exact false can access not direct children
    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-boolVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[true,false,true,true,true]');
    });
  });
  test('Should add one element to a vector modal', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the boolVector parameter
    const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
    expect(doubleVectorElem).toBeInTheDocument();


    const editButton = doubleVectorElem.nextElementSibling;
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const doubleVectorModal = screen.getByRole('presentation');
    expect(doubleVectorModal).toBeInTheDocument();


    const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputs.length).toBe(4);

    //as first input does not have icon, 2nd element is removed
    const addButton = within(doubleVectorModal).getByText('Add');
    if (addButton instanceof HTMLButtonElement) {
      fireEvent.click(addButton);
    }

    const modalInputsAfter =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputsAfter.length).toBe(5);

    const newValues = ['1.0','2.0','3.0','0.5','0.10'];

    for (let i = 0; i < modalInputsAfter.length; i++) {
      // Ensure that modalInputs[i] is treated as an HTMLInputElement
      const inputElement = modalInputsAfter[i] as HTMLInputElement;

      // Clear the existing value in the input
      userEvent.clear(inputElement);

      // Type the new value into the input
      userEvent.type(inputElement, newValues[i]);

      await waitFor(() => {
        // Access the 'value' property of the input element for comparison
        expect(inputElement.value).toBe(newValues[i]);
      });
    }
    const inputElement = modalInputs[0] as HTMLInputElement;
    userEvent.clear(inputElement);
    userEvent.type(inputElement, newValues[0]);

    const saveButton = within(doubleVectorModal).getByText('Save');
    expect(saveButton).toBeInTheDocument();

    act(() => {
      fireEvent.click(saveButton);
    });

    //exact false can access not direct children
    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[1.0,2.0,3.0,0.5,0.10]');
    });
  });
  test('Should remove one element from a vector modal and save it', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the boolVector parameter
    const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
    expect(doubleVectorElem).toBeInTheDocument();


    const editButton = doubleVectorElem.nextElementSibling; // or previousElementSibling, if applicable
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const doubleVectorModal = screen.getByRole('presentation');
    expect(doubleVectorModal).toBeInTheDocument();


    const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputs.length).toBe(4);

    const deleteIcons = within(doubleVectorModal).getAllByTestId('DeleteOutlinedIcon');

    //as first input does not have icon, 2nd element is removed
    const buttonOfFirstIcon = deleteIcons[0].parentNode;
    if (buttonOfFirstIcon instanceof HTMLButtonElement) {
      fireEvent.click(buttonOfFirstIcon);
    }

    const modalInputsAfter =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputsAfter.length).toBe(3);

    const saveButton = within(doubleVectorModal).getByText('Save');
    expect(saveButton).toBeInTheDocument();

    act(() => {
      fireEvent.click(saveButton);
    });

    //exact false can access not direct children
    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[1.0,3.0,0.5]');
    });
  });
  test('Should remove one element from a vector modal and leave it unchanged when canceling', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the boolVector parameter
    const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
    expect(doubleVectorElem).toBeInTheDocument();


    const editButton = doubleVectorElem.nextElementSibling; // or previousElementSibling, if applicable
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      act(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const doubleVectorModal = screen.getByRole('presentation');
    expect(doubleVectorModal).toBeInTheDocument();


    const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputs.length).toBe(4);

    const deleteIcons = within(doubleVectorModal).getAllByTestId('DeleteOutlinedIcon');

    //as first input does not have icon, 2nd element is removed
    const buttonOfFirstIcon = deleteIcons[0].parentNode;
    if (buttonOfFirstIcon instanceof HTMLButtonElement) {
      fireEvent.click(buttonOfFirstIcon);
    }

    const modalInputsAfter =doubleVectorModal.querySelectorAll('input[type="number"]');
    expect(modalInputsAfter.length).toBe(3);

    const cancelButton = within(doubleVectorModal).getByText('Cancel');
    expect(cancelButton).toBeInTheDocument();

    act(() => {
      fireEvent.click(cancelButton);
    });

    //exact false can access not direct children
    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[1.0,2.0,3.0,0.5]');
    });
  });
  test('Should open the modal to edit intVector parameter, save changes and leave other intVectors unchanged', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();

    // Trigger the edit action for the intVector parameter
    const intVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
    expect(intVectorElem).toBeInTheDocument();

    const editButton = intVectorElem.nextElementSibling;
    expect(editButton).toBeInTheDocument();
    expect(editButton?.tagName).toBe('BUTTON');

    if (editButton) {
      waitFor(() => {
        fireEvent.click(editButton);
      });
    } else {
      throw new Error('Edit button not found within intVector');
    }

    const intVectorModal = screen.getByRole('presentation');
    expect(intVectorModal).toBeInTheDocument();

    const modalInputs = within(intVectorModal).getAllByRole('textbox');
    expect(modalInputs.length).toBe(4);

    const newValues = ['-5', '3', '8','-1'];

    for (let i = 0; i < modalInputs.length; i++) {
      // Ensure that modalInputs[i] is treated as an HTMLInputElement
      const inputElement = modalInputs[i] as HTMLInputElement;

      // Clear the existing value in the input
      await userEvent.clear(inputElement);

      // Type the new value into the input
      await userEvent.type(inputElement, newValues[i]);

      await waitFor(() => {
        // Access the 'value' property of the input element for comparison
        expect(inputElement.value).toBe(newValues[i]);
      });
    }

    const saveButton = within(intVectorModal).getByText('Save');
    expect(saveButton).toBeInTheDocument();

    act(() => {
      fireEvent.click(saveButton);
    });

    expect(intVectorModal).not.toBeInTheDocument();

    await waitFor(() => {
      const updatedVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
      expect(updatedVectorElem).toBeInTheDocument();
      const inputElem = updatedVectorElem.querySelector('input');
      expect(inputElem).toBeInTheDocument();
      const inputValue = inputElem?.value;
      expect(inputValue).toBe('[-5,3,8,-1]');

      const vectorElementStatic = screen.getByTestId('test-id-parameter-0-intVector test 2');
      expect(vectorElementStatic).toBeInTheDocument();
      const inputElemStatic = vectorElementStatic.querySelector('input');
      expect(inputElemStatic).toBeInTheDocument();
      const inputValueStatic = inputElemStatic?.value;
      expect(inputValueStatic).toBe('[1000,2000,3000,0]');
    });
  });
  test('Should delete a connection with modal', async () => {
    const component = screen.getByTestId('test-id-draggable-0');
    expect(component).toBeInTheDocument();
    await new Promise((r) => setTimeout(r, 2000));
    const connection = screen.getByTestId('test-id-connection-0');
    expect(connection).toBeInTheDocument();
    act(() => {
      if (connection) fireEvent.click(connection);
    });
    const modal = screen.getByTestId('test-id-modal-Delete Connection');
    expect(modal).toBeInTheDocument();
    const deleteConnectionButton = screen.getByTestId('test-id-delete-connection-button');
    expect(deleteConnectionButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(deleteConnectionButton);
    });
    await waitFor(() => {
      const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
      expect(droppableZone.childElementCount).toBe(3);
    });
  });
  test('Should cancel deletion of connection with modal', async () => {
    const connection = screen.getByTestId('test-id-connection-0');
    expect(connection).toBeInTheDocument();
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    act(() => {
      fireEvent.click(connection);
    });
    const modal = screen.getByTestId('test-id-modal-Delete Connection');
    expect(modal).toBeInTheDocument();
    const cancelButton = await screen.findByText(/Cancel/i);
    expect(cancelButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(cancelButton);
    });
    expect(droppableZone.childElementCount).toBe(getInitialElements(0));
  });
  test('Shoud start a connection', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
    expect(outputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(outputButton);
    });
    expect(outputButton).toHaveClass('selectedConnection')
  });
  test('Should remove the connection start if click two times on it', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
    expect(outputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(outputButton);
    });
    expect(outputButton).toHaveClass('selectedConnection');
    act(() => {
      fireEvent.click(outputButton);
    });
    expect(outputButton).not.toHaveClass('selectedConnection');
  });
  test('Should show modal of connection already started if click on two different outputs without finishing a connection', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
    const outputButtonTwo = screen.getByTestId('test-id-output-button-1-Custom Lane Change Event');
    act(() => {
      fireEvent.click(outputButton);
    });
    expect(outputButton).toHaveClass('selectedConnection');
    act(() => {
      fireEvent.click(outputButtonTwo);
    });
    expect(outputButtonTwo).not.toHaveClass('outputButtonTwo');
    const modal = screen.getByTestId('test-id-modal-Connection Already Started');
    expect(modal).toBeInTheDocument();
  });

  test('Should show modal that says cannot start connection from an input', () => {
    const inputButton = screen.getByTestId('test-id-input-button-1-Desired trajectory');
    expect(inputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(inputButton);
    });
    const modal = screen.getByTestId('test-id-modal-Need to select an output first to start a connection');
    expect(modal).toBeInTheDocument();
  });
  test('Should show modal that says cannot create a connection with the same component', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
    expect(outputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(outputButton);
    });
    const inputButton = screen.getByTestId('test-id-input-button-1-Desired trajectory');
    expect(inputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(inputButton);
    });
    const modal = screen.getByTestId('test-id-modal-Cannot create a connection with the same component.');
    expect(modal).toBeInTheDocument();
  });
  test('Should show modal that says that there is already a connection on the same output and input', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
    expect(outputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(outputButton);
    });
    const inputButton = screen.getByTestId('test-id-input-button-0-Desired trajectory');
    expect(inputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(inputButton);
    });
    const modal = screen.getByTestId('test-id-modal-Theres already a connection between these 2 components');
    expect(modal).toBeInTheDocument();
  });
  test('Should create a new connection', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Gaze Follower Event');
    expect(outputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(outputButton);
    });
    const inputButton = screen.getByTestId('test-id-input-button-0-Desired trajectory');
    expect(inputButton).toBeInTheDocument();
    let arrowsContainer = screen.getByTestId('test-id-arrows-container');
    expect(arrowsContainer.childElementCount).toBe(getInitialArrows(0));
    act(() => {
      fireEvent.click(inputButton);
    });
    arrowsContainer = screen.getByTestId('test-id-arrows-container');
    expect(arrowsContainer.childElementCount).toBe(getInitialArrows(0)+1);
  });
  test('Should display an alert if connection is not valid', () => {
    const outputButton = screen.getByTestId('test-id-output-button-1-Custom Lane Change Event');
    expect(outputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(outputButton);
    });
    const inputButton = screen.getByTestId('test-id-input-button-0-Desired trajectory');
    expect(inputButton).toBeInTheDocument();
    act(() => {
      fireEvent.click(inputButton);
    });
    const invalidIcon = screen.getByTestId('1-invalid-icon');
    expect(invalidIcon).toBeInTheDocument();
  });


  test('Should move an element inside the droppable zone', () => {
    const testingElement = screen.getByTestId('test-id-draggable-ActionLongitudinalDriver')
    const mockDataTransfer = {
      clearData: jest.fn(),
      setData: jest.fn(),
    };
    act(() => {
      fireEvent.dragStart(testingElement, { dataTransfer: mockDataTransfer });
    });
    const testingElementDraggable = screen.getByTestId('test-id-draggable-0')
    act(() => {
      fireEvent.dragStart(testingElementDraggable, { dataTransfer: mockDataTransfer });
    });
    const dataToBeReturnedByGetData = JSON.stringify({
      "id": 27,
      "type": "Algorithm",
      "library": "OpenScenarioActions",
      "title": "OpenScenarioActions",
      "schedule": {
        "offset": 0,
        "cycle": 0,
        "response": 0,
        "priority": 0
      },
      "outputs": [
        {
          "id": 0,
          "type": "TrajectoryEvent",
          "title": "Trajectory Event",
          "unit": ""
        },
        {
          "id": 1,
          "type": "CustomLaneChangeEvent",
          "title": "Custom Lane Change Event",
          "unit": ""
        },
        {
          "id": 2,
          "type": "GazeFollowerEvent",
          "title": "Gaze Follower Event",
          "unit": ""
        },
        {
          "id": 3,
          "type": "SpeedActionEvent",
          "title": "Speed Action Event",
          "unit": ""
        }
      ],
      "position": {
        "x": 19,
        "y": 31
      },
      "dropped": true,
      "color": "#1D2D53",
      "index": 1,
      "offsetY": 28,
      "offsetX": 134,
      "height": 289,
      "width": 318,
      "droppped": true
    });
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    const mockDataTransferDrop = {
      getData: jest.fn().mockReturnValue(dataToBeReturnedByGetData),
      clearData: jest.fn(),
      setData: jest.fn(),
    };
    act(() => {
      fireEvent.drop(droppableZone, { dataTransfer: mockDataTransferDrop, });
    });
    const element = screen.getByTestId('test-id-draggable-1');
    expect(element.style.left).toBe("950px");
    expect(element.style.top).toBe("280px");
  });
  test('Should create a new element when dropped', async () => {
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    const mockDataTransferDrop = {
      getData: jest.fn().mockReturnValue(newDataDropped),
      clearData: jest.fn(),
      setData: jest.fn(),
    };
    expect(droppableZone.childElementCount).toBe(getInitialElements(0));
    await act(() => {
      fireEvent.drop(droppableZone, { dataTransfer: mockDataTransferDrop, });
    });
    expect(droppableZone.childElementCount).toBe(getInitialElements(0)+1);
  });
  test('Should show modal for dropping elements without selecting a system', async() => {
    const system = screen.getByTestId('test-id-system-text-0');
    act(() => {
      fireEvent.click(system);
    });
    const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
    const mockDataTransferDrop = {
      getData: jest.fn().mockReturnValue(newDataDropped),
      clearData: jest.fn(),
      setData: jest.fn(),
    };
    await act(() => {
      fireEvent.drop(droppableZone, { dataTransfer: mockDataTransferDrop, });
    });
    const modal = screen.getByTestId('test-id-modal-Select A System');
    expect(modal).toBeInTheDocument();
  });
});
