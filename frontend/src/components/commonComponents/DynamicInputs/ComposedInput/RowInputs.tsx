import {useTheme} from "@mui/material";
import {Box} from "@mui/system";
import {FC, ReactNode} from "react";

interface RowInputsProps {
    children: ReactNode
}

const RowInputs: FC<RowInputsProps> = (props)=> {
    const theme = useTheme()

    return (
        <Box display='flex' flexDirection={'row'} width={'100%'} marginBottom={theme.spacing(2)} {...props}>
            {props.children}
        </Box>
    )
}

export default RowInputs