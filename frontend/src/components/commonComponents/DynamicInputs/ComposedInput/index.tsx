/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, useTheme} from "@mui/material";
import {StyledBox} from "./index.styles.tsx";
import {FC, ReactNode, useState} from "react";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

interface DynamicInputComposedProps {
    label?: ReactNode;
    children?: ReactNode;
}

const ComposedInput: FC<DynamicInputComposedProps> = (props)=> {
    const theme = useTheme()
    const { label, children, ...rest } = props

    const [isClosed, setClosed] = useState(false)

    return (
        <Box {...rest} display={'flex'} flexDirection={'column'} width={'100%'} marginBottom={theme.spacing(2)}>
            {label && <StyledLabel isImportant sx={{ cursor: 'pointer' }} onClick={()=> setClosed(!isClosed)}>
                <Box marginRight={theme.spacing(1)} lineHeight={0}>{isClosed ? <KeyboardArrowRightIcon /> : <KeyboardArrowDownIcon />}</Box> {label}
            </StyledLabel>}
            {!isClosed && (
                <StyledBox display={'flex'} flexWrap={'wrap'} width={'100%'} paddingLeft={theme.spacing(6)} isItem={!label}>
                    {children}
                </StyledBox>
            )}
        </Box>
    )
}

export default ComposedInput