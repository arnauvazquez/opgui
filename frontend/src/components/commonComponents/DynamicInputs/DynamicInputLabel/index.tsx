import {Box, useTheme} from "@mui/material";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import {WarningRounded} from "@mui/icons-material";
import {StyledLabel} from "./index.styles.tsx";
import {FC, ReactNode} from "react";
import {LightTooltip} from "../../../../styles/general.styles.tsx";

export interface IDynamicInputLabelProps {
    error?: string;
    isClosed?: boolean;
    isImportant: boolean;
    onClick: React.MouseEventHandler<HTMLLabelElement> | undefined;
    children: ReactNode
}

const DynamicInputLabel: FC<IDynamicInputLabelProps> = (props)=> {
    const theme = useTheme()
    const { isClosed, error, children, ...rest } = props

    return (
        <StyledLabel sx={{ fontWeight: 'bold', cursor: 'pointer', justifyContent: 'space-between' }} {...rest}>
            <Box flexDirection={'row'} display={'flex'}>
                <Box marginRight={theme.spacing(1)} lineHeight={0}>
                    {isClosed ? <KeyboardArrowRightIcon /> : <KeyboardArrowDownIcon />}
                </Box>
                {children}
            </Box>
            <Box alignSelf={'flex-end'}>
                {error && (
                    <LightTooltip title={error}>
                        <WarningRounded color={'warning'} fontSize={'large'} sx={{ margin: theme.spacing(-2,0)}}/>
                    </LightTooltip>
                )}
            </Box>
        </StyledLabel>
    )
}

export default DynamicInputLabel