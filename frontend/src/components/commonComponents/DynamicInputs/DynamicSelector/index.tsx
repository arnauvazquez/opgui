
import {Box, CircularProgress, MenuItem, SelectChangeEvent} from "@mui/material";
import {DYNAMIC_SELECTORS, IDynamicInputProps} from "../interfaces.ts";
import { useListData } from "./SelectorContext.tsx";
import { StyledSelect } from "../HourPicker/index.styles.tsx";
import { useEffect } from "react";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";

interface DynamicSelectorProps extends IDynamicInputProps{
    listType?: DYNAMIC_SELECTORS
    constantValues?: any[]
}

const DynamicSelector = (props: DynamicSelectorProps)=> {
    const {
        inputProps= {},
        name,
        value,
        label,
        onChange,
        constantValues,
        listType,
        isImportant= false,
        ...other
    } = props
    const { multiple, ...rest } = inputProps

    const dataFetched = useListData(listType || '')
    const data = dataFetched.length > 0 ? dataFetched : constantValues

    const currentValue = value || (multiple ? (data ? [data[0]] : []) : (data ? data[0] : ''))
    const handleChange = (event: SelectChangeEvent<unknown>) => {
        const selectedValue = (event.target as HTMLInputElement).value;
        onChange && onChange(name, selectedValue)
    }

    useEffect(() => {
        if (data && data?.length > 0 && !value){
            if (multiple){
                onChange && onChange(name, [data[0]])
            }
            else{
                onChange && onChange(name, data[0])
            }
        }
    }, [data, onChange, value, name, multiple]);

    return (
        <Box
            display={'flex'} flexDirection={'row'}
            justifyContent={'space-between'} alignItems={'center'}
            width={'100%'}
            {...other}
        >
            <StyledLabel isImportant={isImportant}>{label}</StyledLabel>
            {data && data.length > 0 ? (
                <StyledSelect {...rest} value={currentValue} onChange={handleChange} multiple={multiple}>
                    {data?.map((sel:any) => (
                        <MenuItem value={sel}>
                            {sel}
                        </MenuItem>
                    ))}
                </StyledSelect>
            ): (
                <CircularProgress size={16}/>
            )}
        </Box>
    )
}

export default DynamicSelector