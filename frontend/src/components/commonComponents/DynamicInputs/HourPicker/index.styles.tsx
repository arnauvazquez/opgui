/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {Select} from "@mui/material";

export const StyledSelect = styled(Select)(({ theme }) => ({
    maxWidth: '200px',
    height: 25,
    marginLeft: theme.spacing(4)
}));