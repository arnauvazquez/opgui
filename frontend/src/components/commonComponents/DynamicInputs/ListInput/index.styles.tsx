/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {Box} from "@mui/material";

export const StyledItem = styled(Box)<{ alignTop?: boolean}>(({ theme, alignTop }) => ({
    display: 'flex',
    alignItems: alignTop ? 'flex-start' : 'center',
    marginBottom: alignTop ? theme.spacing(1) : 0,
    padding: theme.spacing(1, 2)
}));

export const StyledBox = styled(Box)<{ hasContent?: boolean }>(({ theme, hasContent }) => ({
    display: 'flex',
    border: hasContent ? `1px solid ${theme.palette.grey[300]}`: '',
    borderLeft: `1px solid ${theme.palette.grey[300]}`,
    marginLeft: theme.spacing(6),
    width: `calc(100% - ${theme.spacing(6)})`
}));