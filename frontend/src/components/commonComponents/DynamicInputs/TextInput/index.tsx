
import { Box } from "@mui/material";
import {StyledTextField} from "./index.styles.tsx";
import {ChangeEvent, useEffect, useState} from "react";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";
import getTestIdFromName from "../../../../utils/tests/getTestIdFromName.ts";
import {IDynamicInputProps} from "../interfaces.ts";

const TextInput = (props: IDynamicInputProps)=> {
    const { inputProps, name, value, label, onChange, ...other } = props

    const [val, setVal] = useState<string>(value || '')
    const testId = getTestIdFromName(name)

    useEffect(()=> {
        setVal(value)
    }, [value])

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value: newValue } = event.target;
        setVal(newValue)
        onChange && onChange(name, newValue)
    };

    return (
        <Box data-testid={testId} display={'flex'} flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'} {...other}>
            {label && <StyledLabel>{label}</StyledLabel>}
            <StyledTextField data-testid={`${testId}-input`} {...inputProps} size={'small'} value={val} onChange={handleChange} />
        </Box>
    )
}

export default TextInput