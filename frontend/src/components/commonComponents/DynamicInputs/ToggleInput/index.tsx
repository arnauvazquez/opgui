
import {Box} from "@mui/material";
import { StyledToggleInput } from "./index.styles.tsx";
import {ChangeEvent, useEffect, useState} from "react";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";
import getTestIdFromName from "../../../../utils/tests/getTestIdFromName.ts";
import {IDynamicInputProps} from "../interfaces.ts";

const ToggleInput = (props: IDynamicInputProps)=> {
    const { inputProps, name, value, label, onChange, ...other } = props

    const [val, setVal] = useState<boolean>(value || false)
    const testId = getTestIdFromName(name)


    useEffect(()=> {
        setVal(value)
    }, [value])

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { checked } = event.target;
        setVal(checked)
        onChange && onChange(name, checked)
    };

    return (
        <Box data-testid={testId} display={'flex'} flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'} {...other}>
            <StyledLabel>{label}</StyledLabel>
            <StyledToggleInput {...inputProps} data-testid={`${testId}-input`} size={'medium'} checked={val} onChange={handleChange} />
        </Box>
    )
}

export default ToggleInput