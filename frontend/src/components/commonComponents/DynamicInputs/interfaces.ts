/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

export interface IValidationResult {
    error: boolean;
    title?: string;
    message?: string;
}

export interface IOnChangeValidation {
    (name: string, value: any): IValidationResult
}

export interface IDynamicInputProps {
    label?: string;
    name: string
    onChange(inputName: string, value: any): void
    error?: string
    value: any;
    validations?: {
        onChangeValidations?: IOnChangeValidation[]
    }
    inputProps?: any;
    width?: string | number;
    isImportant?: boolean;
}

export enum DYNAMIC_SELECTORS {
    WEATHERS = 'weathers',
    TRAFFIC_RULES = 'trafficRules',
    DATA_BUFFER_LIBRARIES = 'dataBufferLibraries',
    STOCHASTIC_LIBRARIES = 'stochasticsLibraries',
    WORLD_LIBRARIES = 'worldLibraries',
    DEFAULT_LOGGING_GROUPS = 'defaultLoggingGroups',
    SPAWNER_LIBRARIES = 'spawnerLibraries',
    SPAWNER_TYPES = 'spawnerTypes',
}