/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { styled } from '@mui/material/styles';
import {TextField, Card, alpha} from '@mui/material';

export const StyledCard = styled(Card)(({ theme }) => ({
    padding: theme.spacing(2),
    paddingTop: theme.spacing(0),
}))

export const TopIcons = styled('div')(({ theme }) => ({
    borderBottom: '1px solid',
    borderColor: theme.palette.primary.light,
    marginBottom: theme.spacing(4),
    display: 'flex',
    justifyContent: 'flex-end',
}))

export const EditOptions = styled('div')(() => ({
    display: 'flex',
    justifyContent: 'flex-end',
}))

export const StyledTextField = styled(TextField)(({ theme, disabled }) => ({
    width: '100%',
    border: 'none',
    marginBottom: theme.spacing(2),
    '& .MuiInputBase-root': {
        backgroundColor: disabled ? 'transparent !important' :
            (theme.palette.primary?.accent1Soft ? alpha(theme.palette.primary.accent1Soft, 0.2) : ''),
        borderRadius: theme.spacing(2),
        padding: theme.spacing(1, 3),
        border: 'none',
        '&:after': {
            borderBottom: 'none',
            borderBottomStyle: 'none',
        },
        '&:hover:after': {
            borderBottom: 'none',
            borderBottomStyle: 'none',
        },
        '&:hover:before': {
            borderBottom: 'none !important',
            borderBottomStyle: 'none',
        },
        '&:before': {
            borderBottom: 'none',
            borderBottomStyle: 'none',
        }
    },
    '& input': {
        padding:theme.spacing(0),
        fontWeight: 'bold',
        width: '100%',
    },
    '& textarea': {
        padding: theme.spacing(2,0)
    },
}))