/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { useEffect, useState, DragEvent } from 'react';
import {Button, ComponentsProps, IconButton, useTheme} from "@mui/material";
import {EditOptions, StyledCard, StyledTextField, TopIcons} from "./SystemComment.styles.ts";
import { Edit, Delete } from '@mui/icons-material';

interface OnSaveReturn {
    title: string,
    message: string,
}

interface Props extends ComponentsProps {
    position: {
        x: number,
        y: number,
    },
    title: string,
    message: string,
    editing?: boolean,
    draggable?: boolean,
    ['data-testid']?: string,
    onDelete?(): void,
    onSave(value: OnSaveReturn): void,
    onDragStart: (e: DragEvent<HTMLDivElement>) => void
}

const SystemComment = (props: Props)=> {
    const theme = useTheme();
    const {
        position,
        message: messageProps,
        title: titleProps,
        editing: editingProps,
        onDelete,
        onSave,
        ...rest
    } = props;
    const [message, setMessage] = useState(messageProps)
    const [title, setTitle] = useState(titleProps)
    const [editing, setEditing] = useState(editingProps)

    useEffect(() => {
        setMessage(messageProps)
    }, [messageProps]);

    useEffect(() => {
        setTitle(titleProps)
    }, [titleProps]);

    const handleCancelEdit = () => {
        setEditing(false)
        setMessage(messageProps)
    }

    const handleSaveEdit = ()=> {
        onSave({
            message,
            title,
        })
        setEditing(false)
    }

    return <StyledCard
        draggable
        sx={{
            position: 'absolute',
            top: position.y,
            left: position.x,
            background: theme.palette.common.white,
            width: theme.spacing(60),
            borderRadius: theme.spacing(1),
            zIndex: 4,
        }}
        {...rest}
    >
        <TopIcons>
            <IconButton
                aria-label="edit"
                onClick={()=> setEditing(true)}
                data-testid={`${rest['data-testid']}-edit`}>
                <Edit />
            </IconButton>
            {onDelete && <IconButton aria-label="edit" onClick={onDelete}>
                <Delete/>
            </IconButton>}
        </TopIcons>
        <StyledTextField
            size="small"
            name={'title'}
            variant={'filled'}
            value={title}
            onChange={(e => setTitle(e.target.value))}
            disabled={!editing}
            data-testid={`${rest['data-testid']}-title`}
        />
        <StyledTextField
            name={'description'}
            minRows={3}
            value={message}
            variant={'filled'}
            multiline
            onChange={(e => setMessage(e.target.value))}
            disabled={!editing}
            placeholder={'Write a description here'}
            data-testid={`${rest['data-testid']}-message`}
        />
        {editing && (
            <EditOptions>
                <Button
                    variant={'outlined'}
                    onClick={handleCancelEdit}
                    data-testid={`${rest['data-testid']}-cancel`}
                >
                    Cancel
                </Button>
                <Button
                    variant={'contained'}
                    style={{ marginLeft: 8 }}
                    onClick={handleSaveEdit}
                    data-testid={`${rest['data-testid']}-save`}
                >
                    Save
                </Button>
            </EditOptions>
        )}
    </StyledCard>
}

export default SystemComment