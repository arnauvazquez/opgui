/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {FooterComponent, SideWindowBase} from "../../../styles/general.styles";
import {
    GlobalSetupBox,
    GlobalSetupBoxTypography, GlobalSetupCategory,
    GlobalSetupConfigBox,
    GlobalSetupTypography
} from "../../../styles/globalSetup.styles";
import {Button} from "@mui/material";
import UploadFromFileTree from "../../commonComponents/UploadInput/UploadFromFileTree.tsx";
import ModalComponent from "../../Modal/ModalComponent.tsx";
import {INPUT_FILE} from "../../../index.d";
import {useEffect, useMemo, useState} from "react";
import {getConfigElements, saveConfigElements} from "../../../utils/api.util.ts";
import {ConfigElement} from "opGUI-client-frontend";
import {useGlobalContext} from "../../../context/ContextProvider.tsx";
import {CATEGORY_ID, getCategoriesFromVariables} from "./GlobalSetupUtils.tsx";

const GlobalSetup = () => {
    const { modal, setModal } = useGlobalContext();
    const [configs, setConfigs] = useState<ConfigElement[]>([])
    const categories = useMemo(()=> getCategoriesFromVariables(configs, CATEGORY_ID.GENERAL), [configs])

    const loadSetupConfigs = async () => {
        const data = await getConfigElements()
        if (!('error' in data)){
            setConfigs(data)
        }
    }

    const handleChangeConfig = (id: string, newValue: string) => {
        const newConfig = configs.map(conf => ({
            ...conf,
            value: conf.id === id ? newValue : conf.value
        }))
        setConfigs(newConfig)
    }

    const handleSaveConfig = () => async () => {
        try{
            await saveConfigElements(configs)
            setModal({
                ...modal,
                active: true,
                title: 'Variables Saved Successfully',
                description: 'All system variables have been saved successfully'
            })
        }
        catch (error){
            setModal({
                ...modal,
                active: true,
                title: 'Something Went wrong',
                description: `Your changes were rejected by the server. Error: ${error}`
            })
        }
    }

    useEffect(() => {
        loadSetupConfigs()
    }, []);

    return (
    <SideWindowBase>
        <ModalComponent />
        <GlobalSetupConfigBox data-testid="test-id-box-system-editor-section">
            {categories.map(cat => (
                <GlobalSetupCategory key={`${cat.title}`}>
                    <GlobalSetupTypography variant="h5">
                        {cat.title}
                    </GlobalSetupTypography>
                    {cat.variables.map((setup) => (
                        <GlobalSetupBox key={`${setup.id}`}>
                            <GlobalSetupBoxTypography>
                                {setup.title}
                            </GlobalSetupBoxTypography>
                            <UploadFromFileTree
                                basePath = "home"
                                route={setup.value || ''}
                                styles={{ flex: .8 }}
                                error={false} name="pathToComponents"
                                handleFiles={(_name, path) => handleChangeConfig(setup.id || '', path)}
                                type={setup.type === 'folder' ?  INPUT_FILE.dir : INPUT_FILE.file}
                            />
                        </GlobalSetupBox>
                    ))}
                </GlobalSetupCategory>
            ))}
        </GlobalSetupConfigBox>
        <FooterComponent sx={{ justifyContent: 'flex-end' }} data-testid="test-id-footer-setup">
        <Button
            sx={{ marginRight: 6 }}
            color='primary'
            data-testid="test-id-button-saveChanges"
            variant='contained'
            name='saveChanges'
            onClick={handleSaveConfig()}
        >
            Save Changes
        </Button>
        </FooterComponent>
        </SideWindowBase>
    )
}

export default GlobalSetup;