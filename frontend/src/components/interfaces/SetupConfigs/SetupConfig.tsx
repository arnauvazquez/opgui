/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, Typography, useTheme} from "@mui/material";
import {Route, Routes, useLocation, useNavigate} from 'react-router-dom';
import {
  LightTooltip,
  SideWindowBase,
  StickyDiv,
  StyledTab
} from "../../../styles/general.styles";
import ModalComponent from '../../Modal/ModalComponent';
import BasedOnPCM from "./setups/BasedOnPCM";
import {PATHS} from "../../../index.d";
import NewConfig from "./setups/NewConfig";

enum TAB {
    PCM = '',
    END_TO_END = 'END_TO_END',
    NEW = 'NEW'
}

const getLocation = (path: string)=> {
    switch (path){
        case PATHS.SETUP_CONFIGS_PCM:
            return TAB.PCM
        case PATHS.SETUP_CONFIGS_END_TO_END:
            return TAB.END_TO_END
        default:
            return TAB.NEW
    }
}

const SetupConfig = () => {
    const theme = useTheme()
    const location = useLocation()
    const activeTab = getLocation(location.pathname)
    const navigate = useNavigate()

    return (
    <SideWindowBase sx={{ height: '100%', width: '100%' }} className="setupConfig" data-testid="test-id-interface-setupconfigs" >
      <ModalComponent />
      <StickyDiv sx={{ position: 'relative', justifyContent: 'unset', backgroundColor: 'transparent', height: theme.spacing(11) }}>
        <StyledTab className={activeTab === TAB.NEW ? "selected" : ''} id="new-configs" onClick={()=>navigate(PATHS.SETUP_CONFIGS_NEW)}>
          <Typography sx={{ fontWeight: 500 }}>
              New configs
          </Typography>
        </StyledTab>
        <StyledTab className={activeTab === TAB.PCM ? "selected" : ''} id="pcm" onClick={()=>navigate(PATHS.SETUP_CONFIGS_PCM)}>
          <Typography sx={{ fontWeight: 500 }}>
            Based on pcm-database
          </Typography>
        </StyledTab>
        <LightTooltip title={"Feature not yet available"}>
          <Box style={{ height: '100%' }}>
            <StyledTab sx={{ borderLeft: 'unset' }} disabled>
              <Typography className={activeTab === TAB.END_TO_END ? "selected" : ''} sx={{ fontWeight: 500 }}>
                From end-to-end
              </Typography>
            </StyledTab>
          </Box>
        </LightTooltip>
      </StickyDiv>
      <Routes>
          <Route path={'new-config'} element={<NewConfig/>}/>
          <Route path='*' element={<BasedOnPCM />} />
      </Routes>
    </SideWindowBase>
  )
}

export default SetupConfig;