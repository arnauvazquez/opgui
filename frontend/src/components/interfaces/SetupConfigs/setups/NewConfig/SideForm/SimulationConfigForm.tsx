import ListInput from "../../../../../commonComponents/DynamicInputs/ListInput";
import {useCallback, useEffect, useState} from "react";
import ListItem from "../../../../../commonComponents/DynamicInputs/ListInput/ListItem.tsx";
import PercentageInput from "../../../../../commonComponents/DynamicInputs/PercentageInput";
import modifyProperty from "../../../../../../utils/modifyProperty.ts";
import HourPicker from "../../../../../commonComponents/DynamicInputs/HourPicker";
import ComposedInput from "../../../../../commonComponents/DynamicInputs/ComposedInput";
import NumberInput from "../../../../../commonComponents/DynamicInputs/NumberInput";
import {DYNAMIC_SELECTORS} from "../../../../../commonComponents/DynamicInputs/interfaces.ts";
import {CircularProgress, InputAdornment} from "@mui/material";
import IntInput from "../../../../../commonComponents/DynamicInputs/IntInput";
import ToggleInput from "../../../../../commonComponents/DynamicInputs/ToggleInput";
import TextInput from "../../../../../commonComponents/DynamicInputs/TextInput";
import RowInputs from "../../../../../commonComponents/DynamicInputs/ComposedInput/RowInputs.tsx";
import {getValueList} from "../../../../../../utils/api.util.ts";
import DynamicSelector from "../../../../../commonComponents/DynamicInputs/DynamicSelector";

interface IStringProbability {
    value: string;
    probability: number;
}

interface ILogGroup {
    enabled: boolean,
    group: string;
    values: string;
}

interface ISimulationConfigForm {
    profilesCatalog?: string,
    scenario?: {
        openScenarioFile?: string,
    }
    environment: {
        timeOfDays: IStringProbability[],
        visibilityDistances: {
            value: number;
            probability: number;
        }[],
        frictions: {
            value: number;
            probability: number;
        }[],
        weathers: IStringProbability[],
        trafficRules?: string,
        turningRates: {
            incoming: string;
            outgoing: string;
            weight: number;
        }[]
    },
    experiment: {
        experimentID: number,
        numberOfInvocations: number,
        randomSeed: number,
        libraries: {
            stochasticsLibrary?: string,
            dataBufferLibrary?: string,
            worldLibrary?: string,
        }
    },
    observations: {
        log: {
            loggingCyclesToCsv: boolean,
            fileName: string,
            loggingGroups: ILogGroup[]
        },
        entityRepository: {
            fileNamePrefix?: string,
            writePersistentEntities?: string,
        }

    },
    spawners: {
        library: string;
        type: string;
        priority: number;
        profile: string;
    }[]
}

const initialValues: ISimulationConfigForm = {
    scenario: {
        openScenarioFile: 'Scenario',
    },
    environment: {
        timeOfDays: [],
        visibilityDistances: [],
        frictions: [],
        weathers: [],
        turningRates: [],
    },
    profilesCatalog: 'ProfilesCatalog',
    experiment: {
        experimentID: 0,
        numberOfInvocations: 1,
        randomSeed: 0,
        libraries: {
            stochasticsLibrary: ''
        }
    },
    observations: {
        log: {
            loggingCyclesToCsv: false,
            fileName: '',
            loggingGroups: []
        },
        entityRepository: {
            fileNamePrefix: '',
            writePersistentEntities: ''
        }
    },
    spawners: []
}

interface SimulationConfigProps {
    onChange(value: any): void
    values: ISimulationConfigForm
}

const SimulationConfigForm = (props: SimulationConfigProps) => {
    const { onChange, values: valuesProps } = props
    const [values, setValues] = useState(valuesProps || initialValues)
    const [loadingGroups, setLoadingGroups] = useState(true)
    const [constantLoggingGroups, setLoggingGroups] = useState<string[]>([])

    useEffect(() => {
        setValues(valuesProps)
    }, [valuesProps]);

    const handleChange = (name: string, value: any)=> {
        const newValues = modifyProperty(values, name, value)
        setValues(newValues)
        if (onChange) onChange(newValues)
    }

    const fetchLoggingGroups = useCallback(async ()=> {
        const logGroupsData = await getValueList(DYNAMIC_SELECTORS.DEFAULT_LOGGING_GROUPS)
        if (!('error' in logGroupsData)){
            setLoggingGroups(logGroupsData.listValues || [])
            logGroupsData.listValues?.forEach((lg: string)=> {
                const logAlreadyExists = values.observations.log.loggingGroups?.find(((log: ILogGroup) => log.group === lg))
                if (!logAlreadyExists){
                    values.observations?.log?.loggingGroups?.push({
                        group: lg,
                        enabled: false,
                        values: ''
                    })
                }
            })
            setValues({ ...values })
            onChange(values)
            setLoadingGroups(false)
        }
    }, [values, onChange])

    useEffect(() => {
        fetchLoggingGroups()
    }, []);

    return (
        <>
            <ComposedInput label={'Scenario'}>
                <TextInput label='Open Scenario File' name={'scenario.openScenarioFile'} onChange={handleChange} value={values?.scenario?.openScenarioFile}
                   inputProps={{
                       disabled: true,
                       endAdornment: <InputAdornment position="end">.xosc</InputAdornment>
                   }}
                />
            </ComposedInput>
            <ComposedInput label={'Profiles Catalog'}>
                <TextInput label='Profiles Catalog File' name={'profilesCatalog'} onChange={handleChange} value={values.profilesCatalog}
                           inputProps={{
                               disabled: true,
                               endAdornment: <InputAdornment position="end">.xml</InputAdornment>
                           }}
                />
            </ComposedInput>
            <ComposedInput label={'Environment'}>
                <ListInput<IStringProbability> label='Times of Day' value={values.environment.timeOfDays} defaultValue={{ value: '5', probability: 0.1 }} onChange={handleChange} name={'environment.timeOfDays'}>
                    { ({ values: listInputs, remove }) => (
                        listInputs?.map((val, index) => (
                            <ListItem onRemove={remove} index={index} key={`listInputs.${index}`}>
                                <HourPicker label={'Time of Day'} name={`environment.timeOfDays.${index}.value`} onChange={handleChange} value={val.value} />
                                <PercentageInput label={'Probability'} name={`environment.timeOfDays.${index}.probability`} onChange={handleChange} value={val.probability} />
                            </ListItem>
                        ))
                    )}
                </ListInput>
                <ListInput label='Visibility Distances' value={values.environment.visibilityDistances} defaultValue={{ value: 500, probability: 0.1 }} onChange={handleChange} name={'environment.visibilityDistances'}>
                    { ({ values: listInputs, remove }) => (
                        listInputs?.map((val, index) => (
                            <ListItem onRemove={remove} index={index} key={`visibilityDistances.${index}`}>
                                <NumberInput
                                    label={'Distance'}
                                    name={`environment.visibilityDistances.${index}.value`}
                                    onChange={handleChange}
                                    value={val.value}
                                    inputProps={{
                                        endAdornment: <InputAdornment position="end">M</InputAdornment>
                                    }}
                                />
                                <PercentageInput label={'Probability'} name={`environment.visibilityDistances.${index}.probability`} onChange={handleChange} value={val.probability} />
                            </ListItem>
                        ))
                    )}
                </ListInput>
                <ListInput label='Frictions' value={values.environment.frictions} defaultValue={{ value: .5, probability: 0.1 }} onChange={handleChange} name={'environment.frictions'}>
                    { ({ values: listInputs, remove }) => (
                        listInputs?.map((val, index) => (
                            <ListItem onRemove={remove} index={index} key={`frictions.${index}`}>
                                <NumberInput
                                    label={'Friction'}
                                    name={`environment.frictions.${index}.value`}
                                    onChange={handleChange}
                                    value={val.value}
                                />
                                <PercentageInput label={'Probability'} name={`environment.frictions.${index}.probability`} onChange={handleChange} value={val.probability} />
                            </ListItem>
                        ))
                    )}
                </ListInput>
                <ListInput label='Weather' value={values.environment.weathers} defaultValue={{ value: '', probability: 0.1 }} onChange={handleChange} name={'environment.weathers'}>
                    { ({ values: listInputs, remove }) => (
                      listInputs?.map((val, index) => (
                        <ListItem onRemove={remove} index={index} key={`frictions.${index}`}>
                            <DynamicSelector label={'Weather'} name={`environment.weathers.${index}.value`} listType={DYNAMIC_SELECTORS.WEATHERS} onChange={handleChange} value={val?.value} />
                            <PercentageInput label={'Probability'} name={`environment.weathers.${index}.probability`} onChange={handleChange} value={val.probability} />
                        </ListItem>
                      ))
                    )}
                </ListInput>
                <ListInput label='Turning Rates' value={values.environment.turningRates} defaultValue={{ incoming: '', outgoing: '', weight: 30 }} onChange={handleChange} name={'environment.turningRates'}>
                    { ({ values: listInputs, remove }) => (
                      listInputs?.map((val, index) => (
                        <ListItem onRemove={remove} index={index} key={`turningRates.${index}`}>
                            <TextInput
                              label='Incoming'
                              name={`environment.turningRates.${index}.incoming`}
                              value={val?.incoming}
                              onChange={handleChange}
                            />
                            <TextInput
                              label='Outgoing'
                              name={`environment.turningRates.${index}.outgoing`}
                              value={val?.outgoing}
                              onChange={handleChange}
                            />
                            <NumberInput
                              label={'Weight'}
                              name={`environment.turningRates.${index}.weight`}
                              onChange={handleChange}
                              value={val?.weight}
                              inputProps={{
                                  endAdornment: <InputAdornment position="end">kg</InputAdornment>
                              }}
                            />
                        </ListItem>
                      ))
                    )}
                </ListInput>
                <DynamicSelector isImportant label={'Traffic Rules'} name={`environment.trafficRules`} listType={DYNAMIC_SELECTORS.TRAFFIC_RULES} onChange={handleChange} value={values.environment.trafficRules} />
            </ComposedInput>
            <ComposedInput label={'Experiment'}>
                <RowInputs>
                    <IntInput label={'Experiment ID'} name={'experiment.experimentID'} onChange={handleChange} value={values?.experiment?.experimentID || 0} />
                    <IntInput label={'Number of Invocations'} name={'experiment.numberOfInvocations'} onChange={handleChange} value={values?.experiment?.numberOfInvocations} />
                    <IntInput label={'Random Seed'} name={'experiment.randomSeed'} onChange={handleChange} value={values?.experiment?.randomSeed} />
                </RowInputs>
                <ComposedInput label={'Libraries'}>
                    <DynamicSelector label={'World Library'} name={`experiment.libraries.worldLibrary`} listType={DYNAMIC_SELECTORS.WORLD_LIBRARIES} onChange={handleChange} value={values?.experiment?.libraries?.worldLibrary} />
                    <DynamicSelector label={'Stochastic Library'} name={`experiment.libraries.stochasticsLibrary`} listType={DYNAMIC_SELECTORS.STOCHASTIC_LIBRARIES} onChange={handleChange} value={values?.experiment?.libraries?.stochasticsLibrary} />
                    <DynamicSelector label={'Data Buffer Library'} name={`experiment.libraries.dataBufferLibrary`} listType={DYNAMIC_SELECTORS.DATA_BUFFER_LIBRARIES} onChange={handleChange} value={values?.experiment?.libraries?.dataBufferLibrary} />
                </ComposedInput>
            </ComposedInput>
            <ComposedInput label={'Observation Log'}>
                <RowInputs>
                    <ToggleInput
                        label={'Log Cycles To Csv'}
                        name={'observations.log.loggingCyclesToCsv'}
                        onChange={handleChange}
                        value={values.observations.log.loggingCyclesToCsv}
                    />
                    <TextInput
                        label={'Log File Name'}
                        name={'observations.log.fileName'}
                        onChange={handleChange}
                        value={values.observations.log.fileName}
                        inputProps={{
                            endAdornment: <InputAdornment position="end">.log</InputAdornment>
                        }}
                    />
                </RowInputs>
                <ListInput
                    label={'Logging Groups'}
                    value={values?.observations?.log?.loggingGroups}
                    name={'observations.log.loggingGroups'}
                    onChange={handleChange}
                    defaultValue={{ enabled: true, group: '', values: ''}}
                    customAmount={
                        values?.observations?.log?.loggingGroups?.reduce((acc: number, current: ILogGroup) => acc + (current.enabled ? 1 : 0) ,0) || '0'
                    }
                >
                    {( { values: groups, remove }) => (
                        loadingGroups ? (
                            <CircularProgress data-testid={'testid-fetching-groups'} size={16}/>
                        ) :
                        groups.map((log, index: number) => {
                            const isRepeated = groups?.find((l, i) => i < index && log.group===l.group)
                            const isConstant = constantLoggingGroups.includes(log.group) && !isRepeated

                            return (
                                <ListItem index={index} key={`constants-${index}`}
                                          onRemove={!isConstant ? remove : undefined}>
                                    <ToggleInput width={120} label={'Enabled'}
                                                 name={`observations.log.loggingGroups.${index}.enabled`}
                                                 onChange={handleChange} value={log.enabled}/>
                                    <TextInput inputProps={{disabled: isConstant}}
                                               label={'Group'} name={`observations.log.loggingGroups.${index}.group`}
                                               onChange={handleChange} value={log.group}/>
                                    <TextInput label={'Values'} name={`observations.log.loggingGroups.${index}.values`}
                                               onChange={handleChange} value={log.values}/>
                                </ListItem>
                            )
                        })
                    )}
                </ListInput>
            </ComposedInput>
            <ComposedInput label={'Observation Entity Repository'}>
                <TextInput
                    label='File Name Prefix'
                    name={'observations.entityRepository.fileNamePrefix'}
                    value={values.observations?.entityRepository?.fileNamePrefix}
                    onChange={handleChange}
                />
                <TextInput
                    label='Write Persistent Entities'
                    name={'observations.entityRepository.writePersistentEntities'}
                    value={values.observations?.entityRepository?.writePersistentEntities}
                    onChange={handleChange}
                />
            </ComposedInput>
            <ListInput
                label={'Spawner'} name={'spawners'}
                defaultValue={{ library: '', priority: 5, type: '', profile: '' }}
                onChange={handleChange}
                value={values?.spawners}
            >
                {({ values: spawners, remove }) => (
                    spawners?.map((spawn, i) => (
                      <ListItem index={i} onRemove={remove}>
                          <div style={{display: 'flex', flexDirection: 'column', flex: 1}}>
                              <DynamicSelector label={'Library'} name={`spawners.${i}.library`}
                                               listType={DYNAMIC_SELECTORS.SPAWNER_LIBRARIES} onChange={handleChange}
                                               value={spawn.library}/>
                              <IntInput label={'Priority'} name={`spawners.${i}.priority`} onChange={handleChange}
                                        value={spawn.priority}/>
                          </div>
                          <div style={{display: 'flex', flexDirection: 'column', flex: 1}}>
                              <DynamicSelector label={'Type'} name={`spawners.${i}.type`}
                                               listType={DYNAMIC_SELECTORS.SPAWNER_TYPES} onChange={handleChange}
                                               value={spawn.type}/>
                              <TextInput label={'Profile'} name={`spawners.${i}.profile`} onChange={handleChange}
                                         value={spawn.profile}/>
                          </div>
                      </ListItem>
                    ))
                    )}
            </ListInput>
        </>
    )
}

export default SimulationConfigForm