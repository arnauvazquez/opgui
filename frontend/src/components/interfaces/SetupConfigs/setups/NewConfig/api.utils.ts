// This file´s functions will be used once backend is ready
// This is a tempt interface to avoid typescript errors, it will be filled more in the future
// using backend's interface, ask Arnau.
import {ConfigEditorObject} from "opGUI-client-frontend";

export const covertConfigToBackend = (frontendConfig: ConfigEditorObject): ConfigEditorObject => {
    const { simulationConfig } = frontendConfig
    // simulation Config checks
    const timeOfDays = simulationConfig.environment.timeOfDays?.map((timeOfDay) => ({ ...timeOfDay, value: Number(timeOfDay.value) }))
    const {openScenarioFile} = simulationConfig.scenario;
    const scenarioFileName = openScenarioFile.endsWith('.xosc') ? openScenarioFile : `${openScenarioFile}.xosc`
    const profilesCatalogFileName = simulationConfig.profilesCatalog.endsWith('.xml') ? simulationConfig.profilesCatalog : `${simulationConfig.profilesCatalog}.xml`
    return {
        ...frontendConfig,
        simulationConfig: {
            ...simulationConfig,
            environment: {
                ...simulationConfig.environment,
                timeOfDays,
            },
            experiment: {
                ...simulationConfig.experiment,
                experimentID: Number(simulationConfig.experiment.experimentID),
            },
            scenario: {openScenarioFile: scenarioFileName},
            profilesCatalog: profilesCatalogFileName,
        }
    }
}