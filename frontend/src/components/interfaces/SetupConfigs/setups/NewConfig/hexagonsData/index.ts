/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import simulationConfig, {formInitialData as simconfigInitialData} from "./simulationConfig.ts";
import scenarioStructure from "./scenario.ts";

const size = 150
const yExtraSpace = 20
const xExtraSpace = 5
const arrowSize = 20

const getPosition = (x: number, y: number) => {
    const deltaX = size / 2 + x * (size + xExtraSpace + arrowSize)
    const deltaY = size / 2 + (y * (size + yExtraSpace + arrowSize)) + ((x % 2 === 0) ? 0 : size / 1.5)
    return {
        x: deltaX,
        y: deltaY,
    }
}

export interface IHexagonData {
    id: string;
    title: string;
    position: { x: number, y: number },
    disabled?: boolean,
    formInitialData: object,
    formStructure: object
}

export const hexagonsData: IHexagonData[] = [
    {
        id: 'PedestrianModelCatalog',
        title: 'Pedestrian Modal Catalog',
        position: getPosition(0, 0),
        disabled: true,
        formInitialData: simconfigInitialData,
        formStructure: simulationConfig
    },
    {
        id: 'ProfilesCatalog',
        title: 'Profiles Catalog',
        position: getPosition(0, 1),
        disabled: true,
        formInitialData: simconfigInitialData,
        formStructure: simulationConfig
    },
    {
        id: 'Scenario',
        title: 'Scenario',
        position: getPosition(1, 0),
        disabled: true,
        formInitialData: {},
        formStructure: scenarioStructure
    },
    {
        id: 'simulationConfig',
        title: 'Simulation Config',
        position: getPosition(1, 1),
        formInitialData: simconfigInitialData,
        formStructure: simulationConfig
    },
]

export interface IDependency {
    hexagon: string;
    hexagonDependency: string;
}

export const dependencies: IDependency[] = [
    {
        hexagon: 'simConfig',
        hexagonDependency: 'ProfilesCatalog'
    },
    {
        hexagon: 'simConfig',
        hexagonDependency: 'Scenario'
    }
]