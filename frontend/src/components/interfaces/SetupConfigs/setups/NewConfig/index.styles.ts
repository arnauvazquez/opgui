/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { styled } from '@mui/material/styles';
import {ButtonBase} from "@mui/material";

export const HexagonStyled = styled(ButtonBase)<{ disabled?: boolean, selected?: boolean }>(({ theme, disabled, selected }) => ({
    background: selected ? theme.palette.primary.contrastText : `${disabled ? '#444' : '#000'}`,
    width: '150px',
    height: '150px',
    clipPath: `polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)`,
    fontWeight: 'bold',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: 'none',
    padding: '1px'
}));

export const HexagonContent = styled('div')<{ disabled?: boolean, selected?:  boolean}>(({ disabled, selected, theme }) => ({
    background: selected ? theme.palette.primary.main : `${disabled ? '#ddd' : '#fff'}`,
    color: selected ? theme.palette.primary.contrastText : theme.palette.primary.main,
    clipPath: `polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)`,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
}))

export const Positioner = styled('div')(({ position, rotation=0 }: { position: { x: number, y: number }, rotation?: number })=> ({
    position: 'absolute',
    top: position.y,
    left: position.x,
    transform: `translate(-50%,-50%) rotate(${rotation}rad)`,
}))