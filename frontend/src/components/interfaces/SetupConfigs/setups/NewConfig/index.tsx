/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, Button, Typography} from "@mui/material";
import Hexagon from "./Hexagon.tsx";
import {Positioner} from "./index.styles.ts";
import ArrowIcon from "../../../../../assets/icons/arrow.svg";
import {dependencies, hexagonsData, IDependency, IHexagonData} from "./hexagonsData";
import SideForm from "./SideForm";
import {useMemo, useState} from "react";
import {useGlobalContext} from "../../../../../context/ContextProvider.tsx";
import {Variant} from "../../../../../index.d";
import ModalComponent from "../../../../Modal/ModalComponent.tsx";
import {FooterComponent} from "../../../../../styles/general.styles.tsx";
import UploadFromFileTree from "../../../../commonComponents/UploadInput/UploadFromFileTree.tsx";
import {ConfigEditorObject} from "opGUI-client-frontend";
import {getConfigEditor, postConfigEditor} from "../../../../../utils/api.util.ts";
import initialFormData from "./initialFormData.ts";
import {covertConfigToBackend} from "./api.utils.ts";
import {configEditorToFrontend} from "../../../../../utils/apiConverters.ts";

const getDependencyPosition = (dependency: IDependency) => {
    const hexagon = hexagonsData.find(hex => hex.id === dependency.hexagon)
    const dependencyHexagon = hexagonsData.find(hex => hex.id === dependency.hexagonDependency)
    if (!hexagon || !dependencyHexagon){
        return {
            position: {
                x: -100,
                y: -100
            },
            rotation: 0,
        }
    }
    const arrowPosition = {
        x: Math.abs(hexagon.position.x + dependencyHexagon.position.x) / 2,
        y: Math.abs(hexagon.position.y + dependencyHexagon.position.y) / 2
    }
    const arrowRotationDeg = Math.atan(
        (hexagon.position.y - dependencyHexagon.position.y) / (hexagon.position.x - dependencyHexagon.position.x)
    ) - Math.PI / 2

    return {
        position: arrowPosition,
        rotation: arrowRotationDeg
    }
}

const isDifferent = (objectA: object, objectB: object)=> {
    const setupDataString = JSON.stringify(objectA || {})
    const localDataString = JSON.stringify(objectB || {})

    return setupDataString !== localDataString
}

export interface NewConfigProps {
    /**
     * Used for testing*/
    initialFormData?: ConfigEditorObject;
    initialRoute?: string;
}

const NewConfig = ({initialFormData: initialFormDataProps, initialRoute=''}: NewConfigProps)=> {
    const { modal, setModal } = useGlobalContext();
    const [selected, setSelected] = useState(hexagonsData[3])
    const [setupData, setSetupData] = useState<ConfigEditorObject>(initialFormDataProps || initialFormData)
    const [localData, setLocalData] = useState(setupData)
    const [routeConfig, setNewConfigRoute] = useState(initialRoute)
    const unsavedChanges = useMemo(() => isDifferent(localData, setupData || {}), [localData, setupData, selected])

    const handleSubmit = (value: ConfigEditorObject)=> {
        setSetupData(value)
        setLocalData(value)
    }

    const handleLocalUpdate = (value: ConfigEditorObject) => {
        setLocalData(value)
    }

    const handleChangeHexagon = (hexagonData: IHexagonData) => {
        if (unsavedChanges){
            setModal({
                ...modal,
                active: true,
                title: 'Local changes not saved!',
                description: "You haven't saved your local changes. If you leave now, they will be lost. Continue?",
                buttons: [
                    {
                        title: 'Lose Local Changes',
                        action: () => handleConfirmChangeHexagon(hexagonData),
                        variant: Variant.outlined,
                        testId: 'test-id-modalbutton-continue'
                    },
                    {
                        title: 'Save Changes',
                        action: ()=> {
                            handleSubmit(localData)
                            handleConfirmChangeHexagon(hexagonData)
                        },
                    },
                ],
            })
        }
        else{
            handleConfirmChangeHexagon(hexagonData)
        }
    }

    const handleConfirmChangeHexagon = (hexagonData: IHexagonData) => {
        setSelected(hexagonData)
        setLocalData({...(setupData || initialFormData) })
    }
    const handleSubmitConfigCheck = async () => {
        if (unsavedChanges){
            setModal({
                ...modal,
                active: true,
                title: 'Save local changes before submitting?',
                description: 'You have unsaved changes in '+selected?.title+'. Save before submitting xml?',
                buttons: [
                    {
                        title: "Submit without local changes",
                        variant: Variant.outlined,
                        testId: 'test-id-modalbutton-cancel-load'
                    },
                    {
                        title: 'Submit with local changes',
                        action: () => {
                            handleSubmitConfig(localData)
                            handleSubmit(localData)
                        },
                    }
                ]
            })
        }
        else{
            await handleSubmitConfig(setupData)
        }

    }
    const handleSubmitConfig = async (saveData: ConfigEditorObject)=> {
        const configBackend = covertConfigToBackend(saveData);
        const result = await postConfigEditor(routeConfig, configBackend);
        if ('error' in result){
            setModal({
                ...modal,
                active: true,
                title: 'Error saving config',
                description: result.message,
            })
        }
        else{
            setModal({
                ...modal,
                active: true,
                title: 'Config saved successfully',
                description: `Config was successfully saved in path: ${routeConfig}`,
            })
        }
    }

    const handleLoadCheck = async () => {
        setModal({
            ...modal,
            active: true,
            title: 'Load new config files?',
            description: 'This will override your current changes. Are you sure?',
            buttons: [
                  {
                    title: "Don't load",
                    variant: Variant.outlined,
                    testId: 'test-id-modalbutton-cancel-load'
                  },
                {
                    title: 'Lose Local Changes and Load',
                    action: handleLoadConfig,
                }
            ]
        })
    }

    const handleLoadConfig = async () => {
        const result = await getConfigEditor(routeConfig);
        if ('error' in result){
            setModal({
                ...modal,
                active: true,
                title: 'Error loading config',
                description: result.message,
            })
        }
        else{
            setModal({
                ...modal,
                active: true,
                title: 'Config loaded successfully',
                description: `Config was successfully loaded from path: ${routeConfig}`,
            })
            const formattedResult = configEditorToFrontend(result);
            handleSubmit(formattedResult)
        }
    }

    const handleFiles = (_name: string, path: string) => {
        setNewConfigRoute(path)
    }

    return (
        <>
            <ModalComponent />
            <Box width={'100%'} height={'calc(100% - 64px)'} display={'flex'}>
                <Box width={'50%'} position={'relative'}>
                    {hexagonsData.map((h,i)=> (
                        <Positioner position={h.position}>
                            <Hexagon
                                title={`${h.title}${unsavedChanges && selected.id === h.id ? '*' : ''}`}
                                disabled={h.disabled}
                                selected={selected.id === h.id}
                                onClick={()=> handleChangeHexagon(h)}
                                data-testid={`hexagon-${i}`}
                            />
                        </Positioner>
                    ))}
                    {dependencies.map(d => {
                        const { position, rotation } = getDependencyPosition(d)
                        return (
                            <Positioner position={position} rotation={rotation}>
                                <img src={ArrowIcon} style={{ width: 30, height: 30 }}/>
                            </Positioner>
                        )
                    })}
                </Box>
                <SideForm
                    title={`${selected.title} ${unsavedChanges ? '*' : ''}`}
                    formData={localData || {}}
                    onSubmit={handleSubmit}
                    onChange={handleLocalUpdate}
                    key={`${selected.id}`}
                />
            </Box>
            <FooterComponent data-testid="test-id-footer-setup">
                <Box marginLeft={3} flex={1}>
                    <Box sx={{ display: 'inline-flex', alignItems: 'center', gap: 5, padding: 1, paddingRight: 4, width: '90%' }}>
                        <Box>
                            <Typography sx={{ flex: .1 }}> Path to converted cases </Typography>
                        </Box>
                        <UploadFromFileTree route={''} handleFiles={handleFiles} styles={{ flex: 1 }} name='pathToConvertedCases' manualEdit />
                        <Button disabled={!routeConfig} onClick={handleLoadCheck} color='primary' data-testid="test-id-button-load" variant='contained' name='convert'>Load</Button>
                    </Box>
                </Box>
                <Button disabled={!routeConfig} onClick={handleSubmitConfigCheck} color='primary' data-testid="test-id-button-convert" variant='contained' name='convert'>Convert to configs</Button>
            </FooterComponent>
        </>
    )
}

export default NewConfig