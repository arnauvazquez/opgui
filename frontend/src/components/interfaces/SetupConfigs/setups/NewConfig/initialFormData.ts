import {ConfigEditorObject, SpawnerTypeEnum} from "opGUI-client-frontend";

const initialFormData: ConfigEditorObject = {
  "scenario": "",
  "sceneryConfig": "",
  "systemConfig": "",
  "vehiclesCatalog": "",
  "pedestriansCatalog": "",
  "profilesCatalog": "",
  "simulationConfig":{
    "profilesCatalog": "ProfilesCatalog",
    "scenario": {
      "openScenarioFile": "Scenario"
    },
    "experiment": {
      "experimentID": 123,
      "numberOfInvocations": 1,
      "randomSeed": 532725206,
      "libraries": {
        "worldLibrary": "World_OSI",
        "dataBufferLibrary": "DataBuffer",
        "stochasticsLibrary": "Stochastics"
      }
    },
    "environment": {
      "visibilityDistances": [{
          "probability": 1,
          "value": 350
        }],
      "timeOfDays": [{
        probability: 1,
        value: 22
      }],
      "frictions": [{
          "probability": 1,
          "value": 0.2
        }],
      "weathers": [{
          "probability": 1,
          "value": "Clear"
        }],
      "trafficRules": "DE",
      "turningRates": [{
          "incoming": "lane1",
          "outgoing": "lane2",
          "weight": 1
        }]
    },
    "observations": {
      "log": {
        "loggingCyclesToCsv": true,
        "fileName": "output.log",
        "loggingGroups": [
          {
            "enabled": true,
            "group": "Trace",
            "values": "group1,group2"
          },
          {
            "enabled": true,
            "group": "RoadPosition",
            "values": "pos1,pos2"
          }
        ]
      },
      "entityRepository": {
        "fileNamePrefix": "entity_",
        "writePersistentEntities": "Consolidated"
      }
    },
    "spawners": [
      {
        "library": "SpawnerPreRunCommon",
        "type": SpawnerTypeEnum.PreRun,
        "priority": 0,
        "profile": "CustomProfileName"
      },
      {
        "library": "SpawnerRuntimeCommon",
        "type": SpawnerTypeEnum.Runtime,
        "priority": 1
      }
    ]
  }
}

export default initialFormData