/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {Box, Button, IconButton, OutlinedInput, TextField} from "@mui/material";
import IntInput from "../../../commonComponents/DynamicInputs/IntInput";

export const StyledBox = styled(Box)(({ theme }) => ({
   display: "flex",
   flexDirection: "column",
   padding: `0 ${theme.spacing(4)}`,
   gap: theme.spacing(3),
   overflow: "scroll",
   overflowX: 'hidden',
   height: theme.spacing(75),
   borderBottom: `1px solid ${theme.palette.greyScale.main}`
}));

export const StyledButton = styled(Button)(({ theme }) => ({
   margin: `${theme.spacing(4)} auto 0 ${theme.spacing(4)}`
}));

export const StyledInputLine = styled(Box)(({ theme }) => ({
   display: 'flex',
   alignItems: 'center',
   gap: theme.spacing(2)
}));

export const DeleteButton = styled(IconButton)(({ theme }) => ({
    color: theme.palette.primary.main
 }));

export const StyledOutlinedInput = styled(OutlinedInput)({
    width: '100%'
 });

export const StyledTextField = styled(TextField)({
    width: '100%'
 });

export const StyledIntInput = styled(IntInput)({
  width: '100%'
})
