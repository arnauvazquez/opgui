/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {FC, useState} from "react";
import { FileTreeExtended } from "opGUI-client-frontend";
import FileSystemTree from "../../../commonComponents/fileSystemTree/FileSystemTree.tsx";
import {FileNameInput} from "./fileTreeExporte.styles.tsx";

interface FileTreeExporterProps {
    initialFileName?: string;
    fileTreeData: FileTreeExtended;
    onChangePath(path:string): void
    onChangeFileName(path:string): void
}

const FileTreeExporter: FC<FileTreeExporterProps> = (props)=> {
    const { initialFileName,
        onChangePath,
        onChangeFileName,
        fileTreeData
    } = props
    const [ fileName, setFileName] = useState(initialFileName)

    const handleSetPath = (path: string) => {
        onChangePath(path)
    }

    const handleSetFileName = (fileName: string) => {
        setFileName(fileName)
        onChangeFileName(fileName)
    }

    return (
        <>
            <FileSystemTree
                data={fileTreeData.fileSystemTree}
                onSelectFolder={((path: string) => handleSetPath(`${fileTreeData?.basePath}${path}`))}
            />
            <FileNameInput
                label='File Name'
                size={'small'}
                fullWidth
                value={fileName}
                onChange={({ target }) => handleSetFileName(target.value)}
            />
        </>
    )
}

export default FileTreeExporter