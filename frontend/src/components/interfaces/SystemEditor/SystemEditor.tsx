/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {useEffect} from "react";
import { ComponentUI as Component, ComponentUITypeEnum } from 'opGUI-client-frontend';
import {Box} from "@mui/system";
import {Button, Link, Menu, MenuItem, TextField, Typography, useTheme} from "@mui/material";
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import CloseIcon from '@mui/icons-material/Close';
import ControlPointDuplicateOutlinedIcon from '@mui/icons-material/ControlPointDuplicateOutlined';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';
import ZoomInIcon from '@mui/icons-material/ZoomIn';
import ZoomOutIcon from '@mui/icons-material/ZoomOut'
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import AddIcon from '@mui/icons-material/Add';
import CheckIcon from '@mui/icons-material/Check';
import TextsmsIcon from '@mui/icons-material/Textsms';
import TextsmsOutlinedIcon from '@mui/icons-material/TextsmsOutlined';
import {useForm} from "../../../hooks/useForm";
import {ConnectionList, DragComponent, ISystemEditor, System} from "./systemEditor.d";
import {AddIconBox, CommentButton, HideButton, StepperButton} from "../../../styles/systemEditor.styles";
import {useGlobalContext} from "../../../context/ContextProvider";
import {ColorVariant, INPUT_FILE, Variant} from "../../../index.d";
import ModalComponent from "../../Modal/ModalComponent";
import TargetZone from "./TargetZone/TargetZone.tsx";
import ComponentsSideMenu from "./ComponentsSideMenu";
import {
  exportSystemsApi,
  getComponentsApi,
  getFileTreeApi,
  loadSystemsApi,
} from "../../../utils/api.util";
import UploadFromFileTree from "../../commonComponents/UploadInput/UploadFromFileTree.tsx";
import ZoomWrapper from "../../commonComponents/ZoomWrapper/ZoomWrapper.tsx";
import {FooterComponent, LightTooltip, SideWindowBase, StickyDiv, StyledTab} from "../../../styles/general.styles";
import {generateUnique, isSystemNew} from "./EditorUtils.ts";
import FileTreeExporter from "./FileTreeExporter";
import {parseSystemToSystemUI} from "../../../utils/systemEditor/parseLoadSystems.ts";

const formatSystemNameToFile = (systemName: string): string => {
  return `${systemName}.xml`.replaceAll(' ', '_');
}

interface SystemEditorProps {
  initialZoomAmount?: number;
}

const SystemEditor = (props: SystemEditorProps) => {
  const { initialZoomAmount = .75 } = props;
  const theme = useTheme();
  const { scrollByPixels, setModal, modal, setShowDiscardModal } = useGlobalContext()
  const { values, handleChange } = useForm<ISystemEditor>({
    pathToComponents: '',
    componentsOriginal: [],
    components: {
      actions: [],
      algorithms: [],
      sensors: []
    },
    systems: [],
    comments: [],
    deletedSystems: [],
    hideConnections: false,
    selectedSystem: null,
    currentStartIndex: 0,
    currentEndIndex: 0,
    showAccordion: true,
    showComments: false,
    availableElements: [],
    connections: [],
    newSystemName: "",
    renamingSystemId: null,
    zoomAmount: initialZoomAmount,
  });
  const {
    systems,
    hideConnections,
    selectedSystem,
    currentStartIndex,
    currentEndIndex,
    showAccordion,
    showComments,
    availableElements,
    comments,
    connections,
    deletedSystems,
    renamingSystemId,
    newSystemName,
    zoomAmount,
    componentsOriginal
  } = values;

  const handleZoom = (zoomChange: number) => () => {
    const newZoom = Math.max(0.25, Math.min(1, zoomAmount + zoomChange))
    handleChange({ zoomAmount: newZoom })
  }

  const groupByType = (arr: Component[]): Record<ComponentUITypeEnum, Component[]> => {
    return arr.reduce((acc, curr) => {
      if (!acc[curr.type]) {
        acc[curr.type] = [];
      }
      acc[curr.type].push(curr);
      return acc;
    }, {} as Record<ComponentUITypeEnum, Component[]>);
  }

  const getComponents = async () => {
    const response = await getComponentsApi();
    if (!response || 'error' in response) {
      setModal({
        ...modal,
        active: true,
        title: 'An error ocurred while loading the components',
        description: response ? response.message : `Please try to communicate with an admin to solve the problem.`,
        loadingBar: false,
        buttons: [
          {
            title: 'Close',
          }
        ],
      });
      return;
    }
    const responseWithNewIds = response?.map((data, index) => ({ ...data, id: index }));
    handleChange({ components: groupByType(response), componentsOriginal: responseWithNewIds });
  }

  const handleCloseMenu = () => {
    handleChange({ systems: systems.map((data) => ({ ...data, anchorEl: null })) });
  }

  const handleNextClick = () => {
    const remainingSystems = systems.length - currentEndIndex;
    if (remainingSystems >= 4) {
      handleChange({ currentStartIndex: currentStartIndex + 4, currentEndIndex: currentEndIndex + 4 });
      return
    }
    if (remainingSystems > 0) {
      handleChange({
        currentStartIndex: currentStartIndex + remainingSystems,
        currentEndIndex: currentEndIndex + remainingSystems
      });
    }
  };

  const exportSystem = async (saveAsNew: boolean) => {
    if (systems.length === 0) {
      setModal({
        ...modal,
        active: true,
        title: 'No System were loaded or created',
        descriptionTitle: '',
        description: `You should first load or create a system`,
        loadingBar: false,
        buttons: [
          {
            title: 'Close',
          }
        ],
      });
      return;
    }
    if (renamingSystemId !== null) {
      setModal({
        ...modal,
        active: true,
        title: 'You are renaming a system at the moment',
        descriptionTitle: '',
        description: `Either complete or cancel the renaming process to continue.`,
        loadingBar: false,
        buttons: [
          {
            title: 'Close',
          }
        ],
      });
      return;
    }
    if (!selectedSystem){
      setModal({
        ...modal,
        active: true,
        title: 'No System Selected',
        descriptionTitle: '',
        description: `You must have a system selected to export.`,
        loadingBar: false,
        buttons: [
          {
            title: 'Close',
          }
        ],
      });
      return
    }
    const isNew = saveAsNew || isSystemNew(selectedSystem)

    const systemExportFinal: System = {
        ...selectedSystem,
        connections: connections,
        components: availableElements.map((comp) => ({
          ...comp,
          position: {
            x: Math.round(comp.position.x),
            y: Math.round(comp.position.y),
          },
          schedule: {
            offset: comp.schedule.offset ? parseFloat(comp.schedule.offset) : 0,
            cycle: comp.schedule.cycle ? parseFloat(comp.schedule.cycle) : 0,
            response: comp.schedule.response ? parseFloat(comp.schedule.response) : 0,
            priority: comp.schedule.priority ? parseFloat(comp.schedule.priority) : 0,
          },
        })),
        comments: comments?.map((com) => ({
          ...com,
          position: {
            x: Math.round(com.position.x),
            y: Math.round(com.position.y),
          },
        })) || [],
      }
    if (isNew) {
      const fileTreeData = await getFileTreeApi({
        fileTreeRequest: {
          basePathGeneric: 'workspace',
          isFolder: true,
          extension: 'xml'
        }
      }).then((res: any) => {
            return res;
          });
      let modalPath = '';
      let fileName = formatSystemNameToFile(selectedSystem.title)
      setModal({
        ...modal,
        active: true,
        loadingBar: false,
        title: 'Select Folder',
        size: 'big',
        component:
          <FileTreeExporter
              fileTreeData={fileTreeData}
              initialFileName={fileName}
              onChangePath={(path: string) => modalPath = path}
              onChangeFileName={(name: string) => fileName = name}
          />,
        buttons: [
          {
            title: 'Cancel',
            variant: Variant.outlined,
            testId: 'file-tree-cancel'
          },
          {
            title: 'Select',
            variant: Variant.contained,
            action: () => {
              const systemIndex = systems.findIndex(sys => sys.id === systemExportFinal.id)
              const finalPath = `${modalPath}/${formatSystemNameToFile(fileName).replaceAll('.xml', '')}.xml`
              const newSystems = [...systems]
              newSystems[systemIndex] = {
                ...newSystems[systemIndex],
                path: finalPath
              }
              handleChange({
                systems: newSystems,
                selectedSystem: newSystems[systemIndex]
              })
              saveSystem(systemExportFinal, finalPath, isNew)
            },
            testId: 'file-tree-select'
          }
        ]
      });
    }
    else{
      saveSystem(systemExportFinal, selectedSystem.path, isNew)
    }
  }

  const saveSystem = async (system: System, path: string, isNew: boolean) => {
    try {
      const res = await exportSystemsApi(system, path, isNew);
      // Check if the response is an error object
      if ('error' in res && res.error) {
        setModal({
          ...modal,
          active: true,
          title: 'Error', 
          descriptionTitle: 'Failed to save the system',
          description: res.message, 
          loadingBar: false,
          buttons: [
            {
              title: 'Close',
            },
          ],
        });
      } else {
        setModal({
          ...modal,
          active: true,
          title: 'Systems loaded correctly',
          descriptionTitle: '',
          description: ('response' in res ? res.response : 'The system has been saved successfully.'),
          loadingBar: false,
          buttons: [
            {
              title: 'Close',
            },
          ],
        });
      }
    } catch (error) {
      console.error('Unexpected error:', error);
      setModal({
        ...modal,
        active: true,
        title: 'Error',
        descriptionTitle: 'Unexpected Error',
        description: 'An unexpected error occurred while trying to save the system. Please try again.',
        loadingBar: false,
        buttons: [
          {
            title: 'Close',
          },
        ],
      });
    }
    setShowDiscardModal(false);
  };

  const handleBackClick = () => {
    if (currentStartIndex >= 4) {
      handleChange({ currentStartIndex: currentStartIndex - 4, currentEndIndex: currentEndIndex - 4 });
      return
    }
    handleChange({ currentStartIndex: 0, currentEndIndex: Math.min(4, systems.length) });
  };

  const clearAllSystems = () => {
    setModal({
      ...modal,
      active: true,
      title: 'Clear Systems',
      descriptionTitle: 'All The Systems Will Be Removed.',
      description: `After this action, the systems cannot be recovered.`,
      loadingBar: false,
      icon: WarningAmberIcon,
      iconVariant: ColorVariant.warning,
      buttons: [
        {
          title: 'Cancel',
          variant: Variant.outlined
        },
        {
          title: 'Clear Systems',
          action: () => {
            handleChange({ systems: [], selectedSystem: null, currentStartIndex: 0, currentEndIndex: 0, availableElements: [], connections: [] });
            setShowDiscardModal(false);
          },
          testId: 'test-id-modalbutton-clearall'
        },
      ],
    });
  }

  const handleLoadSystems = async (path: string) => {
    const response = (await loadSystemsApi(path));
    if ('error' in response){
      setModal({
        ...modal,
        active: true,
        title: 'Error loading System',
        description: response.message,
        loadingBar: false,
        buttons: [
          {
            title: 'Close',
          },
        ],
      });
      return
    }
    const data = response as System[]
    if (data && data.length > 0) {
      const system = {
        ...data[0],
        path
      }
      const frontSystem = parseSystemToSystemUI(system)
      const newSystemList = system ? [...systems, frontSystem] : systems
      handleChange({
        systems: newSystemList,
        currentEndIndex: Math.min(4, newSystemList.length),
        selectedSystem: frontSystem,
        availableElements: frontSystem.components,
        comments: frontSystem.comments,
        connections: frontSystem.connections,
      });
    }
  }

  const loadSystem = async (path: string) => {
    const isSystemAlreadyOpen = systems?.find(sys => `${sys.path}/${sys.file}`  === path)
    if (isSystemAlreadyOpen){
      setModal({
        ...modal,
        active: true,
        title: 'That system is already opened.',
        description: `This system is already opened. You can select it by clicking on it's tab.`,
        loadingBar: false,
        icon: WarningAmberIcon,
        iconVariant: ColorVariant.warning,
        buttons: [
          {
            title: 'Okay',
          },
        ],
      });
      return
    }
    if (path === ''){
      setModal({
        ...modal,
        active: true,
        title: 'System path is empty.',
        description: `Make sure you filled the system path field.`,
        loadingBar: false,
        icon: WarningAmberIcon,
        iconVariant: ColorVariant.warning,
        buttons: [
          {
            title: 'Okay',
            variant: Variant.outlined
          },
        ],
      });
      return
    }
    await handleLoadSystems(path);
  }

  const duplicateSystem = (system: System) => {
    if (selectedSystem !== null && selectedSystem.id === system.id) {
      system.connections = connections;
      system.components = availableElements;
    }
    const newSystems = systems.map((data) => ({ ...data, anchorEl: null }));
    newSystems.push({
      ...system,
      id: generateUnique(),
      title: `Copy of ${system.title}`,
      anchorEl: null,
      file: formatSystemNameToFile(`Copy_of_${system.title}`),
      path: '',
      deleted: false
    });
    let newCurrentEndIndex = currentEndIndex;
    if (newCurrentEndIndex <= 4) {
      newCurrentEndIndex = Math.min(4, newSystems.length);
    }
    handleChange({
      systems: newSystems,
      currentEndIndex: newCurrentEndIndex
    });
    setShowDiscardModal(true);
  }

  const addNewSystem = () => {
    const newSystems = systems.map((data) => ({ ...data, anchorEl: null }));
    newSystems.push({
      id: generateUnique(),
      title: `System ${systems.length + 1}`,
      anchorEl: null,
      priority: 0,
      components: [],
      connections: [],
      deleted: false,
      file: formatSystemNameToFile(`System_${systems.length + 1}`),
      path: '',
    });
    let newCurrentEndIndex = currentEndIndex;
    if (newCurrentEndIndex <= 4) {
      newCurrentEndIndex = Math.min(4, newSystems.length);
    }
    handleChange({
      systems: newSystems,
      currentEndIndex: newCurrentEndIndex
    });
    setShowDiscardModal(true);
  }

  const closeSystem = async (system: System) => {
    const newSystems = systems.map((data) => ({...data, anchorEl: null}));
    const index = newSystems.findIndex(sys => sys.id === system.id);
    const newDeletedSystems = [...deletedSystems];
    newSystems.splice(index, 1);
    if (currentEndIndex > 4) {
      handleChange({
        systems: newSystems,
        currentStartIndex: currentStartIndex - 4,
        currentEndIndex: currentEndIndex - 4,
        deletedSystems: newDeletedSystems,
        selectedSystem: (system.id === selectedSystem?.id) ? null : selectedSystem,
        availableElements: (system.id === selectedSystem?.id) ? [] : availableElements,
        connections: (system.id === selectedSystem?.id) ? [] : connections
      });
      return;
    } else {
      handleChange({
        systems: newSystems,
        currentEndIndex: Math.min(4, newSystems.length),
        deletedSystems: newDeletedSystems,
        selectedSystem: (system.id === selectedSystem?.id) ? null : selectedSystem,
        availableElements: (system.id === selectedSystem?.id) ? [] : availableElements,
        connections: (system.id === selectedSystem?.id) ? [] : connections
      });
    }
  }

  const handleSelectSystem = (system: System) => {
    const frontAvailableElement = system.components.map((component) => {
      const idOriginal = componentsOriginal.find((comp) => comp.title === component.title);
      return {
        ...component,
        id: (idOriginal) ? idOriginal.id : component.id,
        dropped: true,
        color: (component as DragComponent).color || theme.palette.primary.main,
      }
    });
    let frontConnections: ConnectionList[] = [];
    if (system.connections) {
      frontConnections = system.connections.map((con) => {
        const originalElementOutput = system.components.find((comp) => comp.id === con.source.component);
        const originalElementInput = system.components.find((comp) => comp.id === con.target.component);
        const newIndexElementOutput = frontAvailableElement.findIndex((com) => com.title === originalElementOutput?.title);
        const newElementOutput = frontAvailableElement.find((com) => com.title === originalElementOutput?.title);
        const newIndexElementInput = frontAvailableElement.findIndex((com) => com.title === originalElementInput?.title);
        const newElementInput = frontAvailableElement.find((com) => com.title === originalElementInput?.title);
        return {
          ...con,
          indexComponentInput: newIndexElementInput,
          indexComponentOutput: newIndexElementOutput,
          source: {
            ...con.source,
            component: newElementOutput?.id
          },
          target: {
            ...con.target,
            component: newElementInput?.id
          }
        }
      }) as ConnectionList[];
    }

    if (selectedSystem === null) {
      handleChange({ selectedSystem: system, availableElements: frontAvailableElement, connections: frontConnections });
      return;
    }
    const newSystems = [...systems];
    const index = newSystems.findIndex(sys => sys.id === selectedSystem.id);
    newSystems[index].components = availableElements;
    newSystems[index].connections = connections;
    if (selectedSystem.id === system.id) {
      handleChange({ systems: newSystems, selectedSystem: null, availableElements: [], connections: [] });
      return;
    }
    handleChange({ systems: newSystems, selectedSystem: system, availableElements: frontAvailableElement, connections: frontConnections });
  };

  const handleOpenMenu = (event: React.MouseEvent<SVGElement>, id: number | string) => {
    const newSystems = [...systems];
    const index = newSystems.findIndex(sys => sys.id === id);
    newSystems[index]['anchorEl'] = event.currentTarget;
    handleChange({ systems: newSystems });
  };

  const handleRename = (system: System) => {
    if (renamingSystemId === null) {
      handleChange({
        renamingSystemId: system.id,
        newSystemName: system.title,
        systems: systems.map((data) => ({ ...data, anchorEl: null }))
      });
      return;
    }
    setModal({
      ...modal,
      active: true,
      description: 'Youre already renaming another system',
      descriptionTitle: '',
      title: "Cannot rename 2 systems at the same time.",
      buttons: [{ title: "Close" }]
    });
  };

  const handleConfirmRename = (system: System) => {
    if (newSystemName === "") {
      setModal({
        ...modal,
        active: true,
        description: 'The name cannot be empty',
        descriptionTitle: '',
        title: "You have to give a name for the system.",
        buttons: [{ title: "Close" }]
      });
      return;
    }
    const newSystems = systems.map((data) => ({ ...data, anchorEl: null }));
    const index = newSystems.findIndex((sys) => sys.id === system.id);
    newSystems[index].title = newSystemName;
    let newChangeState: Partial<ISystemEditor> = {
      renamingSystemId: null,
      newSystemName: "",
      systems: newSystems,
    }
    if (newSystems[index].id === selectedSystem?.id){
      newChangeState = {
        ...newChangeState,
        selectedSystem: newSystems[index],
      }
    }
    handleChange(newChangeState);
    setShowDiscardModal(true);
  };

  const handleCancelRename = () => {
    handleChange({ renamingSystemId: null, newSystemName: "" });
  };

  useEffect(() => {
    getComponents();
  }, []);

  return (
    <SideWindowBase className="system-editor" sx={{ height: '100%', position: 'unset' }}>
      <ModalComponent />
      <StickyDiv sx={{ borderBottom: 'unset', padding: 4, position: 'relative', display: 'flex', backgroundColor: theme.palette.common.white }}>
        <UploadFromFileTree
            route={''}
            type={INPUT_FILE.file}
            styles={{ flex: 1, marginRight: 2 }}
            name='system'
            handleFiles={(_name: string, treePath: string) => loadSystem(treePath)}
            hideInputPath={true}
            buttonLabel={'Select System File'}
        />
      </StickyDiv>
      <StickyDiv sx={{ position: 'relative', cursor: 'pointer', alignItems: 'unset', paddingTop: theme.spacing(2), height: theme.spacing(12), background: 'transparent', border: 'unset', justifyContent: 'flex-end', textTransform: 'uppercase', paddingRight: 5 }}>
        <Link onClick={() => scrollByPixels(120)} >Hide setups</Link>
        <KeyboardArrowUpIcon onClick={() => scrollByPixels(120)} color='primary' />
      </StickyDiv>
      <StickyDiv sx={{ justifyContent: 'unset', backgroundColor: theme.palette.primary.accent1Medium, height: theme.spacing(11) }}>
        <Box
            data-testid={'system-slider'}
            sx={{
              display: 'flex',
              width: showAccordion ? '80%' : '99%',
              flexDirection: 'row',
              height: '100%',
              alignItems: 'center',
              transition: 'width 0.5s ease-in-out',
            }}
        >
          {systems.slice(currentStartIndex, currentStartIndex + 4).map((data) => (
            <StyledTab
              className={selectedSystem?.id === data.id ? 'selected' : ''}
              data-testid={`test-id-system-${data.id}`}
              sx={{ zIndex: 1, paddingX: 5, cursor: 'default', gap: 2 }}
              key={data.id}>
              {renamingSystemId === data.id ? (
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <TextField
                    value={newSystemName}
                    onChange={(e) => handleChange({ newSystemName: e.target.value })}
                    placeholder={data.title}
                    variant="outlined"
                    size="small"
                    data-testid="test-id-rename-field"
                  />
                  <CheckIcon data-testid="test-id-rename-confirm" onClick={() => handleConfirmRename(data)} style={{ cursor: 'pointer', color: 'green' }} />
                  <CloseIcon data-testid="test-id-rename-cancel" onClick={handleCancelRename} style={{ cursor: 'pointer', color: 'red' }} />
                </Box>
              ) : (
                <Typography data-testid={`test-id-system-text-${data.id}`} onClick={() => handleSelectSystem(data)} sx={{ fontWeight: 500 }}>
                  {data.title}
                </Typography>
              )}
              <KeyboardArrowDownOutlinedIcon data-testid={`test-id-system-open-menu-${data.id}`} onClick={(event: React.MouseEvent<SVGElement>) => handleOpenMenu(event, data.id)} color="primary" sx={{ cursor: 'pointer', zIndex: 3 }} />
              <Menu
                id="basic-menu"
                data-testid="test-id-menu-dropdown"
                anchorEl={data.anchorEl}
                open={Boolean(data.anchorEl)}
                onClose={handleCloseMenu}
                MenuListProps={{
                  'aria-labelledby': 'basic-button',
                }}
              >
                <MenuItem data-testid="test-id-menu-item-rename" onClick={() => handleRename(data)}>
                  <i className="icon-mask primary rename"></i>
                  <Typography sx={{ color: theme.palette.primary.main, paddingLeft: 2 }} >
                    RENAME
                  </Typography>
                </MenuItem>
                <MenuItem data-testid="test-id-menu-item-duplicate" onClick={() => duplicateSystem(data)}>
                  <ControlPointDuplicateOutlinedIcon color="primary" />
                  <Typography sx={{ color: theme.palette.primary.main, paddingLeft: 2 }}>
                    DUPLICATE
                  </Typography>
                </MenuItem>
                <MenuItem data-testid="test-id-menu-item-delete" onClick={() => closeSystem(data)}>
                  <CloseIcon color="primary" />
                  <Typography sx={{ color: theme.palette.primary.main, paddingLeft: 2 }}>
                    CLOSE
                  </Typography>
                </MenuItem>
              </Menu>
            </StyledTab>
          ))
          }
          <LightTooltip placement="bottom-start" title="Creating a new system..">
            <AddIconBox data-testid="test-id-add-new-system" onClick={() => addNewSystem()}>
              <AddIcon sx={{
                height: theme.spacing(6),
                width: theme.spacing(6),
              }} />
            </AddIconBox>
          </LightTooltip>
          <Box position='absolute' display='flex' justifyContent='end' alignItems='center' width='inherit' gap={theme.spacing(2)}>
            <Typography variant="body1">
              {currentEndIndex} - {systems.length} Systems
            </Typography>
            <StepperButton data-testid="test-id-button-stepper-back" onClick={handleBackClick} disabled={currentStartIndex === 0}><ChevronLeftIcon /></StepperButton>
            <StepperButton data-testid="test-id-button-stepper-next" onClick={handleNextClick} disabled={currentEndIndex >= systems.length}><ChevronRightIcon /></StepperButton>
          </Box>
        </Box>
        <Box sx={{
          height: '100%',
          minWidth: '20%',
          borderLeft: `1px solid ${theme.palette.greyScale.main}`,
          display: 'flex',
          alignItems: 'center',
          paddingRight: theme.spacing(4),
          justifyContent: 'flex-end',
          transition: 'transform 0.5s ease-in-out',
          transform: showAccordion ? 'translateX(0%)' : 'translateX(95%)'
        }}>
          <LightTooltip placement="left" title="Drag and drop one component to the dashboard to create and editate systems.">
            <InfoOutlinedIcon data-testid="test-id-icon-system-info" sx={{ cursor: 'pointer' }} />
          </LightTooltip>
        </Box>
      </StickyDiv>

      <Box sx={{ display: 'flex', width: '100%', height: 'calc(100% - 203px);', position: 'relative' }} data-testid="test-id-box-systemzone">
        <Box
            sx={{
              transition: 'all 0.5s ease-in-out',
              right: `calc(${theme.spacing(2)} + ${showAccordion ? '20%' :'0px'})`,
              top: theme.spacing(2),
              position: 'absolute',
              display: 'flex'
            }}>
          <HideButton
              sx={{
                marginLeft: theme.spacing(3),
              }}
              onClick={handleZoom(-0.25)}
              data-testid="test-id-zoom-out"
          >
            <ZoomOutIcon sx={{ color: theme.palette.common.white }} />
          </HideButton>
          <HideButton
              sx={{
                marginLeft: theme.spacing(3),
              }}
              onClick={handleZoom(0.25)}
              data-testid="test-id-zoom-in"
          >
            <ZoomInIcon sx={{ color: theme.palette.common.white }} />
          </HideButton>
          <HideButton
              sx={{
                marginLeft: theme.spacing(3),
                background: 'transparent',
              }}
              onClick={() => handleChange({ hideConnections: !hideConnections })}
              data-testid="test-id-button-hide-connections"
          >
            {!hideConnections ?
                (<RemoveRedEyeOutlinedIcon data-testid="test-id-show-connections" sx={{ color: theme.palette.primary.main }} />) :
                (<VisibilityOffOutlinedIcon data-testid="test-id-hide-connections" sx={{ color: theme.palette.primary.main }} />)
            }
          </HideButton>
            <LightTooltip placement="left" title={showComments ? "Close comment mode" : "Activate comment mode"}>
                <CommentButton
                    data-testid="test-id-show-comments-button"
                    sx={{
                      marginLeft: theme.spacing(3)
                    }}
                    onClick={() => handleChange({ showComments: !showComments })}
                >
                    {showComments ?
                        <TextsmsIcon data-testid="test-id-show-comments-icon" sx={{ color: theme.palette.primary.main }} />
                        : <TextsmsOutlinedIcon sx={{ color: theme.palette.primary.main }} />}
                </CommentButton>
            </LightTooltip>
          <HideButton
              sx={{
                marginLeft: theme.spacing(3),
              }}
              onClick={() => handleChange({ showAccordion: !showAccordion })}
              data-testid="test-id-button-hide-accordeon"
          >
            {showAccordion ?
                (<ChevronRightIcon data-testid="test-id-show-accordeon" sx={{ color: theme.palette.common.white }} />) :
                (<ChevronLeftIcon data-testid="test-id-hidden-accordeon" sx={{ color: theme.palette.common.white }} />)
            }
          </HideButton>
        </Box>
        <ZoomWrapper zoomScale={zoomAmount} width={`${100 * (showAccordion ? 0.8 : 1)}%`} height={`100%`}>
          <TargetZone handleChange={handleChange} zoomScale={zoomAmount} {...values} />
        </ZoomWrapper>
        <ComponentsSideMenu handleChange={handleChange} {...values} />
      </Box>
      <FooterComponent sx={{ justifyContent: 'space-between', height: theme.spacing(12), zIndex: 10 }} data-testid="test-id-footer-systemeditor">
        <Button color='primary' data-testid="test-id-button-system-clearall" variant='outlined' sx={{ marginLeft: 7, }} name='convert' onClick={() => clearAllSystems()} >Clear All</Button>
        <Box>
          <Button color='primary' data-testid="test-id-button-system-export" variant='contained' sx={{ marginRight: 7 }} name="exportSystems" onClick={()=> exportSystem(false)}>Save </Button>
          <Button color='primary' data-testid="test-id-button-system-export-as" variant='contained' sx={{ marginRight: 7 }} name="exportSystems" onClick={()=> exportSystem(true)}>Save AS </Button>
        </Box>
      </FooterComponent>
    </SideWindowBase>
  )
}

export default SystemEditor
