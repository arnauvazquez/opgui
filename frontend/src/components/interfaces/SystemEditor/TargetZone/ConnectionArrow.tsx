/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {FC, useEffect, useState} from "react";
import Xarrow, { xarrowPropsType } from "react-xarrows";
import ArrowConnectionLabel from "./ArrowConnectionLabel.tsx";
import { ConnectionList } from '../systemEditor.d';
import {Box, useTheme} from "@mui/material";
import { VITE_TEST_REACT } from "../../../../utils/constants.ts";

export interface ConnectionArrowProps extends xarrowPropsType{
    zoomScale?: number;
    connection: ConnectionList;
    inputType?: string;
    outputType?: string;
    isValid?: boolean;
    color?: string;
    onClick?(): void;
    testId?: string;
}

const ConnectionArrow: FC<ConnectionArrowProps> = (props) => {
    const theme = useTheme();
    const [isHover, setHover ] = useState(false)
    const {
        zoomScale = 1,
        connection,
        isValid,
        passProps,
        inputType,
        outputType,
        onClick,
        color,
        testId='arrow',
        ...rest
    } = props

    useEffect(() => {}, [zoomScale]);

    const LabelComponent = <ArrowConnectionLabel
        deleteMode={isHover}
        isValid={isValid}
        alertText={`Warning! Data type of input ${inputType ? `(${inputType})` : ''} and output ${outputType ? `(${outputType})` : ''} differ.`}
        key={'arrow-label'}
        className={'label-container'}
        zoomScale={zoomScale}
        connectionId={connection.id}
        onRemove={onClick}
        testId={`${testId}-label`}
    />

    return (
        // xarrows dont render on tests, so we have to use a different component to test its functionalities
            VITE_TEST_REACT !== 'dev' ? <Xarrow
                showHead={true}
                labels={connection.labels ? connection.labels : <>{LabelComponent}</>}
                strokeWidth={zoomScale < 0.5 ? 2 : 4}
                path={'grid'}
                color={color || theme.palette.primary.main}
                {...rest}
                passProps={{
                    id: `arrow-${connection.id}`,
                    cursor: 'pointer',
                    onMouseOver: () => {
                        setHover(true)
                    },
                    onMouseOut: () => {
                        setHover(false)
                    },
                    onClick: ()=> onClick && onClick(),
                    ...passProps
                }}
            />
            :
                <Box
                    data-testid={testId}
                    onClick={()=> onClick && onClick()}
                    onMouseOver={()=> setHover(true)}
                    onMouseOut={()=> setHover(false)}
                >
                    {connection.labels ? connection.labels : <>{LabelComponent}</>}
                </Box>
    )
}

export default ConnectionArrow;
