/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { ComponentUI as Component, Connection, Output, Comment } from 'opGUI-client-frontend';
export interface ComponentList {
    [key: string]: Component[]
}
export type ConnectionList = Connections & {
    indexComponentInput: number;
    indexComponentOutput: number;
    labels?: OverridableComponent<SvgIconTypeMap<{}, "svg">>  | string;
 };

type ExtendedComponent = Component & { index: number };
export interface DragComponent extends Component {
    offsetX: number;
    offsetY: number;
    position: {
        x: number,
        y: number
    };
    height: number;
    width: number;
    color: string;
    dropped: boolean;
    [key: string]: any;
}
export interface DragComment extends Comment {
    offsetX: number;
    offsetY: number;

}

export type ConnectionComponent = {
    component: DragComponent;
    output: Output;
    index: number;
}

export interface Connections {
    id: number;
    source: {
        component: number;
        output: number;
    }
    target: {
        component: number;
        input: number;
    }
}

export interface System {
    id: number;
    path: string;
    title: string;
    priority: number;
    anchorEl: null | HTMLElement | SVGElement;
    components: Component[];
    connections: Connections[];
    comments?: Comment[];
    file?: string;
    deleted?: boolean;
}

export interface ISystemEditor {
    pathToComponents: string;
    components: ComponentList;
    componentsOriginal: Component[];
    systems: System[];
    deletedSystems: System[];
    hideConnections: boolean;
    selectedSystem: System | null;
    currentStartIndex: number;
    currentEndIndex: number;
    showAccordion: boolean;
    showComments: boolean;
    comments: any[],
    availableElements: any[];
    connections: ConnectionList[];
    newSystemName: string;
    renamingSystemId: number | string | nullÏ;
    zoomAmount: number;
}

export const BASE_DIVISOR = 2;
