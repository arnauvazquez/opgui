/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { Box, Typography } from "@mui/material";
import { styled } from '@mui/material/styles';

export const GlobalSetupBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    borderRadius: theme.spacing(1),
    marginRight: theme.spacing(8),
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.palette.common.white,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    paddingLeft: theme.spacing(8),
    paddingRight: theme.spacing(8)
}));

export const GlobalSetupBoxTypography = styled(Typography)(({ }) => ({
    flex: .4
}));

export const GlobalSetupConfigBox = styled(Box)(({ theme }) => ({
    paddingLeft: theme.spacing(8),
    marginTop: theme.spacing(5),
    paddingBottom: theme.spacing(12)
}));

export const GlobalSetupTypography = styled(Typography)(({ theme }) => ({
    paddingBottom: theme.spacing(2),
    fontWeight: '500',
}));

export const GlobalSetupCategory = styled('div')(({ theme }) => ({
    marginBottom: theme.spacing(6),
}))