/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {
    Configuration,
    VerifyPathApi,
    ExportOpsimulationManagerXmlApi,
    OpSimulationManagerXmlRequest,
    RunOpSimulationApi,
    DeleteInformationApi,
    IsSimulationRunningApi,
    ComponentsApi,
    GetFileTreeApi,
    GetSystemApi,
    SystemUI,
    SaveSystemApi,
    SendPCMFileApi,
    ConvertToConfigsApi,
    ExportToSimulationApi,
    PathToConvertedCasesApi,
    SystemRef,
    GetValueListApi,
    GetConfigElementsApi,
    SetConfigElementsApi,
    ConfigElement,
    GetFileContentsApi,
    VerifyPath200Response,
    SaveConfigEditorApi,
    ConfigEditorObject,
    GetConfigEditorApi,
} from 'opGUI-client-frontend';
import { System } from '../components/interfaces/SystemEditor/systemEditor.d';
import {systemToBackend, systemToFront} from "./apiConverters.ts";
import {updateComponentIds} from "../components/interfaces/SystemEditor/EditorUtils.ts";

const configuration = new Configuration({basePath: ''});
const verifyPathApi = new VerifyPathApi(configuration);
const exportOpsimulationManagerXmlApi = new ExportOpsimulationManagerXmlApi(configuration);
const runOpSimulationApi = new RunOpSimulationApi(configuration);
const deleteInformationApi = new DeleteInformationApi(configuration);
const isSimulationRunningApi = new IsSimulationRunningApi(configuration);
const componentsApi = new ComponentsApi(configuration);
const fileTreeApi = new GetFileTreeApi(configuration);
const getSystemApi = new GetSystemApi(configuration);
const saveSystemApi = new SaveSystemApi(configuration);
const sendPCMFileApi = new SendPCMFileApi(configuration);
const convertToConfigsApi = new ConvertToConfigsApi(configuration);
const exportToSimulationApi = new ExportToSimulationApi(configuration);
const pathToConvertedCasesApi = new PathToConvertedCasesApi(configuration);
// Setup Config
const getConfigElementsApi = new GetConfigElementsApi(configuration);
const setConfigElementsApi = new SetConfigElementsApi(configuration);
// Request File
const fileContentApi = new GetFileContentsApi(configuration);
// Form
const getValueListApi = new GetValueListApi(configuration);
const saveConfigEditor = new SaveConfigEditorApi(configuration);
const getConfigEditorApi = new GetConfigEditorApi(configuration);

export interface ErrorData {
    error: boolean;
    message: string | any;
}

const handleError = async (error: any): Promise<ErrorData> => {
    if (error.response) {
        const errorData = await error.response.json();
        switch (error.response.status) {
            case 400: {
                // Assuming the error structure matches the generated Error400 type
                return { error: true, message: errorData.message };
            }
            case 500: {
                // Assuming the error structure matches the generated Error500 type
                return { error: true, message: errorData.message };
            }
            default:
                console.error('Unexpected Error Status:', error.response.status);
                return { error: true, message: 'An unexpected server error occurred' };
        }
    } else {
        // Handle errors not associated with a backend response, such as network issues
        return { error: true, message: error.message || 'Network or unknown error' };
    }
}

export const isSimRunning = async () => {
    try {
        const res = await isSimulationRunningApi.apiIsSimulationRunningGet();
        return res.response;
    }
    catch(error: any) {
        return await handleError(error)
    }
}
export const verifyPath = async (path: string, isFullPath?: boolean): Promise<VerifyPath200Response | ErrorData> => {
    try {
        return await verifyPathApi.verifyPath({ pathRequest: { path: path, isFullPath: isFullPath } });
    }
    catch(error: any) {
        return await handleError(error)
    }
};

export const exportXml = async (data: OpSimulationManagerXmlRequest) => {
    try {
        const res = await exportOpsimulationManagerXmlApi.apiExportOpsimulationManagerXmlPost({ opSimulationManagerXmlRequest: data })
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const runSimulation = async () => {
    try {
        const res = await runOpSimulationApi.apiRunOpSimulationGet();
        console.log('Execution opSimManager OK!', res.response)
        return res.response;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const deleteInformation = async (path: string) => {
    try {
        const res = await deleteInformationApi.deleteInformation({ pathRequest: { path: path } });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}
export const getComponentsApi = async () => {
    try {
        const res = await componentsApi.apiComponentsGet();
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const getFileTreeApi = async (requestParameters: any) => {
    try {
        const res = await fileTreeApi.apiGetFileTreePost(requestParameters);
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const loadSystemsApi = async (path: string) => {
    try {
        const res = await getSystemApi.apiGetSystemGet({
            path
        })
        const system = systemToFront(res)
        return [system];
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const exportSystemsApi = async (system: System, path: string, isNew: boolean) => {
    try {
        const systemNewIds = updateComponentIds(system)
        const convertedSystem: SystemUI = systemToBackend(systemNewIds)
        const res = await saveSystemApi.apiSaveSystemPost({ apiSaveSystemPostRequest: {
                system: convertedSystem,
                path,
                isNew
            } });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const sendPCMFile = async (path: string) => {
    try {
        const res = await sendPCMFileApi.apiSendPCMFilePost({ pathRequest: { path: path } });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}
export const convertToConfigs = async (selectedExperiments: Array<string>, selectedSystems: SystemRef[]) => {
    try {
        const res = await convertToConfigsApi.apiConvertToConfigsPost({ configsRequest: {selectedExperiments, selectedSystems} });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}
export const exportToSimulation = async (selectedExperiments: Array<string>) => {
    try {
        const res = await exportToSimulationApi.apiExportToSimulationsPost({ selectedExperimentsRequest: { selectedExperiments: selectedExperiments } });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}
export const pathToConvertedCasesPost = async (path: string) => {
    try {
        const res = await pathToConvertedCasesApi.apiPathToConvertedCasesPost({ pathRequest: { path: path } });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

// Config
 export const getConfigElements = async () => {
     try {
         const res = await getConfigElementsApi.apiConfigElementsGet();
         return res;
     }
     catch(error: any) {
         return await handleError(error)
     }
 }

export const saveConfigElements = async (configElements: ConfigElement[]) => {
    try {
        const res = await setConfigElementsApi.apiConfigElementsPost({
            configElement: configElements
        });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const getFileContent = async (path: string, encoded: boolean) => {
    try {
        const res = await fileContentApi.apiGetFileContentsGet({
            path,
            encoded
        });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const getValueList = async (name: string) => {
    try {
        const res = await getValueListApi.apiGetValueListGet({
            name
        });

        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const postConfigEditor = async (path: string, configEditorObject: ConfigEditorObject) => {
    try {
        const res = await saveConfigEditor.apiConfigEditorPost({
            apiConfigEditorPostRequest: {
                path,
                configEditorObject
            }
        });

        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}

export const getConfigEditor = async (path: string) => {
    try {
        const res = await getConfigEditorApi.apiConfigEditorGet({
            path
        });
        return res;
    }
    catch(error: any) {
        return await handleError(error)
    }
}