const getDecimalValues = (num: number): number => {
  const decimals = num.toString().split('.')[1]; // Get the decimal part of the number
  const sign = num < 0 ? -1 : 1;
  if (!decimals) return 0; // If no decimal part, return an empty array
  console.log(decimals)
  // Convert each decimal character to a number and return as an array
  return Number(decimals) * sign;
}

export default getDecimalValues