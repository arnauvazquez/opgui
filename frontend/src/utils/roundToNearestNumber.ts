const roundToNearestMultiple = (num: number, multiple: number): number => {
  if (multiple === 0) return num; // Avoid division by zero
  return Math.round(num / multiple) * multiple;
}

export default roundToNearestMultiple;