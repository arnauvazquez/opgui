import {ComponentUI} from 'opGUI-client-frontend';
import {ConnectionList, Connections, System } from "../../components/interfaces/SystemEditor/systemEditor.d";
import {updateComponentIds} from "../../components/interfaces/SystemEditor/EditorUtils.ts";

interface SystemComponentUI extends ComponentUI {
  id: number;
  dropped?: boolean;
  color?: string;
}

export const parseSystemToSystemUI = (system: System) => {
  const systemUpdatedIds = updateComponentIds(system);
  const frontComponents: SystemComponentUI[] = systemUpdatedIds.components.map((component: ComponentUI) => {
    return {
      ...component,
      dropped: true,
    }
  });

  const frontConnections = parseConnectionsToConnectionsUI(systemUpdatedIds.connections, systemUpdatedIds.components)

  const frontComments = systemUpdatedIds.comments?.map(comment => ({
    ...comment,
    dropped: true,
  }));

  return {
    ...systemUpdatedIds,
    components: frontComponents,
    connections: frontConnections,
    comments: frontComments,
  }
}

const parseConnectionsToConnectionsUI = (connections: Connections[], frontSysComponents: SystemComponentUI[]) => {
  const frontConnections = connections.map((con) => {
    const newIndexElementOutput = frontSysComponents.findIndex((com: ComponentUI) => com.id === con.source.component);
    const newIndexElementInput = frontSysComponents.findIndex((com: ComponentUI) => com.id === con.target.component);
    return {
      ...con,
      indexComponentInput: newIndexElementInput,
      indexComponentOutput: newIndexElementOutput,
    }
  }) as ConnectionList[];

  return frontConnections;
}