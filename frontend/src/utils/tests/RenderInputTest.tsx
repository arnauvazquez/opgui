import {
    IDynamicInputProps,
} from "../../components/commonComponents/DynamicInputs/interfaces.ts";
import {renderWithProviders} from "../test.utils.tsx";
import { useState } from "react";

export const RenderInput = <C extends React.ComponentType<any>>(props: React.ComponentProps<C>, Component: C) => {
    return renderWithProviders(
        <RenderInputWithState {...props} Component={Component} />
    )
}

interface IRenderInputWithStateProps extends IDynamicInputProps{
    Component: React.ComponentType<IDynamicInputProps>
}

const RenderInputWithState = <P extends IRenderInputWithStateProps>(props: P) => {
    const { Component, ...rest} = props
    const [value, setValue] = useState(props.value)

    const handleChange = (name: string, val: any) => {
        setValue(val)
        if (props?.onChange)
            props?.onChange(name, val)
    }

    return (
        <Component {...rest} value={value} onChange={handleChange} />
    )
}