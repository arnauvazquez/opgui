const getTestIdFromName = (inputName: string) => `test-id-${inputName}`

export default getTestIdFromName