import defaultFileTreeResponse from "./mockResponses/defaultFileTreeResponse.ts";
import {screen} from "@testing-library/dom";
import {act} from "react-dom/test-utils";
import {fireEvent, waitFor} from "@testing-library/react";
import {FileTreeExtended} from "opGUI-client-frontend";
import setupMocksCalls, {CallHandler} from "./setupMocksCalls.ts";
import handleResponse from "./handleResponse.ts";
import components from "../fakeComponentData.json";
import getDefaultVerifyPathResponse from "./mockResponses/getDefaultVerifyPathResponse.ts";
import {renderWithProviders} from "../test.utils.tsx";
import SystemEditor from "../../components/interfaces/SystemEditor/SystemEditor.tsx";

interface SetupMocksParams {
  renderSystemEditorFunction?: any;
  system: any
  fileTreeResponse?: FileTreeExtended
}

export const renderSystemEditor = ()=> (renderWithProviders(<SystemEditor initialZoomAmount={1}/>))

export const setupMocks = ({ fileTreeResponse = defaultFileTreeResponse, system }: SetupMocksParams) => {
  const setupCalls: CallHandler[] = [
    {
      url: `api/getFileTree`,
      response: handleResponse(fileTreeResponse)
    },
    {
      url: `api/components`,
      response: handleResponse(components)
    },
    {
      url: `api/verify-path`,
      response: handleResponse(getDefaultVerifyPathResponse(`os/workspace/path/to/folder/test${system.id}`))
    },
    {
      url: `api/getSystem?path=os%2Fworkspace%2Fpath%2Fto%2Ffolder%2Ftest${system.id}`,
      response: handleResponse(system)
    },
    {
      url: `api/deleteSystem?path=os%2Fworkspace%2Fpath%2Fto%2Ffolder%2Ftest`,
      response: handleResponse({
        "response": "System correctly deleted"
      })
    },
  ]
  setupMocksCalls(setupCalls)
}

export const SelectSystemFirst = async ({ fileTreeResponse = defaultFileTreeResponse, system, renderSystemEditorFunction = renderSystemEditor }: SetupMocksParams) => {
  setupMocks({ fileTreeResponse, system })
  await act (()=> renderSystemEditorFunction());
  await SelectAnotherSystem({ fileTreeResponse, system })
}

export const SelectAnotherSystem = async ({ fileTreeResponse = defaultFileTreeResponse, system }: SetupMocksParams) => {
  setupMocks({ fileTreeResponse, system })
  const loadSystem = screen.getByTestId('test-id-input-button-system');
  await act(async () => {
    fireEvent.click(loadSystem);
  });
  await waitFor(() => expect(screen.getByTestId('test-id-modal-Select File')).toBeInTheDocument())
  const modal = screen.getByTestId('test-id-modal-Select File')
  expect(modal).toBeInTheDocument()
  const selectFileButton = screen.getByTestId('file-tree-select')
  await act(() => {
    fireEvent.click(selectFileButton);
  });
  expect(modal).not.toBeInTheDocument();
}