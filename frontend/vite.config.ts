import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import * as path from "path";

function pathFixerPlugin() {
  return {
    name: 'path-fixer',
    enforce: 'pre',
    generateBundle(options, bundle) {
      Object.keys(bundle).forEach(key => {
        const chunk = bundle[key];
        if (chunk.type === 'asset' || chunk.type === 'chunk') {
          // Ensure all assets are in the assets directory
          if (!chunk.fileName.startsWith('assets/') && !chunk.fileName.endsWith('.html')) {
            chunk.fileName = `assets/${chunk.fileName}`;
          }
        }
      });
    }
  };
}

export default defineConfig({
  plugins: [
    pathFixerPlugin(),
    react()
  ],
  base: '/', // Changed from './' to '/'
  build: {
    outDir: 'dist',
    emptyOutDir: true,
    assetsDir: 'assets',
    rollupOptions: {
      input: 'index.html',
      output: {
        entryFileNames: 'assets/[name].[hash].js',
        chunkFileNames: 'assets/[name].[hash].js',
        assetFileNames: 'assets/[name].[hash].[ext]'
      }
    }
  },
  resolve: {
    alias: {
      '@components': path.resolve(__dirname, 'src/components'),
      '@utils': path.resolve(__dirname, 'src/utils'),
      '@hooks': path.resolve(__dirname, 'src/hooks'),
      '@styles': path.resolve(__dirname, 'src/styles'),
      '@config': path.resolve(__dirname, 'src/config'),
      '@context': path.resolve(__dirname, 'src/context'),
      '@assets': path.resolve(__dirname, 'src/assets')
    }
  },
  server: {
    proxy: {
      '^(?!/assets/).*': {  // Proxy everything except /assets routes
        target: 'http://localhost:8848',
        changeOrigin: true,
        secure: false,
        configure: (proxy, _options) => {
          proxy.on('error', (err, _req, _res) => {
            console.log('proxy error', err);
          });
          proxy.on('proxyReq', (proxyReq, req, _res) => {
            console.log('Sending Request to the Target:', req.method, req.url);
          });
          proxy.on('proxyRes', (proxyRes, req, _res) => {
            console.log('Received Response from the Target:', proxyRes.statusCode, req.url);
          });
        },
      }
    }
  }
});