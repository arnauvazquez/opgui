#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Determine the script's directory
MYDIR="$(dirname "$(readlink -f "$0")")"
echo "Current script directory: $MYDIR"
NODE_PATH="/c/op/Program Files/nodejs"
YARN_PATH="/c/op/Program Files (x86)/Yarn/bin"
MINGW_PATH="/c/op/msys64/mingw64/bin"  # Ensure this is the correct path

# Add Node.js, Yarn, and MinGW to the PATH
export PATH="$NODE_PATH:$YARN_PATH:$MINGW_PATH:$PATH"

# Diagnostic: Print the PATH to verify it's set correctly
echo "Updated PATH: $PATH"

echo "Checking DLL dependencies for OPGUICoreTests.exe using objdump:"
# Use objdump to list DLL dependencies and parse them into an array
required_dlls=($($MINGW_PATH/objdump.exe -p "$MYDIR/../../../backend/build/test/OPGUICoreTests.exe" | grep "DLL Name" | sed 's/.*DLL Name: \(.*\).dll/\1.dll/'))

# Function to find and report the path of each DLL
function find_dll {
    found=false
    IFS=':' read -ra ADDR <<< "$PATH"
    for p in "${ADDR[@]}"; do
        if [ -f "$p/$1" ]; then
            echo "$1 found in $p"
            found=true
            break
        fi
    done
    if ! $found; then
        echo "$1 not found in any PATH directories"
    fi
}

# Check each DLL in the dynamically created array
for dll in "${required_dlls[@]}"; do
    find_dll $dll
done
 
if [ -f "/c/op/msys64/mingw64/bin/mingw32-make.exe" ]; then
    echo "mingw32-make.exe is available."
else
    echo "mingw32-make.exe not found."
fi 


# Function to handle command failure
handle_failure() {
    echo "Error: $1"  # $1 can be an exit code or a message
    exit ${2:-1}  # Exit with the provided code, or default to 1
}

# Trim whitespace from USER_PROFILE_MODIFIED using sed
USER_PROFILE_MODIFIED=$(echo "$USER_PROFILE_MODIFIED" | sed 's/^[ \t]*//;s/[ \t]*$//')

echo "Received USERPROFILEMODIFIED: '$USER_PROFILE_MODIFIED'."

# Calculate the correct directory for the JSON file
DIR_PROJECT="$MYDIR/../../.."  # Navigate to project root, adjust as necessary
echo "Project directory: $DIR_PROJECT"
jsonFile="$DIR_PROJECT/backendConfig.json"
echo "Path to backendConfig.json: $jsonFile"

# Check if the JSON file exists
if [ ! -f "$jsonFile" ]; then
    handle_failure "backendConfig.json does not exist at $jsonFile"
fi

# Display original content of backendConfig.json
echo "Original content of backendConfig.json:"
cat "$jsonFile"

# Replace <USER_HOME_PATH> in the JSON file with the modified userProfile
sed -i "s|<USER_HOME_PATH>|$USER_PROFILE_MODIFIED|g" "$jsonFile"

# Display updated content of backendConfig.json
echo "Updated content of backendConfig.json:"
cat "$jsonFile" 


# Create necessary directories and check each
mkdir -p "$USER_PROFILE_MODIFIED/Documents/workspaces/opGUI-workspace1" || handle_failure "Failed to create workspace directory"
mkdir -p "$USER_PROFILE_MODIFIED/opSimulation/bin/core" || handle_failure "Failed to create core directory"
mkdir -p "$USER_PROFILE_MODIFIED/opSimulation/bin/core/components" || handle_failure "Failed to create components directory"
mkdir -p "$USER_PROFILE_MODIFIED/opSimulation/bin/core/modules" || handle_failure "Failed to create modules directory"

# Create necessary files and check each
touch "$USER_PROFILE_MODIFIED/opSimulation/bin/core/opSimulation.exe" || handle_failure "Failed to create opSimulation.exe"
touch "$USER_PROFILE_MODIFIED/opSimulation/bin/core/opSimulationManager.exe" || handle_failure "Failed to create opSimulationManager.exe"

# Navigate to the project root directory, assuming MYDIR is correctly set
DIR_PROJECT="$MYDIR/../../.."  # Adjust as necessary
echo "Project directory: $DIR_PROJECT"

# Print the contents of the project directory
echo "Listing contents of the project directory in test script:"
ls "$DIR_PROJECT"

# Print the contents of the project directory
echo "Listing contents of the backend directory in test script:"
ls "$DIR_PROJECT/backend"

# Navigate to the backend build directory
cd "$DIR_PROJECT/backend/build" || handle_failure "Failed to change directory to backend build"
echo "Changed to backend build directory in test script."

# List the contents of the backend build directory
echo "Listing contents of backend build directory in test script:"
ls -la

# Print the contents of the test directory
echo "Listing contents of the test directory in test script:"
ls "$DIR_PROJECT/backend/build/test"

# Print a message to prove the script is the one being modified
echo "TIMESTAMP $(date +%Y-%m-%d_%H:%M:%S)"


# Run the test executable
cd "$DIR_PROJECT/backend/build"
echo "Running tests from $(pwd), with PATH: $PATH"
./OPGUICore.exe || echo "Failed to run core exe"

# Run the test executable
cd "$DIR_PROJECT/backend/build/test"
echo "Running tests from $(pwd), with PATH: $PATH"
./OPGUICoreTests.exe || echo "Failed to run tests"

# Uncomment the line below to enable the original test command
#/c/msys64/mingw64/bin/mingw32-make.exe run_tests



