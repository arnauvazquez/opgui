#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Add MSYS2 paths if not already present
export PATH="/c/op/msys64/usr/bin:/c/op/msys64/mingw64/bin:$PATH"

# Function to handle command failure
handle_failure() {
    echo "Error: $1"  # $1 can be an exit code or a message
    exit ${2:-1}  # Exit with the provided code, or default to 1
}

# Function to convert Windows path to MSYS path
win_to_msys_path() {
    local winpath="$1"
    # Remove quotes and trailing spaces
    winpath="${winpath//\"/}"
    winpath="${winpath%"${winpath##*[![:space:]]}"}"
    # Convert backslashes to forward slashes
    winpath="${winpath//\\//}"
    # Convert drive letter
    winpath="/${winpath,,[A-Z]}"
    winpath="${winpath/:/}"
    echo "$winpath"
}

# Function to verify Node.js environment
verify_node_env() {
    echo "Verifying Node.js environment..."
    echo "Current PATH: $PATH"
    echo "Current NODE_PATH: $NODE_PATH"

    # If NODE_PATH is set, use it to set up the environment
    if [ -n "$NODE_PATH" ]; then
        local msys_node_path=$(win_to_msys_path "$NODE_PATH")
        NODE_DIR=$(dirname "$msys_node_path")
        echo "Adding Node.js directory to PATH: $NODE_DIR"
        export PATH="$NODE_DIR:$PATH"
    fi

    # Check if Node.js is available in the PATH
    if command -v node >/dev/null 2>&1; then
        echo "Node.js is installed:"
        node --version
        ACTUAL_NODE_PATH=$(command -v node)
        echo "Node.js found at: $ACTUAL_NODE_PATH"
    else
        handle_failure "Node.js not found in PATH"
    fi

    # Check if Yarn is available
    if command -v yarn >/dev/null 2>&1; then
        echo "Yarn is installed:"
        yarn --version
    else
        if command -v npm >/dev/null 2>&1; then
            echo "Installing Yarn globally..."
            npm install -g yarn || handle_failure "Failed to install Yarn"
        else
            handle_failure "Neither Yarn nor npm found in PATH"
        fi
    fi
}

# Use absolute paths based on /w/repo
DIR_PROJECT="/w/repo"
echo "Project root directory set to: $DIR_PROJECT"

# Debug: Print current environment
echo "Initial PATH: $PATH"
echo "Initial NODE_PATH: $NODE_PATH"

# Verify Node.js environment
verify_node_env

echo "Building project in $DIR_PROJECT"


# Attempt to navigate to directories or exit with a message
cd "$DIR_PROJECT/frontend/openAPI/generateApiClientFrontend" || handle_failure "Failed to change directory to generateApiClientFrontend"

yarn install && yarn build || handle_failure "Yarn install or build failed in generateApiClientFrontend"

# Navigate back to the frontend directory to build the frontend
cd "$DIR_PROJECT/frontend" || handle_failure "Failed to change directory to frontend"

yarn install && yarn build || handle_failure "Yarn install or build failed in frontend"

# Copy the dist folder into the backend build folder
cp -r dist "$DIR_PROJECT/backend/build" || handle_failure "Failed to copy dist folder into backend build"

# Run cmake at the project root directory
cd "$DIR_PROJECT/backend/build" || handle_failure "Failed to change directory to backend build"
echo "Current directory: $(pwd)"


# Calculate make job count
if hash nproc 2>/dev/null; then
    MAKE_JOB_COUNT=$(($(nproc)/4))
else
    MAKE_JOB_COUNT=1
fi

make -j$MAKE_JOB_COUNT

# Print directory contents for verification
echo "Listing contents of the project directory in build script:"
ls "$DIR_PROJECT"

echo "Listing contents of the backend directory in build script:"
ls "$DIR_PROJECT/backend"

echo "Listing contents of backend build directory in build script:"
ls -la