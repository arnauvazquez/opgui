#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Determine the script's directory
MYDIR="$(dirname "$(readlink -f "$0")")"
echo "Current script directory: $MYDIR"
NODE_PATH="/c/Program Files/nodejs"
YARN_PATH="/c/Program Files (x86)/Yarn/bin"

# Load the userProfile from a previously stored file
userProfileEncoded=$(cat userprofile.txt)
userProfile=$(echo "$userProfileEncoded" | base64 --decode)

# Function to handle command failure
handle_failure() {
    echo "Error: $1"  # $1 can be an exit code or a message
    exit ${2:-1}  # Exit with the provided code, or default to 1
}

echo "Received USERPROFILE: $userProfile"

handle_failure() {
    echo "Error: $1"  # $1 can be an exit code or a message
    exit ${2:-1}  # Exit with the provided code, or default to 1
}

# Check if userProfile is empty
if [ -z "$userProfile" ]; then
    handle_failure "User profile is empty"
fi

# Remove the directories and files
rm -rf "$userProfile/Documents/workspaces/opGUI-workspace1" 2>/dev/null
rm -rf "$userProfile/opSimulation/bin/core" 2>/dev/null
rm -rf "$userProfile/opSimulation/bin/core/components" 2>/dev/null
rm -rf "$userProfile/opSimulation/bin/core/modules" 2>/dev/null

rm "$userProfile/opSimulation/bin/core/opSimulation.exe" 2>/dev/null
rm "$userProfile/opSimulation/bin/core/opSimulationManager.exe" 2>/dev/null


# TODO: remove repo, etc...