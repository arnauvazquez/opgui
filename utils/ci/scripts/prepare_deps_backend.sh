#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the thirdParty dependencies
################################################################################

# At this step is assumed that the backend dependencies (except the ones that need
# installed with pip) are already installed in the agent, 
# Any other missing deps would make the process to fail on a later step.

yes | pip install sphinx-rtd-theme
yes | pip install sphinx-tabs

# This dependency is not compatible with windows systems, for this reason pdf
# conversion takes place only in unix based systems
# yes | pip install rest2pdf

