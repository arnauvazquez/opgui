#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Debug: Print current environment
echo "Current PATH: $PATH"
echo "Current NODE_PATH: $NODE_PATH"

# Function to convert Windows path to MSYS path
win_to_msys_path() {
    local winpath=$1
    # Remove any quotes and trailing spaces
    winpath=$(echo "$winpath" | tr -d '"' | sed 's/[[:space:]]*$//')
    # Convert Windows backslashes to forward slashes
    winpath=${winpath//\\/\/}
    # Convert C: to /c
    winpath=$(echo "$winpath" | sed 's/^\([A-Za-z]\):/\/\L\1/')
    echo "$winpath"
}

# If NODE_PATH is set, use it to set up the environment
if [ -n "$NODE_PATH" ]; then
    NODE_DIR=$(dirname "$(win_to_msys_path "$NODE_PATH")")
    echo "Adding Node.js directory to PATH: $NODE_DIR"
    export PATH="$NODE_DIR:$PATH"
fi

# Check if Node.js is available in the PATH
if command -v node >/dev/null 2>&1; then
    echo "Node.js is installed:"
    node --version
    ACTUAL_NODE_PATH=$(command -v node)
    echo "Node.js found at: $ACTUAL_NODE_PATH"
else
    echo "ERROR: Node.js not found. Please ensure Node.js is installed via Jenkins 'nodejs' step"
    echo "Current PATH: $PATH"
    echo "Current NODE_PATH: $NODE_PATH"
    exit 1
fi

# Check if Yarn is available in the PATH
if command -v yarn >/dev/null 2>&1; then
    echo "Yarn is already installed:"
    yarn --version
else
    echo "Yarn not found in PATH, attempting to install globally"
    # Try to install Yarn globally using npm
    if command -v npm >/dev/null 2>&1; then
        echo "Installing Yarn globally using npm..."
        npm install -g yarn
        if [ $? -ne 0 ]; then
            echo "ERROR: Failed to install Yarn globally"
            exit 1
        fi
    else
        echo "ERROR: npm not found, cannot install Yarn"
        exit 1
    fi
fi

# Final verification with detailed output
echo "=== Final Environment Verification ==="
echo "Node.js version and location:"
which node
node --version

echo "npm version and location:"
which npm
npm --version

echo "Yarn version and location:"
which yarn
yarn --version

echo "=== Node.js and Yarn setup completed successfully ==="